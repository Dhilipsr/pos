<?php
class Nexo_article extends CI_Model
{
    /**
     * Get categories
     *
     * @param int
     * @return array/bool
    **/
    
    public function get()
    {
        /*if ($id != null) {
            if ($filter == 'as_id') {
                $this->db->where('ID', $id);
            } elseif ($filter == 'as_nom') {
                $this->db->where('NOM', $id);
            }
        }*/
        
        $query    =    $this->db->get( store_prefix() . 'tendoo_nexo_articles');
        return $query->result_array();
    }
      public function update($id ,$updatevalue ,$tax_id)
    {        
        $query    =    $this->db->get( store_prefix() . 'tendoo_nexo_articles');
        if ($this->db->where('ID', $id)->where('REF_TAXE', $tax_id)->update('tendoo_nexo_articles', array(
            'PRIX_DE_VENTE_TTC'                =>    $updatevalue
        )));
    }

}
