<?php
class Nexo_Clients extends CI_Model
{
    /**
     * Get categories
     *
     * @param int
     * @return array/bool
    **/
    
    public function get($id)
    {
        /*if ($id != null) {
            if ($filter == 'as_id') {
                $this->db->where('ID', $id);
            } elseif ($filter == 'as_nom') {
                $this->db->where('NOM', $id);
            }
        }*/
        
        $query    =    $this->db->where('ID',$id)->get( store_prefix() . 'nexo_clients');
        return $query->result_array();
    }
    // public function FunctionName($value='')
    // {
    //     # code...
    // }
}
