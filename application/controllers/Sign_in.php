<?php
defined('BASEPATH') or exit('No direct script access allowed');
 
use Carbon\Carbon;
class Sign_in extends Tendoo_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/sing-in
     *	- or -
     * 		http://example.com/index.php/welcome/sing-in
     */
    public function __construct()
    {   
       
        parent::__construct();
      // header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
   
        $this->load->library('form_validation');
        $this->load->model('Login_Model');
        
    }
    
    /**
     * Sign In index page
     *
     *	Displays login page
     * 	@return : void
    **/
    
    public function index()
    {
        $this->events->do_action('set_login_rules');
        // in order to let validation return true
        $this->form_validation->set_rules('submit_button', __('Submit button'), 'alpha_dash');
		
        if ($this->form_validation->run()) {
            // Log User After Applying Filters
            $this->events->do_action( 'do_login' );
            $exec        =    $this->events->apply_filters('tendoo_login_notice', 'user-logged-in');
            
            if ($exec    == 'user-logged-in') {
                if (riake('redirect', $_GET)) {

                    redirect(urldecode(riake('redirect', $_GET)));
                } else {
                    $url    =   $this->events->apply_filters( 'login_redirection', site_url( array( 'dashboard' ) ) );
                    redirect( $url );
                }
            }
            $this->notice->push_notice($this->lang->line($exec));
        }
		
        // load login fields
        $this->config->set_item('signin_fields', $this->events->apply_filters('signin_fields', $this->config->item('signin_fields')));
        
        Html::set_title(sprintf(__('Sign In &mdash; %s'), get('core_signature')));
        $this->load->view('shared/header');
        $this->load->view('sign-in/body');
    }
    
    /**
     * 	Recovery Method
     *	
     *	Allow user to get reset email for his account
     *
     *	@return void
    **/
    
    public function recovery()
    {
        $this->form_validation->set_rules('user_email', __('User Email'), 'required|valid_email');
        if ($this->form_validation->run()) {
            /**
             * Actions to be run before sending recovery email
             * It can allow use to edit email
            **/
            $this->events->do_action('do_send_recovery');
        }
        Html::set_title(sprintf(__('Recover Password &mdash; %s'), get('core_signature')));
        $this->load->view('shared/header');
        $this->load->view('sign-in/recovery');
    }
    
    /**
     * 	Reset
     * 	
     *	Checks a verification code an send a new password to user email
     *
     * 	@access : public
     *	@param : int user_id
     * 	@param : string verfication code
     * 	@return : void
     * 
    **/
    
    public function reset($user_id, $ver_code)
    {
        $this->events->do_action('do_reset_user', $user_id, $ver_code);
    }
    
    /**
     * Verify
     * 
     * 	Verify actvaton code for specifc user
     *
     *	@access : public
     *	@param : int user_id
     *	@param : string verification code
     *	@status	: untested
    **/
    
    public function verify($user_id, $ver_code)
    {
        $this->events->do_action('do_verify_user', $user_id, $ver_code);
    }

    public function pin_login(){

        $post   =   json_decode( get_instance()->input->raw_input_stream, true );
        $user   =   $this->db->where( 'pin', md5( $post[ 'pin' ] ) )
            ->get( 'aauth_users' )
            ->result_array();


        if ( $user ) {
            get_instance()->auth->login_fast( $user[0][ 'id' ]);
            echo json_encode([
                'status'    =>  'success',
                'message'   =>  __( 'The user has been successfully logged' )
            ]);
            return;
        }
        echo json_encode([
            'status'    => 'failed',
            'message'   => __( 'Unable to login using the provided PIN Code.', 'pin-login' )
        ]);
        return;
    }
    public function mobile_login(){
         header('Access-Control-Allow-Origin: *');
        //print_r($this->generateKey());die;
        $json = file_get_contents('php://input');
        $inputparams = json_decode($json);
        $this->db->where('email',$inputparams->username);
        $this->db->where('banned', 0);
        $usercheck = $this->db->get(store_prefix() .'aauth_users')->row();

        $returndata = array();

        if($usercheck){
            // $this->db->where('email',$inputparams->username);
            // $this->db->where('banned', 0);
            // $this->db->where('pass', $this->hash_password($inputparams->password, $usercheck->id));
            // $userpasscheck = $this->db->get(store_prefix() .'aauth_users')->row();
            
            $this->db->where(store_prefix().'aauth_users.email',$inputparams->username);
            $this->db->where(store_prefix().'aauth_users.banned', 0);
            $this->db->where(store_prefix().'aauth_users.pass', $this->hash_password($inputparams->password, $usercheck->id));
            $this->db->select(store_prefix().'aauth_users.*,'.store_prefix().'aauth_user_to_group.group_id,'.store_prefix().'aauth_groups.definition as usertype')
            ->from(store_prefix() .'aauth_users')
            ->join(store_prefix().'aauth_user_to_group', store_prefix() .'aauth_users.id ='. store_prefix().'aauth_user_to_group.user_id','left')
            ->join(store_prefix().'aauth_groups', store_prefix() .'aauth_user_to_group.group_id = '. store_prefix().'aauth_groups.id','left');
            $userpasscheck = $this->db->get()->row();

            if($userpasscheck){
                $this->db->where('user',$userpasscheck->id);
                $apitokencheck = $this->db->get(store_prefix().'tendoo_restapi_keys')->row();
                if($apitokencheck){
                    $returndata['status'] = "true";
                    $returndata['data'] = array('auth_token_key'=>$apitokencheck->key,'userDetails'=>$userpasscheck);
                    $returndata['message'] = "User Logged in Successfully!";    
                }else{
                    $generateKey            =   $this->generateKey();
                    $this->load->config( 'oauth' );

                    $expiration             =   $this->config->item( 'oauth_key_expiration_days' );
                    $date                   =   Carbon::parse( date('Y-m-d H:i:s') );
                    $expiration             =   $date->addDays( $expiration );

                    $this->db->insert( 'restapi_keys', array(
                        'user'              =>  $userpasscheck->id,
                        'scopes'            =>  "core",
                        'date_created'      =>  $date,
                        'app_name'          =>  "Tendoo CMS",
                        'key'               =>  $generateKey,
                        'expire'            =>  $expiration->toDateTimeString()
                    ) );
                    $lastinsertid = $this->db->insert_id();
                    $this->db->where('id',$lastinsertid);
                    $apitokencheck = $this->db->get(store_prefix().'tendoo_restapi_keys')->row();
                    $returndata['status'] = "true";
                    $returndata['data'] = array('auth_token_key'=>$apitokencheck->key,'userDetails'=>$userpasscheck);
                    $returndata['message'] = "Auth Token Created and User Logged in Successfully!";  
                }
            }else{
                $returndata['status'] = "false";
                $returndata['data'] = array();
                $returndata['message'] = "Incorrect Password!";
            }
            
        }else{
            $returndata['status'] = "false";
            $returndata['data'] = array();
            $returndata['message'] = "Username or Email Not Exist!";
        }

       echo json_encode($returndata);die;
       
    }
    public function generateKey()
    {
        $keys        =    get_instance()->db->get('restapi_keys')->result();

        if (is_array($keys)) {
            $all_keys    =    array();
            foreach ($keys as $key) {
                $all_keys[]    =    $key->key;
            }
        }

        // Generate key
        do {
            // Create default Keys
            $length        =    40;
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
        } while (in_array($randomString, $all_keys));

        return $randomString;
    }
    public function hash_password($pass, $userid)
    {
        $salt = md5($userid);
        return hash('sha256', $salt.$pass);
    }

    public function change_password($pass, $userid)
    {
        $salt = md5($userid);
        return hash('sha256', $salt.$pass);
    }


}
