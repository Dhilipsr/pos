<?php
// Nexo Updater
include_once( MODULESPATH . 'nexo-updater/vendor/autoload.php' );

class NexoUpdaterLicence extends Tendoo_Module
{
    public function __construct()
    {
        parent::__construct();
        
        if ( ! User::in_group( 'master' ) ) {
            show_error(__( 'You`\'re not allowed to see that page', 'nexo-updater' ) );
        }

        $this->base     =   'https://platform.nexopos.com/';
        // $this->base     =   'http://laravels.go/';
        $this->url      =   $this->base . 'api/';
    }

    /**
     * Validation Index
     * @return void
     */
    public function index()
    {   
        $licences           =   [];
        if ( get_option( 'nexopos_store_access_key' ) ) {
            $apiRequest     =    Requests::get( $this->base . 'api/nexopos/envato-licences', [
                'X-API-TOKEN'   =>  get_option( 'nexopos_store_access_key' )
            ]);
            $licences       =    json_decode( $apiRequest->body, true );
        }

        $this->Gui->set_title( __( 'Licence Management', 'nexo-updater' ) );
        $this->load->module_view( 'nexo-updater', 'activate.gui', compact( 'licences' ) );
    }

    /**
     * Check the licnence
     */
    public function check() 
    {
        $apiRequest    =    Requests::post( $this->base . 'api/nexopos/use-licence', [
            'X-API-TOKEN'   =>  get_option( 'nexopos_store_access_key' )
        ], [
            'licence_id'    =>  $this->input->post( 'licence_id' )
        ]);

        $licences       =    json_decode( $apiRequest->body );

        if ( ! is_object( $licences ) ) {
            return redirect([ 'dashboard', 'nexo', 'licence?notice=select-licence' ]);
        }

        if ( $licences->status === 'success' ) {
            set_option( 'updater2_validated', $this->input->post( 'licence_id' ) );
            $this->options->delete( 'always_ask' );
            return redirect([ 'dashboard', 'nexo', 'licence', 'activated' ]);
        } else {
            return show_error( sprintf( __( 'Unable to use the licence. Nexo Platform has returned the following error : %s', 'nexo-updater' ), $licences->message ) );
        }
    }

    /**
     * Activated
     * @return view
     */
    public function activated()
    {
        $this->Gui->set_title( __( 'Thank You', 'nexo-updater' ) );
        $this->load->module_view( 'nexo-updater', 'activated.gui' );
    }

    /**
     * Auth to envato
     * @return void
     */
    public function auth()
    {
        $client_key     =   'IQtok7Iyp6lkdB1dok4SnN1Fd8SnNhtPph5JnySU';
        $client_secret  =   'be3zUChtAtrGeP08X2mzGaCXmWK4hGEWSLjfDbUe';
        redirect( $this->base . 'oauth?scopes=nexopos.envato-licences,nexopos.sync&client_secret=' . $client_secret . '&client_key=' . $client_key . '&new-comer=true&callback_url=' . urlencode( dashboard_url([ 'callback-auth' ]) ) . '&rest_key=' . get_option( 'rest_key' ) );
    }

    /***
     * Callback for the auth
     */
    public function callback()
    {
        if( @$_GET[ 'access_token' ] != null ) {
            set_option( 'nexopos_store_access_key', $_GET[ 'access_token' ]);
            return redirect( dashboard_url([ 'licence' ]) );
        }
        return show_error( __( 'You don\'t have access to that page' ) );
    }

    /**
     * Revoke connexion
     * @return void
     */
    public function revoke()
    {
        $this->options->delete( 'nexopos_store_access_key' );
        return redirect( dashboard_url( [ 'licence' ] ) );
    }

    /**
     * revoke licence
     */
    public function revokeLicence()
    {
        try {
            $apiRequest    =    Requests::post( $this->base . 'api/nexopos/unuse-licence', [
                'X-API-TOKEN'   =>  get_option( 'nexopos_store_access_key' )
            ], [
                'licence_id'    =>  get_option( 'updater2_validated' )
            ]);
    
            $this->options->delete( 'updater2_validated' );
            set_option( 'always_ask', 'yes' );
            return redirect( dashboard_url([ 'licence' ]) );
            
        } catch( Exception $e ) {
            show_error( sprintf( __( 'An error occured while the deactivation of the licence : "%s"<br>
            This issue might be related to a non existing connexion to internet. Make sure your system has access to internet.', 'nexo-updater' ), $e->getMessage() ) );
        }

    }

    /**
     * Update index
     * @return void
     */
    public function updateIndex()
    {
        $this->load->module_config( 'nexo-updater', 'updater' );
        
        if ( ! $this->config->item( 'enable-update' ) ) {
            return show_error( __( 'The update has been disabled on the system.' ) );
        }

        if ( ! User::in_group([ 'admin', 'master' ]) ) {
            return show_error( __( 'Youre not allowed to access to that page.', 'nexo-updater' ) );
        }
        
        $this->Gui->set_title( __( 'A2000 Update Center', 'nexo-updater' ) );
        $this->load->module_view( 'nexo-updater', 'update.gui' );
    }
}