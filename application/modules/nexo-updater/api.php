<?php
$Routes->get( 'nexopos/update/check/{param?}', 'ApiUpdateController@check' ); // check all or single
$Routes->post( 'nexopos/update', 'ApiUpdateController@update' ); // single
$Routes->post( 'nexopos/update/download', 'ApiUpdateController@download' ); // single
$Routes->post( 'nexopos/update/step/{index}', 'ApiUpdateController@updateStep' ); // single or all