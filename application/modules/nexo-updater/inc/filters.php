<?php
class Nexo_Updater_Filters extends Tendoo_Module
{
    public function __construct()
    {
        parent::__construct();
    }
    public function admin_menus( $menus )
    {
        if ( ! is_multistore() ) {
            
            $this->load->module_config( 'nexo-updater', 'updater' );
            
            if ( @$menus[ 'dashboard' ][1] !== null && $this->config->item( 'enable-update' ) ) {
                // change update route link
                $menus[ 'dashboard' ][1][ 'href' ]          =   site_url([ 'dashboard', 'nexo', 'updates' ]);
                $menus[ 'dashboard' ][1][ 'notices_nbr' ]   =   get_option( 'update_menu_notices' );
            }
            
            /*if( @$menus[ 'nexo_settings' ] ) {
                $menus[ 'nexo_settings' ][]   =    [
                    'title'        =>   __( 'Licence Management', 'nexo' ),
                    'href'         =>   site_url([ 'dashboard', 'nexo', 'licence' ]),
                    'permission' 		=>	[ 'nexo.manage.settings' ]
                ];
            }*/
        }
        return $menus;
    }
}