<script>
const NexoUpdaterCron    =   ( force = false ) => {
    return new Promise( ( resolve, reject ) => {
        HttpRequest.get( '/api/nexopos/update/check' + ( force ? '?force=true' : '' ) ).then( response => {
            resolve( response );
        })
    })
}
$( document ).ready( function(){
    $( '#check-updates' ).click(function(){
        $( this ).html( `<i class="fa fa-refresh fa-spin"></i> <?php echo __( 'Processing...', 'nexo-updater' );?>` );
        NexoUpdaterCron(true).then( ( response ) => {
            if ( response.data.status === 'failed' ) {
                NexoAPI.Notify().warning( 
                    '<?php echo _s( 'An error has occured !', 'nexo' );?>', 
                    response.data.message 
                );
                $( this ).html( `<?php echo __( 'Check Updates', 'nexo-updater' );?>` );
            } else {
                document.location   =   '<?php echo current_url();?>';
            }
        });
    })
    <?php
    $cache      =   new CI_Cache( array( 'adapter' => 'file', 'backup' => 'file', 'key_prefix'    =>    'nexo_updater' ));
    if ( $cache->get( 'check-update' ) === false && ! in_array( $this->config->item( 'update-verification' ), [0, false ] ) ) {
        echo 'NexoUpdaterCron();';
        $cache->save( 'check-update', true, $this->config->item( 'update-verification' ) );
    }
    ?>
})

</script>