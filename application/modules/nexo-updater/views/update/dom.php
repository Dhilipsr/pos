<div id="update-vue" class="row">
    <div class="col-md-6">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title"><?php echo __( 'Available Updates', 'nexo-updater' );?></h3>

                <div class="box-tools">
                    <ul class="pagination pagination-sm no-margin pull-right">
                        <li>
                            <a href="javascript:void(0)" id="check-updates"><?php echo __( 'Check Updates', 'nexo-updater' );?></a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding hidden update-container">
                <table class="table">
                    <tbody v-if="hasLoaded">
                        <tr>
                            <th>
                                <?php echo __( 'Name', 'nexo-updater' );?>
                            </th>
                            <th style="width: 150px">
                                <?php echo __( 'Current Version', 'nexo-updater' );?>
                            </th>
                            <th style="width: 150px">
                                <?php echo __( 'New Version', 'nexo-updater' );?>
                            </th>
                            <th style="width: 200px" class="text-right">
                                <?php echo __( 'Action', 'nexo-updater' );?>
                            </th>
                            <th style="width: 100px" class="text-right">
                                <?php echo __( 'Info', 'nexo-updater' );?>
                            </th>
                        </tr>
                        <tr>
                            <td colspan="5" v-if="updatables.length === 0"><?php echo __( 'No update is available', 'nexo-updater' );?></td>
                        </tr>
                        <tr v-for="module in updatables">
                            <td>{{ module.application.name }}</td>
                            <td>{{ module.application.version }}</td>
                            <td>{{ module.version }}</td>
                            <td>
                                <ul class="pagination pagination-sm no-margin pull-right">
                                    <li @click="update( module )" v-if="! module.updateStatus || module.updateStatus == 'error'">
                                        <a href="javascript:void(0)"><i class="fa fa-refresh"></i> <?php echo __( 'Update', 'nexo-updater' );?></a>
                                    </li>
                                    <li v-if="module.updateStatus === 'ongoing'" class="disabled">
                                        <a href="javascript:void(0)"><i class="fa fa-refresh fa-spin"></i> <?php echo __( 'On going...', 'nexo-updater' );?></a>
                                    </li>
                                    <li v-if="module.updateStatus === 'verifiying'" class="disabled">
                                        <a href="javascript:void(0)"><i class="fa fa-refresh fa-spin"></i> <?php echo __( 'Licence validation...', 'nexo-updater' );?></a>
                                    </li>
                                    <li v-if="module.updateStatus === 'done'"class="disabled">
                                        <a href="javascript:void(0)"><i class="fa fa-check"></i> <?php echo __( 'Updated !', 'nexo-updater' );?></a>
                                    </li>
                                    <li @click="popupError( module )" v-if="module.updateStatus === 'error'">
                                        <a href="javascript:void(0)"><i class="fa fa-remove"></i> <?php echo __( 'See Error', 'nexo-updater' );?></a>
                                    </li>
                                    <li v-if="module.updateStatus === 'migration'">
                                        <a :href="module.migration.link"><i class="fa fa-check"></i> <?php echo __( 'Run Migration', 'nexo-updater' );?></a>
                                    </li>
                                </ul>
                            </td>
                            <td>
                                <ul class="pagination pagination-sm no-margin pull-right">
                                    <li @click="changelog( module )">
                                        <a href="javascript:void(0)"><i class="fa fa-info"></i> <?php echo __( 'Changelog', 'nexo-updater' );?></a>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <!-- <div class="overlay">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div> -->
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>
<script>
    const serverModulesStatus = <?php echo json_encode( get_option( 'nexo_updater_modules_status', []) );?>;
    const modules = <?php echo json_encode( Modules::get() );?>;
    const routes = {
        update: '<?php echo site_url([ 'api', 'nexopos', 'update' ]);?>',
        checkRoute: '<?php echo site_url([ 'api', 'nexopos', 'update', 'check' ]);?>',
        bulkUpdate: '<?php echo site_url([ 'api', 'nexopos', 'update', 'bulk-update' ]);?>',
        download: '<?php echo site_url([ 'api', 'nexopos', 'update', 'download']);?>',
        migrate: '<?php echo site_url([ 'dashboard', 'modules', 'migrate' ]);?>'
    }
    const textDomain    =   {
        unableToInstall: '<?php echo _s( 'Unable to update', 'nexo-updater' );?>',
        [ 'old-version-cannot-be-installed' ] : '<?php echo _s( 'The update is less recent than the current version.', 'nexo-updater' );?>',
        [ 'unable-to-update' ] : '<?php echo _s( 'Unable to find a version number.', 'nexo-updater' );?>',
        [ 'incorrect-config-file' ] : '<?php echo _s( 'Unable to find the config.xml file.', 'nexo-updater' );?>',
        unexpected: '<?php echo _s( 'Unexpected error occured', 'nexo-updater' );?>',
        emptyChangelog: '<?php echo _s( 'The changelog is not defined for this version.', 'nexo-updater' );?>',
        updated: '<?php echo _s( 'The update has been made successfully', 'nexo-updater' );?>'
    }

    for (let namespace in modules) {
        if (serverModulesStatus[namespace] !== undefined) {
            modules[namespace] = Object.assign({}, modules[namespace], serverModulesStatus[namespace], {
                updateStatus: false
            });
        }
    }
</script>
<script>
    const UpdateVue = new Vue({
        el: '#update-vue',
        data: {
            modules: Object.values(modules),
            hasLoaded: false,
            textDomain,
            hasLoaded   :   false
        },
        mounted() {
            this.hasLoaded = true;
            $( '.update-container' ).removeClass( 'hidden' );
        },
        methods: {
            update(module) {
                this.changelog( module ).then( result => {
                    if ( result.value ) {
                        module.updateStatus = 'verifiying';
                        HttpRequest.post( routes.update, { module }).then(response => {
                            module.response    =   response.data;
                            if ( response.data.status === 'success' ) {
                                this.downloadUpdate( response.data.code, module );
                            } else {
                                this.showError( module );
                            }
                        }).catch(( data ) => {
                            console.log( data );
                            this.showError( module );
                        })
                    }
                })
            },

            downloadUpdate( code, module ) {
                module.updateStatus     =   'ongoing';
                HttpRequest.post( routes.download, { code, namespace : module.application.namespace }).then(response => {
                    
                    module.response    =   response.data;

                    if ( response.data.status === 'success' ) {
                        switch( response.data.message ) {
                            case 'module-updated-migrate-required': this.showMigration( response.data, module ); break;
                            case 'module-updated': this.updateDone( module ); break;
                            default: 
                                NexoAPI.Notify().success( 
                                    this.textDomain.updated, 
                                    response.data.message
                                );
                                this.updateDone( module ); 
                                NexoUpdaterCron(true).then( result => {
                                    document.location   =   '<?php echo current_url();?>?notice=nexo-updated';
                                })
                            break;
                        };
                    } else {
                        switch( response.data.message ) {
                            case 'old-version-cannot-be-installed': this.showError( module ); break;
                            case 'unable-to-update': this.showError( module ); break;
                            case 'incorrect-config-file': this.showError( module ); break;
                        }
                    }
                }).catch(() => {
                    this.showError( module );
                })
            },

            popupError( module ) {
                NexoAPI.Notify().warning( this.textDomain.unableToInstall, 
                    this.textDomain[ module.response.message ] || module.response.message 
                );
            },

            showMigration( details, module ) {
                module.updateStatus     =   'migration';
                module.migration        =   details;
                module.migration.link   =   routes.migrate + '/' + module.migration.namespace + '/' + module.migration.from;
            },

            showError( module ) {
                module.updateStatus     =   'error';
            },

            updateDone( module ) {
                module.updateStatus     =   'done';
            },

            changelog( module ) {
                var converter = new showdown.Converter(),
                text        =   module.changelog,
                html        =   converter.makeHtml(text);
                html        =   html || textDomain.emptyChangelog;
                return swal({
                    title : '<?php echo _s( 'Confirm Update', 'nexo-updater' );?>',
                    html: `<div style="text-align: left">${html}</div>`,
                    showCancelButton: true
                });
            }
        },
        computed: {
            updatables() {
                return this.modules.filter(module => module.status === 'success');
            }
        }
    })
</script>