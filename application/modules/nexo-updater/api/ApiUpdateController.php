<?php
class ApiUpdateController extends Tendoo_Api
{
    private $base       =   'http://platform.nexopos.com/';
    public function check( $namespace = null )
    {
        $modules    =   Modules::get( $namespace );

        if ( $modules ) {
            
            $apiRequest    =    Requests::post( $this->base . 'api/nexopos/check-update', [
                'X-API-TOKEN'       =>  get_option( 'nexopos_store_access_key' ),
                'X-Requested-With'  =>  'XMLHttpRequest'
            ], [
                'modules'           =>  $modules,
                'domain'            =>  base_url()
            ]);
            
            $json   =   json_decode( $apiRequest->body, true );

            /**
             * save the server result on the options
             */
            set_option( 'nexo_updater_modules_status', $json );

            /**
             * save a notice for updates
             */
            $notices    =   count( array_filter( $json, function( $module ) {
                return @$module[ 'status' ]  === 'success';
            }) );

            set_option( 'update_menu_notices', $notices );

            if ( $json ) {
                return $this->response( $json );
            }
            
            return $this->response([
                'status'    =>  'failed',
                'message'   =>  $apiRequest->body
            ], 403 );
        }

        return $this->response([
            'status'   =>  'failed',
            'message'   =>  __( 'Unable to find the module', 'nexo-updater' )
        ], 404 );
    }

    public function update()
    {
        $module                 =   $this->post( 'module' );
        $apiRequest             =    Requests::post( $this->base . 'api/nexopos/verify-update', [
            'X-API-TOKEN'       =>  get_option( 'nexopos_store_access_key' ),
            'X-Requested-With'  =>  'XMLHttpRequest'
        ], [
            'zipball_id'    =>  $module[ 'id' ],
            'version'       =>  $module[ 'version' ],
            'domain'        =>  base_url()
        ]);

        return $apiRequest->body;
    }

    /**
     * Download update and install it
     * @return json
     */
    public function download()
    {
        $opts   =     [
            'http'=>    [
                'method'    => "GET",
                'header'    =>
                    "Accept-language: en\r\n" .
                    "X-API-TOKEN: " . get_option( 'nexopos_store_access_key' ) . "\r\n"
            ]
        ];

        $context    =   stream_context_create( $opts );
        $file       =   file_get_contents( $this->base . 'api/nexopos/download-update/' . $this->post( 'code' ), false, $context );
        
        if ( $http_response_header[0] === 'HTTP/1.0 403 Forbidden' ) {
            return $this->response([
                'status'    =>  'failed',
                'message'   =>  __( 'An error has occured during the download. The Code might no more be valid.', 'nexo-update' )
            ], 403 );
        } 

        $moduleZip  =   UPLOADPATH . 'modules-zip' . DIRECTORY_SEPARATOR;
        if ( ! is_dir( $moduleZip ) ) {
            mkdir( $moduleZip );
        }
        file_put_contents( $moduleZip . $this->post( 'namespace' ) . '.zip', $file );

        $zipPath    =   $moduleZip . $this->post( 'namespace' ) . '.zip';
        $path       =   dirname( $zipPath );
        
        if ( $this->post( 'namespace' ) === 'nexo' ) {
            /**
             * let's empty the temp folder
             */
            SimpleFileManager::drop( APPPATH . DIRECTORY_SEPARATOR . 'temp' );
            mkdir( APPPATH . DIRECTORY_SEPARATOR . 'temp' );

            get_instance()->load->library('unzip');
            get_instance()->unzip->extract(
                $zipPath,
                APPPATH . DIRECTORY_SEPARATOR . 'temp' //must change
            );
            get_instance()->unzip->close();

            /**
             * Point to the right folder on temp
             */
            $temp   =   scandir( APPPATH . DIRECTORY_SEPARATOR . 'temp' );
            $temp   =   array_values( array_filter( $temp, function( $folder ) {
                return ! in_array( $folder, [ '.', '..' ]);
            }) );

            if ( @$temp[0] !== null && is_dir( APPPATH . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR . $temp[0] ) ) {
                SimpleFileManager::copyContent( 
                    APPPATH . DIRECTORY_SEPARATOR . 'temp' . DIRECTORY_SEPARATOR . $temp[0] . DIRECTORY_SEPARATOR,
                    FCPATH
                );

                // clear temp
                SimpleFileManager::drop( APPPATH . DIRECTORY_SEPARATOR . 'temp' );
                mkdir( APPPATH . DIRECTORY_SEPARATOR . 'temp' );
                SimpleFileManager::copy( APPPATH . DIRECTORY_SEPARATOR . 'index.html', APPPATH . DIRECTORY_SEPARATOR . 'temp' );
                
                return $this->response([
                    'status'    =>  'success',
                    'message'   =>  __( 'NexoPOS has been updated', 'nexo-updater' )
                ]);

            } else {

                return $this->response([
                    'status'    =>  'failed',
                    'message'   =>  __( 'Unable to identify the zip file uploaded as a NexoPOS update.', 'nexo-updater' )
                ]);

            }

        } else {
            
            
            $upload     =   [];
            $upload[ 'upload_data' ][ 'file_path' ]     =   $path . DIRECTORY_SEPARATOR;
            $upload[ 'upload_data' ][ 'raw_name' ]      =   $this->post( 'namespace' );
            $upload[ 'upload_data' ][ 'full_path' ]     =   $moduleZip . $this->post( 'namespace' ) . '.zip';
            
            $message    =   Modules::__treatZipFile( $upload );

            if ( is_array( $message ) ) {
                $response   =   [
                    'status'    =>  'success',
                    'message'   =>  $message[ 'msg' ],
                    'from'      =>  $message[ 'from' ],
                    'namespace' =>  $message[ 'namespace' ]
                ];
            } else {
                $response   =   [
                    'status'    =>  'failed',
                    'message'   =>  $message
                ];
            }

            return $this->response( $response );
        }

        return $this->response([
            'status'    =>  'success',
            'message'   =>  __( 'The update has been correctly installed.', 'nexo-update' )
        ]);
    }
}