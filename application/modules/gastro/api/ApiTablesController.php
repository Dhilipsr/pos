<?php

use Carbon\Carbon;
use Curl\Curl;

class ApiTablesController extends Tendoo_Api
{
    public function orders()
    {


    // print_r(__LINE__);
    // die();
    
     $startofday = new DateTime();
        $startofday->setTime(0,0);
        $startDate =$startofday->format('Y-m-d H:i:s');

        // echo date('Y-m-d H:i:s',$today);

        // echo "<br>";
        $endOfDay = new DateTime();
        $endOfDay->setTime(23,59);
        $endDate = $endOfDay->format('Y-m-d H:i:s');

        $this->db
        ->select( '
        aauth_users.name as AUTHOR_NAME,
        ' . store_prefix() . 'nexo_commandes.CODE as CODE,
        ' . store_prefix() . 'nexo_commandes.TYPE as TYPE,
        ' . store_prefix() . 'nexo_commandes.ID as ID,
        ' . store_prefix() . 'nexo_commandes.ID as ORDER_ID,
        ' . store_prefix() . 'nexo_commandes.DATE_CREATION as DATE_CREATION,
        ' . store_prefix() . 'nexo_commandes.DATE_MOD as DATE_MOD,
        ' . store_prefix() . 'nexo_commandes.REMISE_TYPE as REMISE_TYPE,
        ' . store_prefix() . 'nexo_commandes.REF_CLIENT as REF_CLIENT,
        ' . store_prefix() . 'nexo_commandes.DESCRIPTION as DESCRIPTION,
        ' . store_prefix() . 'nexo_commandes.REMISE_PERCENT as REMISE_PERCENT,
        ' . store_prefix() . 'nexo_commandes.GROUP_DISCOUNT as GROUP_DISCOUNT,
        ' . store_prefix() . 'nexo_commandes.PAYMENT_TYPE as PAYMENT_TYPE,
        ' . store_prefix() . 'nexo_commandes.SHIPPING_AMOUNT as SHIPPING_AMOUNT,
        ' . store_prefix() . 'nexo_commandes.REMISE as REMISE,
        ' . store_prefix() . 'nexo_commandes.RESTAURANT_TABLE_ID,
        ' . store_prefix() . 'nexo_commandes.RESTAURANT_SEAT_USED,
        ' . store_prefix() . 'nexo_commandes.RESTAURANT_BOOKED_FOR,
        ' . store_prefix() . 'nexo_clients.NOM as CUSTOMER_NAME,
        ' . store_prefix() . 'nexo_commandes.TITRE as TITRE,
        ' . store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_TYPE as REAL_TYPE,
        ' . store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS as STATUS' )
        
        ->from( store_prefix() . 'nexo_commandes' )
        ->join( store_prefix() . 'nexo_clients', store_prefix() . 'nexo_commandes.REF_CLIENT = ' . store_prefix() . 'nexo_clients.ID' )
        // ->join( store_prefix() . 'nexo_commandes_meta', store_prefix() . 'nexo_commandes_meta.REF_ORDER_ID = ' . store_prefix() . 'nexo_commandes.ID', 'left' )
        ->join( 'aauth_users', 'aauth_users.id = ' . store_prefix() . 'nexo_commandes.AUTHOR' )
        ->where( store_prefix() . 'nexo_commandes.DATE_CREATION >=', $startDate )
        ->where( store_prefix() . 'nexo_commandes.DATE_CREATION <=', $endDate );

        $this->db->where_in( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_TYPE',['takeaway','delivery' ]);
        
        /**
         * determine wether it should display
         * only paid order on the ready order screen
         */
        // if ( store_option( 'gastro_display_only_unpaid_ready_orders', 'no' ) === 'yes' ) {
        //    $this->db->where( store_prefix() . 'nexo_commandes.TYPE !=', 'nexo_order_comptant' );
        // }
        
        $this->db->where_in( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS', [ 'ready', 'collected', 'ongoing', 'partially' ]);

        $this->db->group_by( store_prefix() . 'nexo_commandes.CODE' );
        $this->db->order_by( store_prefix() . 'nexo_commandes.DATE_CREATION', 'desc' );
        
        $query    =    $this->db->order_by( store_prefix() . 'nexo_commandes.ID', 'desc' )
        ->get();

        $data   =   $query->result_array();
        
        if ( $data ) {
    
            /**
             * get order metas and fill
             * it within the current request
             */
            $metas  =   $this->db->where( 'REF_ORDER_ID', $data[0][ 'ID' ] )
                ->get( store_prefix() . 'nexo_commandes_meta' )
                ->result_array();
    
            $data[0][ 'metas' ]        =   [];
    
            foreach( $metas as $meta ) {
                $data[0][ 'metas' ][ $meta[ 'KEY' ] ]   =   $meta[ 'VALUE' ];
            }
            
            foreach( $data as $key => $order ) {

                // ' . store_prefix() . 'nexo_articles.PRIX_DE_VENTE_TTC as PRIX_DE_VENTE_TTC,
                // ' . store_prefix() . 'nexo_articles.PRIX_DE_VENTE as PRIX_DE_VENTE,

                $this->db
                ->select('
                ' . store_prefix() . 'nexo_commandes_produits.REF_PRODUCT_CODEBAR as CODEBAR,
                ' . store_prefix() . 'nexo_articles.ID as ID,
                ' . store_prefix() . 'nexo_articles.APERCU as APERCU,
                ' . store_prefix() . 'nexo_commandes_produits.QUANTITE as QTE_ADDED,
                ' . store_prefix() . 'nexo_commandes_produits.DISCOUNT_TYPE as DISCOUNT_TYPE,
                ' . store_prefix() . 'nexo_commandes_produits.DISCOUNT_PERCENT as DISCOUNT_PERCENT,
                ' . store_prefix() . 'nexo_commandes_produits.DISCOUNT_AMOUNT as DISCOUNT_AMOUNT,
                ' . store_prefix() . 'nexo_commandes_produits.INLINE as INLINE,
                ' . store_prefix() . 'nexo_commandes_produits.ID as COMMAND_PRODUCT_ID,

                ' . store_prefix() . 'nexo_commandes_produits.PRIX_BRUT as PRIX_DE_VENTE_TTC,
                ' . store_prefix() . 'nexo_commandes_produits.PRIX_BRUT as PRIX_DE_VENTE_BRUT,
                ' . store_prefix() . 'nexo_commandes_produits.PRIX as PRIX_DE_VENTE,
                ' . store_prefix() . 'nexo_commandes_produits.PRIX as PRIX,

                ' . store_prefix() . 'nexo_commandes_produits.TAX as TAX,
                ' . store_prefix() . 'nexo_commandes_produits.TOTAL_TAX as TOTAL_TAX,
                ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_MODIFIERS,
                ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_NOTE,
                ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_STATUS,
                ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_ISSUE,
                ' . store_prefix() . 'nexo_articles.DESIGN as DESIGN,
                ' . store_prefix() . 'nexo_articles.PRODUCT_SKIP_COOKING,
                ' . store_prefix() . 'nexo_articles.STOCK_ENABLED as STOCK_ENABLED,
                ' . store_prefix() . 'nexo_articles.SPECIAL_PRICE_START_DATE as SPECIAL_PRICE_START_DATE,
                ' . store_prefix() . 'nexo_articles.SPECIAL_PRICE_END_DATE as SPECIAL_PRICE_END_DATE,
                ' . store_prefix() . 'nexo_articles.SHADOW_PRICE as SHADOW_PRICE,
                ' . store_prefix() . 'nexo_articles.PRIX_PROMOTIONEL as PRIX_PROMOTIONEL,
                ' . store_prefix() . 'nexo_articles.QUANTITE_RESTANTE as QUANTITE_RESTANTE,
                ' . store_prefix() . 'nexo_articles.QUANTITE_VENDU as QUANTITE_VENDU,
                ' . store_prefix() . 'nexo_articles.REF_CATEGORIE as REF_CATEGORIE,
                ' . store_prefix() . 'nexo_commandes_produits.NAME as NAME')
                ->from( store_prefix() . 'nexo_commandes')
                ->join( store_prefix() . 'nexo_commandes_produits', store_prefix() . 'nexo_commandes.CODE = ' . store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE', 'inner')
                ->join( store_prefix() . 'nexo_articles', store_prefix() . 'nexo_articles.CODEBAR = ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_PRODUCT_REAL_BARCODE', 'left')
                // ->join( store_prefix() . 'nexo_commandes_produits_meta', store_prefix() . 'nexo_commandes_produits.ID = ' . store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_PRODUCT', 'left' )
                // ->group_by([ 
                //     store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_PRODUCT',
                //     store_prefix() . 'nexo_commandes_produits.REF_PRODUCT_CODEBAR'
                // ])
                ->where( store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE', $order[ 'CODE' ]);

                $sub_query        =    $this->db->get();

                $data[ $key ][ 'items' ]    =   $sub_query->result_array();
            }
        }

               $this->db
                ->select( '
                aauth_users.name as AUTHOR_NAME,
                ' . store_prefix() . 'nexo_commandes.CODE as CODE,
                ' . store_prefix() . 'nexo_commandes.TYPE as TYPE,
                ' . store_prefix() . 'nexo_commandes.ID as ID,
                ' . store_prefix() . 'nexo_commandes.ID as ORDER_ID,
                ' . store_prefix() . 'nexo_commandes.DATE_CREATION as DATE_CREATION,
                ' . store_prefix() . 'nexo_commandes.DATE_MOD as DATE_MOD,
                ' . store_prefix() . 'nexo_commandes.REMISE_TYPE as REMISE_TYPE,
                ' . store_prefix() . 'nexo_commandes.REF_CLIENT as REF_CLIENT,
                ' . store_prefix() . 'nexo_commandes.DESCRIPTION as DESCRIPTION,
                ' . store_prefix() . 'nexo_commandes.REMISE_PERCENT as REMISE_PERCENT,
                ' . store_prefix() . 'nexo_commandes.GROUP_DISCOUNT as GROUP_DISCOUNT,
                ' . store_prefix() . 'nexo_commandes.PAYMENT_TYPE as PAYMENT_TYPE,
                ' . store_prefix() . 'nexo_commandes.SHIPPING_AMOUNT as SHIPPING_AMOUNT,
                ' . store_prefix() . 'nexo_commandes.REMISE as REMISE,
                ' . store_prefix() . 'nexo_commandes.RESTAURANT_TABLE_ID,
                ' . store_prefix() . 'nexo_commandes.RESTAURANT_SEAT_USED,
                ' . store_prefix() . 'nexo_commandes.RESTAURANT_BOOKED_FOR,
                ' . store_prefix() . 'nexo_clients.NOM as CUSTOMER_NAME,
                ' . store_prefix() . 'nexo_commandes.TITRE as TITRE,
                ' . store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_TYPE as REAL_TYPE,
                ' . store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS as STATUS' )
                
                ->from( store_prefix() . 'nexo_commandes' )
                ->join( store_prefix() . 'nexo_clients', store_prefix() . 'nexo_commandes.REF_CLIENT = ' . store_prefix() . 'nexo_clients.ID' )
                // ->join( store_prefix() . 'nexo_commandes_meta', store_prefix() . 'nexo_commandes_meta.REF_ORDER_ID = ' . store_prefix() . 'nexo_commandes.ID', 'left' )
                ->join( 'aauth_users', 'aauth_users.id = ' . store_prefix() . 'nexo_commandes.AUTHOR' )
                ->where( store_prefix() . 'nexo_commandes.DATE_CREATION >=', $startDate )
                ->where( store_prefix() . 'nexo_commandes.DATE_CREATION <=', $endDate );

                $this->db->where( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_TYPE ', 'dinein' );

                // $this->db->where( store_prefix() . 'nexo_commandes.PAYMENT_TYPE ', 'unpaid' );
              
                
                /**
                 * determine wether it should display
                 * only paid order on the ready order screen
                 */
                // if ( store_option( 'gastro_display_only_unpaid_ready_orders', 'no' ) === 'yes' ) {
                //    $this->db->where( store_prefix() . 'nexo_commandes.TYPE !=', 'nexo_order_comptant' );
                // }
                
                $this->db->where_in( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS', [ 'ready', 'collected', 'ongoing', 'partially' ]);

                $this->db->group_by( store_prefix() . 'nexo_commandes.CODE' );
                $this->db->order_by( store_prefix() . 'nexo_commandes.DATE_CREATION', 'desc' );
                
                $query_1    =    $this->db->order_by( store_prefix() . 'nexo_commandes.ID', 'desc' )
                ->get();

                $data1   =   $query_1->result_array();
                
                if ( $data1 ) {
            
                    /**
                     * get order metas and fill
                     * it within the current request
                     */
                    $metas1  =   $this->db->where( 'REF_ORDER_ID', $data1[0][ 'ID' ] )
                        ->get( store_prefix() . 'nexo_commandes_meta' )
                        ->result_array();
            
                    $data1[0][ 'metas' ]        =   [];
            
                    foreach( $metas1 as $meta ) {
                        $data1[0][ 'metas' ][ $meta[ 'KEY' ] ]   =   $meta[ 'VALUE' ];
                    }
                    
                    foreach( $data1 as $key => $order ) {

                        // ' . store_prefix() . 'nexo_articles.PRIX_DE_VENTE_TTC as PRIX_DE_VENTE_TTC,
                        // ' . store_prefix() . 'nexo_articles.PRIX_DE_VENTE as PRIX_DE_VENTE,

                        $this->db
                        ->select('
                        ' . store_prefix() . 'nexo_commandes_produits.REF_PRODUCT_CODEBAR as CODEBAR,
                        ' . store_prefix() . 'nexo_articles.ID as ID,
                        ' . store_prefix() . 'nexo_articles.APERCU as APERCU,
                        ' . store_prefix() . 'nexo_commandes_produits.QUANTITE as QTE_ADDED,
                        ' . store_prefix() . 'nexo_commandes_produits.DISCOUNT_TYPE as DISCOUNT_TYPE,
                        ' . store_prefix() . 'nexo_commandes_produits.DISCOUNT_PERCENT as DISCOUNT_PERCENT,
                        ' . store_prefix() . 'nexo_commandes_produits.DISCOUNT_AMOUNT as DISCOUNT_AMOUNT,
                        ' . store_prefix() . 'nexo_commandes_produits.INLINE as INLINE,
                        ' . store_prefix() . 'nexo_commandes_produits.ID as COMMAND_PRODUCT_ID,

                        ' . store_prefix() . 'nexo_commandes_produits.PRIX_BRUT as PRIX_DE_VENTE_TTC,
                        ' . store_prefix() . 'nexo_commandes_produits.PRIX_BRUT as PRIX_DE_VENTE_BRUT,
                        ' . store_prefix() . 'nexo_commandes_produits.PRIX as PRIX_DE_VENTE,
                        ' . store_prefix() . 'nexo_commandes_produits.PRIX as PRIX,

                        ' . store_prefix() . 'nexo_commandes_produits.TAX as TAX,
                        ' . store_prefix() . 'nexo_commandes_produits.TOTAL_TAX as TOTAL_TAX,
                        ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_MODIFIERS,
                        ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_NOTE,
                        ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_STATUS,
                        ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_ISSUE,
                        ' . store_prefix() . 'nexo_articles.DESIGN as DESIGN,
                        ' . store_prefix() . 'nexo_articles.PRODUCT_SKIP_COOKING,
                        ' . store_prefix() . 'nexo_articles.STOCK_ENABLED as STOCK_ENABLED,
                        ' . store_prefix() . 'nexo_articles.SPECIAL_PRICE_START_DATE as SPECIAL_PRICE_START_DATE,
                        ' . store_prefix() . 'nexo_articles.SPECIAL_PRICE_END_DATE as SPECIAL_PRICE_END_DATE,
                        ' . store_prefix() . 'nexo_articles.SHADOW_PRICE as SHADOW_PRICE,
                        ' . store_prefix() . 'nexo_articles.PRIX_PROMOTIONEL as PRIX_PROMOTIONEL,
                        ' . store_prefix() . 'nexo_articles.QUANTITE_RESTANTE as QUANTITE_RESTANTE,
                        ' . store_prefix() . 'nexo_articles.QUANTITE_VENDU as QUANTITE_VENDU,
                        ' . store_prefix() . 'nexo_articles.REF_CATEGORIE as REF_CATEGORIE,
                        ' . store_prefix() . 'nexo_commandes_produits.NAME as NAME')
                        ->from( store_prefix() . 'nexo_commandes')
                        ->join( store_prefix() . 'nexo_commandes_produits', store_prefix() . 'nexo_commandes.CODE = ' . store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE', 'inner')
                        ->join( store_prefix() . 'nexo_articles', store_prefix() . 'nexo_articles.CODEBAR = ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_PRODUCT_REAL_BARCODE', 'left')
                        // ->join( store_prefix() . 'nexo_commandes_produits_meta', store_prefix() . 'nexo_commandes_produits.ID = ' . store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_PRODUCT', 'left' )
                        // ->group_by([ 
                        //     store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_PRODUCT',
                        //     store_prefix() . 'nexo_commandes_produits.REF_PRODUCT_CODEBAR'
                        // ])
                        ->where( store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE', $order[ 'CODE' ]);

                        $sub_query1        =    $this->db->get();

                        $data1[ $key ][ 'items' ]    =   $sub_query1->result_array();
                    }
                   
                  
                }

               $id =$this->events->apply_filters('group_details', '')->id;

          


                $downloads = array();
             if($id!="12"){
                if(is_array($data))
                    $downloads = array_merge($downloads, $data);
               

            }
            if(is_array($data1 ))
                $downloads = array_merge($downloads, $data1);
        
            // $final_array = array_merge($data,$data1);
             return response()->json( $downloads );

    }
      public function billorders()
    {


    // print_r(__LINE__);
    // die();
    
     $startofday = new DateTime();
        $startofday->setTime(0,0);
        $startDate =$startofday->format('Y-m-d H:i:s');

        // echo date('Y-m-d H:i:s',$today);

        // echo "<br>";
        $endOfDay = new DateTime();
        $endOfDay->setTime(23,59);
        $endDate = $endOfDay->format('Y-m-d H:i:s');

        $this->db
        ->select( '
        aauth_users.name as AUTHOR_NAME,
        ' . store_prefix() . 'nexo_commandes.CODE as CODE,
        ' . store_prefix() . 'nexo_commandes.TYPE as TYPE,
        ' . store_prefix() . 'nexo_commandes.ID as ID,
        ' . store_prefix() . 'nexo_commandes.ID as ORDER_ID,
        ' . store_prefix() . 'nexo_commandes.DATE_CREATION as DATE_CREATION,
        ' . store_prefix() . 'nexo_commandes.DATE_MOD as DATE_MOD,
        ' . store_prefix() . 'nexo_commandes.REMISE_TYPE as REMISE_TYPE,
        ' . store_prefix() . 'nexo_commandes.REF_CLIENT as REF_CLIENT,
        ' . store_prefix() . 'nexo_commandes.DESCRIPTION as DESCRIPTION,
        ' . store_prefix() . 'nexo_commandes.REMISE_PERCENT as REMISE_PERCENT,
        ' . store_prefix() . 'nexo_commandes.GROUP_DISCOUNT as GROUP_DISCOUNT,
        ' . store_prefix() . 'nexo_commandes.PAYMENT_TYPE as PAYMENT_TYPE,
        ' . store_prefix() . 'nexo_commandes.SHIPPING_AMOUNT as SHIPPING_AMOUNT,
        ' . store_prefix() . 'nexo_commandes.REMISE as REMISE,
        ' . store_prefix() . 'nexo_commandes.RESTAURANT_TABLE_ID,
        ' . store_prefix() . 'nexo_commandes.RESTAURANT_SEAT_USED,
        ' . store_prefix() . 'nexo_commandes.RESTAURANT_BOOKED_FOR,
        ' . store_prefix() . 'nexo_clients.NOM as CUSTOMER_NAME,
        ' . store_prefix() . 'nexo_commandes.TITRE as TITRE,
        ' . store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_TYPE as REAL_TYPE,
        ' . store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS as STATUS' )
        
        ->from( store_prefix() . 'nexo_commandes' )
        ->join( store_prefix() . 'nexo_clients', store_prefix() . 'nexo_commandes.REF_CLIENT = ' . store_prefix() . 'nexo_clients.ID' )
        // ->join( store_prefix() . 'nexo_commandes_meta', store_prefix() . 'nexo_commandes_meta.REF_ORDER_ID = ' . store_prefix() . 'nexo_commandes.ID', 'left' )
        ->join( 'aauth_users', 'aauth_users.id = ' . store_prefix() . 'nexo_commandes.AUTHOR' )
        ->where( store_prefix() . 'nexo_commandes.DATE_CREATION >=', $startDate )
        ->where( store_prefix() . 'nexo_commandes.DATE_CREATION <=', $endDate );

        $this->db->where_in( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_TYPE',['takeaway','delivery','quickbill' ]);
        
        /**
         * determine wether it should display
         * only paid order on the ready order screen
         */
        // if ( store_option( 'gastro_display_only_unpaid_ready_orders', 'no' ) === 'yes' ) {
        //    $this->db->where( store_prefix() . 'nexo_commandes.TYPE !=', 'nexo_order_comptant' );
        // }
        
        // $this->db->where_in( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS', [ 'ready', 'collected', 'ongoing', 'partially' ]);

        $this->db->group_by( store_prefix() . 'nexo_commandes.CODE' );
        $this->db->order_by( store_prefix() . 'nexo_commandes.DATE_CREATION', 'desc' );
        
        $query    =    $this->db->order_by( store_prefix() . 'nexo_commandes.ID', 'desc' )
        ->get();

        $data   =   $query->result_array();
        
        if ( $data ) {
    
            /**
             * get order metas and fill
             * it within the current request
             */
            $metas  =   $this->db->where( 'REF_ORDER_ID', $data[0][ 'ID' ] )
                ->get( store_prefix() . 'nexo_commandes_meta' )
                ->result_array();
    
            $data[0][ 'metas' ]        =   [];
    
            foreach( $metas as $meta ) {
                $data[0][ 'metas' ][ $meta[ 'KEY' ] ]   =   $meta[ 'VALUE' ];
            }
            
            foreach( $data as $key => $order ) {

                // ' . store_prefix() . 'nexo_articles.PRIX_DE_VENTE_TTC as PRIX_DE_VENTE_TTC,
                // ' . store_prefix() . 'nexo_articles.PRIX_DE_VENTE as PRIX_DE_VENTE,

                $this->db
                ->select('
                ' . store_prefix() . 'nexo_commandes_produits.REF_PRODUCT_CODEBAR as CODEBAR,
                ' . store_prefix() . 'nexo_articles.ID as ID,
                ' . store_prefix() . 'nexo_articles.APERCU as APERCU,
                ' . store_prefix() . 'nexo_commandes_produits.QUANTITE as QTE_ADDED,
                ' . store_prefix() . 'nexo_commandes_produits.DISCOUNT_TYPE as DISCOUNT_TYPE,
                ' . store_prefix() . 'nexo_commandes_produits.DISCOUNT_PERCENT as DISCOUNT_PERCENT,
                ' . store_prefix() . 'nexo_commandes_produits.DISCOUNT_AMOUNT as DISCOUNT_AMOUNT,
                ' . store_prefix() . 'nexo_commandes_produits.INLINE as INLINE,
                ' . store_prefix() . 'nexo_commandes_produits.ID as COMMAND_PRODUCT_ID,
                ' . store_prefix() . 'nexo_commandes_produits.PRIX_BRUT as PRIX_DE_VENTE_TTC,
                ' . store_prefix() . 'nexo_commandes_produits.PRIX_BRUT as PRIX_DE_VENTE_BRUT,
                ' . store_prefix() . 'nexo_commandes_produits.PRIX as PRIX_DE_VENTE,
                ' . store_prefix() . 'nexo_commandes_produits.PRIX as PRIX,
                ' . store_prefix() . 'nexo_commandes_produits.TAX as TAX,
                ' . store_prefix() . 'nexo_commandes_produits.TOTAL_TAX as TOTAL_TAX,
                ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_MODIFIERS,
                ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_NOTE,
                ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_STATUS,
                ' . store_prefix() . 'nexo_commandes.TOTAL as TOTAL,
                ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_ISSUE,
                ' . store_prefix() . 'nexo_articles.DESIGN as DESIGN,
                ' . store_prefix() . 'nexo_articles.PRODUCT_SKIP_COOKING,
                ' . store_prefix() . 'nexo_articles.STOCK_ENABLED as STOCK_ENABLED,
                ' . store_prefix() . 'nexo_articles.SPECIAL_PRICE_START_DATE as SPECIAL_PRICE_START_DATE,
                ' . store_prefix() . 'nexo_articles.SPECIAL_PRICE_END_DATE as SPECIAL_PRICE_END_DATE,
                ' . store_prefix() . 'nexo_articles.SHADOW_PRICE as SHADOW_PRICE,
                ' . store_prefix() . 'nexo_articles.PRIX_PROMOTIONEL as PRIX_PROMOTIONEL,
                ' . store_prefix() . 'nexo_articles.QUANTITE_RESTANTE as QUANTITE_RESTANTE,
                ' . store_prefix() . 'nexo_articles.QUANTITE_VENDU as QUANTITE_VENDU,
                ' . store_prefix() . 'nexo_articles.REF_CATEGORIE as REF_CATEGORIE,
                ' . store_prefix() . 'nexo_commandes_produits.NAME as NAME')
                ->from( store_prefix() . 'nexo_commandes')
                ->join( store_prefix() . 'nexo_commandes_produits', store_prefix() . 'nexo_commandes.CODE = ' . store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE', 'inner')
                ->join( store_prefix() . 'nexo_articles', store_prefix() . 'nexo_articles.CODEBAR = ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_PRODUCT_REAL_BARCODE', 'left')
                // ->join( store_prefix() . 'nexo_commandes_produits_meta', store_prefix() . 'nexo_commandes_produits.ID = ' . store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_PRODUCT', 'left' )
                // ->group_by([ 
                //     store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_PRODUCT',
                //     store_prefix() . 'nexo_commandes_produits.REF_PRODUCT_CODEBAR'
                // ])
                ->where( store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE', $order[ 'CODE' ]);

                $sub_query        =    $this->db->get();

                $data[ $key ][ 'items' ]    =   $sub_query->result_array();
            }
        }

               $this->db
                ->select( '
                aauth_users.name as AUTHOR_NAME,
                ' . store_prefix() . 'nexo_commandes.CODE as CODE,
                ' . store_prefix() . 'nexo_commandes.TYPE as TYPE,
                ' . store_prefix() . 'nexo_commandes.ID as ID,
                ' . store_prefix() . 'nexo_commandes.ID as ORDER_ID,
                ' . store_prefix() . 'nexo_commandes.DATE_CREATION as DATE_CREATION,
                ' . store_prefix() . 'nexo_commandes.DATE_MOD as DATE_MOD,
                ' . store_prefix() . 'nexo_commandes.REMISE_TYPE as REMISE_TYPE,
                ' . store_prefix() . 'nexo_commandes.REF_CLIENT as REF_CLIENT,
                ' . store_prefix() . 'nexo_commandes.DESCRIPTION as DESCRIPTION,
                ' . store_prefix() . 'nexo_commandes.REMISE_PERCENT as REMISE_PERCENT,
                ' . store_prefix() . 'nexo_commandes.GROUP_DISCOUNT as GROUP_DISCOUNT,
                ' . store_prefix() . 'nexo_commandes.PAYMENT_TYPE as PAYMENT_TYPE,
                ' . store_prefix() . 'nexo_commandes.SHIPPING_AMOUNT as SHIPPING_AMOUNT,
                ' . store_prefix() . 'nexo_commandes.REMISE as REMISE,
                ' . store_prefix() . 'nexo_commandes.RESTAURANT_TABLE_ID,
                ' . store_prefix() . 'nexo_commandes.RESTAURANT_SEAT_USED,
                ' . store_prefix() . 'nexo_commandes.RESTAURANT_BOOKED_FOR,
                ' . store_prefix() . 'nexo_clients.NOM as CUSTOMER_NAME,
                ' . store_prefix() . 'nexo_commandes.TITRE as TITRE,
                ' . store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_TYPE as REAL_TYPE,
                ' . store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS as STATUS' )
                
                ->from( store_prefix() . 'nexo_commandes' )
                ->join( store_prefix() . 'nexo_clients', store_prefix() . 'nexo_commandes.REF_CLIENT = ' . store_prefix() . 'nexo_clients.ID' )
                // ->join( store_prefix() . 'nexo_commandes_meta', store_prefix() . 'nexo_commandes_meta.REF_ORDER_ID = ' . store_prefix() . 'nexo_commandes.ID', 'left' )
                ->join( 'aauth_users', 'aauth_users.id = ' . store_prefix() . 'nexo_commandes.AUTHOR' )
                ->where( store_prefix() . 'nexo_commandes.DATE_CREATION >=', $startDate )
                ->where( store_prefix() . 'nexo_commandes.DATE_CREATION <=', $endDate );

                $this->db->where( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_TYPE ', 'dinein' );

                // $this->db->where( store_prefix() . 'nexo_commandes.PAYMENT_TYPE ', 'unpaid' );
              
                
                /**
                 * determine wether it should display
                 * only paid order on the ready order screen
                 */
                // if ( store_option( 'gastro_display_only_unpaid_ready_orders', 'no' ) === 'yes' ) {
                //    $this->db->where( store_prefix() . 'nexo_commandes.TYPE !=', 'nexo_order_comptant' );
                // }
                
                // $this->db->where_in( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS', [ 'ready', 'collected', 'ongoing', 'partially' ]);

                $this->db->group_by( store_prefix() . 'nexo_commandes.CODE' );
                $this->db->order_by( store_prefix() . 'nexo_commandes.DATE_CREATION', 'desc' );
                
                $query_1    =    $this->db->order_by( store_prefix() . 'nexo_commandes.ID', 'desc' )
                ->get();

                $data1   =   $query_1->result_array();
                
                if ( $data1 ) {
            
                    /**
                     * get order metas and fill
                     * it within the current request
                     */
                    $metas1  =   $this->db->where( 'REF_ORDER_ID', $data1[0][ 'ID' ] )
                        ->get( store_prefix() . 'nexo_commandes_meta' )
                        ->result_array();
            
                    $data1[0][ 'metas' ]        =   [];
            
                    foreach( $metas1 as $meta ) {
                        $data1[0][ 'metas' ][ $meta[ 'KEY' ] ]   =   $meta[ 'VALUE' ];
                    }
                    
                    foreach( $data1 as $key => $order ) {

                        // ' . store_prefix() . 'nexo_articles.PRIX_DE_VENTE_TTC as PRIX_DE_VENTE_TTC,
                        // ' . store_prefix() . 'nexo_articles.PRIX_DE_VENTE as PRIX_DE_VENTE,

                        $this->db
                        ->select('
                        ' . store_prefix() . 'nexo_commandes_produits.REF_PRODUCT_CODEBAR as CODEBAR,
                        ' . store_prefix() . 'nexo_articles.ID as ID,
                        ' . store_prefix() . 'nexo_articles.APERCU as APERCU,
                        ' . store_prefix() . 'nexo_commandes_produits.QUANTITE as QTE_ADDED,
                        ' . store_prefix() . 'nexo_commandes_produits.DISCOUNT_TYPE as DISCOUNT_TYPE,
                        ' . store_prefix() . 'nexo_commandes_produits.DISCOUNT_PERCENT as DISCOUNT_PERCENT,
                        ' . store_prefix() . 'nexo_commandes_produits.DISCOUNT_AMOUNT as DISCOUNT_AMOUNT,
                        ' . store_prefix() . 'nexo_commandes_produits.INLINE as INLINE,
                        ' . store_prefix() . 'nexo_commandes_produits.ID as COMMAND_PRODUCT_ID,
                    
                        ' . store_prefix() . 'nexo_commandes_produits.PRIX_BRUT as PRIX_DE_VENTE_TTC,
                        ' . store_prefix() . 'nexo_commandes_produits.PRIX_BRUT as PRIX_DE_VENTE_BRUT,
                        ' . store_prefix() . 'nexo_commandes_produits.PRIX as PRIX_DE_VENTE,
                        ' . store_prefix() . 'nexo_commandes_produits.PRIX as PRIX,
                
                
                        ' . store_prefix() . 'nexo_commandes_produits.TAX as TAX,
                        ' . store_prefix() . 'nexo_commandes_produits.TOTAL_TAX as TOTAL_TAX,
                        ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_MODIFIERS,
                        ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_NOTE,
                        ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_STATUS,
                        ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_ISSUE,
                        ' . store_prefix() . 'nexo_articles.DESIGN as DESIGN,
                        ' . store_prefix() . 'nexo_articles.PRODUCT_SKIP_COOKING,
                        ' . store_prefix() . 'nexo_articles.STOCK_ENABLED as STOCK_ENABLED,
                        ' . store_prefix() . 'nexo_articles.SPECIAL_PRICE_START_DATE as SPECIAL_PRICE_START_DATE,
                        ' . store_prefix() . 'nexo_articles.SPECIAL_PRICE_END_DATE as SPECIAL_PRICE_END_DATE,
                        ' . store_prefix() . 'nexo_articles.SHADOW_PRICE as SHADOW_PRICE,
                          ' . store_prefix() . 'nexo_commandes.TOTAL as TOTAL,
                        ' . store_prefix() . 'nexo_articles.PRIX_PROMOTIONEL as PRIX_PROMOTIONEL,
                        ' . store_prefix() . 'nexo_articles.QUANTITE_RESTANTE as QUANTITE_RESTANTE,
                        ' . store_prefix() . 'nexo_articles.QUANTITE_VENDU as QUANTITE_VENDU,
                        ' . store_prefix() . 'nexo_articles.REF_CATEGORIE as REF_CATEGORIE,
                        ' . store_prefix() . 'nexo_commandes_produits.NAME as NAME')
                        ->from( store_prefix() . 'nexo_commandes')
                        ->join( store_prefix() . 'nexo_commandes_produits', store_prefix() . 'nexo_commandes.CODE = ' . store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE', 'inner')
                        ->join( store_prefix() . 'nexo_articles', store_prefix() . 'nexo_articles.CODEBAR = ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_PRODUCT_REAL_BARCODE', 'left')
                        // ->join( store_prefix() . 'nexo_commandes_produits_meta', store_prefix() . 'nexo_commandes_produits.ID = ' . store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_PRODUCT', 'left' )
                        // ->group_by([ 
                        //     store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_PRODUCT',
                        //     store_prefix() . 'nexo_commandes_produits.REF_PRODUCT_CODEBAR'
                        // ])
                        ->where( store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE', $order[ 'CODE' ]);

                        $sub_query1        =    $this->db->get();

                        $data1[ $key ][ 'items' ]    =   $sub_query1->result_array();
                    }
                   
                  
                }

               $id =$this->events->apply_filters('group_details', '')->id;

          


                $downloads = array();
             if($id!="12"){
                if(is_array($data))
                    $downloads = array_merge($downloads, $data);
               

            }
            if(is_array($data1 ))
                $downloads = array_merge($downloads, $data1);
        
            // $final_array = array_merge($data,$data1);
             return response()->json( $downloads );

    }

    /**
     *  get Rooms
     *  @param string int
     *  @return json
    **/
    public function tables_get( $id = null )
    {
        $this->db->select(
            store_prefix() . 'nexo_restaurant_tables.NAME as TABLE_NAME,' .
            store_prefix() . 'nexo_restaurant_tables.STATUS as STATUS,' .
            store_prefix() . 'nexo_restaurant_tables.MAX_SEATS as MAX_SEATS,' .
            store_prefix() . 'nexo_restaurant_tables.CURRENT_SEATS_USED as CURRENT_SEATS_USED,' .
            store_prefix() . 'nexo_restaurant_tables.ID as TABLE_ID,' .
            store_prefix() . 'nexo_restaurant_tables.SINCE as SINCE'
        )->from( store_prefix() . 'nexo_restaurant_tables' );

        if( $id != null ) {
            $this->db->where( 'ID', $id );
        }

        $this->response(
            $this->db->get()
            ->result(),
            200
        );
    }

    /**
     *  Get Area from Rooms
     *  @param int room id
     *  @return json
    **/
    public function tables_from_area_get( $areaID )
    {
        $this->db->select(
            store_prefix() . 'nexo_restaurant_tables.NAME as TABLE_NAME,' .
            store_prefix() . 'nexo_restaurant_tables.STATUS as STATUS,' .
            store_prefix() . 'nexo_restaurant_tables.MAX_SEATS as MAX_SEATS,' .
            store_prefix() . 'nexo_restaurant_tables.CURRENT_SEATS_USED as CURRENT_SEATS_USED,' .
            store_prefix() . 'nexo_restaurant_tables.ID as TABLE_ID,' .
            store_prefix() . 'nexo_restaurant_areas.ID as AREA_ID,' .
            store_prefix() . 'nexo_restaurant_tables.SINCE as SINCE'
        )->from( store_prefix() . 'nexo_restaurant_tables' )
        ->join( store_prefix() . 'nexo_restaurant_areas', store_prefix() . 'nexo_restaurant_tables.REF_AREA = ' . store_prefix() . 'nexo_restaurant_areas.ID' )
        ->where( store_prefix() . 'nexo_restaurant_areas.ID', $areaID );

        $query  =   $this->db->get();

        $this->response( $query->result(), 200 );
    }

    /**
     *  Edit Table
     *  @param
     *  @return
    **/
    public function table_usage_put( $table_id )
    {
        $this->load->module_model( 'gastro', 'Nexo_Gastro_Tables_Models', 'gastro_model' );


   //  $tablefields = store_prefix().'nexo_commandes.DATE_CREATION';

      

       // echo "<pre>";
       // print_r($tables_history_unpaid);
       // die();


    // echo $this->put( 'STATUS' );
    // die();

    if($this->put( 'STATUS' )=='available'){



        $startofday = new DateTime();
        $startofday->setTime(0,0);
        $startDate =$startofday->format('Y-m-d H:i:s');

        // echo date('Y-m-d H:i:s',$today);

        // echo "<br>";
        $endOfDay = new DateTime();
        $endOfDay->setTime(23,59);
        $endDate = $endOfDay->format('Y-m-d H:i:s');


        $tables_history_unpaid = $this->db->select('*')->from( store_prefix() . 'nexo_restaurant_tables_relation_orders' )->join( store_prefix() . 'nexo_commandes', store_prefix() . 'nexo_restaurant_tables_relation_orders.REF_ORDER = ' . store_prefix() . 'nexo_commandes.ID' )
                ->where( store_prefix() . 'nexo_restaurant_tables_relation_orders.REF_TABLE', $table_id)
                ->where( store_prefix() . 'nexo_commandes.STATUS','pending')
                ->where( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS!=','hold')
                ->where( store_prefix() . 'nexo_commandes.PAYMENT_TYPE','unpaid')->get()->result_array();



    

                if(empty($tables_history_unpaid)){    

                
                    
                    $result         =   get_instance()->gastro_model->table_status([
                        'ORDER_ID'              =>  $this->put( 'ORDER_ID' ),
                        'CURRENT_SEATS_USED'    =>  $this->put( 'CURRENT_SEATS_USED' ),
                        'STATUS'                =>  $this->put( 'STATUS' ),
                        'CURRENT_SESSION_ID'    =>  $this->put( 'CURRENT_SESSION_ID' ),
                        'SINCE'                 =>  '0000-00-00 00:00:00',
                        'TABLE_ID'              =>  $table_id
                    ]);

                } else {


                    $result = array('empty');
                }


                if( $result ) {
                    return $this->response( $result );
                }
                return $this->__failed();    


            } else {


                             $result         =   get_instance()->gastro_model->table_status([
                                'ORDER_ID'              =>  $this->put( 'ORDER_ID' ),
                                'CURRENT_SEATS_USED'    =>  $this->put( 'CURRENT_SEATS_USED' ),
                                'STATUS'                =>  $this->put( 'STATUS' ),
                                'CURRENT_SESSION_ID'    =>  $this->put( 'CURRENT_SESSION_ID' ),
                                'SINCE'                 =>  $this->put( 'SINCE' ),
                                'TABLE_ID'              =>  $table_id
                            ]);



                if( $result ) {
                    return $this->response( $result );
                }
                return $this->__failed();    

      }

        
    }

    /**
     * Dettache order to table
     * @param order id
     * @param table
     * @return void
    **/
    public function dettach_order_to_table( $order_id, $table_id ) 
    {
        $this->db->where( 'REF_ORDER', $order_id )
        ->where( 'TABLE_ID', $table_id )
        ->delete();
        $this->__success();
    }

    /**
     * Pay an order
     * @param int order id
     * @return json
    **/
    public function pay_order_put( $order_id )
    {
        $current_order          =    $this->db->where('ID', $order_id)
        ->get( store_prefix() . 'nexo_commandes')
        ->result_array();

        // @since 2.9 
        // @package nexopos
        // Save order payment
        $this->load->config( 'rest' );
        $Curl           =   new Curl;
        // $header_key      =   $this->config->item( 'rest_key_name' );
        // $header_value    =   $_SERVER[ 'HTTP_' . $this->config->item( 'rest_key_name' ) ];
        $Curl->setHeader($this->config->item('rest_key_name'), $_SERVER[ 'HTTP_' . $this->config->item('rest_header_key') ]);

        if( is_array( $this->put( 'payments' ) ) ) {
            foreach( $this->put( 'payments' ) as $payment ) {

                $Curl->post( site_url( array( 'rest', 'nexo', 'order_payment', store_get_param( '?' ) ) ), array(
                    'author'        =>  User::id(),
                    'date'          =>  date_now(),
                    'payment_type'  =>  $payment[ 'namespace' ],
                    'amount'        =>  $payment[ 'amount' ],
                    'order_code'    =>  $current_order[0][ 'CODE' ]
                ) );

                // @since 3.1
                // if the payment is a coupon, then we'll increase his usage
                if( $payment[ 'namespace' ] == 'coupon' ) {
                    
                    $coupon         =   $this->db->where( 'ID', $payment[ 'meta' ][ 'coupon_id' ] )
                    ->get( store_prefix() . 'nexo_coupons' )
                    ->result_array();

                    $this->db->where( 'ID', $payment[ 'meta' ][ 'coupon_id' ] )
                    ->update( store_prefix() . 'nexo_coupons', [
                        'USAGE_COUNT'   =>  intval( $coupon[0][ 'USAGE_COUNT' ] ) + 1
                    ]);
                }
            }
        }
        
        $this->response(array(
            'order_id'          =>    $order_id,
            'order_type'        =>    $current_order[0][ 'TYPE' ],
            'order_code'        =>    $current_order[0][ 'CODE' ]
        ), 200);
    }

    /**
     * Get table history by table id
     * @param int table id
     * @return json
     */
    public function table_order_history_get( $table_id )
    {
        $this->load->model( 'Nexo_Checkout' );
        $this->load->module_config( 'gastro', 'gastro' );
        $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'order_model' );
        $orders         =   $this->db->select('*,
        aauth_users.name as WAITER_NAME,
        ' . store_prefix() . 'nexo_commandes.AUTHOR as AUTHOR,
        ' . store_prefix() . 'nexo_commandes.ID as ORDER_ID,
        ' . store_prefix() . 'nexo_commandes.ID as ORDER_CODE,
        ' . store_prefix() . 'nexo_commandes.TYPE as TYPE,
        ' . store_prefix() . 'nexo_restaurant_tables_sessions.ID as SESSION_ID' )
        // ( SELECT ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_meta.VALUE FROM ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_meta
        //     WHERE ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_meta.REF_ORDER_ID = ORDER_ID
        //     AND ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_meta.KEY = "order_real_type"
        // ) as REAL_TYPE
        ->from( store_prefix() . 'nexo_restaurant_tables' )
        // ->join( 
        //     store_prefix() . 'nexo_restaurant_tables_sessions', 
        //     store_prefix() . 'nexo_restaurant_tables_sessions.REF_TABLE = ' . store_prefix() . 'nexo_restaurant_tables.ID',
        //     'inner' 
        // )
        ->join( 
            store_prefix() . 'nexo_restaurant_tables_relation_orders', 
            store_prefix() . 'nexo_restaurant_tables_relation_orders.REF_TABLE = ' . store_prefix() . 'nexo_restaurant_tables.ID' ,
            'inner'
        )
        ->join( 
            store_prefix() . 'nexo_restaurant_tables_sessions', 
            store_prefix() . 'nexo_restaurant_tables_sessions.ID = ' . store_prefix() . 'nexo_restaurant_tables_relation_orders.REF_SESSION',
            'inner' 
        )
        ->join( 
            store_prefix() . 'nexo_commandes',
            store_prefix() . 'nexo_commandes.ID = ' . store_prefix() . 'nexo_restaurant_tables_relation_orders.REF_ORDER',
            'inner'
        )
        ->join( 
            'aauth_users', 
            'aauth_users.id = ' . store_prefix() . 'nexo_commandes.AUTHOR' ,
            'inner'
        )
        // ->join( 
        //     store_prefix() . 'nexo_commandes_meta', 
        //     store_prefix() . 'nexo_commandes_meta.REF_ORDER_ID = ' . store_prefix() . 'nexo_commandes.ID' 
        // )
       // ->limit( $this->config->item( 'gastro_order_history_limit' ) ) // 5 last orders
        ->where( store_prefix() . 'nexo_restaurant_tables_sessions.REF_TABLE', $table_id )
        ->where( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS!=', 'hold' )
         ->where( store_prefix() . 'nexo_commandes.PAYMENT_TYPE', 'unpaid' )
        // ->group_by( store_prefix() . 'nexo_commandes_meta.REF_ORDER_ID' )
        // ->order_by( store_prefix() . 'nexo_restaurant_tables_sessions.SESSION_STARTS', 'desc' )
         ->order_by( store_prefix() . 'nexo_commandes.DATE_CREATION', 'desc' )
        ->get()->result_array();

        // var_dump( $orders );

        $finalOrders    =   [];
        foreach( $orders as &$order ) {
            $finalOrders[ $order[ 'ORDER_CODE' ] ]  =   $order;
        }

        foreach( $finalOrders as &$order ) {
            $metas       =   $this->db->where( 'REF_ORDER_ID', $order[ 'REF_ORDER' ] )
            ->get( store_prefix() . 'nexo_commandes_meta' )
            ->result_array();
            
            if( $metas ) {
                foreach( $metas as $meta ) {
                    if( empty( @$order[ 'METAS' ] ) ) {
                        $order[ 'metas' ]   =   [];
                    }
                    
                    $order[ 'metas' ][ $meta[ 'KEY' ] ]     =   $meta[ 'VALUE' ];
                }
            }

            $order[ 'items' ]       =    $this->db
            ->select('*,
            ' . store_prefix() . 'nexo_commandes_produits.PRIX as PRIX_DE_VENTE,
            ' . store_prefix() . 'nexo_commandes_produits.PRIX as PRIX_DE_VENTE_BRUT,
            ' . store_prefix() . 'nexo_commandes_produits.PRIX_BRUT as PRIX_DE_VENTE_TTC,
            ' . store_prefix() . 'nexo_commandes_produits.REF_PRODUCT_CODEBAR as CODEBAR,
            ' . store_prefix() . 'nexo_commandes_produits.ID as ITEM_ID,
            ' . store_prefix() . 'nexo_commandes_produits.QUANTITE as QTE_ADDED,
            ' . store_prefix() . 'nexo_commandes_produits.NAME as DESIGN,
            ' . store_prefix() . 'nexo_articles.DESIGN as ORIGINAL_NAME,
            ' . store_prefix() . 'nexo_commandes_produits.created_on as item_addedon,
            ' . store_prefix() . 'nexo_commandes_produits.expiry_item_time as item_expiredon')
            ->from( store_prefix() . 'nexo_commandes')
            ->join( store_prefix() . 'nexo_commandes_produits', store_prefix() . 'nexo_commandes.CODE = ' . store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE', 'inner')
            ->join( store_prefix() . 'nexo_articles', store_prefix() . 'nexo_articles.CODEBAR = ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_PRODUCT_REAL_BARCODE', 'left')
            ->where( store_prefix() . 'nexo_commandes.ID', $order[ 'REF_ORDER' ] )
            ->get()
            ->result_array();

            $order[ 'payments' ]    =   $this->order_model->getPayments( $order[ 'REF_ORDER' ], 'incoming' );
        }
        return response()->json( array_values( $finalOrders ) );
    }

    /**
     * Serve Food
     * @param int order id
     * @return json response 
    **/
    public function serve_post()
    {
        $order      =   $this->db->where( 'ID', $this->post( 'order_id' ) )
        ->get( store_prefix() . 'nexo_commandes' )
        ->result_array();

        if( $order ) {
            if( $order[0][ 'RESTAURANT_ORDER_STATUS' ] == 'ready' ) {
                $this->db->where( 'ID', $this->post( 'order_id' ) )
                ->update( store_prefix() . 'nexo_commandes', [
                    'RESTAURANT_ORDER_STATUS'       =>  'served'
                ]);
            }
            return $this->__success();
        }
        return $this->__failed();
    }

    /**
     * Collected Meal
     * @param void
     * @return json
    **/       
    public function collect_meal_post()
    {
        $this->db->where( 'ID', $this->post( 'meal_id' ) )
       // ->where( 'KEY', 'RESTAURANT_FOOD_STATUS' )
        ->update( store_prefix() . 'nexo_commandes_produits', [
            'RESTAURANT_FOOD_STATUS'     =>  'collected'
        ]);

        return $this->__success();
    }

    /**
     * Mering orders
     * @param void
     * @return json
     */
    public function merge_order()
    {
        $this->load->model( 'Nexo_Checkout' );
        $this->load->module_model( 'gastro', 'Nexo_Gastro_Tables_Models', 'gastro_model' );
        $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'orders_model' );
        
        /**
         * Saving Order metas
         */
        $table          =   $this->gastro_model->get_table( $this->post( 'table' ) );
        $lastSession    =   $this->gastro_model->last_table_session( $this->post( 'table' ) );
        $code           =   $this->Nexo_Checkout->shuffle_code();


        $curr_date = date('Y-m-d');

        $this->db->select("*");
        $this->db->from(store_prefix() . 'nexo_commandes');
        //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
        $this->db->where('DATE(DATE_CREATION)',$curr_date);//use date function
        $this->db->limit(1);
        $this->db->order_by('ID',"DESC");
        $query = $this->db->get()->result_array();

        if(!empty($query)){

             $value_new = $query[0]['COUNT']+1;

        } else {

            $value_new=1;
        }


        $New_Number_temp = substr($code, 0, 6);
        $New_Number =  $New_Number_temp.$value_new;

        $code = $New_Number;



        $order_status   =   [
            'pending'   =>  0,
            'ready'     =>  0,
            'ongoing'   =>  0,
            'served'    =>  0
        ];

        // changing items order
        foreach( $this->post( 'orders' ) as $index => $order_id ) {
            /**
             * TODO
             * -> Delete session for merged orders
             */
            
            $order   =   $this->db->where( 'ID', $order_id )
                ->get( store_prefix() . 'nexo_commandes' )
                ->result_array();

            /**
             * Count the order status to detect the merged order status.
             */
            if( $order_status[ $order[0][ 'RESTAURANT_ORDER_STATUS' ] ] ) {
                $order_status[ $order[0][ 'RESTAURANT_ORDER_STATUS' ] ]++;
            }

            // update order code
            $this->db->where( 'REF_COMMAND_CODE', $order[0][ 'CODE' ] )
                ->update( store_prefix() . 'nexo_commandes_produits', [
                    'REF_COMMAND_CODE'      =>      $code
                ]);

            // set previous used table as available
            // if that table is empty
            if( $order[0][ 'RESTAURANT_ORDER_TYPE' ] == 'dinein' ) {
                $relation       =   $this->db->where( 'REF_ORDER', $order_id )
                    ->get( store_prefix() . 'nexo_restaurant_tables_relation_orders' )
                    ->result_array();

                /**
                 * delete the reference of the order 
                 * on the table session
                 */
                $this->gastro_model->unbind_order( $order_id, $relation[0][ 'REF_TABLE' ] );

                $orders         =   $this->gastro_model->get_session_orders( $relation[0][ 'REF_SESSION' ] );
                tendoo_debug( $orders, 'session-orders-' . $index . '.json' );

                /**
                 * Set a table as available if
                 * and only if it doesn't have any other orders
                 */
                tendoo_debug( count( $orders ), 'count-orders-' . $index . '.json' );

                if( count( $orders ) === 0 ) {
                    $this->db->where( 'ID', $relation[0][ 'REF_TABLE' ] )
                        ->update( store_prefix() . 'nexo_restaurant_tables', [
                            'STATUS'                =>  'available',
                            'CURRENT_SEATS_USED'    =>  0,
                            'CURRENT_SESSION_ID'    =>  0
                        ]);
                }
            }
        }

        // Create order
        $this->db->insert( store_prefix() . 'nexo_commandes', [
            'CODE'  =>  $code,
            'REF_CLIENT'                =>  $this->post( 'customer' ),
            'COUNT'=>$value_new,
            // 'RESTAURANT_ORDER_STATUS'   =>  'ready', should be updated according to merged order status
            'DATE_CREATION'             =>  date_now(),
            'AUTHOR'                    =>  User::id(),
            'TYPE'                      =>  'nexo_order_devis',
            'STATUS'                    =>  'processing',
            'RESTAURANT_ORDER_TYPE'     =>  $this->post( 'type' ),
            'RESTAURANT_SEAT_USED'      =>  $this->post( 'seats' ),
            'RESTAURANT_ORDER_STATUS'   =>  'pending',
            'RESTAURANT_TABLE_ID'       =>  $this->post( 'table' ),
            'PAYMENT_TYPE'              =>  'unpaid',
            'TITRE'                     =>  sprintf( __( 'Merged %s', 'gastro' ), $this->getTitle( $this->post( 'type' ) ) )
        ]);

        $index_id   =   $this->db->insert_id();

        tendoo_debug( $table, 'api-tables-controller-' . __LINE__ . '.json' );

        $this->db->insert_batch( store_prefix() . 'nexo_commandes_meta', [
            [
                'REF_ORDER_ID'  =>  $index_id,
                'KEY'           =>  'table_id',
                'VALUE'         =>  $this->post( 'table' ),
                'DATE_CREATION'     =>  date_now(),
                'AUTHOR'        =>  User::id()
            ],
            [
                'REF_ORDER_ID'  =>  $index_id,
                'KEY'           =>  'area_id',
                'VALUE'         =>  @$table[ 'REF_AREA' ],
                'DATE_CREATION'     =>  date_now(),
                'AUTHOR'        =>  User::id()
            ],
            [
                'REF_ORDER_ID'  =>  $index_id,
                'KEY'           =>  'seat_used',
                'VALUE'         =>  $this->post( 'seats' ),
                'DATE_CREATION'     =>  date_now(),
                'AUTHOR'        =>  User::id()
            ],
        ]);

        // create session if type is dinein
        if( $this->post( 'type' ) == 'dinein' ) {
            // save order status
            get_instance()->gastro_model->delete_session( @$lastSession[0][ 'ID' ] );
            get_instance()->gastro_model->table_status([
                'STATUS'                =>  'in_use',
                'SINCE'                 =>  date_now(),
                /**
                 * if the last session is not closed, then we use the destination table SESSION_STARTS otherwise
                 * we'll let the system set it for us by defining "false".
                 */
                'SESSION_STARTS'        =>  @$lastSession[0][ 'SESSION_ENDS' ] === '0000-00-00 00:00:00' ? $lastSession[0][ 'SESSION_STARTS' ] : false,
                'CURRENT_SEATS_USED'    =>  $this->post( 'seats' ),
                'TABLE_ID'              =>  $this->post( 'table' ),
                'ORDER_ID'              =>  $index_id
            ]);
        }

        // detect real order status
        $final_status       =   'pending';
        if( $order_status[ 'pending' ] > 0 ) {
            $final_status   =   'pending';
        } else if ( $order_status[ 'ongoing' ] > 0 ) {
            $final_status   =   'ongoing';
        } else if ( $order_status[ 'ready' ] == count( $this->post( 'orders' ) ) ) {
            $final_status   =   'ready';
        } else if ( $order_status[ 'served' ] == count( $this->post( 'orders' ) ) ) {
            $final_status   =   'served';
        }

        // update order
        $this->db->where( 'CODE', $code )
            ->update( store_prefix() . 'nexo_commandes', [
                'TITRE'                     =>   sprintf( __( 'Dine in on %s', 'gastro' ), $table[ 'NAME' ]),
                'RESTAURANT_ORDER_STATUS'   =>   $final_status
            ]);

        /**
         * before both order get deleted
         * we do pass their id and the new order id
         */
        $this->events->do_action( 'gastro_merge_order', $index_id, $this->post( 'orders' ) );

        /**
         * delete previous orders
         */
        foreach( $this->post( 'orders' ) as $index => $order_id ) {
            // delete order
            $this->db->where( 'ID', $order_id )
                ->delete( store_prefix() . 'nexo_commandes' );
        }
        
        /**
         * calculate the order
         * to make sure the taxes are updated as well
         */
        $this->orders_model->recalculateOrder( $index_id, [] );

        return $this->__success();
    }

    /**
     * Get Type name for provided type
     * @param string type
     * @return string full type
     */
    public function getTitle( $type ) 
    {
        switch( $type ) {
            case 'dinein'       :   return __( 'Dine in', 'gastro' ); break;
            case 'takeaway'     :   return __( 'Take Away', 'gastro' ); break;
            case 'delivery'     :   return __( 'Delivery', 'gastro' ); break;
            default             :   return __( 'Unknown Type', 'gastro' ); break;
        }
    }

    /**
     * Cancel table item
     * @return json
     */
    public function cancel_item() 
    {
        $item   =   $this->db->where( 'ID', $this->post( 'item_id' ) )
        ->get( store_prefix() . 'nexo_commandes_produits' )
        ->result_array();

        /**
         * hook to dispatch
         * an event when a item is being canceled
         */
        $this->events->do_action( 'gastro_cancel_item', $item );

        if( $item ) {

            //die();

            /**
             * Change status since the item has been canceled.
             */
            $this->db->where( 'ID', $this->post( 'item_id' ) ) 
            ->update( store_prefix() . 'nexo_commandes_produits', [
                'RESTAURANT_FOOD_STATUS' =>   'canceled'
            ]);

            // calculate item cost
            $order  =   $this->db->where( 'CODE', $item[0][ 'REF_COMMAND_CODE' ] )
                ->get( store_prefix() . 'nexo_commandes' )
                ->result_array();




            /**
             * we should get the tax
             * to calculate properly the tax accordingly
             */
            $tax    =   $this->db->where( 'ID', $order[0][ 'REF_TAX' ] )
                ->get( store_prefix() . 'nexo_taxes' )
                ->result_array();


            /**
             * if no tax is set, then the 
             * default tax value will be used
             */
            $newVat             =   0;

            /**
             * if a tax is found, we assome the tax type is "variable"
             * then we might calculate the tax directly
             */
            if ( $tax ) {
                $oldVat         =   ( floatval( $order[0][ 'TOTAL' ] ) * floatval( $tax[0][ 'RATE' ] ) ) / 100;
                $rawTotal       =   floatval( $order[0][ 'TOTAL' ] ) - $oldVat;
                $newRawTotal    =   $rawTotal - floatval( $item[0][ 'PRIX_TOTAL' ] );
                $newVat         =   ( $newRawTotal * floatval( $tax[0][ 'RATE' ] ) ) / 100;
                $total          =   $newRawTotal + $newVat;
            } else {

                /**
                 * if the tax is fixed and defined
                 * we should calculate the tax
                 */
                if ( store_option( 'nexo_vat_type' ) == 'fixed' && floatval( store_option( 'nexo_vat_percent' ) ) > 0 ) {
                    $oldVat         =   floatval( $order[0][ 'TVA' ] );
                    $rawTotal       =   floatval( $order[0][ 'TOTAL' ] ) - $oldVat;
                    $newRawTotal    =   $rawTotal - floatval( $item[0][ 'PRIX_TOTAL' ] );
                    $newVat         =   ( $newRawTotal * floatval( store_option( 'nexo_vat_percent' ) ) ) / 100;
                    $total          =   $newRawTotal + $newVat;
                } else {
                    $total          =   floatval( $order[0][ 'TOTAL' ] ) - floatval( $item[0][ 'PRIX_TOTAL' ] );
                }
            }

            /**
             * Updating the costs
             */
            $this->db->where( 'CODE', $item[0][ 'REF_COMMAND_CODE' ] )
            ->update( store_prefix() . 'nexo_commandes', [
                'TOTAL'     =>  $total,
                'TVA'       =>  $newVat
            ]);



            /**
             * Updating the price, since we don't what it to be counted as sound item even on report.
             */
            $this->db->where( 'ID', $this->post( 'item_id' ) )
            ->update( store_prefix() . 'nexo_commandes_produits', [
                'PRIX'          =>  0,
                'PRIX_BRUT'     =>  0,
                'PRIX_TOTAL'    =>  0,
                'QUANTITE'      =>  1
            ]);


            $remaining_products = $this->db->select('*')->from(store_prefix() . 'nexo_commandes_produits')->where( 'REF_COMMAND_CODE', $item[0][ 'REF_COMMAND_CODE' ] )
                ->get()->result_array();

            $remaining_products_status= array();
            $remaining_products_status_temp = array();
            foreach ($remaining_products as $data) {
                array_push($remaining_products_status_temp,$data['RESTAURANT_FOOD_STATUS']);
            }
            $remaining_products_status = array_unique($remaining_products_status_temp);

            //print_r(array_unique($remaining_products_status));
            // echo "<pre>";
            // print_r($remaining_products_status);

            if (in_array("canceled", $remaining_products_status)){

                if(count($remaining_products_status)==1){


                        $this->db->where( 'CODE', $item[0][ 'REF_COMMAND_CODE' ] )
                                ->update( store_prefix() . 'nexo_commandes', [
                                    'TYPE'     =>  'nexo_order_comptant',
                                    'PAYMENT_TYPE'       =>  'cash',
                                    'STATUS'=>'completed',
                                    'RESTAURANT_ORDER_STATUS'       =>  'served'
                        ]);


                // echo "<pre>";
                // print_r($order);
                // die();

                 $table_history_pending_orders = $this->db->select('*')->from(store_prefix() . 'nexo_commandes')->where('RESTAURANT_TABLE_ID', $order[0][ 'RESTAURANT_TABLE_ID' ] )->where('RESTAURANT_ORDER_STATUS!=','hold')->where('PAYMENT_TYPE','unpaid')->where('STATUS','pending')->get()->result_array();

                 if(empty($table_history_pending_orders)){

                       $this->db->where( 'ID', $order[0][ 'RESTAURANT_TABLE_ID' ] )
                        ->update( store_prefix() . 'nexo_restaurant_tables', [
                            'STATUS'                =>  'available',
                            'CURRENT_SEATS_USED'    =>  0,
                            'CURRENT_SESSION_ID'    =>  0
                        ]);
                    }
                }

            }

            $this->events->do_action_ref_array( 'gastro_after_cancel_item', [ $item, $order ]);




            /**
             * we should be able to verifiy if all the items are canceled, 
             * the table should be set free. or at least a way should be set to close the session
             * or a way to place a new order over it.
             */
            return $this->response([
                'status'                    =>  'success',
                'message'                   =>  'item_removed',
                'restaurant_food_status'    =>  'canceled'
            ]);
        }

        /**
         * 
         */
        return $this->response([
            'status'    =>  'failed',
            'message'   =>  'item_404',
            'food_status'   =>  ''
        ], 404 );
    }

    /**
     * This controller free a table
     * so that a new order can be placed over it
     */
    public function free_table()
    {
        $this->load->module_model( 'gastro', 'Nexo_Gastro_Tables_Models', 'gastro_model' );

        /**
         * get orders as provided as a POST value
         */
        $order     =   $this->post( 'order' );

        /**
         * delete the table session to set that as free
         */
        $this->gastro_model->free_table( $order[ 'REF_TABLE' ] );

        $this->response([
            'status'    =>  'success',
            'message'   =>  __( 'The table has be successfully freed' )
        ]);
    }

    /**
     * add a new order to a used table rather than
     * forcing to add more items
     * @incomplete
     * @return json
     */
    public function pushOrder()
    {

    }

}