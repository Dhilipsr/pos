<?php

use Curl\Curl;
use Carbon\Carbon;

class ApiOrdersController extends Tendoo_Api
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get daily orders
     * @return json
     */
    public function daily( $type = null, $status = null, $restaurant_type = null)
    {

        $startofday = new DateTime();
        $startofday->setTime(0,0);
        $startDate =$startofday->format('Y-m-d H:i:s');

        // echo date('Y-m-d H:i:s',$today);

        // echo "<br>";
        $endOfDay = new DateTime();
        $endOfDay->setTime(23,59);
        $endDate = $endOfDay->format('Y-m-d H:i:s');


        $this->load->module_model( 'nexo', 'NexoCustomersModel', 'customer_model' );
        $this->db->select( '*' );

        if( $type != null ) {
            switch( $type ) {
                case 'unpaid'   :   
                    $this->db->where( store_prefix() . 'nexo_commandes.TYPE', 'nexo_order_devis' );
                break;
            }            
        }
        $id= $this->session->id;
        $sql="select group_id from tendoo_aauth_user_to_group where user_id=$id";
        $query = $this->db->query($sql);
        $query=$query->result_array();
        $grp_id= $query[0]['group_id'];
        
        if($grp_id=="12"){
         
            $this->db->where( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_TYPE', 'dinein' );

        }
        if( @$_GET[ 'restaurant_type' ] != null ) {
            $this->db->where( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_TYPE', $this->input->get( 'restaurant_type' ) );
        }
         if( @$_GET[ 'restaurant_type_merge' ] != null ) {

            $restaurant_type_merge = explode(" ",$_GET[ 'restaurant_type_merge' ]);

            // print_r($restaurant_type_merge);
           $this->db->where_in( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_TYPE', $restaurant_type_merge );
        }

        if( $status != null ) {
            $this->db->where( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS', $status );
        }

        $today              =   Carbon::parse( date_now() );
        $this->db->where( 'DATE_CREATION >=', $startDate );
        $this->db->where( 'DATE_CREATION <=', $endDate );
        $this->db->order_by( store_prefix() . 'nexo_commandes.DATE_CREATION', 'desc' );
        
        $orders     =   $this->db->get( store_prefix() . 'nexo_commandes' )
        ->result_array();

        /**
         * Adding items on the process
         */
        foreach( $orders as &$order ) {
            $order[ 'items' ]   =   $this->db->where( 'REF_COMMAND_CODE', $order[ 'CODE' ] )
                ->join( store_prefix() . 'nexo_articles', store_prefix() . 'nexo_articles.CODEBAR = ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_PRODUCT_REAL_BARCODE' )
                ->get( store_prefix() . 'nexo_commandes_produits' )
                ->result_array();

            /**
             * get orders table
             */
            $relation                   =    $this->db->where( 'REF_ORDER', $order[ 'ID' ] )
                ->order_by( 'ID', 'desc' )
                ->get( store_prefix() . 'nexo_restaurant_tables_relation_orders' )
                ->result_array();

            $order[ 'table_relation' ]      =  @$relation[0];
            
            /**
             * assign table to a relation
             */
            $table                      =   $this->db->where( 'ID', $order[ 'table_relation' ][ 'REF_TABLE' ] )
                ->get( store_prefix() . 'nexo_restaurant_tables' )
                ->result_array();
                
            $order[ 'table' ]        =   @$table[0];

            /**
             * join customer
             */
            $customer               =   $this->customer_model->get( $order[ 'REF_CLIENT' ]);
            $order[ 'customer' ]    =   @$customer[0];
        }

        return $this->response( $orders );
    }

    /**
     * Move order from one table to another table
     * @return json response
     */
    public function moveOrder()
    {
        $this->load->module_model( 'gastro', 'Nexo_Gastro_Tables_Models', 'gastro_model' );
        $this->load->module_model( 'nexo', 'NexoLogModel', 'log_model' );
        
        $order_id   =   $this->post( 'order_id' );
        $table_id   =   $this->post( 'table_id' );
        $seat_used  =   $this->post( 'seat_used' );
        $lastTable  =   $this->gastro_model->get_table_used( $order_id );

        /**
         * Perform binding
         */
        if ( $this->gastro_model->bind_order( $order_id, $table_id, $seat_used ) ) {

            /**
             * delete last table session
             * if the table doesn't have any other orders on the session
             */
            $session            =   $this->gastro_model->last_table_session( $lastTable[0][ 'ID' ] );
            $sessionOrders      =   $this->gastro_model->get_session_orders( $session[0][ 'ID' ] );

            /**
             * if the session has'nt any order
             */
            tendoo_debug( $session, 'table-session.json' );
            tendoo_debug( $sessionOrders, 'session-orders.json' );

            if( count( $sessionOrders ) === 0 ) {
                $this->gastro_model->delete_table_session( $lastTable[0][ 'ID' ] );
            }
        
            $this->log_model->log( 
                __( 'Gastro &mdash; Move Orders', 'gastro' ), 
                sprintf( 
                    __( 'The order with id %s has been moved from %s to table with ID : %s by %s' ),
                    $order_id,
                    $lastTable[0][ 'NAME' ],
                    $table_id,
                    User::pseudo()
                )
            );

            $this->events->do_action( 'gastro_move_order', [
                'order_id'              =>  $order_id,
                'previous_table_id'     =>  $lastTable[0][ 'ID' ],
                'new_table_id'          =>  $table_id
            ]);
            
            return $this->response([
                'status'    =>  'success',
                'message'   =>  __( 'The order was successfully bound to the table', 'gastro' )
            ]);
        }

        /**
         * We were unable to find the table
         */
        return $this->response([
            'status'    =>  'failed',
            'message'   =>  __( 'Unable to find that order', 'gastro' )
        ]);
    }

    /**
     * Split order
     * @return json
     */
    public function split()
    {
        $order  =   $this->post( 'order' );
        $this->load->config( 'rest' );
        $this->load->model( 'Nexo_Checkout' );
        $this->load->module_model( 'nexo', 'NexoLogModel', 'log_model' );
        $this->load->module_model( 'gastro', 'Nexo_Gastro_Tables_Models', 'gastro_model' );
        $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'order_model' );
        
        /**
         * get previous order details
         */
        $table      =   $this->gastro_model->get_table_used( $order[ 'ID' ] );
        
        /**
         * Let's create new orders 
         * first
         */
        $response       =   [];

        foreach( $this->post( 'parts' ) as $partOrder ) {

            foreach( $partOrder[ 'items' ] as &$item ) {
                $item[ 'codebar' ]                      =   $item[ 'RESTAURANT_PRODUCT_REAL_BARCODE' ];
                $item[ 'qte_added' ]                    =   $item[ 'QTE_ADDED' ];
                $item[ 'sale_price' ]                   =   $item[ 'PRIX_BRUT' ];
                $item[ 'stock_enabled' ]                =   $item[ 'STOCK_ENABLED' ];
                $item[ 'discount_type' ]                =   @$item[ 'DISCOUNT_TYPE' ] ? $item[ 'DISCOUNT_TYPE' ] : '';
                $item[ 'discount_amount' ]              =   @$item[ 'DISCOUNT_AMOUNT' ] ? $item[ 'DISCOUNT_AMOUNT' ] : 0;
                $item[ 'discount_percent' ]             =   @$item[ 'DISCOUNT_PERCENT' ] ? $item[ 'DISCOUNT_PERCENT' ] : '';
                $item[ 'name' ]                         =   @$item[ 'NAME' ] ? $item[ 'NAME' ] : '';
                $item[ 'inline' ]                       =   @$item[ 'INLINE' ] ? $item[ 'INLINE' ] : '';
                $item[ 'alternative_name' ]             =   @$item[ 'ALTERNATIVE_NAME' ] ? $item[ 'ALTERNATIVE_NAME' ] : '';
                $item[ 'tax' ]                          =   @$item[ 'TAX' ];
                $item[ 'total_tax' ]                    =   @$item[ 'TOTAL_TAX' ];
                $item[ 'restaurant_food_note' ]         =   @$item[ 'RESTAURANT_FOOD_NOTE' ];
                $item[ 'restaurant_food_printed' ]      =   @$item[ 'RESTAURANT_FOOD_PRINTED' ];
                $item[ 'restaurant_food_status' ]       =   @$item[ 'RESTAURANT_FOOD_STATUS' ];
                $item[ 'restaurant_food_modifiers' ]    =   json_decode( @$item[ 'RESTAURANT_FOOD_MODIFIERS' ] );
                $item[ 'restaurant_food_issue' ]        =   @$item[ 'RESTAURANT_FOOD_ISSUE' ];
                $item[ 'category_id' ]                  =   @$item[ 'RESTAURANT_PRODUCT_CATEGORY' ];
            }

            $_response   =   $this->Nexo_Checkout->postOrder( array(
                'RISTOURNE'             =>  0,
                'REMISE'                =>  0,
                'REMISE_PERCENT'        =>  0,
                'REMISE_TYPE'           =>  0,
                'RABAIS'                =>  0,
                'GROUP_DISCOUNT'        =>  0,
                'AUTHOR'                =>  0,
                'PAYMENT_TYPE'          =>  'unpaid',
                'TVA'                   =>  0,
                'DISCOUNT_TYPE'         =>  'disable', // by default discount are disabled when we're splitting orders
                'SHIPPING_AMOUNT'       =>  0,
                'REF_SHIPPING_ADDRESS'  =>  0,
                'REF_TAX'               =>  0,
                'TOTAL'                 =>  $partOrder[ 'total' ],
                'NET_TOTAL'             =>  $partOrder[ 'total' ],
                'SOMME_PERCU'           =>  0,
                'TYPE'                  =>  $order[ 'TYPE' ],
                'REF_CLIENT'            =>  $partOrder[ 'customer' ],
                'REF_WAITER'            =>  $partOrder[ 'waiters' ],

                'HMB_DISCOUNT'          =>  0,
                'ITEMS'                 =>  $partOrder[ 'items' ],
                'payments'              =>  [],
                'shipping'              =>  [
                    'id'                =>  $order[ 'REF_SHIPPING_ADDRESS' ],
                    'price'             =>  0
                ],
                'DESCRIPTION'           =>  $order[ 'DESCRIPTION' ] ? $order[ 'DESCRIPTION' ] : '',
                'REGISTER_ID'           =>  $order[ 'REF_REGISTER' ],
                'REF_TAX'               =>  $order[ 'REF_TAX' ],
                'TITRE'                 =>  sprintf( __( 'Dine in on %s'), $table[0][ 'NAME' ] ),
                'RESTAURANT_ORDER_STATUS'       =>  $order[ 'RESTAURANT_ORDER_STATUS' ],
                'RESTAURANT_ORDER_TYPE'         =>  @$order[ 'RESTAURANT_ORDER_TYPE' ] ? $order[ 'RESTAURANT_ORDER_TYPE' ] : 'dinein',
                'RESTAURANT_TABLE_ID'           =>  $order[ 'RESTAURANT_TABLE_ID' ],
                'RESTAURANT_SEATS_USED'         =>  $table[0][ 'CURRENT_SEATS_USED' ] ?? 0,
            ), User::id() );

            $this->events->do_action( 'gastro_order_split_result', $_response[ 'current_order' ][0][ 'ID' ], $order[ 'ID' ] );

            /**
             * let's recalculate the order 
             * to make the tax calculated accordingly
             */
            $this->order_model->recalculateOrder( $_response[ 'current_order' ][0][ 'ID' ], []);

            /**
             * bind new created to the order sessions
             */
            $this->gastro_model->push_order( $table[0][ 'ID' ], $_response[ 'current_order' ][0][ 'ID' ] );

            /**
             * Pushing Response to the resonse array
             */
            $response[]   =   $_response;
        }

        $this->events->do_action( 'gastro_split_order', $response, $order );
        
        /**
         * Deleting previous order from the table session
         */
        $this->db->where( 'REF_ORDER', $order[ 'ID' ] )
            ->delete( store_prefix() . 'nexo_restaurant_tables_relation_orders' );

        /**
         * Delete order with items
         */
        $this->Nexo_Checkout->commandes_delete( $order[ 'ID' ], true );   // force delete an order
        
        /**
         * Since Nexo_Checkout::commandes_delete doesn't delete the order him self
         */
        $this->db->where( 'ID', $order[ 'ID' ])->delete( store_prefix() . 'nexo_commandes' );    

        $this->log_model->log( 
            __( 'Gastro &mdash; Order Splitted', 'gastro' ), 
            sprintf( 
                __( 'The order %s has been splitted in %s parts by %s' ),
                $order[ 'CODE' ],
                count( $this->post( 'parts' ) ),
                User::pseudo()
            )
        );

        return $this->response( $response );
    }

    /**
     * Queue bookend order when the
     * time reached. Set the order to pending
     * @return json
     */
    public function queueBooked()
    {
        $this->load->module_model( 'gastro', 'Nexo_Gastro_Tables_Models', 'table_model' );

        $order  =   $this->post( 'order' );
        
        $this->table_model->table_status([
            'ORDER_ID'              =>  $order[ 'ID' ],
            'TABLE_ID'              =>  $order[ 'RESTAURANT_TABLE_ID' ],
            'CURRENT_SEATS_USED'    =>  $order[ 'RESTAURANT_SEAT_USED' ],
            'STATUS'                =>  'busy',
        ]);

        $this->db->where( 'ID', $order[ 'ID' ] )
            ->update( store_prefix() . 'nexo_commandes', [
                'RESTAURANT_ORDER_STATUS'   =>  'pending'
            ]);

        return $this->response([
            'status'    =>  'success',
            'message'   =>  __( 'The order has been placed to the pending order queue.', 'gastro' )
        ]);
    }

    public function ordersStaffHistory( $id )
    {
        $this->load->module_model( 'gastro', 'Gastro_Staff_Model', 'staff_model' );
        return $this->response( 
            $this->staff_model->getStaffActions( $id ) 
        );
    }

    public function ordersent()
    {
       


        $curr_date = date('Y-m-d');

        $this->db->select("*");
        $this->db->from(store_prefix() . 'nexo_commandes');
        $this->db->where('TYPE', 'nexo_order_comptant');
        $this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
        $this->db->limit(1);
        $this->db->order_by('ID',"DESC");


        $query_data = $this->db->get()->result_array();

        // print_r($query_data);
        // die();


          $data = array();


        foreach ($query_data as $datanew) {
            # code...
           
            $datanew_array = array();

            $datanew_array['CashInvoice']['Code'] = 'EComm13';

            $datanew_array['CashInvoice']['TrnType'] =  "CA";

           // $datanew_array['CashInvoice']['Code'] =   "Ecomm"; //ver

            $datanew_array['CashInvoice']['CrtUser'] = "A21tech"; 

            $datanew_array['CashInvoice']['DAdd1'] ='T1';

             $datanew_array['CashInvoice']['DocNo'] = (int)$datanew['CODE'];


               $datanew_array['CashInvoice']['GTTrnNo'] = 0;


                 $datanew_array['CashInvoice']['TrnNo'] = 0;




            $this->db->select("*");
            $this->db->from(store_prefix() . 'nexo_clients');

            $this->db->where('ID',$datanew['REF_CLIENT']);

            $query_data = $this->db->get()->result_array();

            $datanew_array['CashInvoice']['Narration'] =  $query_data[0]['NOM'];



            $datanew_array['CashInvoice']['Project'] =  'B1';


            $datanew_array['CashInvoice']['Department'] =  'NA';

            $datanew_array['CashInvoice']['Section'] =  'NA';

             $datanew_array['CashInvoice']['JobCode'] =  'NA';


            

            $datanew_array['CashInvoice']['ChqNo'] = $datanew['PAYMENT_TYPE'];

            $datanew_array['CashInvoice']['UpdUser'] = 'A21tech';




            $datanew_array['CashInvoice']['SalesPerson'] = 'A21tech';

            $datanew_array['CashInvoice']['TrnDate'] = date('Y-m-d\TH:i:s.NNN\Z',strtotime($datanew['DATE_CREATION']));

            $datanew_array['CashInvoice']['TrnTime'] = date('Y-m-d\TH:i:s.NNN\Z',strtotime($datanew['DATE_CREATION']));

            $datanew_array['CashInvoice']['DocDate'] = date('Y-m-d\TH:i:s.NNN\Z',strtotime($datanew['DATE_CREATION']));


             $datanew_array['CashInvoice']['DelDate'] = date('Y-m-d\TH:i:s.NNN\Z',strtotime($datanew['DATE_CREATION']));


             $datanew_array['CashInvoice']['CustPORef'] = 'CCSKID';

             $datanew_array['CashInvoice']['OurDO'] = 'MCC';

            $datanew_array['CashInvoice']['CustField1'] = 'NA';

            $datanew_array['CashInvoice']['CustField2'] = 'NA';


           


             



            $this->db->select('*');

            $this->db->from(store_prefix() . 'nexo_commandes_produits');

            $this->db->where('REF_COMMAND_CODE',$datanew['CODE']);

            $data_orders = $this->db->get()->result_array();

            // echo "<pre>";
            // print_r($data_orders);
            // die();


            foreach ($data_orders as $data_new) {


                $tax+=$data_new['TOTAL_TAX'];

                 $datanew_array['CashInvoice']['GSTAmount'] = $tax;
               
                $datanew_array['CashInvoice']['Details']['LineDiscount1']= '00';

                $datanew_array['CashInvoice']['Details']['LineDiscount2']= '00';

                $datanew_array['CashInvoice']['Details']['LineDiscount3']= '00';

                $datanew_array['CashInvoice']['Details']['ProdDesc']= 'POS-Item-'.$datanew['CODE'];

                $datanew_array['CashInvoice']['Details']['Product']= 'positem';

                $datanew_array['CashInvoice']['Details']['LotID']= 'positem';
            

                $datanew_array['CashInvoice']['Details']['Quantity'] += $data_new['QUANTITE'];

              //  $datanew_array['CashInvoice']['Details']['OrderQuantity']= $query['0']['SUM(QUANTITE)'];

                $datanew_array['CashInvoice']['Details']['UOM']=  'PCS';


                $datanew_array['CashInvoice']['Details']['UnitPrice'] += $data_new['PRIX_BRUT_TOTAL'];


                $datanew_array['CashInvoice']['Details']['Warehouse']= 'WH';

            }

              foreach ($data_orders as $data_Amount) {
                   
                    $datanew_array['CashInvoice']['ClearAccounts']['Name']= 'Visa';

                   

                    $datanew_array['CashInvoice']['ClearAccounts']['Amount'] += $data_Amount['PRIX_BRUT_TOTAL'];


             }


            $datanew_array['WarehouseID']= "WH";

            $datanew_array['DBKey']= "123456";



            $data = array();

            array_push($data, $datanew_array);

            $yourJson ='';
            $yourJson = trim(json_encode($data), '[]');


            //  echo "<br>";
            // print_r($yourJson);
            // echo "<br>";



            $curl = curl_init('http://a21.sg:91/inply_webservice/ERP_WebServices.svc/CreatePOSTransaction'); 
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($curl, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json' )); 
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
            curl_setopt($curl, CURLOPT_POSTFIELDS, $yourJson); 
            $result = curl_exec($curl); // Free up the resources $curl is using curl_close($curl); 
            echo $result;
          


        }

    }

    public function test(){

        
          $array = array(
            "company"=>store_option('site_name'),
            "UserAccess"=>"hvy",
            "Email"=>store_option('nexo_shop_email'),
            "Mobile"=>"",
            "Code"=>"EComm13",
            "CName"=>store_option('site_name'),
            "Add1"=>store_option('nexo_shop_address_1'),
            "Currency"=>store_option('nexo_currency_iso'),
            "GSTType"=>"G_SR",
            "GSTInc"=>'0',
            "Type"=>"C",
            "CusAcctGrp"=>"LOCA",
            "CusType"=>"A",
            "CusClass1"=>"NULL",
            "SalesPerson"=>"NULL",
            "DBKey"=>"123456"
        );
        
        //$data = array("Company"=>store_option('site_name'),"UserAccess"=>"","Code"=>"","CName"=>store_option('site_name'),"Add1"=>store_option('nexo_shop_address_1'),"Currency"=>store_option('nexo_currency'),"Type"=>"C","SalesPerson"=>"","DBKey"=>"T1111222233334445");


                //$data = array( 'patient_id' => '1', 'department_name' => 'a', 'patient_type' => 'b' ); 
        $data_string = json_encode($array); 
        $curl = curl_init('http://a21.sg:91/inply_webservice/ERP_WebServices.svc/CreateCustomerSupplier'); 
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json', 'Content-Length: ' . strlen($data_string)) ); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string); 
        $result = curl_exec($curl); // Free up the resources $curl is using curl_close($curl); 
        echo $result;
    }



}