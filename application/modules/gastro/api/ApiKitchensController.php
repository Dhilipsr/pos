<?php


use Carbon\Carbon;
use Dompdf\Dompdf;

class ApiKitchensController extends Tendoo_Api
{
    public function getPartiallyReady()
    {

        $startofday = new DateTime();
        $startofday->setTime(0,0);
        $startDate =$startofday->format('Y-m-d H:i:s');

        // echo date('Y-m-d H:i:s',$today);

        // echo "<br>";
        $endOfDay = new DateTime();
        $endOfDay->setTime(23,59);
        $endDate = $endOfDay->format('Y-m-d H:i:s');

        $this->db->select( '*,
        ' . store_prefix() . 'nexo_commandes.ID as ORDER_ID,
        ' . store_prefix() . 'nexo_restaurant_tables.NAME as TABLE_NAME,
        ' . store_prefix() . 'nexo_restaurant_areas.NAME as AREA_NAME,
        ' . store_prefix() . 'nexo_commandes_produits.NAME as NAME,
        ' . store_prefix() . 'nexo_commandes.DATE_CREATION as DATE' 
        )
        ->from( store_prefix() . 'nexo_commandes' )
        ->join( 
            store_prefix() . 'nexo_commandes_produits',
            store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE = ' . store_prefix() . 'nexo_commandes.CODE'
        )
        ->join( 
            store_prefix() . 'nexo_restaurant_tables_relation_orders',
            store_prefix() . 'nexo_restaurant_tables_relation_orders.REF_ORDER = ' . store_prefix() . 'nexo_commandes.ID'
        )
        ->join( 
            store_prefix() . 'nexo_restaurant_tables',
            store_prefix() . 'nexo_restaurant_tables.ID = ' . store_prefix() . 'nexo_restaurant_tables_relation_orders.REF_TABLE'
        )
        ->join( 
            store_prefix() . 'nexo_restaurant_areas',
            store_prefix() . 'nexo_restaurant_areas.ID = ' . store_prefix() . 'nexo_restaurant_tables.REF_AREA',
            'left'
        );
        $orders     =   $this->db->where_in( 'RESTAURANT_ORDER_STATUS', [ 'ready', 'partially' ])
        ->where( store_prefix() . 'nexo_commandes.DATE_CREATION >', $startDate )
        ->where( store_prefix() . 'nexo_commandes.DATE_CREATION <', $endDate )
        ->get()
        ->result_array();

        return $this->response( $orders, 200 );
    }

      public function orderstatus($order_id)
    {
        $this->db->select( '*');
        $orders     =   $this->db->where_in( 'REF_COMMAND_CODE', [ $order_id ])
        ->get()
        ->result_array();

        return $this->response( $orders, 200 );
    }

    public function orders()
    {


        $startofday = new DateTime();
        $startofday->setTime(0,0);
        $startDate =$startofday->format('Y-m-d H:i:s');

        // echo date('Y-m-d H:i:s',$today);

        // echo "<br>";
        $endOfDay = new DateTime();
        $endOfDay->setTime(23,59);
        $endDate = $endOfDay->format('Y-m-d H:i:s');

       

       // $startDate      =   Carbon::parse( date_now() )->startOfDay()->toDateTimeString();
        //$endDate        =   Carbon::parse( date_now() )->endOfDay()->toDateTimeString();
        // get order id where items belongs to a specific category
        $kitchen        =   $this->db->where( 'ID', @$_GET[ 'current_kitchen' ] )
            ->get( store_prefix() . 'nexo_restaurant_kitchens' )
            ->result_array();

        // if( ! $kitchen ) {
        //     echo json_encode([
        //         'message'       =>  __( 'Unable to locate the kitchen', 'gastro' ),
        //         'status'        =>  'failed'
        //     ]);
        //     return;
        // }

        // check if kitchen listen to specific categories
        $categories             =   @$kitchen[0][ 'REF_CATEGORY' ];
        $categories_ids         =   [];
        $filtred_order_ids      =   [];
        $filtred_item_ids       =   [];

        if( ! empty( $categories ) ) {
            $categories_ids         =   explode( ',', $categories );
        }

        if( ! empty( $categories_ids ) ) {
            $orders         =   $this->db
            ->select( '*,
            ' . store_prefix() . 'nexo_commandes.ID as ORDER_ID,
            ' . store_prefix() . 'nexo_commandes_produits.ID as ITEM_ID' )
            ->from( store_prefix() . 'nexo_commandes' )
            ->join( 
                store_prefix() . 'nexo_commandes_produits', 
                store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE = ' . 
                store_prefix() . 'nexo_commandes.CODE' 
            )
            ->join( 
                store_prefix() . 'nexo_articles', 
                store_prefix() . 'nexo_articles.CODEBAR = ' . 
                store_prefix() . 'nexo_commandes_produits.RESTAURANT_PRODUCT_REAL_BARCODE' 
            )
            ->where( store_prefix() . 'nexo_commandes.DATE_CREATION >=', $startDate )
            ->where( store_prefix() . 'nexo_commandes.DATE_CREATION <=', $endDate )
            ->where_in( 'REF_CATEGORIE', $categories_ids );

            if( @$_GET[ 'from-kitchen' ] ) {
                $this->db
                ->where_in( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS', [ 'pending', 'ongoing', 'partially' ] );

                /**
                 * Define orders which are allowed to 
                 * appear at the kitchen
                 */
                $orders         =   $this->events->apply_filters( 'gastro_kitchen_orders', [ 'nexo_order_devis' ]);
                if( $orders ) {
                    $this->db->where_in( store_prefix() . 'nexo_commandes.TYPE', $orders );
                }

            } else {
                $this->db
                /** ->where_in( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS', [ 'ready', 'collected' ] ) **/
                ->where( store_prefix() . 'nexo_commandes.TYPE !=', 'nexo_order_comptant' );
            }

            $orders        =   $this->db            
            ->get()
            ->result_array();
            
            // keep order ids
            if( $orders ) {
                foreach( $orders as $order ) {
                    $filtred_order_ids[]     =   $order[ 'ORDER_ID' ];
                    $filtred_item_ids[]      =   $order[ 'ITEM_ID' ];
                }

                $filtred_order_ids       =   array_unique( $filtred_order_ids );
                $filtred_item_ids        =   array_unique( $filtred_item_ids );
            }
        } 

        if( $filtred_order_ids || @$_GET[ 'from-kitchen' ] == 'true' ) {
            $this->db
            ->select( '
            aauth_users.name as AUTHOR_NAME,
            ' . store_prefix() . 'nexo_commandes.CODE as CODE,
            ' . store_prefix() . 'nexo_commandes.TYPE as TYPE,
            ' . store_prefix() . 'nexo_commandes.ID as ID,
            ' . store_prefix() . 'nexo_commandes.ID as ORDER_ID,
            ' . store_prefix() . 'nexo_commandes.DATE_CREATION as DATE_CREATION,
            ' . store_prefix() . 'nexo_commandes.DATE_MOD as DATE_MOD,
            ' . store_prefix() . 'nexo_commandes.REMISE_TYPE as REMISE_TYPE,
            ' . store_prefix() . 'nexo_commandes.REMISE_PERCENT as REMISE_PERCENT,
            ' . store_prefix() . 'nexo_commandes.GROUP_DISCOUNT as GROUP_DISCOUNT,
            ' . store_prefix() . 'nexo_commandes.SHIPPING_AMOUNT as SHIPPING_AMOUNT,
            ' . store_prefix() . 'nexo_commandes.REMISE as REMISE,
            ' . store_prefix() . 'nexo_clients.NOM as CUSTOMER_NAME,
            ' . store_prefix() . 'nexo_commandes.TITRE as TITRE,
            ' . store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_TYPE as REAL_TYPE,
            ' . store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS as STATUS,

            ( SELECT ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_meta.VALUE FROM ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_meta
                WHERE ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_meta.REF_ORDER_ID = ORDER_ID
                AND ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_meta.KEY = "table_id"
            ) as TABLE_ID,
            
            ( SELECT ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_meta.VALUE FROM ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_meta
                WHERE ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_meta.REF_ORDER_ID = ORDER_ID
                AND ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_meta.KEY = "nexostore_datetime"
            ) as BOOKED_FOR,

            ( SELECT ' . $this->db->dbprefix . store_prefix() . 'nexo_restaurant_tables.NAME FROM ' . $this->db->dbprefix . store_prefix() . 'nexo_restaurant_tables
                WHERE TABLE_ID = ' . $this->db->dbprefix . store_prefix() . 'nexo_restaurant_tables.ID
            ) as TABLE_NAME,

            ( SELECT ' . $this->db->dbprefix . store_prefix() . 'nexo_restaurant_tables.REF_AREA FROM ' . $this->db->dbprefix . store_prefix() . 'nexo_restaurant_tables
                WHERE TABLE_ID = ' . $this->db->dbprefix . store_prefix() . 'nexo_restaurant_tables.ID
            ) as AREA_ID,

            ( SELECT ' . $this->db->dbprefix . store_prefix() . 'nexo_restaurant_areas.NAME FROM ' . $this->db->dbprefix . store_prefix() . 'nexo_restaurant_areas
                WHERE AREA_ID = ' . $this->db->dbprefix . store_prefix() . 'nexo_restaurant_areas.ID
            ) as AREA_NAME' )
            
            ->from( store_prefix() . 'nexo_commandes' )
            ->join( store_prefix() . 'nexo_clients', store_prefix() . 'nexo_commandes.REF_CLIENT = ' . store_prefix() . 'nexo_clients.ID' )
            ->join( store_prefix() . 'nexo_commandes_meta', store_prefix() . 'nexo_commandes_meta.REF_ORDER_ID = ' . store_prefix() . 'nexo_commandes.ID' )
            ->join( 'aauth_users', 'aauth_users.id = ' . store_prefix() . 'nexo_commandes.AUTHOR' )
            ->where( store_prefix() . 'nexo_commandes.DATE_CREATION >=', Carbon::parse( date_now() )->startOfDay()->toDateTimeString() );
            // ->where( store_prefix() . 'nexo_commandes.DATE_CREATION <=', Carbon::parse( date_now() )->endOfDay()->toDateTimeString() );

            /**
             * Define orders which are allowed to 
             * appear at the kitchen
             */
            switch( store_option( 'order_status_shown', 'all' ) ) {
                case 'all': $allowed_orders = [ 'nexo_order_devis', 'nexo_order_comptant' ]; break;
                case 'from_unpaid': $allowed_orders = [ 'nexo_order_devis', 'nexo_order_comptant' ]; break;
                case 'paid': $allowed_orders = [ 'nexo_order_comptant' ]; break;
                default: $allowed_orders = [ 'nexo_order_devis', 'nexo_order_comptant' ]; break;
            }
            
            $orders         =   $this->events->apply_filters( 'gastro_kitchen_orders', $allowed_orders );

            if( $orders ) {
                $this->db->where_in( store_prefix() . 'nexo_commandes.TYPE', $orders );
            }

            /**
             * No more filtering for the end of the day since
             * we would like to display booked order as well
             */

            if( $filtred_order_ids ) {
                $this->db->where_in( store_prefix() . 'nexo_commandes.ID', $filtred_order_ids );
            } else if( @$_GET[ 'from-kitchen' ] == 'true' ) {
                $this->db->where_in( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS', [ 'pending', 'ongoing', 'partially', 'booking' ]);
            } else {
                $this->db->where( store_prefix() . 'nexo_commandes.TYPE !=', 'nexo_order_comptant' );
                $this->db->where_in( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS', [ 'ready', 'collected' ]);
            }

            // $this->db->or_where( '( SELECT ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_meta.VALUE FROM ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_meta
            //     WHERE ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_meta.REF_ORDER_ID = ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes.ID
            //     AND ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes.DATE_CREATION >= "' . Carbon::parse( date_now() )->startOfDay()->toDateTimeString() . '"
            //     AND ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes.DATE_CREATION <= "' . Carbon::parse( date_now() )->endOfDay()->toDateTimeString() . '"
            //     AND ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_meta.KEY = "order_real_type"
            // ) = "takeaway"' );

            $this->db->group_by( store_prefix() . 'nexo_commandes.CODE' );

            /**
             * change the way orders are sorted
             */
            if ( store_option( 'gastro_kitchen_sort' ) === 'old_to_new' ) {
                $this->db->order_by( store_prefix() . 'nexo_commandes.DATE_CREATION', 'desc' );
            } else {
                $this->db->order_by( store_prefix() . 'nexo_commandes.DATE_CREATION', 'asc' );
            }
            
            $query      =   $this->db->get(); // ->order_by( store_prefix() . 'nexo_commandes.ID', 'desc' )
            $data       =   $query->result_array();

            if ( $data ) {
                foreach( $data as $key => $order ) {

                    // ' . store_prefix() . 'nexo_articles.PRIX_DE_VENTE_TTC as PRIX_DE_VENTE_TTC,
                    // ' . store_prefix() . 'nexo_articles.PRIX_DE_VENTE as PRIX_DE_VENTE,

                    $this->db
                    ->select('
                    ' . store_prefix() . 'nexo_commandes_produits.REF_PRODUCT_CODEBAR as CODEBAR,
                    ' . store_prefix() . 'nexo_articles.ID as ID,
                    ' . store_prefix() . 'nexo_articles.APERCU as APERCU,
                    ' . store_prefix() . 'nexo_commandes_produits.QUANTITE as QTE_ADDED,
                    ' . store_prefix() . 'nexo_commandes_produits.DISCOUNT_TYPE as DISCOUNT_TYPE,
                    ' . store_prefix() . 'nexo_commandes_produits.DISCOUNT_PERCENT as DISCOUNT_PERCENT,
                    ' . store_prefix() . 'nexo_commandes_produits.DISCOUNT_AMOUNT as DISCOUNT_AMOUNT,
                    ' . store_prefix() . 'nexo_commandes_produits.INLINE as INLINE,
                    ' . store_prefix() . 'nexo_commandes_produits.ID as COMMAND_PRODUCT_ID,
                    ' . store_prefix() . 'nexo_commandes_produits.PRIX as PRIX_DE_VENTE_TTC,
                    ' . store_prefix() . 'nexo_commandes_produits.PRIX as PRIX_DE_VENTE,
                    ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_STATUS,
                    ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_ISSUE,
                    ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_MODIFIERS,
                    ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_NOTE,
                    ' . store_prefix() . 'nexo_articles.DESIGN as DESIGN,
                    ' . store_prefix() . 'nexo_articles.STOCK_ENABLED as STOCK_ENABLED,
                    ' . store_prefix() . 'nexo_articles.SPECIAL_PRICE_START_DATE as SPECIAL_PRICE_START_DATE,
                    ' . store_prefix() . 'nexo_articles.SPECIAL_PRICE_END_DATE as SPECIAL_PRICE_END_DATE,
                    ' . store_prefix() . 'nexo_articles.SHADOW_PRICE as SHADOW_PRICE,
                    ' . store_prefix() . 'nexo_articles.PRIX_PROMOTIONEL as PRIX_PROMOTIONEL,
                    ' . store_prefix() . 'nexo_articles.QUANTITE_RESTANTE as QUANTITE_RESTANTE,
                    ' . store_prefix() . 'nexo_articles.QUANTITE_VENDU as QUANTITE_VENDU,
                    ' . store_prefix() . 'nexo_articles.REF_CATEGORIE as REF_CATEGORIE,
                    ' . store_prefix() . 'nexo_commandes_produits.NAME as NAME')
                    ->from( store_prefix() . 'nexo_commandes')
                    ->join( store_prefix() . 'nexo_commandes_produits', store_prefix() . 'nexo_commandes.CODE = ' . store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE', 'inner')
                    ->join( store_prefix() . 'nexo_articles', store_prefix() . 'nexo_articles.CODEBAR = ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_PRODUCT_REAL_BARCODE', 'left')
                    // ->join( store_prefix() . 'nexo_commandes_produits_meta', store_prefix() . 'nexo_commandes_produits.ID = ' . store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_PRODUCT', 'left' )
                    // ->group_by( store_prefix() . 'nexo_commandes_produits.REF_PRODUCT_CODEBAR' )
                    ->where( store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE', $order[ 'CODE' ]);
                    
                    // if some items has to be filtred
                    if( $filtred_item_ids ) {
                        $this->db->where_in( store_prefix() . 'nexo_commandes_produits.ID', $filtred_item_ids );
                    }

                    $sub_query        =    $this->db->get();

                    $data[ $key ][ 'items' ]    =   $sub_query->result_array();

                    /**
                     * retreive order metas
                     */
                    $metas      =   $this->db->where( 'REF_ORDER_ID', $order[ 'ORDER_ID' ])
                        ->get( store_prefix() . 'nexo_commandes_meta' )
                        ->result_array();
                    
                    /**
                     * if the meta is set
                     */
                    $data[ $key ][ 'metas' ]    =   [];
                    if ( $metas ) {
                        foreach( $metas as $meta ) {
                            $data[ $key ][ 'metas' ][ $meta[ 'KEY' ] ]  =   $meta[ 'VALUE' ];
                        }
                    }
                    
                    /**
                     * Fill food state
                     */
                    foreach( $data[ $key ][ 'items' ] as &$sub_q ) {
                        $foodStatus  =   $this->db->where( 'REF_COMMAND_CODE', $order[ 'CODE' ] )
                            ->where( 'REF_COMMAND_PRODUCT', $sub_q[ 'COMMAND_PRODUCT_ID' ] )
                            ->get( store_prefix() . 'nexo_commandes_produits_meta' )
                            ->result_array();

                        foreach( $foodStatus as $foodState ) {
                            if ( $foodState[ 'KEY' ] == 'restaurant_note' ) {
                                $sub_q[ 'FOOD_NOTE' ]   =   $foodState[ 'VALUE' ];
                            }
                            if ( $foodState[ 'KEY' ] == 'restaurant_food_status' ) {
                                $sub_q[ 'FOOD_STATUS' ]   =   $foodState[ 'VALUE' ];
                            }
                            if ( $foodState[ 'KEY' ] == 'restaurant_food_issue' ) {
                                $sub_q[ 'FOOD_ISSUE' ]   =   $foodState[ 'VALUE' ];
                            }
                            if ( $foodState[ 'KEY' ] == 'modifiers' ) {
                                $sub_q[ 'MODIFIERS' ]   =   $foodState[ 'VALUE' ];
                            }
                            if ( $foodState[ 'KEY' ] == 'meal' ) {
                                $sub_q[ 'MEAL' ]   =   $foodState[ 'VALUE' ];
                            }
                        }
                    }
                }

                return response()->json( $data );
            }
        }
    
        return $this->__empty();
    }  

    public function getOrders()
    {   

    //      echo "hi";
    //	die();
        $startofday = new DateTime();
        $startofday->setTime(0,0);
        $startDate =$startofday->format('Y-m-d H:i:s');

        // echo date('Y-m-d H:i:s',$today);

        // echo "<br>";
        $endOfDay = new DateTime();
        $endOfDay->setTime(23,59);
        $endDate = $endOfDay->format('Y-m-d H:i:s');



         $current_datetime = date('Y-m-d H:i:s');




        $data_hold = $this->db->select('*')->from(store_prefix() . 'nexo_commandes')->where( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS!=','hold')->where_in( 'RESTAURANT_ORDER_TYPE', [ 'takeaway', 'delivery','dinein'])->get()->result_array();

        $takeaway_delivery_dinein_productorder_temp = array();

        foreach ($data_hold as $new) {
            
            array_push($takeaway_delivery_dinein_productorder_temp,$new['CODE']);

        }

        $takeaway_delivery_dinein_productorder_temp = array_unique($takeaway_delivery_dinein_productorder_temp);
        




       //$datavalue['RESTAURANT_ORDER_STATUS']!='hold'

        //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
        // $this->db->where( store_prefix() . 'nexo_commandes_produits.created_on >=', $startDate )->where( store_prefix() . 'nexo_commandes_produits.created_on <=', $endDate );

        // $this->db->where( store_prefix() . 'nexo_commandes_produits.expiry_item_time <=', $current_datetime )->where( store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_STATUS =', 'not_ready' )->where_not_in('REF_COMMAND_CODE',$takeaway_delivery_dinein_productorder_temp)->update( store_prefix() . 'nexo_commandes_produits', [
        //     'RESTAURANT_FOOD_STATUS'      =>  'ready'
        // ]);
        
        $this->db->where( store_prefix() . 'nexo_commandes_produits.created_on >=', $startDate )->where( store_prefix() . 'nexo_commandes_produits.created_on <=', $endDate );

        $this->db->where( store_prefix() . 'nexo_commandes_produits.expiry_item_time <=', $current_datetime )->where( store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_STATUS =', 'not_ready' )->where_in( store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE',$takeaway_delivery_dinein_productorder_temp )->update( store_prefix() . 'nexo_commandes_produits', [
            'RESTAURANT_FOOD_STATUS'      =>  'ready'
        ]);




       

        // //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
        // $this->db->where( store_prefix() . 'nexo_commandes_produits.created_on >=', $startDate )->where( store_prefix() . 'nexo_commandes_produits.created_on <=', $endDate );

        // $this->db->where( store_prefix() . 'nexo_commandes_produits.expiry_item_time <=', $current_datetime )->where( store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_STATUS =', 'not_ready' )->update( store_prefix() . 'nexo_commandes_produits', [
        //     'RESTAURANT_FOOD_STATUS'      =>  'ready'
        // ]);




        $this->db->where( store_prefix() . 'nexo_commandes_produits.created_on >=', $startDate )->where( store_prefix() . 'nexo_commandes_produits.created_on <=', $endDate );

        $this->db->where( store_prefix() . 'nexo_commandes_produits.close_time <=', $current_datetime )->where( store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_STATUS =', 'in_preparation' )->where( store_prefix() . 'nexo_commandes_produits.REST_ORDER_TYPE =', 'takeaway' )->update( store_prefix() . 'nexo_commandes_produits', [
            'RESTAURANT_FOOD_STATUS'      =>  'ready'
        ]);




         $this->db->where( store_prefix() . 'nexo_commandes_produits.created_on >=', $startDate )->where( store_prefix() . 'nexo_commandes_produits.created_on <=', $endDate );

        $this->db->where( store_prefix() . 'nexo_commandes_produits.close_time <=', $current_datetime )->where( store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_STATUS =', 'in_preparation' )->where( store_prefix() . 'nexo_commandes_produits.REST_ORDER_TYPE =', 'delivery' )->update( store_prefix() . 'nexo_commandes_produits', [
            'RESTAURANT_FOOD_STATUS'      =>  'ready'
        ]);








        $data = $this->db->select('*')->from(store_prefix() . 'nexo_commandes_produits')->where( store_prefix() . 'nexo_commandes_produits.created_on >=', $startDate )->where( store_prefix() . 'nexo_commandes_produits.created_on <=', $endDate )->where( store_prefix() . 'nexo_commandes_produits.close_time <=', $current_datetime )->where_in( store_prefix() . 'nexo_commandes_produits.REST_ORDER_TYPE',['delivery','takeaway'] )->get()->result_array();

        $takeaway_delivery_productorder_temp = array();

        foreach ($data as $new) {
            
            array_push($takeaway_delivery_productorder_temp,$new['REF_COMMAND_CODE']);

        }

        $takeaway_delivery_productorder = array_unique($takeaway_delivery_productorder_temp); 



       
       if(!empty($takeaway_delivery_productorder)){

            $this->db->where_in('CODE', $takeaway_delivery_productorder)->update( store_prefix() . 'nexo_commandes', [
                'RESTAURANT_ORDER_STATUS'      =>  'ready'
            ]);
      


       }


      $data = $this->db->select('*')->from(store_prefix() . 'nexo_commandes_produits')->where( store_prefix() . 'nexo_commandes_produits.created_on >=', $startDate )->where( store_prefix() . 'nexo_commandes_produits.created_on <=', $endDate )->where( store_prefix() . 'nexo_commandes_produits.expiry_item_time <=', $current_datetime )->where( 'RESTAURANT_FOOD_STATUS','ready')->where_in('REST_ORDER_TYPE',['dinein','hold'] )->get()->result_array();

        $dinein_productorder_temp = array();

        foreach ($data as $new) {


            $data_temp = $this->db->select('*')->from(store_prefix() . 'nexo_commandes_produits')->where( store_prefix() . 'nexo_commandes_produits.created_on >=', $startDate )->where( store_prefix() . 'nexo_commandes_produits.created_on <=', $endDate )->where( store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE', $new['REF_COMMAND_CODE'])->where( 'RESTAURANT_FOOD_STATUS','not_ready')->get()->result_array();


            if(empty($data_temp)){


                  $this->db->where('CODE', $new['REF_COMMAND_CODE'])->update( store_prefix() . 'nexo_commandes', [
                                                        'RESTAURANT_ORDER_STATUS'      =>  'ready'
                                         ]);


             }

        }


        // echo "<pre>";
        // print_r($_GET);
        // die();

     // echo $startDate;
     //    echo "<br>";
     //    echo $endDate;
     //    die();
    



        // get order id where items belongs to a specific category
        $kitchen        =   $this->db->where( 'ID', @$_GET[ 'current_kitchen' ] )
            ->get( store_prefix() . 'nexo_restaurant_kitchens' )
            ->result_array();


        $takeaway_kitchen        =   $this->db->where( 'ID', @$_GET[ 'takeaway_kitchen' ] )
            ->get( store_prefix() . 'nexo_restaurant_kitchens' )
            ->result_array();

         $delivery_kitchen        =   $this->db->where( 'ID', @$_GET[ 'delivery_kitchen' ] )
            ->get( store_prefix() . 'nexo_restaurant_kitchens' )
            ->result_array();


       




        // check if kitchen listen to specific categories
        $categories             =   @$kitchen[0][ 'REF_CATEGORY' ];
        $categories_ids         =   [];
      // $startDate              =   Carbon::parse( date_now() )->startOfDay()->toDateTimeString();
       // $endDate                =   Carbon::parse( date_now() )->endOfDay()->toDateTimeString();

        if( ! empty( $categories ) ) {
            $categories_ids         =   explode( ',', $categories );
        }

        $this->db
            ->select( 
                store_prefix() . 'nexo_commandes.*,'
                // . store_prefix() . 'nexo_commandes_produits.RESTAURANT_PRODUCT_REAL_BARCODE,'
                // . store_prefix() . 'nexo_articles.REF_CATEGORIE'
            )
            ->from( store_prefix() . 'nexo_commandes' )
            ->join( 
                store_prefix() . 'nexo_commandes_produits', 
                store_prefix() . 'nexo_commandes.CODE = ' . store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE' 
            )
            ->join( store_prefix() . 'nexo_articles', store_prefix() . 'nexo_commandes_produits.RESTAURANT_PRODUCT_REAL_BARCODE = ' . store_prefix() . 'nexo_articles.CODEBAR' );


         $this->db->where( store_prefix() . 'nexo_commandes.DATE_CREATION >=', $startDate )
        ->where( store_prefix() . 'nexo_commandes.DATE_CREATION <=', $endDate );

        /**
         * Define orders which are allowed to 
         * appear at the kitchen
         */
        switch( store_option( 'order_status_shown', 'all' ) ) {
            case 'all': $allowed_orders = [ 'nexo_order_devis', 'nexo_order_comptant' ]; break;
            case 'from_unpaid': $allowed_orders = [ 'nexo_order_devis', 'nexo_order_comptant' ]; break;
            case 'paid': $allowed_orders = [ 'nexo_order_comptant' ]; break;
            default: $allowed_orders = [ 'nexo_order_devis', 'nexo_order_comptant' ]; break;
        }

        $orders         =   $this->events->apply_filters( 'gastro_kitchen_orders', $allowed_orders );

        if( $orders ) {
            $this->db->where_in( store_prefix() . 'nexo_commandes.TYPE', $orders );
        }

        if ( ! empty( $categories_ids ) ) {
            $this->db->where_in( store_prefix() . 'nexo_articles.REF_CATEGORIE', $categories_ids );
        }

        /**
         * No more filtering for the end of the day since
         * we would like to display booked order as well
         */

        if( isset( $_GET[ 'from-kitchen' ] ) && $_GET[ 'from-kitchen' ] === 'true' ) {
            $this->db->where_in( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS', [ 'pending', 'ongoing', 'partially', 'booking' ]);

            
        } else {
            $this->db->where( store_prefix() . 'nexo_commandes.TYPE !=', 'nexo_order_comptant' );
            $this->db->where_in( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS', [ 'ready', 'collected' ]);
        }
          $order_type_take = array('takeaway');

         // $data = array('dinein');

          $order_type_delivery= array('delivery');

          $order_type_delivery_dinein= array('delivery','dinein');

          $order_type_takeaway_dinein= array('takeaway','dinein');

          $order_type_takeaway_delivery_dinein= array('takeaway','dinein','delivery');

          $data = '';

       //   $data = array('dinein');

         if($kitchen[0][ 'ID' ]==$_GET[ 'takeaway_kitchen' ]){

            //$takeaway_order = 'yes';
           $data = $order_type_takeaway_dinein;
            //$this->db ->where( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_TYPE!= ', 'takeaway' );

            // $this->db->where( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_TYPE ', 'dinein' );

        }  
        // else {

        //     $this->db->where( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_TYPE ','dinein');

        // }
         if($kitchen[0][ 'ID' ]== $_GET[ 'delivery_kitchen' ]){

           //  $this->db->where_in( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_TYPE ',$order_type_delivery_dinein);
              $data = $order_type_delivery_dinein;
        }

        if($_GET[ 'delivery_kitchen' ]==$kitchen[0][ 'ID' ]&&$_GET[ 'takeaway_kitchen' ]==$kitchen[0][ 'ID' ]){

             $data = $order_type_takeaway_delivery_dinein;

        }

       // print_r($data);
       // die();

        if($data!=''){
         $this->db->where_in( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_TYPE ',$data);
        }



        $this->db->group_by([
            store_prefix() . 'nexo_commandes.CODE' ,
            // store_prefix() . 'nexo_commandes_produits.RESTAURANT_PRODUCT_REAL_BARCODE' ,
            // store_prefix() . 'nexo_commandes_produits.ID' ,
        ]);

        /**
         * change the way orders are sorted
         */
        if ( store_option( 'gastro_kitchen_sort' ) === 'old_to_new' ) {
            $this->db->order_by( store_prefix() . 'nexo_commandes.DATE_CREATION', 'desc' );
        } else {
            $this->db->order_by( store_prefix() . 'nexo_commandes.DATE_CREATION', 'asc' );
        }

        $orders     =   $this->db->get()->result_array();

        // $orders = shuffle($orders);

        // echo "<pre>";
        // print_r($orders);
        // die();
        
        if ( $orders ) {
            foreach( $orders as &$order ) {
                $this->db
                    ->select('
                    ' . store_prefix() . 'nexo_commandes_produits.REF_PRODUCT_CODEBAR as CODEBAR,
                    ' . store_prefix() . 'nexo_commandes_produits.QUANTITE as QTE_ADDED,
                    ' . store_prefix() . 'nexo_commandes_produits.DISCOUNT_TYPE as DISCOUNT_TYPE,
                    ' . store_prefix() . 'nexo_commandes_produits.DISCOUNT_PERCENT as DISCOUNT_PERCENT,
                    ' . store_prefix() . 'nexo_commandes_produits.DISCOUNT_AMOUNT as DISCOUNT_AMOUNT,
                    ' . store_prefix() . 'nexo_commandes_produits.INLINE as INLINE,
                    ' . store_prefix() . 'nexo_commandes_produits.ID as COMMAND_PRODUCT_ID,
                    ' . store_prefix() . 'nexo_commandes_produits.PRIX as PRIX_DE_VENTE_TTC,
                    ' . store_prefix() . 'nexo_commandes_produits.PRIX as PRIX_DE_VENTE,
                    ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_STATUS,
                    ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_ISSUE,
                    ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_MODIFIERS,
                    ' . store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_NOTE,

                     ' . store_prefix() . 'nexo_commandes_produits.created_on,
                    ' . store_prefix() . 'nexo_commandes_produits.close_time as close_time,
                    ' . store_prefix() . 'nexo_commandes_produits.NAME as NAME')
                    ->from( store_prefix() . 'nexo_commandes_produits')
                    ->where( store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE', $order[ 'CODE' ]);

                // if some items has to be filtred
                if( $categories_ids ) {
                    $this->db->where_in( store_prefix() . 'nexo_commandes_produits.RESTAURANT_PRODUCT_CATEGORY', $categories_ids );
                }


                $currtime = date("Y-m-d H:i:s");

                $order[ 'items' ]   =   $this->db->get()->result_array();

                foreach ($order['items'] as $itemkey => $itemvalue) {

                   // $time = date($itemvalue['created_on'], time() + 30);


                    $time_temp= strtotime($itemvalue['created_on']);
                    $time_new = date("Y-m-d H:i:s",$time_temp + 20);

                    // if($currtime<=$time_new){

                    $restricted_status = array("not_ready", "in_preparation","canceled");

                        if(in_array($itemvalue['RESTAURANT_FOOD_STATUS'],$restricted_status)) {



                          $order_time_temp= strtotime($order['DATE_CREATION']);

                          $ordertime_new = date("Y-m-d H:i:s",$order_time_temp + 5);

                          $itemtime_temp= strtotime($itemvalue['created_on']);

                          $itemtime_tempnew= date("Y-m-d H:i:s",$itemtime_temp);





                        if($ordertime_new<=$itemtime_tempnew) {

                             $order['items'][$itemkey]['isnew'] = 'yes'; 

                        } else {


                             $order['items'][$itemkey]['isnew'] = 'no'; 


                        }



                      //  $order['items'][$itemkey]['isnew'] = 'yes'; 



                    } else {
                        $order['items'][$itemkey]['isnew'] = 'no'; 
                    }


                }

            }
        }


        return $this->response($orders );
    }

    /**
     *  Start Cook
     *  @param
     *  @return
    **/

   public function array_random($array, $amount = 1)
{
    $keys = array_rand($array, $amount);

    if ($amount == 1) {
        return $array[$keys];
    }

    $results = [];
    foreach ($keys as $key) {
        $results[] = $array[$key];
    }

    return $results;
}



    public function shuffle_assoc(&$array) {
        $keys = array_keys($array);

        shuffle($keys);

        foreach($keys as $key) {
            $new[$key] = $array[$key];
        }

        $array = $new;

        return $array;
    } 

    public function start_cooking_post()
    {
        $this->db->where( 'CODE', $this->post( 'order_code' ) )
        ->update( store_prefix() . 'nexo_commandes', [
            'TYPE'      =>  'nexo_order_dinein_ongoing'
        ]);

        /**
         * extra attention
         * required since, the post value is not always 
         * an array provided.
         */
        foreach( $this->post( 'during_cooking' ) as $item_id ) {
            $this->db
            ->where( 'REF_COMMAND_PRODUCT', $item_id )
            ->where( 'KEY', 'restaurant_food_status' )
            ->update( store_prefix() . 'nexo_commandes_produits_meta', [
                'VALUE'   =>    'in_preparation'
            ]);
        }

        $this->events->do_action( 'gastro_start_cooking', [
            'order_code'    =>  $this->post( 'order_code' ),
            'items'         =>  $this->post( 'during_cooking' )
        ]);
    }

    /**
     *  Are Ready, change food state
     *  @param void
     *  @return json
    **/

    public function food_state_post()
    {
        $this->load->module_model( 'gastro', 'Gastro_Orders_Model', 'order_model' );

        $current_datetime_for_cook = date('Y-m-d H:i:s');

        if($this->put('state')=='in_preparation'){


           $this->db->select('*')->from(store_prefix() . 'nexo_commandes_produits')->join( store_prefix().'nexo_commandes', store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE = '.store_prefix() . 'nexo_commandes.CODE');

            $this->db->where( store_prefix() . 'nexo_commandes_produits.expiry_item_time <=', $current_datetime_for_cook );
            $this->db->where( store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE =',  $this->post( 'order_code' ));

            $order  =   $this->db->get()->result_array();


            // print_r($order);
            // die();


            if(!empty($order)){


                // foreach ($ as $key => $value) {
                //     # code...
                // }

              $this->db
                ->where( 'REF_COMMAND_CODE', $this->post( 'order_code' ) )
               // ->where( 'KEY', 'restaurant_food_status' )
                ->update( store_prefix() . 'nexo_commandes_produits', [
                    'RESTAURANT_FOOD_STATUS'   =>    'in_preparation'
                ]);

                /*$this->order_model->changeMealsStatus( 
                    $this->post( 'selected_foods' ),
                    $this->post( 'state' ),
                    $this->post( 'all_foods' ),
                    $this->post( 'order_code' )
                );*/
                return $this->__success();
            }

         }

        return $this->__success();
    }

    /**
     * Print To Kitchen
    **/

    public function print_to_kitchen_get( $order_id, $npsPrint = false, $base64 = false )
    {
        $this->load->library( 'Curl' );
        $this->load->model( 'options' );
        $this->load->model( 'Nexo_Checkout' );
        $this->load->module_config( 'nexo' );
        $this->load->module_model( 'gastro', 'Nexo_Gastro_Tables_Models', 'gastro_model' );
        $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'orders_model' );
        $this->load->module_model( 'gastro', 'Gastro_Orders_Model', 'gastro_orders_model' );
        $this->load->module_model( 'nexo', 'NexoCustomersModel', 'customer_model' );
        // get Printer id associate to that printer
        $Options        =   $this->options->get();
        
        // Get kitchen id
        $order          =   $this->Nexo_Checkout->get_order_with_metas( $order_id );
        $customer       =   $this->customer_model->getSingle( $order[0][ 'REF_CLIENT' ] );
        $table          =   $this->gastro_model->get_table_used( $order_id );

        // get author 
        $author         =   $this->db->where( 'id', $order[0][ 'AUTHOR' ] )
        ->get( 'aauth_users' )
        ->result_array();

        $order[0][ 'AUTHOR_NAME' ]   =  $author[0][ 'name' ];

        /**
         * detect the gateway used for printing
         * @var boolean
         */
        $npsPrint   =   store_option( 'gastro_print_gateway', 'nps' ) === 'nps';

        if( store_option( 'disable_kitchens', 'yes' ) == 'yes' ) {
            $printer_id     =   store_option( 'printer_takeway' );
        } else {
            if( ! empty( $order[0][ 'METAS' ][ 'room_id' ] ) ) {
                // get Kitchen linked to that room
                $kitchen        =   $this->get_kitchen( $order[0][ 'METAS' ][ 'room_id' ], 'REF_ROOM' );
                $printer_id     =   store_option( 'printer_kitchen_' . $kitchen[0][ 'ID' ] );
            } else {
                $printer_id     =   store_option( 'printer_takeway' );
            }
        }

        $document       =   json_encode( $order );

        if( ( $printer_id != null && ! in_array( $order[0][ 'RESTAURANT_ORDER_STATUS' ], [ 'ready', 'collected' ] ) || store_option( 'disable_kitchen_screen', 'no' ) === 'yes' || @$_GET[ 'caching' ] == 'false' ) ) {
            $orderDetails   =   [
                'order'     =>  $order[0],
                'items'     =>  collect( $this->orders_model->getOrderItems( $order[0][ 'ID' ] ) )->map( function( $item ) {
                    $this->gastro_orders_model->setAsPrinted( $item[ 'ITEM_ID' ] );
                    return $item;
                })->toArray(),
                'table'     =>  $table,
                'customer'  =>  $customer
            ];

            if ( $npsPrint ) {
                $viewPath   =   $base64 ? 'gastro/views/print/base64-single' : 'gastro/views/print/nps-single-v2';
                //return $this->load->view( '../modules/' . $this->events->apply_filters( 'gastro_filter_receipt_path', $viewPath ), $orderDetails, true );

                print_r($this->load->view( '../modules/' . $this->events->apply_filters( 'gastro_filter_receipt_path', $viewPath ), $orderDetails, true ));
                die();
            } else {
                $data               =   $this->curl->post( module_config( 'nexo', 'nexo.store_url' ) . '/api/gcp/submit-print-job/' . $printer_id . '?app_code=' . @$_GET[ 'app_code' ], [
                    'content'       =>  $this->load->module_view( 'gastro', 'print.kitchen-receipt', $orderDetails, true ),
                    'title'         =>  $order[0][ 'TITRE' ]
                ]);
    
                return $data;
            }
        }
        return $this->__failed();
    }

    /**
     * Split print
     * @param int order id
     * @return void
    **/

    public function split_print_get( $order_id,$kitchen_id='', $npsPrint = false, $base64 = false ) 
    {
        //$kitchen_id = substr($kitchen_id, 0, strlen($kitchen_id)-1);
        //echo $kitchen_id;
       // die();
        $this->load->library( 'Curl' );
        $this->load->model( 'options' );
        $this->load->model( 'Nexo_Checkout' );
        $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'orders_model' );
        $this->load->module_model( 'gastro', 'Gastro_Orders_Model', 'gastro_order_model' );
        $this->load->module_config( 'nexo' );
        $this->load->module_model( 'nexo', 'NexoCustomersModel', 'customer_model' );
        
        // let's make sure those items has not yet been printed
        $this->cache        =   new CI_Cache(array( 'adapter' => 'file', 'backup' => 'file', 'key_prefix'    =>    'gastro_print_status_' . store_prefix() ));
        // get Printer id associate to that printer
        $Options        =   $this->options->get();
        $kitchens       =   $this->db->where('ID',$kitchen_id)->get( store_prefix() . 'nexo_restaurant_kitchens' )->result_array();

        $errors         =   [];
        // Get kitchen id
        $order          =   $this->Nexo_Checkout->get_order_with_metas( $order_id );




        /**
         * For Nexo Print Server
         */
        $receipts       =   [];

        if( $kitchens ) {
            foreach( $kitchens as $kitchen ) {
                $printer_id             =   store_option( 'printer_kitchen_' . $kitchen[ 'ID' ], false );


                       
                // print_r($kitchen);
                // die();

                // if printer is not set, then break it
                if( ! $printer_id ) {
                    break;
                }

                // check if kitchen listen to specific categories
                $categories             =   $kitchen[ 'REF_CATEGORY' ];
                $categories_ids         =   [];

                if( ! empty( $categories ) ) {
                    $categories_ids         =   explode( ',', $categories );
                }

                 // echo "<pre>";
                 //        print_r($categories_ids);
                 //        die();

                if( ! empty( $categories_ids ) ) {
                    $orders         =   $this->db
                    ->select( '*,
                    aauth_users.name  as AUTHOR_NAME,
                    ' . store_prefix() . 'nexo_commandes.TYPE as TYPE,
                    ' . store_prefix() . 'nexo_commandes.DATE_CREATION as DATE_CREATION,
                    ' . store_prefix() . 'nexo_commandes.ID as ORDER_ID,
                    ' . store_prefix() . 'nexo_commandes_produits.ID as ITEM_ID,
                    ' . store_prefix() . 'nexo_commandes_produits.REF_PRODUCT_CODEBAR' )
                    ->from( store_prefix() . 'nexo_commandes' )
                    ->join( 
                        store_prefix() . 'nexo_commandes_produits', 
                        store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE = ' . 
                        store_prefix() . 'nexo_commandes.CODE' 
                    )
                    ->join( 
                        store_prefix() . 'nexo_articles', 
                        store_prefix() . 'nexo_articles.CODEBAR = ' . 
                        store_prefix() . 'nexo_commandes_produits.RESTAURANT_PRODUCT_REAL_BARCODE' 
                    )
                    ->join( 
                        'aauth_users',
                        'aauth_users.id = '. 
                        store_prefix() . 'nexo_commandes.AUTHOR' 
                    )
                   // ->where_not_in( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS', [ 'ready', 'collected' ] )
                    ->where( store_prefix() . 'nexo_commandes.ID', $order_id )
                    ->where_in( 'REF_CATEGORIE', $categories_ids )
                    ->get()
                    ->result_array(); 

                    $order_unprinted_items            =   $this->gastro_order_model->getUnprintedOrderItems( $order_id );

                    
                 
                    //echo "<pre>";
                   
                    // keep order ids

                    // basically that order should be printed
                    if( $orders ) {

                        $items_to_print             =   [];

                        foreach( $orders as $order ) {
                            if( 
                                ( $order[ 'RESTAURANT_ORDER_TYPE' ] == 'dinein' && $order[ 'TYPE' ] == 'nexo_order_comptant' && $order[ 'RESTAURANT_ORDER_STATUS' ] == 'ready' )
                            ) {
                                // if the order restaurant type is "dine in" and the order has been paid. 
                                // Then we can't allow printing
                                $errors[]   =   [
                                    'status'    =>  'failed',
                                    'message'   =>  sprintf( __( 'cant print dinein ready paid order %s', 'gastro' ), $order[ 'CODE' ] )
                                ];
                                log_message( 'error', sprintf( __( 'cant print dinein ready paid order %s', 'gastro' ), $order[ 'CODE' ] ) );
                                break;
                            }

                            // if looped item match was has yet been printed, then just remove it from
                            // the copy of printed items
                            // $isUnprinted    =   collect( $order_unprinted_items )->filter( function( $item ) use ( $order ) {
                            //     return $item[ 'ID' ] === $order[ 'ITEM_ID' ];
                            // })->count() > 0;
                            
                            //if( $isUnprinted ) {
                                // We assume that item has'nt yet been printed
                                $items_to_print[]       =      $order[ 'ITEM_ID' ]; 
                            //}
                        }
                        // print_r(__LINE__);

                        // print_r($items_to_print);
                        // die();

                        // if there is at least something to print
                        if( $items_to_print ) {
                            
                            foreach( $items_to_print as $order_product_id ) {
                              //  $this->gastro_order_model->setAsPrinted( $order_product_id );
                            }

                            $table              =   $this->db->select( '*' )
                                ->from( store_prefix() . 'nexo_restaurant_tables_relation_orders' )
                                ->join( store_prefix() . 'nexo_restaurant_tables', store_prefix() . 'nexo_restaurant_tables.ID = ' . store_prefix() . 'nexo_restaurant_tables_relation_orders.REF_TABLE' )
                                ->where( store_prefix() . 'nexo_restaurant_tables_relation_orders.REF_ORDER', $order_id )
                                ->get()->result_array();
                            
                            if ( $npsPrint ) {
                              //  print_r(__LINE__);
                                $orderDetails   =   [
                                    'order'     =>  $orders[0],
                                    'items'     =>  $this->orders_model->pickOrderItems( $orders[0][ 'ORDER_ID' ], $items_to_print, 'ID' ),
                                    'table'     =>  $table,
                                    'customer'  =>  $this->customer_model->getSingle( $orders[0][ 'REF_CLIENT' ] ),
                                    'kitchen'   =>  $kitchen
                                ];

                                $viewPath       =   $base64 ? 'gastro/views/print/base64-single' : 'gastro/views/print/nps-single';

                                $receipts[]        =   [
                                    'content'       =>  $this->load->view( '../modules/' . $this->events->apply_filters( 'gastro_filter_receipt_path', $viewPath ), $orderDetails, true ),
                                    'printer'       =>  $printer_id
                                ];

                                 // $crud = new grocery_CRUD();
                                 // $crud->set_theme('bootstrap');
                                 // $crud->set_subject(__('Articles', 'nexo'));

                                return $this->load->view( '../modules/' . $this->events->apply_filters( 'gastro_filter_receipt_path', $viewPath ), $orderDetails, true );
                                die();

                 
                                // print_r($printer_id);

                            } else {
                                //print_r(__LINE__);
                                $data               =   $this->curl->post( module_config( 'nexo', 'nexo.store_url' ) . '/api/gcp/submit-print-job/' . $printer_id . '?app_code=' . @$_GET[ 'app_code' ], [
                                    'content'       =>  $this->load->module_view( 'gastro', 'print.kitchen-receipt', [
                                        'order'     =>  $orders[0],
                                        'table'     =>  $table,
                                        'kitchen'   =>  $kitchen,
                                        'Options'   =>  $Options,
                                        'items'     =>  $this->get_order_items( $order[ 'CODE' ], $items_to_print ) // get order code from last entry on $orders loop
                                    ], true ),
                                    'title'         =>  $order[ 'TITRE' ]
                                ]);
                                $errors[]       =   [
                                    'status'     => 'success',
                                    'message'   =>  sprintf( __( '%s item(s) has been printed', 'gastro' ), count( $items_to_print ) ),
                                    'response'  =>  json_decode( $data, true )
                                ];
    
                                log_message( 'debug', sprintf( __( '%s item(s) has been printed', 'gastro' ), count( $items_to_print ) ) );
                            }
                            
                        } else {
                            //print_r(__LINE__);
                            $errors[]   =   [
                                'status'    =>  'failed',
                                'message'   =>  __( 'No new item to print', 'gastro' )
                            ];

                            log_message( 'debug', __( 'No new item to print', 'gastro' ) );
                        }
                    }
                }
            }

            /**
             * If printers is provided. Then we're trying to print
             * from Nexo Print Server
             * there might be an error if the category is not handled by a kitchen. 
             */

            if ( count( $receipts ) > 0 ) {
                return $this->response([
                    'status'    =>  'success',
                    'message'   =>  __( 'The receipts are ready.', 'gastro' ),
                    'receipts'  =>  $receipts
                ]);
            }

            return $errors ? $this->response( $errors ) : $this->response([
                'status'    =>  'failed',
                'message'   =>  __( 'The items sold aren\'t handled by any existing kitchen, or no printer is assigned to the kitchen.', 'gastro' )
            ], 403 );
        }
        return $this->__failed();
    }

    /**
     *  Get Kitchen
     *  @param int kitchen id
     *  @return array
    **/

    private function get_kitchen( $id = null, $filter = 'ID' )
    {
        if( $id != null && $filter == 'ID' ) {
            $this->db->where( 'ID', $id );
        } else if( $filter == 'REF_ROOM' && $id != null ) {
            $this->db->where( 'REF_ROOM', $id );
        }

        $query =    $this->db->get( store_prefix() . 'nexo_restaurant_kitchens' );
        return $query->result_array();
    }

    /** 
     * Refresh Google
    **/

    public function google_refresh_get()
    {
        $this->load->library( 'Curl' );
        $this->response( $this->curl->get( module_config( 'nexo', 'nexo.store_url' ) . '/api/google-refresh?app_code=' . $_GET[ 'app_code' ] ), 200 );
    }

    private function get_order_items( $order_code, $ids = []) 
    {
        $this->db
        ->select('
        ' . store_prefix() . 'nexo_articles.CODEBAR as CODEBAR,
        ' . store_prefix() . 'nexo_commandes_produits.QUANTITE as QTE_ADDED,
        ' . store_prefix() . 'nexo_commandes_produits.PRIX as PRIX,
        ' . store_prefix() . 'nexo_commandes_produits.PRIX_TOTAL as PRIX_TOTAL,
        ' . store_prefix() . 'nexo_commandes_produits.ID as COMMAND_PRODUCT_ID,
        ' . store_prefix() . 'nexo_articles.DESIGN as DESIGN,
        ' . store_prefix() . 'nexo_articles.REF_CATEGORIE as REF_CATEGORIE,
        ' . store_prefix() . 'nexo_commandes_produits.NAME as NAME,

        ( SELECT ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.VALUE FROM ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta
            WHERE ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_CODE = "' . $order_code . '"
            AND ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.KEY = "restaurant_note"
            AND ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_PRODUCT = COMMAND_PRODUCT_ID
        ) as FOOD_NOTE,
        ( SELECT ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.VALUE FROM ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta
            WHERE ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_CODE = "' . $order_code . '"
            AND ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.KEY = "restaurant_food_status"
            AND ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_PRODUCT = COMMAND_PRODUCT_ID
        ) as FOOD_STATUS,
        ( SELECT ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.VALUE FROM ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta
            WHERE ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_CODE = "' . $order_code . '"
            AND ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.KEY = "meal"
            AND ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_PRODUCT = COMMAND_PRODUCT_ID
        ) as MEAL,
        ( SELECT ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.VALUE FROM ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta
            WHERE ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_CODE = "' . $order_code . '"
            AND ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.KEY = "restaurant_food_issue"
            AND ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_PRODUCT = COMMAND_PRODUCT_ID
        ) as FOOD_ISSUE,
        ( SELECT ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.VALUE FROM ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta
            WHERE ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_CODE = "' . $order_code . '"
            AND ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.KEY = "restaurant_food_status"
            AND ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_PRODUCT = COMMAND_PRODUCT_ID
        ) as FOOD_STATUS,
        ( SELECT ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.VALUE FROM ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta
            WHERE ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_CODE = "' . $order_code . '"
            AND ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.KEY = "modifiers"
            AND ' . $this->db->dbprefix . store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_PRODUCT = COMMAND_PRODUCT_ID
        ) as MODIFIERS')
        ->from( store_prefix() . 'nexo_commandes')
        ->join( store_prefix() . 'nexo_commandes_produits', store_prefix() . 'nexo_commandes.CODE = ' . store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE', 'inner')
        ->join( store_prefix() . 'nexo_articles', store_prefix() . 'nexo_articles.CODEBAR = ' . store_prefix() . 'nexo_commandes_produits.REF_PRODUCT_CODEBAR', 'left')
        // ->join( store_prefix() . 'nexo_commandes_produits_meta', store_prefix() . 'nexo_commandes_produits.ID = ' . store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_PRODUCT', 'left' )
        // ->group_by( store_prefix() . 'nexo_commandes_produits_meta.REF_COMMAND_PRODUCT' )
        ->where( store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE', $order_code );
        // ->where( store_prefix() . 'nexo_commandes_produits_meta.KEY', 'restaurant_food_status' )
        // ->where( store_prefix() . 'nexo_commandes_produits_meta.VALUE', 'not_ready' ); // make sure that item which are not ready are printed

        if( ! empty( $ids ) ) {
            $this->db->where_in( store_prefix() . 'nexo_commandes_produits.ID', $ids );
        }

        $query  = $this->db->get();

        return $query->result_array();
    }

    /**
     * Get Ready Orders
     * @param 
    **/

    public function ready_orders_get()
    {


        $startofday = new DateTime();
        $startofday->setTime(0,0);
        $startDate =$startofday->format('Y-m-d H:i:s');

        // echo date('Y-m-d H:i:s',$today);

        // echo "<br>";
        $endOfDay = new DateTime();
        $endOfDay->setTime(23,59);
        $endDate = $endOfDay->format('Y-m-d H:i:s');

        // echo $startDate;
        // echo "<br>";
        // echo $endDate;
        // die();
        $kitchens       =   $this->db->where('ID',$kitchen_id)->get( store_prefix() . 'nexo_restaurant_kitchens' )->result_array();


        $this->db->select( '*,
        ' . store_prefix() . 'nexo_commandes.ID as ORDER_ID,
        ' . store_prefix() . 'nexo_restaurant_tables.NAME as TABLE_NAME,
        ' . store_prefix() . 'nexo_restaurant_areas.NAME as AREA_NAME,
        ' . store_prefix() . 'nexo_commandes_produits.NAME as NAME,
        ' . store_prefix() . 'nexo_commandes.DATE_CREATION as DATE' 
        )
        ->from( store_prefix() . 'nexo_commandes' )
        ->join( 
            store_prefix() . 'nexo_commandes_produits',
            store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE = ' . store_prefix() . 'nexo_commandes.CODE'
        )
        ->join( 
            store_prefix() . 'nexo_restaurant_tables_relation_orders',
            store_prefix() . 'nexo_restaurant_tables_relation_orders.REF_ORDER = ' . store_prefix() . 'nexo_commandes.ID'
        )
        ->join( 
            store_prefix() . 'nexo_restaurant_tables',
            store_prefix() . 'nexo_restaurant_tables.ID = ' . store_prefix() . 'nexo_restaurant_tables_relation_orders.REF_TABLE'
        )
        ->join( 
            store_prefix() . 'nexo_restaurant_areas',
            store_prefix() . 'nexo_restaurant_areas.ID = ' . store_prefix() . 'nexo_restaurant_tables.REF_AREA',
            'left'
        );
        $orders     =   $this->db
        ->where( store_prefix() . 'nexo_commandes.DATE_CREATION >', $startDate )
        ->where( store_prefix() . 'nexo_commandes.DATE_CREATION <',  $endDate)
        ->get()
        ->result_array();

        return $this->response( $orders, 200 );
    }

    /**
     * Set an order has collected
     * @param void
     * @return void
    **/
    public function order_collected_post()
    {
        $order      =   $this->db->where( 'ID', $this->post( 'order_id' ) )
        ->get( store_prefix() . 'nexo_commandes' )
        ->result_array();

        // exists
        if( ! empty( $order ) ) {
            $this->events->do_action( 'gastro_collect_order', $order[0] );
            
            $this->db->where( 'ID', $this->post( 'order_id' ) )
            ->update( store_prefix() . 'nexo_commandes', [
                'RESTAURANT_ORDER_STATUS'   =>   'collected'
            ]);
            return $this->__success();
        } else {
            return $this->__failed();
        }
    }

    /**
     * Single Print Server
     */
    public function nps_single_print_post( $order_id )
    {
        return $this->print_to_kitchen_get( $order_id, true );
    }

    /**
     * Single Print Server
     */
    public function base64_single_print_post( $order_id )
    {
        return $this->print_to_kitchen_get( $order_id, true, true );
    }

    /**
     * Single Print Server
     */
    public function nps_split_print_post( $order_id,$kitchen_id )
    {
        return $this->split_print_get( $order_id,$kitchen_id, true );
    }

    public function base64_split_print_post( $order_id )
    {
        return $this->split_print_get( $order_id,$kitchen_id, true, true ); // nps : true, base64: true
    }
    public function nps_split_print_post_print_ready( $order_id,$kitchen_id )
    {       
            //$kitchen_id = substr($kitchen_id, 0, strlen($kitchen_id)-1);
        //     echo $kitchen_id;
        //    die();
            $this->load->library( 'Curl' );
            $this->load->model( 'options' );
            $this->load->model( 'Nexo_Checkout' );
            $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'orders_model' );
            $this->load->module_model( 'gastro', 'Gastro_Orders_Model', 'gastro_order_model' );
            $this->load->module_config( 'nexo' );
            $this->load->module_model( 'nexo', 'NexoCustomersModel', 'customer_model' );
            
            // let's make sure those items has not yet been printed
            $this->cache        =   new CI_Cache(array( 'adapter' => 'file', 'backup' => 'file', 'key_prefix'    =>    'gastro_print_status_' . store_prefix() ));
            // get Printer id associate to that printer
            $Options        =   $this->options->get();
            $kitchens       =   $this->db->where('ID',$kitchen_id)->get( store_prefix() . 'nexo_restaurant_kitchens' )->result_array();
    
            $errors         =   [];
            // Get kitchen id
            $order          =   $this->Nexo_Checkout->get_order_with_metas( $order_id );
    
    
    
    
            /**
             * For Nexo Print Server
             */
            $receipts       =   [];
    
            if( $kitchens ) {
                foreach( $kitchens as $kitchen ) {
                    $printer_id             =   store_option( 'printer_kitchen_' . $kitchen[ 'ID' ], false );
    
    
                           
                    // print_r($kitchen);
                    // die();
    
                    // if printer is not set, then break it
                    if( ! $printer_id ) {
                        break;
                    }
    
                    // check if kitchen listen to specific categories
                    $categories             =   $kitchen[ 'REF_CATEGORY' ];
                    $categories_ids         =   [];
    
                    if( ! empty( $categories ) ) {
                        $categories_ids         =   explode( ',', $categories );
                    }
    
                     // echo "<pre>";
                     //        print_r($categories_ids);
                     //        die();
    
                    if( ! empty( $categories_ids ) ) {
                        $orders         =   $this->db
                        ->select( '*,
                        aauth_users.name  as AUTHOR_NAME,
                        ' . store_prefix() . 'nexo_commandes.TYPE as TYPE,
                        ' . store_prefix() . 'nexo_commandes.DATE_CREATION as DATE_CREATION,
                        ' . store_prefix() . 'nexo_commandes.ID as ORDER_ID,
                        ' . store_prefix() . 'nexo_commandes_produits.ID as ITEM_ID,
                        ' . store_prefix() . 'nexo_commandes_produits.REF_PRODUCT_CODEBAR' )
                        ->from( store_prefix() . 'nexo_commandes' )
                        ->join( 
                            store_prefix() . 'nexo_commandes_produits', 
                            store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE = ' . 
                            store_prefix() . 'nexo_commandes.CODE' 
                        )
                        ->join( 
                            store_prefix() . 'nexo_articles', 
                            store_prefix() . 'nexo_articles.CODEBAR = ' . 
                            store_prefix() . 'nexo_commandes_produits.RESTAURANT_PRODUCT_REAL_BARCODE' 
                        )
                        ->join( 
                            'aauth_users',
                            'aauth_users.id = '. 
                            store_prefix() . 'nexo_commandes.AUTHOR' 
                        )
                       // ->where_not_in( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS', [ 'ready', 'collected' ] )
                        ->where( store_prefix() . 'nexo_commandes.ID', $order_id )
                        ->where_in( 'REF_CATEGORIE', $categories_ids )
                        ->get()
                        ->result_array(); 
    
                        $order_unprinted_items            =   $this->gastro_order_model->getUnprintedOrderItems( $order_id );
    
                        
                     
                        //echo "<pre>";
                       
                        // keep order ids
    
                        // basically that order should be printed
                        if( $orders ) {
    
                            $items_to_print             =   [];
    
                            foreach( $orders as $order ) {
                                if( 
                                    ( $order[ 'RESTAURANT_ORDER_TYPE' ] == 'dinein' && $order[ 'TYPE' ] == 'nexo_order_comptant')
                                ) {
                                    // if the order restaurant type is "dine in" and the order has been paid. 
                                    // Then we can't allow printing
                                    $errors[]   =   [
                                        'status'    =>  'failed',
                                        'message'   =>  sprintf( __( 'cant print dinein ready paid order %s', 'gastro' ), $order[ 'CODE' ] )
                                    ];
                                    log_message( 'error', sprintf( __( 'cant print dinein ready paid order %s', 'gastro' ), $order[ 'CODE' ] ) );
                                    break;
                                }
    
                                // if looped item match was has yet been printed, then just remove it from
                                // the copy of printed items
                                // $isUnprinted    =   collect( $order_unprinted_items )->filter( function( $item ) use ( $order ) {
                                //     return $item[ 'ID' ] === $order[ 'ITEM_ID' ];
                                // })->count() > 0;
                                
                                //if( $isUnprinted ) {
                                    // We assume that item has'nt yet been printed
                                    $items_to_print[]       =      $order[ 'ITEM_ID' ]; 
                                //}
                            }
                            // print_r(__LINE__);
    
    
                            // if there is at least something to print
                            if( $items_to_print ) {
                                
                            // print_r($items_to_print);
                            // die();
                                
                                foreach( $items_to_print as $order_product_id ) {
                                  //  $this->gastro_order_model->setAsPrinted( $order_product_id );
                                }
    
                                $table              =   $this->db->select( '*' )
                                    ->from( store_prefix() . 'nexo_restaurant_tables_relation_orders' )
                                    ->join( store_prefix() . 'nexo_restaurant_tables', store_prefix() . 'nexo_restaurant_tables.ID = ' . store_prefix() . 'nexo_restaurant_tables_relation_orders.REF_TABLE' )
                                    ->where( store_prefix() . 'nexo_restaurant_tables_relation_orders.REF_ORDER', $order_id )
                                    ->get()->result_array();
                                
                                // if ( $npsPrint ) {
                                  //  print_r(__LINE__);
                                    $orderDetails   =   [
                                        'order'     =>  $orders[0],
                                        'items'     =>  $this->orders_model->pickOrderItems( $orders[0][ 'ORDER_ID' ], $items_to_print, 'ID' ),
                                        'table'     =>  $table,
                                        'customer'  =>  $this->customer_model->getSingle( $orders[0][ 'REF_CLIENT' ] ),
                                        'kitchen'   =>  $kitchen
                                    ];
    
                                    $viewPath       =    'gastro/views/print/nps-single_ready';
    
                                    $receipts[]        =   [
                                        'content'       =>  $this->load->view( '../modules/' . $this->events->apply_filters( 'gastro_filter_receipt_path', $viewPath ), $orderDetails, true ),
                                        'printer'       =>  $printer_id
                                    ];
    
                                     // $crud = new grocery_CRUD();
                                     // $crud->set_theme('bootstrap');
                                     // $crud->set_subject(__('Articles', 'nexo'));
    
                                    return $this->load->view( '../modules/' . $this->events->apply_filters( 'gastro_filter_receipt_path', $viewPath ), $orderDetails, true );
                                    die();
    
                     
                                    // print_r($printer_id);
    
                                // } else {
                                //     //print_r(__LINE__);
                                //     $data               =   $this->curl->post( module_config( 'nexo', 'nexo.store_url' ) . '/api/gcp/submit-print-job/' . $printer_id . '?app_code=' . @$_GET[ 'app_code' ], [
                                //         'content'       =>  $this->load->module_view( 'gastro', 'print.kitchen-receipt', [
                                //             'order'     =>  $orders[0],
                                //             'table'     =>  $table,
                                //             'kitchen'   =>  $kitchen,
                                //             'Options'   =>  $Options,
                                //             'items'     =>  $this->get_order_items( $order[ 'CODE' ], $items_to_print ) // get order code from last entry on $orders loop
                                //         ], true ),
                                //         'title'         =>  $order[ 'TITRE' ]
                                //     ]);
                                //     $errors[]       =   [
                                //         'status'     => 'success',
                                //         'message'   =>  sprintf( __( '%s item(s) has been printed', 'gastro' ), count( $items_to_print ) ),
                                //         'response'  =>  json_decode( $data, true )
                                //     ];
        
                                //     log_message( 'debug', sprintf( __( '%s item(s) has been printed', 'gastro' ), count( $items_to_print ) ) );
                                // }
                                
                            } else {
                                //print_r(__LINE__);
                                $errors[]   =   [
                                    'status'    =>  'failed',
                                    'message'   =>  __( 'No new item to print', 'gastro' )
                                ];
    
                                log_message( 'debug', __( 'No new item to print', 'gastro' ) );
                            }
                        }
                    }
                }
    
                /**
                 * If printers is provided. Then we're trying to print
                 * from Nexo Print Server
                 * there might be an error if the category is not handled by a kitchen. 
                 */
    
                if ( count( $receipts ) > 0 ) {
                    return $this->response([
                        'status'    =>  'success',
                        'message'   =>  __( 'The receipts are ready.', 'gastro' ),
                        'receipts'  =>  $receipts
                    ]);
                }
    
                return $errors ? $this->response( $errors ) : $this->response([
                    'status'    =>  'failed',
                    'message'   =>  __( 'The items sold aren\'t handled by any existing kitchen, or no printer is assigned to the kitchen.', 'gastro' )
                ], 403 );
            }
            return $this->__failed();
    }

    public function getkitchen()
    {
        
        $kitchen        =   $this->db->get( store_prefix() . 'nexo_restaurant_kitchens' )->result_array();

        foreach ($kitchen as $key=>$value) {

           // $i=0;
            $kitchen[$key]['Printer_name'] =  store_option( 'printer_kitchen_' . $value['ID'], false );
           // $i++;
           
        }
        
        return $this->response($kitchen);
    
    }
      public function kitchen_status()
    {
        
        $kitchen        =  array();

        $kitchen['takeaway_kitchen'] = store_option( 'takeaway_kitchen' );
        $kitchen['Delivery_kitchen'] = store_option( 'delivery_kitchen' );
        return $this->response($kitchen);
    
    }

    public function kitchens_products($kitchen_id,$order_id)
    {

        $this->load->library( 'Curl' );
        $this->load->model( 'options' );
        $this->load->model( 'Nexo_Checkout' );
        $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'orders_model' );
        $this->load->module_model( 'gastro', 'Gastro_Orders_Model', 'gastro_order_model' );
        $this->load->module_config( 'nexo' );
        $this->load->module_model( 'nexo', 'NexoCustomersModel', 'customer_model' );

        $kitchen       =   $this->db->where('ID',$kitchen_id)->get( store_prefix() . 'nexo_restaurant_kitchens' )->result_array();



        // print_r($kitchen);
        // die();

        $categories             =   $kitchen[0][ 'REF_CATEGORY' ];
        $categories_ids         =   [];

        if( ! empty( $categories ) ) {
            $categories_ids         =   explode( ',', $categories );
        }

        if(!empty($categories_ids)){
     // $categories_ids  = array_filter($categories_ids);
             $orders         =   $this->db
                        ->select( '*,
                        aauth_users.name  as AUTHOR_NAME,
                        ' . store_prefix() . 'nexo_commandes.TYPE as TYPE,
                        ' . store_prefix() . 'nexo_commandes.DATE_CREATION as DATE_CREATION,
                        ' . store_prefix() . 'nexo_commandes.ID as ORDER_ID,
                        ' . store_prefix() . 'nexo_commandes_produits.ID as ITEM_ID,
                        ' . store_prefix() . 'nexo_commandes_produits.REF_PRODUCT_CODEBAR' )
                        ->from( store_prefix() . 'nexo_commandes' )
                        ->join( 
                            store_prefix() . 'nexo_commandes_produits', 
                            store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE = ' . 
                            store_prefix() . 'nexo_commandes.CODE' 
                        )
                        ->join( 
                            store_prefix() . 'nexo_articles', 
                            store_prefix() . 'nexo_articles.CODEBAR = ' . 
                            store_prefix() . 'nexo_commandes_produits.RESTAURANT_PRODUCT_REAL_BARCODE' 
                        )
                        ->join( 
                            'aauth_users',
                            'aauth_users.id = '. 
                            store_prefix() . 'nexo_commandes.AUTHOR' 
                        )
                        ->where_not_in( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS', [ 'ready', 'collected' ] )
                        ->where_in( store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_PRINTED',1)
                        ->where( store_prefix() . 'nexo_commandes.ID', $order_id )
                        ->where_in( 'REF_CATEGORIE', $categories_ids )
                        ->get()
                        ->result_array();
                        
        
            if(!empty($orders)){
                echo "1";
            } else {
                echo "0";
            }

        } else {
            echo "2";
            die();
        }



        # code...
    }

    public function kitchens_products_ready($kitchen_id,$order_id)
    {

        $this->load->library( 'Curl' );
        $this->load->model( 'options' );
        $this->load->model( 'Nexo_Checkout' );
        $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'orders_model' );
        $this->load->module_model( 'gastro', 'Gastro_Orders_Model', 'gastro_order_model' );
        $this->load->module_config( 'nexo' );
        $this->load->module_model( 'nexo', 'NexoCustomersModel', 'customer_model' );

        $kitchen       =   $this->db->where('ID',$kitchen_id)->get( store_prefix() . 'nexo_restaurant_kitchens' )->result_array();



        // print_r($kitchen);
        // die();

        $categories             =   $kitchen[0][ 'REF_CATEGORY' ];
        $categories_ids         =   [];

        if( ! empty( $categories ) ) {
            $categories_ids         =   explode( ',', $categories );
        }

        if(!empty($categories_ids)){
     // $categories_ids  = array_filter($categories_ids);
             $orders         =   $this->db
                        ->select( '*,
                        aauth_users.name  as AUTHOR_NAME,
                        ' . store_prefix() . 'nexo_commandes.TYPE as TYPE,
                        ' . store_prefix() . 'nexo_commandes.DATE_CREATION as DATE_CREATION,
                        ' . store_prefix() . 'nexo_commandes.ID as ORDER_ID,
                        ' . store_prefix() . 'nexo_commandes_produits.ID as ITEM_ID,
                        ' . store_prefix() . 'nexo_commandes_produits.REF_PRODUCT_CODEBAR' )
                        ->from( store_prefix() . 'nexo_commandes' )
                        ->join( 
                            store_prefix() . 'nexo_commandes_produits', 
                            store_prefix() . 'nexo_commandes_produits.REF_COMMAND_CODE = ' . 
                            store_prefix() . 'nexo_commandes.CODE' 
                        )
                        ->join( 
                            store_prefix() . 'nexo_articles', 
                            store_prefix() . 'nexo_articles.CODEBAR = ' . 
                            store_prefix() . 'nexo_commandes_produits.RESTAURANT_PRODUCT_REAL_BARCODE' 
                        )
                        ->join( 
                            'aauth_users',
                            'aauth_users.id = '. 
                            store_prefix() . 'nexo_commandes.AUTHOR' 
                        )
                        // ->where_not_in( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS', [ 'ready', 'collected' ] )
                        ->where( store_prefix() . 'nexo_commandes.ID', $order_id )
                        ->where_in( store_prefix() . 'nexo_commandes_produits.RESTAURANT_FOOD_PRINTED',1)
                        ->where_in( 'REF_CATEGORIE', $categories_ids )
                        ->get()
                        ->result_array(); 


            if(!empty($orders)){
                echo "1";
            } else {
                echo "0";
            }

        } else {
            echo "2";
            die();
        }



        # code...
    }

    public function getprinters()
    {
        $arrayName = array();
        $printers       =   $this->db->get( store_prefix() . 'nexo_printer' )->result_array();

        if(!empty($printers)){
          //$;
          return $this->response( $printers, 200 );

        } else {
         return $this->response( $arrayName, 200 );
        }
        die();
       
    }

    public function printers()
    {
        //echo "<pre>";
        
        $postdata = file_get_contents("php://input");

        $data = explode(',', $postdata);


        $this->db->empty_table(store_prefix() . 'nexo_printer');
         //print_r($data);
         //die();

         foreach ($data as $key => $value) {
             # code...

            $this->db->insert( store_prefix() . 'nexo_printer', array(
                                 'name'                =>   $value
                             ));
         }

         echo "DATA Stored";
         die();

    }

    public function printresults($order_id)
    {
        
       //  $this->load->library('parser');
       //  $this->load->model('Nexo_Checkout');
       //  $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'orderModel' );

       //  $data                                   =   [];

       //   $data = $this->Nexo_Checkout->get_order_products($order_id, true);


        
       //  $data[ 'order' ]                        =   $this->Nexo_Checkout->get_order_products($order_id, true);
       //  $data[ 'cache' ]                        =   $this->cache;
       //  $data[ 'shipping' ]                     =   $this->db->where( 'ref_order', $order_id )->get( store_prefix() . 'nexo_commandes_shippings' )->result_array();
       //  $data[ 'tax' ]                          =   '';

       //  $data[ 'template' ]                     =   [];
       //  $orderDate                              =   new DateTime( $data[ 'order' ][ 'order' ][0][ 'DATE_CREATION' ] );
       //  $data[ 'template' ][ 'order_date' ]     =   $orderDate->format( store_option( 'nexo_datetime_format', 'Y-m-d h:i:s' ) );
       //  $orderUpdated                           =   new DateTime( $data[ 'order' ][ 'order' ][0][ 'DATE_MOD' ] );
       //  $data[ 'template' ][ 'order_updated' ]  =   $orderUpdated->format( store_option( 'nexo_datetime_format', 'Y-m-d h:i:s' ) );
        
       //  $data[ 'template' ][ 'order_code' ]     =   $data[ 'order' ][ 'order' ][0][ 'CODE' ];
       //  $data[ 'template' ][ 'order_id' ]       =   $data[ 'order' ][ 'order' ][0][ 'ORDER_ID' ];
       //  $data[ 'template' ][ 'order_status' ]   =   $this->Nexo_Checkout->get_order_type( $data[ 'order' ][ 'order' ][0][ 'TYPE' ] );
       //  $data[ 'template' ][ 'order_note' ]     =   $data[ 'order' ][ 'order' ][0][ 'DESCRIPTION' ];

       //  $data[ 'template' ][ 'order_cashier' ]  =   User::pseudo( $data[ 'order' ][ 'order' ][0][ 'AUTHOR' ] );
       //  $data[ 'template' ][ 'shop_name' ]      =   store_option( 'site_name' );
       //  $data[ 'template' ][ 'shop_pobox' ]     =   store_option( 'nexo_shop_pobox' );
       //  $data[ 'template' ][ 'shop_fax' ]       =   store_option( 'nexo_shop_fax' );
       //  $data[ 'template' ][ 'shop_email' ]     =   store_option( 'nexo_shop_email' );
       //  $data[ 'template' ][ 'shop_street' ]    =   store_option( 'nexo_shop_street' );
       //  $data[ 'template' ][ 'shop_phone' ]     =   store_option( 'nexo_shop_phone' );
       //  $data[ 'template' ][ 'customer_name' ]  =   $data[ 'order' ][ 'order' ][0][ 'customer_name' ];
       //  $data[ 'template' ][ 'customer_phone' ]  =   $data[ 'order' ][ 'order' ][0][ 'customer_phone' ];

       //  $data[ 'template' ][ 'delivery_address_1' ]     =   @$data[ 'shipping' ][0][ 'address_1' ];
       //  $data[ 'template' ][ 'delivery_address_2' ]     =   @$data[ 'shipping' ][0][ 'address_2' ];
       //  $data[ 'template' ][ 'city' ]               =   @$data[ 'shipping' ][0][ 'city' ];
       //  $data[ 'template' ][ 'country' ]            =   @$data[ 'shipping' ][0][ 'country' ];
       //  $data[ 'template' ][ 'name' ]               =   @$data[ 'shipping' ][0][ 'name' ];
       //  $data[ 'template' ][ 'surname' ]            =   @$data[ 'shipping' ][0][ 'surname' ];
       //  $data[ 'template' ][ 'state' ]              =   @$data[ 'shipping' ][0][ 'surname' ];
       //  $data[ 'template' ][ 'delivery_cost' ]      =   @$data[ 'shipping' ][0][ 'price' ];

       //  /**
       //   * Get order metas
       //   */
       //  $freshOrder     =   $data[ 'order' ][ 'order' ][0];
       //  $freshOrder[ 'metas' ]  =   $this->Nexo_Checkout->getOrderMetas( $order_id );

       //  /**
       //   * allow modification of data 
       //   * used on the receipts
       //   */
       //  $filtered   =   $this->events->apply_filters( 'nexo_filter_receipt_template', [
       //      'template'          =>      $data[ 'template' ],
       //      'order'             =>      $freshOrder,
       //      'items'             =>      $data[ 'order' ][ 'products' ],
       //      'tax'               =>      $data[ 'tax' ],
       //      'shipping'          =>      $data[ 'shipping' ]
       //  ]);

       

       //  $allowed_order_for_print        =   $this->events->apply_filters( 'allowed_order_for_print', array( 'nexo_order_comptant' ) );

       // //  print_r(json_encode($arrayName));
       // //  die();
       // // return $arrayName;

       //  return $this->load->view( 
       //      '../modules/' . $this->events->apply_filters( 'nps_receipt_path', 'nexo/views/receipts/nps/basic' ), 
       //      $filtered, 
       //      true 
       //  );
        if ($order_id != null) {
            $this->cache        =    new CI_Cache(array( 'adapter' => 'file', 'backup' => 'file', 'key_prefix'    =>    'nexo_order_' . store_prefix() ));

            if ($order_cache = $this->cache->get($order_id) && @$_GET[ 'refresh' ] != 'true') {
                echo $this->cache->get($order_id);
                return;
            }

            $this->load->library('parser');
            $this->load->model('Nexo_Checkout');
            $this->load->model('Nexo_Misc');

            global $Options;

            $data                           =   array();
            $data[ 'order' ]                =   $this->Nexo_Checkout->get_order_products($order_id, true);
            $data[ 'order' ][ 'metas' ]     =   $this->Nexo_Checkout->getOrderMetas( $order_id );
            $data[ 'cache' ]                =   $this->cache;
            $data[ 'shipping' ]             =   $this->db->where( 'ref_order', $order_id )->get( store_prefix() . 'nexo_commandes_shippings' )->result_array();
            $data[ 'tax' ]                  =   $this->Nexo_Misc->get_taxes( $data[ 'order' ][ 'order' ][0][ 'REF_TAX' ] );


            // $data[ 'total_tax' ]                  =   $this->events->apply_filters('tax', $this->config->item('default_user_names'));

          //  print_r($this->events->apply_filters('awayordertime', $this->config->item('default_user_names')));

            $allowed_order_for_print        =   $this->events->apply_filters( 'allowed_order_for_print', array( 'nexo_order_comptant' ) );
    

            // Allow only cash order to be printed
            // if ( ! in_array( $data[ 'order' ]['order'][0][ 'TYPE' ], $allowed_order_for_print ) ) {
            //     redirect(array( 'dashboard', 'nexo', 'orders', '?notice=print_disabled' ));
            // }

            if (count($data[ 'order' ]) == 0) {
                return show_error(sprintf(__('Impossible d\'afficher le ticket de caisse. Cette commande ne possède aucun article &mdash; <a href="%s">Retour en arrière</a>', 'nexo'), $_SERVER['HTTP_REFERER']));
            }

            // @since 2.7.9
            $data[ 'template' ]                     =   array();
            $dateCreation                           =   new DateTime( $data[ 'order' ][ 'order' ][0][ 'DATE_CREATION' ] );
            $data[ 'template' ][ 'order_date' ]     =   $dateCreation->format( store_option( 'nexo_datetime_format', 'Y-m-d h:i:s' ) );
            $dateModification                       =   new DateTime( $data[ 'order' ][ 'order' ][0][ 'DATE_MOD' ] );
            $data[ 'template' ][ 'order_updated' ]  =   $dateModification->format( store_option( 'nexo_datetime_format', 'Y-m-d h:i:s' ) );
            $data[ 'template' ][ 'order_code' ]     =   $data[ 'order' ][ 'order' ][0][ 'CODE' ];
            $data[ 'template' ][ 'order_id' ]       =   $data[ 'order' ][ 'order' ][0][ 'ORDER_ID' ];
            $data[ 'template' ][ 'order_status' ]   =   $this->Nexo_Checkout->get_order_type($data[ 'order' ][ 'order' ][0][ 'TYPE' ]);
            $data[ 'template' ][ 'order_note' ]     =   $data[ 'order' ][ 'order' ][0][ 'DESCRIPTION' ];

            $data[ 'template' ][ 'order_cashier' ]  =   User::pseudo( $data[ 'order' ][ 'order' ][0][ 'AUTHOR' ] );
            $data[ 'template' ][ 'shop_name' ]      =   @$Options[ store_prefix() . 'site_name' ];
            $data[ 'template' ][ 'shop_pobox' ]     =   @$Options[ store_prefix() . 'nexo_shop_pobox' ];
            $data[ 'template' ][ 'shop_fax' ]       =   @$Options[ store_prefix() . 'nexo_shop_fax' ];
            $data[ 'template' ][ 'shop_email' ]     =   @$Options[ store_prefix() . 'nexo_shop_email' ];
            $data[ 'template' ][ 'shop_street' ]    =   @$Options[ store_prefix() . 'nexo_shop_street' ];
            $data[ 'template' ][ 'shop_phone' ]     =   @$Options[ store_prefix() . 'nexo_shop_phone' ];
            $data[ 'template' ][ 'customer_name' ]  =   $data[ 'order' ][ 'order' ][0][ 'customer_name' ];
            $data[ 'template' ][ 'customer_phone' ]  =   $data[ 'order' ][ 'order' ][0][ 'customer_phone' ];

            $data[ 'template' ][ 'delivery_address_1' ]     =   @$data[ 'shipping' ][0][ 'address_1' ];
            $data[ 'template' ][ 'delivery_address_2' ]     =   @$data[ 'shipping' ][0][ 'address_2' ];
            $data[ 'template' ][ 'city' ]               =   @$data[ 'shipping' ][0][ 'city' ];
            $data[ 'template' ][ 'country' ]            =   @$data[ 'shipping' ][0][ 'country' ];
            $data[ 'template' ][ 'name' ]               =   @$data[ 'shipping' ][0][ 'name' ];
            $data[ 'template' ][ 'phone' ]              =   @$data[ 'shipping' ][0][ 'phone' ];
            $data[ 'template' ][ 'surname' ]            =   @$data[ 'shipping' ][0][ 'surname' ];
            $data[ 'template' ][ 'state' ]              =   @$data[ 'shipping' ][0][ 'surname' ];
            $data[ 'template' ][ 'delivery_cost' ]      =   @$data[ 'shipping' ][0][ 'price' ];

            $filtered   =   $this->events->apply_filters( 'nexo_filter_receipt_template', [
                'template'          =>      $data[ 'template' ],
                'order'             =>      $data[ 'order' ][ 'order' ][0],
                'items'             =>      $data[ 'order' ][ 'products' ]
            ]);

            $data[ 'template' ]             =   $filtered[ 'template' ];
            $theme                          =   @$Options[ store_prefix() . 'nexo_receipt_theme' ] ? @$Options[ store_prefix() . 'nexo_receipt_theme' ] : 'default';
            $path                           =   '../modules/nexo/views/receipts/' . $theme . '.php';

           return $this->load->view(
                $this->events->apply_filters( 'nexo_receipt_theme_path', $path ),
                $data,
                $theme
            );

        } else {
            die(__('Cette commande est introuvable.', 'nexo'));
        }
    }
    public function printresults_free($order_id)
    {
        
       //  $this->load->library('parser');
       //  $this->load->model('Nexo_Checkout');
       //  $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'orderModel' );

       //  $data                                   =   [];

       //   $data = $this->Nexo_Checkout->get_order_products($order_id, true);


        
       //  $data[ 'order' ]                        =   $this->Nexo_Checkout->get_order_products($order_id, true);
       //  $data[ 'cache' ]                        =   $this->cache;
       //  $data[ 'shipping' ]                     =   $this->db->where( 'ref_order', $order_id )->get( store_prefix() . 'nexo_commandes_shippings' )->result_array();
       //  $data[ 'tax' ]                          =   '';

       //  $data[ 'template' ]                     =   [];
       //  $orderDate                              =   new DateTime( $data[ 'order' ][ 'order' ][0][ 'DATE_CREATION' ] );
       //  $data[ 'template' ][ 'order_date' ]     =   $orderDate->format( store_option( 'nexo_datetime_format', 'Y-m-d h:i:s' ) );
       //  $orderUpdated                           =   new DateTime( $data[ 'order' ][ 'order' ][0][ 'DATE_MOD' ] );
       //  $data[ 'template' ][ 'order_updated' ]  =   $orderUpdated->format( store_option( 'nexo_datetime_format', 'Y-m-d h:i:s' ) );
        
       //  $data[ 'template' ][ 'order_code' ]     =   $data[ 'order' ][ 'order' ][0][ 'CODE' ];
       //  $data[ 'template' ][ 'order_id' ]       =   $data[ 'order' ][ 'order' ][0][ 'ORDER_ID' ];
       //  $data[ 'template' ][ 'order_status' ]   =   $this->Nexo_Checkout->get_order_type( $data[ 'order' ][ 'order' ][0][ 'TYPE' ] );
       //  $data[ 'template' ][ 'order_note' ]     =   $data[ 'order' ][ 'order' ][0][ 'DESCRIPTION' ];

       //  $data[ 'template' ][ 'order_cashier' ]  =   User::pseudo( $data[ 'order' ][ 'order' ][0][ 'AUTHOR' ] );
       //  $data[ 'template' ][ 'shop_name' ]      =   store_option( 'site_name' );
       //  $data[ 'template' ][ 'shop_pobox' ]     =   store_option( 'nexo_shop_pobox' );
       //  $data[ 'template' ][ 'shop_fax' ]       =   store_option( 'nexo_shop_fax' );
       //  $data[ 'template' ][ 'shop_email' ]     =   store_option( 'nexo_shop_email' );
       //  $data[ 'template' ][ 'shop_street' ]    =   store_option( 'nexo_shop_street' );
       //  $data[ 'template' ][ 'shop_phone' ]     =   store_option( 'nexo_shop_phone' );
       //  $data[ 'template' ][ 'customer_name' ]  =   $data[ 'order' ][ 'order' ][0][ 'customer_name' ];
       //  $data[ 'template' ][ 'customer_phone' ]  =   $data[ 'order' ][ 'order' ][0][ 'customer_phone' ];

       //  $data[ 'template' ][ 'delivery_address_1' ]     =   @$data[ 'shipping' ][0][ 'address_1' ];
       //  $data[ 'template' ][ 'delivery_address_2' ]     =   @$data[ 'shipping' ][0][ 'address_2' ];
       //  $data[ 'template' ][ 'city' ]               =   @$data[ 'shipping' ][0][ 'city' ];
       //  $data[ 'template' ][ 'country' ]            =   @$data[ 'shipping' ][0][ 'country' ];
       //  $data[ 'template' ][ 'name' ]               =   @$data[ 'shipping' ][0][ 'name' ];
       //  $data[ 'template' ][ 'surname' ]            =   @$data[ 'shipping' ][0][ 'surname' ];
       //  $data[ 'template' ][ 'state' ]              =   @$data[ 'shipping' ][0][ 'surname' ];
       //  $data[ 'template' ][ 'delivery_cost' ]      =   @$data[ 'shipping' ][0][ 'price' ];

       //  /**
       //   * Get order metas
       //   */
       //  $freshOrder     =   $data[ 'order' ][ 'order' ][0];
       //  $freshOrder[ 'metas' ]  =   $this->Nexo_Checkout->getOrderMetas( $order_id );

       //  /**
       //   * allow modification of data 
       //   * used on the receipts
       //   */
       //  $filtered   =   $this->events->apply_filters( 'nexo_filter_receipt_template', [
       //      'template'          =>      $data[ 'template' ],
       //      'order'             =>      $freshOrder,
       //      'items'             =>      $data[ 'order' ][ 'products' ],
       //      'tax'               =>      $data[ 'tax' ],
       //      'shipping'          =>      $data[ 'shipping' ]
       //  ]);

       

       //  $allowed_order_for_print        =   $this->events->apply_filters( 'allowed_order_for_print', array( 'nexo_order_comptant' ) );

       // //  print_r(json_encode($arrayName));
       // //  die();
       // // return $arrayName;

       //  return $this->load->view( 
       //      '../modules/' . $this->events->apply_filters( 'nps_receipt_path', 'nexo/views/receipts/nps/basic' ), 
       //      $filtered, 
       //      true 
       //  );
        if ($order_id != null) {
            $this->cache        =    new CI_Cache(array( 'adapter' => 'file', 'backup' => 'file', 'key_prefix'    =>    'nexo_order_' . store_prefix() ));

            if ($order_cache = $this->cache->get($order_id) && @$_GET[ 'refresh' ] != 'true') {
                echo $this->cache->get($order_id);
                return;
            }

            $this->load->library('parser');
            $this->load->model('Nexo_Checkout');
            $this->load->model('Nexo_Misc');

            global $Options;

            $data                           =   array();
            $data[ 'order' ]                =   $this->Nexo_Checkout->get_order_products($order_id, true);
            $data[ 'order' ][ 'metas' ]     =   $this->Nexo_Checkout->getOrderMetas( $order_id );
            $data[ 'cache' ]                =   $this->cache;
            $data[ 'shipping' ]             =   $this->db->where( 'ref_order', $order_id )->get( store_prefix() . 'nexo_commandes_shippings' )->result_array();
            $data[ 'tax' ]                  =   $this->Nexo_Misc->get_taxes( $data[ 'order' ][ 'order' ][0][ 'REF_TAX' ] );


            // $data[ 'total_tax' ]                  =   $this->events->apply_filters('tax', $this->config->item('default_user_names'));

          //  print_r($this->events->apply_filters('awayordertime', $this->config->item('default_user_names')));

            $allowed_order_for_print        =   $this->events->apply_filters( 'allowed_order_for_print', array( 'nexo_order_comptant' ) );
    

            // Allow only cash order to be printed
            // if ( ! in_array( $data[ 'order' ]['order'][0][ 'TYPE' ], $allowed_order_for_print ) ) {
            //     redirect(array( 'dashboard', 'nexo', 'orders', '?notice=print_disabled' ));
            // }

            if (count($data[ 'order' ]) == 0) {
                return show_error(sprintf(__('Impossible d\'afficher le ticket de caisse. Cette commande ne possède aucun article &mdash; <a href="%s">Retour en arrière</a>', 'nexo'), $_SERVER['HTTP_REFERER']));
            }

            // @since 2.7.9
            $data[ 'template' ]                     =   array();
            $dateCreation                           =   new DateTime( $data[ 'order' ][ 'order' ][0][ 'DATE_CREATION' ] );
            $data[ 'template' ][ 'order_date' ]     =   $dateCreation->format( store_option( 'nexo_datetime_format', 'Y-m-d h:i:s' ) );
            $dateModification                       =   new DateTime( $data[ 'order' ][ 'order' ][0][ 'DATE_MOD' ] );
            $data[ 'template' ][ 'order_updated' ]  =   $dateModification->format( store_option( 'nexo_datetime_format', 'Y-m-d h:i:s' ) );
            $data[ 'template' ][ 'order_code' ]     =   $data[ 'order' ][ 'order' ][0][ 'CODE' ];
            $data[ 'template' ][ 'order_id' ]       =   $data[ 'order' ][ 'order' ][0][ 'ORDER_ID' ];
            $data[ 'template' ][ 'order_status' ]   =   $this->Nexo_Checkout->get_order_type($data[ 'order' ][ 'order' ][0][ 'TYPE' ]);
            $data[ 'template' ][ 'order_note' ]     =   $data[ 'order' ][ 'order' ][0][ 'DESCRIPTION' ];

            $data[ 'template' ][ 'order_cashier' ]  =   User::pseudo( $data[ 'order' ][ 'order' ][0][ 'AUTHOR' ] );
            $data[ 'template' ][ 'shop_name' ]      =   @$Options[ store_prefix() . 'site_name' ];
            $data[ 'template' ][ 'shop_pobox' ]     =   @$Options[ store_prefix() . 'nexo_shop_pobox' ];
            $data[ 'template' ][ 'shop_fax' ]       =   @$Options[ store_prefix() . 'nexo_shop_fax' ];
            $data[ 'template' ][ 'shop_email' ]     =   @$Options[ store_prefix() . 'nexo_shop_email' ];
            $data[ 'template' ][ 'shop_street' ]    =   @$Options[ store_prefix() . 'nexo_shop_street' ];
            $data[ 'template' ][ 'shop_phone' ]     =   @$Options[ store_prefix() . 'nexo_shop_phone' ];
            $data[ 'template' ][ 'customer_name' ]  =   $data[ 'order' ][ 'order' ][0][ 'customer_name' ];
            $data[ 'template' ][ 'customer_phone' ]  =   $data[ 'order' ][ 'order' ][0][ 'customer_phone' ];

            $data[ 'template' ][ 'delivery_address_1' ]     =   @$data[ 'shipping' ][0][ 'address_1' ];
            $data[ 'template' ][ 'delivery_address_2' ]     =   @$data[ 'shipping' ][0][ 'address_2' ];
            $data[ 'template' ][ 'city' ]               =   @$data[ 'shipping' ][0][ 'city' ];
            $data[ 'template' ][ 'country' ]            =   @$data[ 'shipping' ][0][ 'country' ];
            $data[ 'template' ][ 'name' ]               =   @$data[ 'shipping' ][0][ 'name' ];
            $data[ 'template' ][ 'phone' ]              =   @$data[ 'shipping' ][0][ 'phone' ];
            $data[ 'template' ][ 'surname' ]            =   @$data[ 'shipping' ][0][ 'surname' ];
            $data[ 'template' ][ 'state' ]              =   @$data[ 'shipping' ][0][ 'surname' ];
            $data[ 'template' ][ 'delivery_cost' ]      =   @$data[ 'shipping' ][0][ 'price' ];

            $filtered   =   $this->events->apply_filters( 'nexo_filter_receipt_template', [
                'template'          =>      $data[ 'template' ],
                'order'             =>      $data[ 'order' ][ 'order' ][0],
                'items'             =>      $data[ 'order' ][ 'products' ]
            ]);

            $data[ 'template' ]             =   $filtered[ 'template' ];
            $theme                          =   'default_free';
            $path                           =   '../modules/nexo/views/receipts/' . $theme . '.php';

           return $this->load->view(
                $this->events->apply_filters( 'nexo_receipt_theme_path', $path ),
                $data,
                $theme
            );

        } else {
            die(__('Cette commande est introuvable.', 'nexo'));
        }
    }
    public function register_history($register_id)
    {
            

             $this->load->library( 'parser' );
        $this->load->model( 'Nexo_Misc' );
        $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'orderModel' );

         
      

        $results=[];


        $startofday = new DateTime();
        $startofday->setTime(0,0);
        $startDate =$startofday->format('Y-m-d H:i:s');

        // echo date('Y-m-d H:i:s',$today);

        // echo "<br>";
        $endOfDay = new DateTime();
        $endOfDay->setTime(23,59);
        $endDate = $endOfDay->format('Y-m-d H:i:s');




          $pagination     =   new Pagination([
            'table'     =>  store_prefix() . 'nexo_registers_activities',
            'perPage'   =>  200
        ]); 

          
        $curr_date = date('Y-m-d');


                $data[ 'template' ]                     =   "registerhistory";


                // echo "<pre>";
                // print_r(__LINE__);
                // print_r($new_array);
                // print_r($total_amount);
                // die();

              //   $this->db->select("*");
              //   $this->db->from(store_prefix() . 'nexo_registers_activities');
              //   //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
              //   $this->db->where('DATE(DATE_CREATION)',$curr_date);//use date function

              //   $this->db->where('TYPE','opening');
              //  // $this->db->join('aauth_users', 'aauth_users.id ='.store_prefix() .'nexo_registers_activities.REF_REGISTER');
              //  //use date function
              //   $this->db->limit(1);
              //   $this->db->order_by('ID',"DESC");

              //   $opening_amount = $this->db->get()->result_array();





              //   $this->db->select("*");
              //   $this->db->from(store_prefix() . 'nexo_registers_activities');
              //   //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
              //   $this->db->where('DATE(DATE_CREATION)',$curr_date);//use date function

              //   $this->db->where('TYPE','closing');
              //  // $this->db->join('aauth_users', 'aauth_users.id ='.store_prefix() .'nexo_registers_activities.REF_REGISTER');
              //   // $this->db->where('REF_REGISTER',$register_id);//use date function
              //   $this->db->limit(1);
              //   $this->db->order_by('ID',"DESC");
              //   $closing_amount = $this->db->get()->result_array();




              //    $curr_date = date('Y-m-d');

              //   $this->db->select("*");
              //   $this->db->from(store_prefix() . 'nexo_commandes');
              //    $this->db->where('REF_REGISTER',$register_id);
              //   //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
              //   $this->db->where('DATE(DATE_CREATION)',$curr_date);//use date function

              //  // $this->db->where('DATE_CREATION BETWEEN "'. date('Y-m-d H:i:s', strtotime($opening_amount[0]['DATE_CREATION'])). '" and "'. date('Y-m-d H:i:s', strtotime($closing_amount[0]['DATE_CREATION'])).'"');
              //   // $this->db->limit(1);
              //   $this->db->order_by('ID',"DESC");
              //   $query = $this->db->get()->result_array();


              //   $curr_date = date('Y-m-d');


              



              

                


              //   $this->db->select("*");
              //   $this->db->from(store_prefix() .'nexo_registers');
              //   //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
              //   $this->db->where('ID',$register_id);//use date function
              //    $registers_details = $this->db->get()->result_array();


              //     $this->db->select("*");
              //   $this->db->from(store_prefix() .'aauth_users');
              //   //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
              //   $this->db->where('id',$registers_details[0]['AUTHOR']);//use date fun
              //   $registers = $this->db->get()->result_array();


              //  $filtered   =   $this->events->apply_filters( 'nexo_filter_receipt_template1', [
              //       'template'          =>      $data[ 'template' ],
                    
              //        'transactions'             =>      $query,
              //        'opening_amount' =>$opening_amount,
              //        'closing_amount' =>$closing_amount,
              //        'registers'=>$registers
              //    ]);

              //  // echo "<pre>";
              //  // print_r($opening_amount);
              //  // print_r($closing_amount);
              //  //  print_r($registers);
              //  // die();


              // //  $data[ 'refund' ]                       =   $this->orderModel->get_refund( $refund_id );

              
              //       // $data[ 'template' ][ 'refund_author' ]  =   $data[ 'refund' ][ 'author' ][ 'name' ];
              //       // $refundDate                             =   new DateTime( $data[ 'refund' ][ 'DATE_CREATION' ] );
              //       // $data[ 'template' ][ 'refund_date' ]    =   $refundDate->format( store_option( 'nexo_datetime_format', 'Y-m-d h:i:s' ) );
              //       // $data[ 'template' ][ 'refund_type' ]    =   $data[ 'refund' ][ 'TYPE' ] === 'withstock' ? __( 'Avec retour de stock', 'nexo' )  : __( 'Sans retour de stock', 'nexo' );
                    
              //       $this->load->module_view( 'nexo', 'receipts.nps.registerhistory', $filtered );
                    


                      $curr_date = date('Y-m-d');

                $this->db->select("*");
                $this->db->from(store_prefix() . 'nexo_commandes');
                //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
                $this->db->where('DATE(DATE_CREATION)',$curr_date);//use date function
                $this->db->where('REF_REGISTER',$register_id);//use date function
                
                // $this->db->limit(1);
                $this->db->order_by('ID',"DESC");
                $query = $this->db->get()->result_array();


                $curr_date = date('Y-m-d');


                $this->db->select("*");
                $this->db->from(store_prefix() . 'nexo_registers_activities');
                //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
                $this->db->where('DATE(DATE_CREATION)',$curr_date);//use date function

                $this->db->where('TYPE','opening');
               // $this->db->join('aauth_users', 'aauth_users.id ='.store_prefix() .'nexo_registers_activities.REF_REGISTER');
                $this->db->where('REF_REGISTER',$register_id);//use date function
                $this->db->limit(1);
                $this->db->order_by('ID',"DESC");

                $opening_amount = $this->db->get()->result_array();



              

                $this->db->select("*");
                $this->db->from(store_prefix() . 'nexo_registers_activities');
                //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
                $this->db->where('DATE(DATE_CREATION)',$curr_date);//use date function

                $this->db->where('TYPE','closing');
               // $this->db->join('aauth_users', 'aauth_users.id ='.store_prefix() .'nexo_registers_activities.REF_REGISTER');
                $this->db->where('REF_REGISTER',$register_id);//use date function
                $this->db->limit(1);
                $this->db->order_by('ID',"DESC");
                $closing_amount = $this->db->get()->result_array();


                $this->db->select("*");
                $this->db->from(store_prefix() .'nexo_registers');
                //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
                $this->db->where('ID',$register_id);//use date function
                 $registers_details = $this->db->get()->result_array();


                  $this->db->select("*");
                $this->db->from(store_prefix() .'aauth_users');
                //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
                $this->db->where('id',$registers_details[0]['AUTHOR']);//use date fun
                $registers = $this->db->get()->result_array();
        
                
           
               $filtered   =   $this->events->apply_filters( 'nexo_filter_receipt_template1', [
                    'template'          =>      $data[ 'template' ],
                    
                     'transactions'             =>      $query,
                     'opening_amount' =>$opening_amount,
                     'closing_amount' =>$closing_amount,
                     'registers'=>$registers
                 ]);

            
                    
                $this->load->module_view( 'nexo', 'receipts.nps.registerhistory', $filtered );
            



        }

        public function tables_get_all()
        {

           // print_r(__LINE__);
           // die();
            $this->db->select(
                store_prefix() . 'nexo_restaurant_tables.NAME as TABLE_NAME,' .
                store_prefix() . 'nexo_restaurant_tables.STATUS as STATUS,' .
                store_prefix() . 'nexo_restaurant_tables.MAX_SEATS as MAX_SEATS,' .
                store_prefix() . 'nexo_restaurant_tables.CURRENT_SEATS_USED as CURRENT_SEATS_USED,' .
                store_prefix() . 'nexo_restaurant_tables.ID as TABLE_ID,' .
                store_prefix() . 'nexo_restaurant_tables.SINCE as SINCE'
            )->from( store_prefix() . 'nexo_restaurant_tables' );

            $result = $this->db->get()
                ->result();

            // print_r(__LINE__);
            // print_r($result);
            // die();

            // if( $id != null ) {
            //     $this->db->where( 'ID', $id );
            // }

            $this->response(
                $result,
                200
            );
        }
        public function printserver(){
        
            $printserver = store_option('nexo_print_server_url');
            $printer = array('print server url'=>$printserver);
            if(!empty($printers)){
              
              return $this->response( $printer, 200 );

            } else {
               
               return $this->response( $printer, 200 );
            }
        
        
        }
         public function nexo_store_vat(){
        
            $nexo_vat_type = store_option('nexo_vat_type');
            $nexo_vat_percent = store_option('nexo_vat_percent');
             $printer = array('nexo_vat_type'=>$nexo_vat_type,'nexo_vat_percent'=>$nexo_vat_percent);
             if(!empty($printers)){
              
              return $this->response( $printer, 200 );
    
            } else {
               
               return $this->response( $printer, 200 );
            }
            
            
        }
     public function nexo_store_vat_tax(){
        
       
         $datas       =   $this->db->get( store_prefix() . 'nexo_taxes' )->result_array();
      
          return $this->response( $datas, 200 );  
        
    }
 public function createitem(){

      //  $this->db->select('*')->from( store_prefix() . 'nexo_articles' );

        $result       =   $this->db->get( store_prefix() . 'nexo_articles' )->result_array();
        
        $image_url = base_url().'public/uploads/items-images/';
        //echo $image_url;
        //die;
      //  $this->db->select('*')->from( store_prefix() . 'nexo_articles' );

        $result       =   $this->db->get( store_prefix() . 'nexo_articles' )->result_array();

        foreach($result as $key => $value){
            if($result[$key]['APERCU']!=''){
             $result[$key]['APERCU_URL'] =  $image_url.$result[$key]['APERCU'];
            } else {
                
                $result[$key]['APERCU_URL']='';
            }
        }
        
        $result['store_id']=1;

        // echo "<pre>";
        // print_r($result);
        // die();
        
        $result['store_id']=2;

        $data_string = json_encode($result); 
        $curl = curl_init('http://a2000demo.com/restuarant_dashboard/api/items'); 
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json', 'Content-Length: ' . strlen($data_string)) ); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string); 
        $result = curl_exec($curl); // Free up the resources $curl is using curl_close($curl); 
        echo $result;

    }
    public function branchorders(){


        $curr_date = date('Y-m-d');

        $this->db->select("*");
        $this->db->from(store_prefix() . 'nexo_commandes');
        $this->db->where('TYPE', 'nexo_order_comptant');
        //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
        $yesterday = date('Y-m-d H:i:s', strtotime('yesterday'));
        $this->db->where('DATE_CREATION >',  $yesterday);
        $this->db->where('STATUS', 'completed');
        //$this->db->limit(1);
        $this->db->order_by('ID',"DESC");


        $query_data = $this->db->get()->result_array();
        
       // echo "<pre>";
        //print_r($query_data);
        //die;

        foreach($query_data as $key => $value){

            $registers_details =array();
            $registers_users_details=array();
            $registers_users_name ='';

            $client_details =array();
            $client_users_details=array();
            $client_users_name ='';

            $waiter_details =array();
            $waiter_users_details=array();
            $waiter_users_name ='';

            $author_details =array();
            $author_users_details=array();
            $author_users_name ='';

            $this->db->select("*");
            $this->db->from(store_prefix() . 'nexo_registers');
            //$this->db->where('TYPE', 'nexo_order_comptant');
            //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
            $this->db->where('ID',$query_data[$key]['REF_REGISTER'] );
            //$this->db->limit(1);
            $this->db->order_by('ID',"DESC");

            $registers_details = $this->db->get()->result_array();

            if(!empty($registers_details)){

                $this->db->select("*");
                $this->db->from(store_prefix() . 'aauth_users');
                //$this->db->where('TYPE', 'nexo_order_comptant');
                //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
                $this->db->where('ID',$registers_details[0]['AUTHOR'] );
                //$this->db->limit(1);
                $this->db->order_by('ID',"DESC");

                $registers_users_details = $this->db->get()->result_array();

                if(!empty($registers_details)){

                    $registers_users_name =  $registers_users_details[0]['name'] ; 

                } else {

                    $registers_users_name='unknown';

                }


            } else {
                $registers_users_name='unknown';
            }
            //echo $query_data[$key]['REF_CLIENT'];
            //die;
            $this->db->select("*");
            $this->db->from(store_prefix() . 'nexo_clients');
            //$this->db->where('TYPE', 'nexo_order_comptant');
            //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
            $this->db->where('ID',$query_data[$key]['REF_CLIENT'] );
            //$this->db->limit(1);
           // $this->db->order_by('ID',"DESC");

            $client_details = $this->db->get()->result_array();

            // echo "<pre>";
            // print_r($client_details);
            // die();

            if(!empty($client_details)){

                $client_users_name= $client_details[0]['NOM'];

            } else {
                $client_users_name='unknown';
            }

            $this->db->select("*");
            $this->db->from(store_prefix() . 'aauth_users');
            //$this->db->where('TYPE', 'nexo_order_comptant');
            //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
            $this->db->where('ID',$query_data[$key]['REF_WAITER'] );
            //$this->db->limit(1);
            $this->db->order_by('ID',"DESC");

            $waiter_details = $this->db->get()->result_array();

            if(!empty($waiter_details)){

                $waiter_users_name= $waiter_details[0]['name'];

            } else {

                $this->db->select("*");
                $this->db->from(store_prefix() . 'aauth_users');
                //$this->db->where('TYPE', 'nexo_order_comptant');
                //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
                $this->db->where('email','waiter@a2000host.com' );
                //$this->db->limit(1);
                $this->db->order_by('id',"DESC");

               

                $waiter_users_details = $this->db->get()->result_array();

                if(!empty($waiter_users_details)){

                    $waiter_users_name=$waiter_users_details[0]['name'];
                } else {
                    $waiter_users_name='unknown';
                }

                
            }

            $this->db->select("*");
            $this->db->from(store_prefix() . 'aauth_users');
            //$this->db->where('TYPE', 'nexo_order_comptant');
            //$this->db->where('date_format(DATE_CREATION,"%Y-%m-%d")', 'CURDATE()', FALSE);
            $this->db->where('id',$query_data[$key]['AUTHOR'] );
            //$this->db->limit(1);
            $this->db->order_by('ID',"DESC");

            $author_details = $this->db->get()->result_array();

            if(!empty($author_details)){

                $author_users_name= $author_details[0]['name'];

            } else {

              
                    $author_users_name='unknown';


                
            }
            

            


            $query_data[$key]['REF_REGISTER'] =  $registers_users_name;
            $query_data[$key]['REF_CLIENT'] = $client_users_name;
            $query_data[$key]['REF_WAITER'] =  $waiter_users_name;
            $query_data[$key]['AUTHOR'] =  $author_users_name;
            
        }
        $query_data['store_id']=2;
       
    

        $data_string = json_encode($query_data); 
        $curl = curl_init('http://a2000demo.com/restuarant_dashboard/api/orders'); 
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json', 'Content-Length: ' . strlen($data_string)) ); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string); 
        $result = curl_exec($curl); // Free up the resources $curl is using curl_close($curl); 
        echo $result;

    }
    public function ordersitems(){


        $curr_date = date('Y-m-d');

        $this->db->select("*");
        $this->db->from(store_prefix() . 'nexo_commandes_produits');
        //$this->db->where('TYPE', 'nexo_order_comptant');
      //  $this->db->where('date_format(created_on,"%Y-%m-%d")', 'CURDATE()', FALSE);
        //$this->db->where('RESTAURANT_FOOD_STATUS', 'ready');
        //$this->db->limit(1);
        //  $yesterday = date('Y-m-d H:i:s', strtotime('yesterday'));
        // $this->db->where('DATE_CREATION >',  $yesterday);
        $this->db->order_by('ID',"DESC");


        $query_data = $this->db->get()->result_array();

        $query_data['store_id']=2;

        $data_string = json_encode($query_data); 
        $curl = curl_init('http://a2000demo.com/restuarant_dashboard/api/ordersitems'); 
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_HTTPHEADER, array( 'Content-Type: application/json', 'Content-Length: ' . strlen($data_string)) ); 
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string); 
        $result = curl_exec($curl); // Free up the resources $curl is using curl_close($curl); 
        echo $result;

    }
     public function itemimage(){
         
          $json = file_get_contents('php://input');

         //print_r(__LINE__);
         //echo "<pre>";
         $itemslist_branch = json_decode($json,true);
         
        return $itemslist_branch['URL'];
     
    }

	
}
