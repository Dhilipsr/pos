<?php
class GastroMigrationController extends Tendoo_Api
{
    public function migrate()
    {
        $this->load->model( 'Nexo_Stores' );
        $version        =   $this->post( 'version' );
        $offset         =   $this->post( 'offset' );
        $handle         =   $this->post( 'handle' );
        $db_prefix      =   $this->post( 'db_prefix' );

        if ( $version === '2.3.70' && $handle === 'orders' ) {
            /**
             * migrate orders that has been placed 
             * during the previous two days
             */
            $stores         =   $this->Nexo_Stores->get();

            array_unshift( $stores, [
                'ID'        =>  0
            ]);

            foreach( $stores as $store ) {
                $store_prefix       =   $store[ 'ID' ] == 0 ? '' : 'store_' . $store[ 'ID' ] . '_';
                $totalMetas         =   $this->db->select( '*' )->from( $store_prefix . 'nexo_commandes_meta' )->get()->num_rows();
                $perRequest         =   400;
                $totalPage          =   ceil( $totalMetas / $perRequest );
                $orderMetas         =   $this->db
                // ->where( 'DATE_CREATION >=', $startDate )
                // ->where( 'DATE_CREATION <=', $endDate )
                ->limit( $perRequest, $offset * $perRequest )
                ->get( $store_prefix . 'nexo_commandes_meta' )
                ->result_array();
    
                $mergeMetas         =   [];
                foreach( $orderMetas as $meta ) {
                    $mergeMetas[ $meta[ 'REF_ORDER_ID' ] ][ $meta[ 'KEY' ] ]   =   $meta[ 'VALUE' ];
                    $mergeMetas[ $meta[ 'REF_ORDER_ID' ] ][ 'REF_ORDER_ID' ]   =   $meta[ 'REF_ORDER_ID' ];
                }
    
                foreach( $mergeMetas as $id => $meta ) {
                    $this->db->where( 'ID', $id )
                        ->update( $store_prefix . 'nexo_commandes', [
                            'RESTAURANT_TABLE_ID'   =>  $meta[ 'table_id' ] ?? 0,
                            'RESTAURANT_SEAT_USED'  =>  $meta[ 'seat_used' ] ?? 0
                        ]);
                }
            }

            return $this->response([
                'status'    =>  'success',
                'message'   =>  __( 'Migration successful', 'gastro' ),
                'data'      =>  [
                    'offset'    =>  ( $totalPage ) <= $offset ? 0 : $offset + 1,
                    'url'       =>  site_url([ 'api', 'gastro', 'migration' ]),
                    'version'   =>  '2.3.70',
                    'class'     =>  'update-orders',
                    'title'     =>  sprintf( __( 'Update Orders by range of %s (%s/%s)', 'gastro' ), $perRequest, $offset, $totalPage ),
                    'handle'    =>  ( $totalPage ) <= $offset ? 'products' : 'orders'
                ]
            ]);

        } else if ( $version === '2.3.70' && $handle === 'products' ) {
            /**
             * migrate orders that has been placed 
             * during the previous two days
             */
            $stores         =   $this->Nexo_Stores->get();

            array_unshift( $stores, [
                'ID'        =>  0
            ]);

            foreach( $stores as $store ) {
                /**
                 * migrate products that has been placed 
                 * during the previous two days
                 */
                $store_prefix       =   $store[ 'ID' ] == 0 ? '' : 'store_' . $store[ 'ID' ] . '_';
                $totalMetas         =   $this->db->select( '*' )->from( $store_prefix . 'nexo_commandes_produits_meta' )->get()->num_rows();
                $perRequest         =   400;
                $totalPage          =   ceil( $totalMetas / $perRequest );
                $orderMetas         =   $this->db
                    ->limit( $perRequest, $offset * $perRequest )
                    ->get( $store_prefix . 'nexo_commandes_produits_meta' )
                    ->result_array();

                $mergeMetas         =   [];
                foreach( $orderMetas as $meta ) {        
                    $mergeMetas[ $meta[ 'REF_COMMAND_PRODUCT' ] ][ $meta[ 'KEY' ] ]         =   $meta[ 'VALUE' ];
                    $mergeMetas[ $meta[ 'REF_COMMAND_PRODUCT' ] ][ 'REF_COMMAND_PRODUCT' ]  =   $meta[ 'REF_COMMAND_PRODUCT' ];
                }

                foreach( $mergeMetas as $id => $meta ) {
                    $this->db->where( 'ID', $id )
                        ->update( $store_prefix . 'nexo_commandes_produits', [
                            'RESTAURANT_FOOD_STATUS'        =>  $meta[ 'restaurant_food_status' ] ?? '',
                            'RESTAURANT_FOOD_ISSUE'         =>  $meta[ 'restaurant_food_issue' ] ?? '',
                            'RESTAURANT_FOOD_NOTE'          =>  $meta[ 'restaurant_food_note' ] ?? '',
                            'RESTAURANT_FOOD_MODIFIERS'     =>  $meta[ 'modifiers' ] ?? '[]'
                        ]);
                }
            }

            if ( ( $totalPage ) <= $offset ) {
                set_option( 'migration_' . 'gastro', '2.3.70', true);
                return $this->response([
                    'status'    =>  'success',
                    'message'   =>  __( 'Migration complete' )
                ]);
            }

            return $this->response([
                'status'    =>  'success',
                'message'   =>  __( 'Migration successful', 'gastro' ),
                'data'      =>  [
                    'offset'    =>  $offset+1,
                    'class'     =>  'update-products',
                    'url'       =>  site_url([ 'api', 'gastro', 'migration' ]),
                    'version'   =>  '2.3.70',
                    'title'     =>  sprintf( __( 'Update products by range of %s (%s/%s) ', 'gastro' ), $perRequest, $offset + 1, $totalPage ),
                    'handle'    =>  'products'
                ]
            ]);
        }        
    }
}
