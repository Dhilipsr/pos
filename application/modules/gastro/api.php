<?php
$Routes->get( 'gastro/tables_get_all', 'ApiKitchensController@tables_get_all' );
$Routes->get( 'gastro/areas', 'ApiAreasController@areas_get' );
$Routes->get( 'gastro/areas/tables', 'ApiAreasController@areas_get' );
$Routes->get( 'gastro/modifiers', 'ApiModifiersController@modifiers' );
$Routes->get( 'gastro/modifiers/by-group/{id}', 'ApiModifiersController@modifiers_by_group_get' );
$Routes->get( 'gastro/tables/orders', 'ApiTablesController@orders' );
$Routes->get( 'gastro/tables/billorders', 'ApiTablesController@billorders' );
// $Routes->get( 'gastro/tables/orders', 'ApiTablesController@getOrders' );
$Routes->get( 'gastro/tables/{id?}', 'ApiTablesController@tables_get' );
$Routes->get( 'gastro/tables/area/{id}', 'ApiTablesController@tables_from_area_get' );
$Routes->get( 'gastro/tables/history/{table_id}', 'ApiTablesController@table_order_history_get' );
$Routes->get( 'gastro/kitchens/orders', 'ApiKitchensController@getOrders' );
$Routes->get( 'gastro/kitchens/ready-orders', 'ApiKitchensController@ready_orders_get' );
$Routes->get( 'gastro/kitchens/partially-orders', 'ApiKitchensController@getPartiallyReady' );


// $Routes->get( 'gastro/kitchens/orderstatus/{order_id}', 'ApiKitchensController@orderstatus' );

$Routes->get( 'gastro/kitchens/getkitchen', 'ApiKitchensController@getkitchen' );
$Routes->get( 'gastro/kitchens/kitchen_status', 'ApiKitchensController@kitchen_status' );
$Routes->get( 'gastro/kitchens/google-refresh', 'ApiKitchensController@start_cooking_post' );
$Routes->get( 'gastro/kitchens/print/{order_id}', 'ApiKitchensController@print_to_kitchen_get' );
// $Routes->get( 'gastro/kitchens/split-print/{order_id}', 'ApiKitchensController@split_print_get' );
//$Routes->get( 'gastro/kitchens/nps-splitted-print/{order_id}', 'ApiKitchensController@nps_split_print_post' );
// $Routes->get( 'gastro/kitchens/nps-single-print/{order_id}', 'ApiKitchensController@nps_single_print_post' );
$Routes->get( 'gastro/kitchens/base64-splitted-print/{order_id}', 'ApiKitchensController@base64_split_print_post' );
$Routes->get( 'gastro/kitchens/base64-single-print/{order_id}', 'ApiKitchensController@base64_single_print_post' );
$Routes->get( 'gastro/orders/daily/{type?}/{status?}', 'ApiOrdersController@daily' );

$Routes->put( 'gastro/tables/status/{id}', 'ApiTablesController@table_usage_put' );
$Routes->put( 'gastro/tables/pay-order/{id}', 'ApiTablesController@pay_order_put' );

$Routes->post( 'gastro/tables/serve/', 'ApiTablesController@serve_post' );
$Routes->post( 'gastro/tables/collect/', 'ApiTablesController@collect_meal_post' );
$Routes->post( 'gastro/tables/orders-merging/', 'ApiTablesController@merge_order' );
$Routes->post( 'gastro/tables/cancel-item', 'ApiTablesController@cancel_item' );
$Routes->post( 'gastro/tables/push-order', 'ApiTablesController@pushOrder' );
$Routes->post( 'gastro/tables/free-table', 'ApiTablesController@free_table' );
$Routes->post( 'gastro/kitchens/cook', 'ApiKitchensController@start_cooking_post' );
$Routes->post( 'gastro/kitchens/collected-orders', 'ApiKitchensController@order_collected_post' );
$Routes->post( 'gastro/kitchens/food-status', 'ApiKitchensController@food_state_post' );
$Routes->post( 'gastro/kitchens/queue-booked', 'ApiOrdersController@queueBooked' );
$Routes->post( 'gastro/orders/split', 'ApiOrdersController@split' );
$Routes->post( 'gastro/orders/move', 'ApiOrdersController@moveOrder' );
$Routes->post( 'gastro/migration', 'GastroMigrationController@migrate' );
$Routes->get( 'gastro/staff-actions/{id}', 'ApiOrdersController@ordersStaffHistory' );
$Routes->get( 'gastro/modifiers-groups', 'ApiModifiersController@getModifiersGroupsWithModifiers' );




$Routes->get( 'gastro/orderstatus', 'ApiOrdersController@ordersent' );
$Routes->get( 'gastro/test', 'ApiOrdersController@test' );



///multiprint
$Routes->get( 'gastro/kitchens/nps-splitted-print/{order_id}/{kitchen_id}', 'ApiKitchensController@nps_split_print_post' );
$Routes->get( 'gastro/kitchens/split-print/{order_id}/{kitchen_id}', 'ApiKitchensController@split_print_get' );



///Singleprint
$Routes->get( 'gastro/kitchens/nps-single-print/{order_id}', 'ApiKitchensController@nps_single_print_post' );


$Routes->get( 'gastro/kitchens/nps-splitted-print_ready/{order_id}/{kitchen_id}', 'ApiKitchensController@nps_split_print_post_print_ready' );



$Routes->get( 'gastro/kitchens/kitchens_products/{kitchen_id}/{order_id}', 'ApiKitchensController@kitchens_products' );

$Routes->get( 'gastro/kitchens/kitchens_products_ready/{kitchen_id}/{order_id}', 'ApiKitchensController@kitchens_products_ready' );

$Routes->get( 'gastro/kitchens/getprinters', 'ApiKitchensController@getprinters' );



$Routes->post( 'gastro/printers', 'ApiKitchensController@printers' );
$Routes->get( 'gastro/localprint/{order_id}', 'ApiKitchensController@printresults' );

$Routes->get( 'gastro/localprint_free/{order_id}', 'ApiKitchensController@printresults_free' );

$Routes->get( 'gastro/registryhistory/{order_id}', 'ApiKitchensController@register_history' );


$Routes->get( 'gastro/printserver', 'ApiKitchensController@printserver' );

$Routes->get( 'gastro/nexo_store_vat', 'ApiKitchensController@nexo_store_vat' );

$Routes->get( 'gastro/nexo_store_vat_tax', 'ApiKitchensController@nexo_store_vat_tax' );



$Routes->get( 'gastro/branch/items', 'ApiKitchensController@createitem' );

$Routes->get( 'gastro/branch/orders', 'ApiKitchensController@branchorders' );

$Routes->get( 'gastro/branch/ordersitems', 'ApiKitchensController@ordersitems' );

$Routes->post( 'gastro/itemimage', 'ApiKitchensController@itemimage' );





