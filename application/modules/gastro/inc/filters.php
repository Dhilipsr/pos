<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nexo_Restaurant_Filters extends Tendoo_Module
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Filter Product while order is edited
     * @param array
     * @return array
     */
    public function pos_edited_items( $item ) 
    {
        $this->load->module_model( 'gastro', 'Nexo_Gastro_Tables_Models', 'table_model' );
        $item[ 'DESIGN' ]                   =   $item[ 'NAME' ];
        $item[ 'CODEBAR' ]                  =   $item[ 'RESTAURANT_PRODUCT_REAL_BARCODE' ];
        $item[ 'STATUS' ]                   =   '1';
        $item[ 'QTE_ADDED' ]                =   floatval( $item[ 'QTE_ADDED' ] );
        $item[ 'PRIX_DE_VENTE' ]            =   $item[ 'PRIX' ];
        $item[ 'PRIX_DE_VENTE_TTC' ]        =   $item[ 'PRIX' ];
        $modifier                           =   get_instance()->table_model->get_modifiers( $item[ 'ITEM_ID' ], $item[ 'REF_COMMAND_CODE' ] );
        $item[ 'metas' ][ 'modifiers' ]     =   @$modifier ? json_decode( $modifier, true ) : [];
        return $item;
    }

    /**
     *  Admin Menus
     *  @param array menus
     *  @return array current menu
    **/

    public function admin_menus( $menus )
    {
        if( @$menus[ 'caisse' ] != null ) {
            if ( User::can( 'nexo.manage.settings' ) ) {
                $menus      =   array_insert_after( 'dashboard', $menus, 'restaurant', [
                    [
                        'title'     =>      __( 'Restaurant', 'gastro' ),
                        'href'      =>      '#',
                        'icon'      =>      'fa fa-cutlery',
                        'disable'   =>      true
                    ],
                    [
                        'title'     =>      __( 'Tables', 'gastro' ),
                        'href'      =>      site_url([ 'dashboard', store_slug(), 'gastro', 'tables' ]),
                        'disable'   =>      true
                    ]      
                ]);

                if( store_option( 'disable_area_rooms' ) != 'yes' ) {
                    $menus[ 'restaurant' ][]    =   [
                        'title'     =>      __( 'Areas', 'gastro' ),
                        'href'      =>      site_url([ 'dashboard', store_slug(), 'gastro', 'areas' ]),
                        'disable'   =>      true
                    ];
                }
            }

            if( store_option( 'disable_kitchen_screen', 'no' ) == 'no' ) {
                if ( store_option( 'disable_kitchens', 'yes' ) === 'no' && User::can( 'gastro.use.kitchen-screen' ) ) {
                    
                    
                $details = User::group_details();
                
                  if($details->group_id==4) {
                    
                    // if the rooms and area still available, then the multiple kitchen can be used
                    $menus[ 'restaurant' ][]    =   [
                        'title'     =>      __( 'Kitchens', 'gastro' ),
                        'href'      =>      site_url( [ 'dashboard', store_slug(), 'gastro', 'kitchens' ] )
                    ];
                    
                  }
                }
    
                if ( store_option( 'disable_kitchens', 'yes' ) === 'yes' && User::can( 'gastro.use.kitchen-screen' ) ) {
                    // hide kitchen menu 
                    // since @1.4.3
                   // When area and rooms are disable, then the single kitchen view is enabled
                    $menus[ 'restaurant' ][]    =   [
                        'title'     =>      __( 'Kitchen View', 'gastro' ),
                        'href'      =>      site_url([ 'dashboard', store_slug(), 'gastro', 'kitchen-screen', '0?single-view=true' ]),
                        'disable'   =>      true
                    ]; 
                }   

                /**
                 * The waiter screen is tightly linked to the kitchen
                 * screen, if all means which goes to the kitchen are already ready
                 * no need to report them to the waiter. 
                 * @todo unless if they need to use the "collected" feature.
                 */
                if( store_option( 'disable_waiter_screen', 'no' ) == 'no' && User::can( 'gastro.use.waiter-screen' ) ) {
                    // Waiter screen
                    $menus[ 'restaurant' ][]    =   [
                        'title'     =>      __( 'Waiter Screen', 'gastro' ),
                        'href'      =>      site_url([ 'dashboard', store_slug(), 'gastro', 'waiters-screen' ]),
                        'disable'   =>      true
                    ]; 
                }        
            }

            
        }

        // if( @$menus[ 'nexo_settings' ] ) {
            $menus[ 'nexo_settings' ][]     =   [
                'title'         =>      __( 'Restaurant Settings', 'gastro' ),
                'href'          =>      site_url([ 'dashboard', store_slug(), 'gastro', 'settings' ]),
                'permission' 		=>	'nexo.manage.settings'
            ];
        // }

        if( @$menus[ 'arrivages' ] ) {
            $menus[ 'arrivages' ][]     =   [
                'title'         =>      __( 'Modifiers', 'gastro' ),
                'href'          =>      site_url([ 'dashboard', store_slug(), 'gastro', 'modifiers' ])
            ];

            $menus[ 'arrivages' ][]     =   [
                'title'         =>      __( 'Modifiers Groups', 'gastro' ),
                'href'          =>      site_url([ 'dashboard', store_slug(), 'gastro', 'mod-groups' ])
            ];
        }
        return $menus;
    }

    /**
     *  Add Cart Buttons
     *  @param
     *  @return
    **/

    public function cart_buttons( $menus )
    {


        $this->load->model('Users_Model', 'users');


        $details = User::group_details();

          $menus[]            =   [
            'class' =>  'default dinein-button show-tables-button',
          
            'text'  =>  __( 'Dine in', 'gastro' ) . ' <span ng-show="selectedTable != false">' . sprintf( __( 'on %s', 'gastro' ), '<strong id="textdine">{{ selectedTable.NAME || selectedTable.TABLE_NAME }}</strong>' ) . '</span>',
            'icon'  =>  'table',
            'attrs' =>  [
                'ng-click'  =>  'openTables()',
                'ng-controller' => 'selectTableCTRL',
                'ng-class'  =>  '{
                    \'btn-primary btn-no-border\'       :   selectedOrderType.namespace == \'dinein\',
                    \'btn-default\'                     :   selectedOrderType.namespace != \'dinein\'
                }',
                 "id"=>'dinein'
            ]
        ];

   
        $menus[]            =   [
            'class' =>  'default takeaway-button',
            'text'  =>  __( 'Take Away', 'gastro' ),
            'icon'  =>  'shopping-bag',
            'attrs' =>  [
                'ng-click'  =>  'switchOrderType( \'takeaway\' )',
                'ng-class'  =>  '{
                    \'btn-primary btn-no-border\'       :   selectedOrderType.namespace == \'takeaway\',
                    \'btn-default\'                     :   selectedOrderType.namespace != \'takeaway\'
                }',
                 "id"=>'takeaway',



            ]
        ];

        $menus[]            =   [
            'class' =>  'default delivery-button',
            'text'  =>  __( 'Delivery', 'gastro' ),
            'icon'  =>  'truck',
            'attrs' =>  [
                'ng-click'  =>  'switchOrderType( \'delivery\' )',
                'ng-class'  =>  '{
                    \'btn-primary btn-no-border\'       :   selectedOrderType.namespace == \'delivery\',
                    \'btn-default\'                     :   selectedOrderType.namespace != \'delivery\'
                }',
            "id"=>'delivery'
            ]
        ];

        $menus[]            =   [
            'class' =>  'default quickbill-button',
            'text'  =>  __( 'Quick bill', 'gastro' ),
            'icon'  =>  'shopping-bag',
            'attrs' =>  [
                'ng-click'  =>  'switchOrderType( \'quickbill\' )',
                 // 'ng-controller' => 'selectTableCTRL',
                'ng-class'  =>  '{
                    \'btn-primary btn-no-border\'       :   selectedOrderType.namespace == \'quickbill\',
                    \'btn-default\'                     :   selectedOrderType.namespace != \'quickbill\'
                }',
                 "id"=>'quickbill',



            ]
        ];


        if($details->group_id==12) {
        ?>

        <style type="text/css">
            


            #takeaway{
                display: none;
            }

              #delivery{
                display: none;
            }

              .takeaway{
                display: none;
            }

              .delivery{
                display: none;
            }
            .history-box-button{
                display: none !important;
            }

        </style>

        <?php

    }
    


        $menus[]            =   [
            'class' =>  'default merging-button',
            'text'  =>  __( 'Merge', 'gastro' ),
                    'icon'  =>  'random',
                    'attrs' =>  [
                        'ng-click'  =>  'openMergingOrder( \'delivery\' )',
                        'ng-controller'  =>  'mergingCTRL'
                    ],
            "id"=>"merge"
        ];

         $menus[]            =   [
          'class' =>  'default merging-button',
                    'text'  =>  __( 'Split', 'gastro' ),
                    'icon'  =>  'scissors',
                    'attrs' =>  [
                        'ng-click'  =>  'openSplitOrder( \'dinein\' )',
                        'ng-controller'  =>  'splittingCTRL'
                    ],
                                "id"=>"split"
        ];
        
        
        
        if( store_option( 'disable_delivery' ) == 'yes' ):?>
        
        <style type="text/css">
          #delivery{
                display: none !important;
            }
            
            
            
        </style>
            
        <?php endif;?>
        
        <?php if( store_option( 'disable_dinein' ) == 'yes' ):?>
        <style type="text/css">
          #dinein{
                display: none !important;
            }
        .merging-button{
            display:none !important;
        }
         </style>  
        <?php endif;?>
        
        <?php if( store_option( 'disable_takeaway' ) == 'yes' ):?>
        <style type="text/css">
          #takeaway{
                display: none !important;
            }
        </style>
       
        <?php endif;  ?>
        
         <?php if( store_option( 'quickorders' ) != 'yes' ):?>
        <style type="text/css">
          #quickbill{
                display: none !important;
            }
       
         </style>  
        <?php endif;?>

            
        <?php
        

       /* $menus[]            =   [
            'class' =>  'default foobutton',
            'text'  =>  __( 'Options', 'gastro' ),
            'icon'  =>  'wrench',
            'type'  =>  'dropdown',
            'options'   =>  [
                [
                    'class' =>  'default merging-button',
                    'text'  =>  __( 'Merge Order', 'gastro' ),
                    'icon'  =>  'random',
                    'attrs' =>  [
                        'ng-click'  =>  'openMergingOrder( \'delivery\' )',
                        'ng-controller'  =>  'mergingCTRL'
                    ],
                ], [
                    'class' =>  'default merging-button',
                    'text'  =>  __( 'Split Order', 'gastro' ),
                    'icon'  =>  'scissors',
                    'attrs' =>  [
                        'ng-click'  =>  'openSplitOrder( \'dinein\' )',
                        'ng-controller'  =>  'splittingCTRL'
                    ],
                ]
            ]
        ];*/

        return $menus;
    }

    /** 
     * Dashbaord Dependencies
     * @param array
     * @return array
    **/

    public function dashboard_dependencies( $deps )
    {
        // waiting for booking to be ready
        // return $deps;
        // if( ! in_array( 'mwl.calendar', $deps ) ) {
        //     $deps[]     =   'mwl.calendar';
        // }

        // if( ! in_array( 'ui.bootstrap', $deps ) ) {
        //     $deps[]     =   'ui.bootstrap';
        // }

        // if( ! in_array( 'ngMasonry', $deps ) ) {
        //     $deps[]     =   'ngMasonry';
        // }

        return $deps;
    }
    
    /**
     * cart_buttons_2
     * @param array buttons
     * @return array new buttons
    **/
    public function cart_buttons_2( $menus ) 
    {
        if( store_option( 'disable_pos_waiter', 'no' ) == 'no' ) {
            // $menus[]            =   [
            //     'class' =>  'default show-ready-meal-button',
            //     'text'  =>  __( 'Waiter Screen', 'gastro' ) . ' <span ng-show="items.length > 0" class="label label-primary">{{ items.length }}</span>',
            //     'icon'  =>  'user',
            //     'attrs' =>  [
            //         'ng-click'  =>  'openReadyMeals()',
            //         'ng-controller'     =>  'readyMealCTRL'
            //     ]
            // ];
        }

      	 if( store_option( 'ready_orders' ) == 'yes' ):
        $menus[]            =   [
            'class' =>  'default new-orders-button',
            'text'  =>  __( 'Ready Orders', 'gastro' ) . ' <span class="badge" ng-show="newOrders > 0">{{ newOrders }}</span>',
            'icon'  =>  'check',
            'attrs' =>  [
                'ng-click'  =>  'openReadyOrders()',
                'ng-controller' => 'readerOrdersCTRL',
                 'id'=>'readyorders'
            ],

        ];
        endif;
        if( store_option( 'bill_orders' ) == 'yes' ):
            
             $menus[]            =   [
            'class' =>  'default new-orders-button',
            'text'  =>  __( 'Bills', 'gastro' ),
            'icon'  =>  'check',
            'attrs' =>  [
                'ng-click'  =>  'billOrders()',
                'ng-controller' => 'billordersCTRL',
                 'id'=>'billorders'
            ],

        ];
        
        endif;  

        return $menus;
    }

    /**
     * Add Combo
     * @param string before cart pay button
     * @deprecated
     * @return string
    **/
    
    public function add_combo( $string )
    {
        // $this->load->module_view( 'gastro', 'combo/combo-button' );
    }

    /**
     * Restaurant Demo
     * @param array demo list
     * @return array demo list
    **/
    
    public function restaurant_demo( $demo )
    {
        $demo[ 'gastro' ]     =   __( 'Restaurant Demo', 'gastro' );
        return $demo;
    }

    /**
     * Customize Product Crud
     * @param object crud object
     * @return object
    **/

    public function load_product_crud( $crud ) 
    {
        $raw_modifiers  =   $this->db->get( store_prefix() . 'nexo_restaurant_modifiers_categories' )
        ->result_array();
        
        $crud->display_as( 'REF_MODIFIERS_GROUP', __( 'Modifiers Group', 'gastro' ) );  
        $crud->field_type( 'REF_MODIFIERS_GROUP','multiselect', nexo_convert_raw( $raw_modifiers, 'ID', 'NAME' ) );
        // $crud->set_relation('REF_MODIFIERS_GROUP', store_prefix() . 'nexo_restaurant_modifiers_categories', 'NAME' );
        $crud->field_description('REF_MODIFIERS_GROUP', __( 'Set a modifiers which will be used for this item. According to the modifiers group, the modifiers selection can be forced.', 'gastro' ) );
        
        $crud->display_as( 'PRODUCT_SKIP_COOKING', __( "Do you Don't want to display this item in the Kitchen?", 'gastro' ) );  
        $crud->field_type( 'PRODUCT_SKIP_COOKING','dropdown', [
            'yes'   =>  __( 'Yes', 'gastro' ),
            'no'    =>  __( 'No', 'gastro' )
        ]);
        $crud->display_as( 'PRODUCT_SKIP_PRINT', __( 'Do you want this item to print on the Kitchen receipt?', 'gastro' ) );  
        $crud->field_type( 'PRODUCT_SKIP_PRINT','dropdown', [
            'yes'   =>  __( 'Yes', 'gastro' ),
            'no'    =>  __( 'No', 'gastro' )
        ]);
        // $crud->set_relation('PRODUCT_SKIP_COOKING', store_prefix() . 'nexo_restaurant_modifiers_categories', 'NAME' );
        $crud->field_description('PRODUCT_SKIP_COOKING', __( 'If a product doesn\'t need to be cooked, once sent at the kitchen it will be marked as ready immediatley.', 'gastro' ) );
        return $crud;
    }

    /** 
     * Filter Pay Button
     * If the kitchen screen is disabled, then we'll only display
     * @param string current button dom string
     * @return string
    **/

    public function cart_pay_button( $button )
    {
        // if use don't have access to the pay button, the we can delete it.
        if( ! User::can( 'gastro.view.paybutton' ) ) {
            $button     =   '';
        }

        return $button . $this->load->module_view( 'gastro', 'checkout.pay_button', null, true );
        // if( store_option( 'disable_kitchen_screen' ) != 'yes' ) {
        // }
        return $button;
    }

    /**
     * Post order details
     * @param array order details
     * @param array data
     * @return array
    **/

    public function post_order_details( $order_details, $data ) 
    {
        $metas       =   @$data[ 'metas' ];
        if( @$metas[ 'add_on' ] != null ) {
            
            $order          =   $this->db->where( 'ID', $metas[ 'add_on' ] )
            ->get( store_prefix() . 'nexo_commandes' )
            ->result_array();

            $total_price        =   0;

            foreach( $data[ 'ITEMS' ] as $item ) {

                $closetimeforitem='';

                $expirytimeforitem = '';

                $currtime = date("Y-m-d H:i:s");
                
                $timer = new DateTime($currtime);


                if($item['TIME_TYPE'] == "minutes"){
                    $form = "M";
                }else if($item['TIME_TYPE'] == 'seconds'){
                    $form = "S";
                }else{
                    $form = "H";
                }
               $timer->add(new DateInterval('PT' . $item['TIME'] . $form));


               $expirytimeforitem = $timer->format('Y-m-d H:i:s');




                // Adding to order product
                if( $item[ 'discount_type' ] == 'percentage' && $item[ 'discount_percent' ] != '0' ) {
                    $discount_amount		=	__floatval( ( __floatval($item[ 'qte_added' ]) * __floatval($item['sale_price']) ) * floatval( $item[ 'discount_percent' ] ) / 100 );
                } elseif( $item[ 'discount_type' ] == 'flat' ) {
                    $discount_amount		=	__floatval( $item[ 'discount_amount' ] );
                } else {
                    $discount_amount		=	0;
                }

                $item_data		=	array(
                    'REF_PRODUCT_CODEBAR'           =>  $item[ 'codebar' ],
                    'REF_COMMAND_CODE'              =>  $order[0][ 'CODE' ],
                    'QUANTITE'                      =>  $item[ 'qte_added' ],
                    'PRIX'                          =>  floatval( $item[ 'PRIX_DE_VENTE' ] ) - $discount_amount,
                    'PRIX_TOTAL'                    =>  ( __floatval($item[ 'qte_added' ]) * __floatval($item[ 'sale_price' ]) ) - $discount_amount,
		    'PRIX_BRUT'                          =>  floatval( $item[ 'sale_price' ] ) - $discount_amount,
                    // @since 2.9.0 
                    'DISCOUNT_TYPE'			        =>  $item['discount_type'],
                    'DISCOUNT_AMOUNT'		        =>  $item['discount_amount'],
                    'DISCOUNT_PERCENT'		        =>  $item['discount_percent'],
                    // @since 3.1   
                    'NAME'                          =>  $item[ 'name' ],
                    'INLINE'                        =>  $item[ 'inline' ],
 'TAX'                        =>  $item[ 'tax' ],
'TOTAL_TAX'                        =>  $item[ 'total_tax' ],
                    'created_on'                =>  date('Y-m-d H:i:s'),
                    'expiry_item_time'          =>   $expirytimeforitem,
                    'close_time'=> $closetimeforitem,
                    'REST_ORDER_TYPE'=>$data['RESTAURANT_ORDER_TYPE']
                );

                // filter item
                $item_data                  =     $this->events->apply_filters_ref_array( 'post_order_item', [ $item_data, $item ]);
    
                $this->db->insert( store_prefix() . 'nexo_commandes_produits', $item_data );

                // getcommande product id
                $insert_id = $this->db->insert_id();

                $total_price                    +=  floatval( $item[ 'sale_price' ] );

                /**
                 * allow to take an action
                 * on the product that has just
                 * been sold
                 */
                $this->events->do_action( 'after_order_item_saved', [
                    'product_id'    =>  $insert_id,
                    'data'          =>  $item_data,
                    'order'         =>  $order,
                    'item'          =>  $item
                ]);
            }

            $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'order_model' );

            $this->order_model->recalculateOrder( $order[0][ 'ID' ], null );

            $this->db
                ->where( 'CODE', $order[0][ 'CODE' ])
                ->update( store_prefix() . 'nexo_commandes', [
                    'DATE_MOD'                  =>  date_now(),
                    'RESTAURANT_ORDER_STATUS'   =>  store_option( 'disable_kitchen_screen', 'no' ) === 'no' ? 'pending' : 'ready' // change the order status
                ]);
            
            /**
             * an action
             * to be aware of item addition
             */
            $this->events->do_action( 'gastro_after_added_item', $order[0], $data );

            return response()->json([
                'status'        =>  'success',
                'code'          =>  'item_added',
                'message'       =>  __( 'The item has been added to the order', 'gastro' ),
                'order_code'    =>  $order[0][ 'CODE' ],
                'order_id'      =>  $order[0][ 'ID' ]
            ]);
        }

        if ( ! empty( $data[ 'RESTAURANT_ORDER_STATUS' ] ) && ! empty( $data[ 'RESTAURANT_ORDER_TYPE' ] ) ) {
            $order_details[ 'RESTAURANT_ORDER_TYPE' ]           =   @$data[ 'RESTAURANT_ORDER_TYPE' ];
            $order_details[ 'RESTAURANT_ORDER_STATUS' ]         =   @$data[ 'RESTAURANT_ORDER_STATUS' ];
        }
       

        // $order_details[ 'RESTAURANT_TABLE_ID' ]     =   @$metas[ 'table_id' ]  ?? @$metas[ 'nexostore_table_id' ]  ?? 0;

        $order_details[ 'RESTAURANT_TABLE_ID' ]     =  @$data['RESTAURANT_TABLE_ID']?? @$data['RESTAURANT_TABLE_ID']??@$metas[ 'table_id' ]  ?? @$metas[ 'nexostore_table_id' ]  ?? 0;
        
        $order_details[ 'RESTAURANT_SEAT_USED' ]    =   @$metas[ 'seat_used' ] ?? @$metas[ 'nexostore_table_seats' ] ?? 0;

        if ( @$metas[ 'nexostore_datetime' ] ) {
            $order_details[ 'RESTAURANT_BOOKED_FOR' ]     =    @$metas[ 'nexostore_datetime' ];
        }

        return $order_details;
    }

    /**
     * Put order details
     * @param array order details
     * @return array
    **/

    public function put_order_details( $order_details, $data, $order_id ) 
    {
        // check if the order is still editable
        $this->load->module_model( 'gastro', 'Nexo_Gastro_Tables_Models', 'gastro_model' );
        $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'nexo_orders' );
        $order              =   get_instance()->nexo_orders->get( $order_id );
        $table_used         =   get_instance()->gastro_model->get_table_used( $order_id );

        if( $order ) {
            if( 
                ( 
                    ! in_array( $order[ 'RESTAURANT_ORDER_STATUS' ], [ 'hold', 'pending' ]) && 
                    store_option( 'disable_kitchen_screen', 'no' ) === 'no'
                ) && 
                @$_GET[ 'from-pos-waiter-screen' ] == null 
            ) {
                return response()->httpCode( 403 )->json(array(
                    'message'   =>  __( 'Unable to edit this order, the cooking proceess may have started or the order is now "Ready".', 'gastro' ),
                    'status'    =>  'failed'
                ) );
            }
        } else {
            return response()->httpCode( 403 )->json(array(
                'message'   =>  __( 'Unable to identify this order.', 'gastro' ),
                'status'    =>  'failed'
            ) );
        }

        if( $order[ 'RESTAURANT_ORDER_TYPE' ] == 'dinein' ) {
            // set previous table as available.
            // if( $table_used ) {
            //     $result                     =   get_instance()->gastro_model->table_status([
            //         'ORDER_ID'              =>  $order_id,
            //         'STATUS'                =>  'available',
            //         'CURRENT_SESSION_ID'    =>  @$table_used[0][ 'CURRENT_SESSION_ID' ],
            //         'TABLE_ID'              =>  @$table_used[0][ 'ID' ],
            //         'CURRENT_SEATS_USED'    =>  0,
            //     ]);

            //     // Remove order form intory in order to avoid same order on multiple history
            //     get_instance()->gastro_model->unbind_order(
            //         $order_id, 
            //         $table_used[0][ 'ID' ]
            //     );
            // }
        }

        if ( ! empty( $data[ 'RESTAURANT_ORDER_STATUS' ] ) && ! empty( $data[ 'RESTAURANT_ORDER_STATUS' ] ) ) {
            $order_details[ 'RESTAURANT_ORDER_TYPE' ]           =   @$data[ 'RESTAURANT_ORDER_TYPE' ];
            $order_details[ 'RESTAURANT_ORDER_STATUS' ]         =   @$data[ 'RESTAURANT_ORDER_STATUS' ];
        }

        return $order_details;
    }

    /**
     * Fitler Item  Name to inject modifiers
     * @param array data
     * @return array formated data
    **/

    public function receipt_after_item_name( $data ) {

        $restaurant_note    =   $data[ 'item' ][ 'RESTAURANT_FOOD_NOTE' ];

        $string                 =   '';
        if( $restaurant_note ) {
            $string         .=  '<strong>' . __( 'Note : ', 'gastro' ) . '</strong>' . $restaurant_note . '<br>';
        }

        $metas                  =   json_decode( $data[ 'item' ][ 'RESTAURANT_FOOD_MODIFIERS' ], true );
        if( $metas ) {
            foreach( $metas as $meta ) {
                if($meta['default']=="1"){
                $string             .=  '&mdash; ' . $meta[ 'name' ] . '<br> + ' . get_instance()->Nexo_Misc->cmoney_format( floatval( $meta[ 'price' ] ) * $data[ 'item' ][ 'QUANTITE' ] ) . '<br>';
                    }
            }
        }

        $data[ 'output' ]       =   $string;
        return $data;
    }

    /**
     * Filter item name
     * @param int current price
     * @return int formated price
    **/

    public function receipt_filter_item_price( $item_price ) {
        global $current_item;
        $raw_meta           =   $this->db->where( 'REF_COMMAND_PRODUCT', $current_item[ 'ITEM_ID' ])
        ->where( 'KEY', 'modifiers' )
        ->where( 'REF_COMMAND_CODE', $current_item[ 'CODE' ])
        ->get( store_prefix() . 'nexo_commandes_produits_meta' )
        ->result_array();
        
        //var_dump( $current_item );

        if( $raw_meta ) {
            $metas                  =   json_decode( $raw_meta[0][ 'VALUE'], true );
            if( $metas ) {
                $allPrices              =   0;
                foreach( @$metas as $meta ) {
                    $allPrices          +=  floatval( @$meta[ 'price' ] );
                }
                return  $item_price     - $allPrices;
            }
        }
        return $item_price;
    }

    /**
     * Update Real order type
     * @param item
     * @return item
    **/

    public function put_order_item( $data, $item )
    {
        $barcode    =   explode( '-barcode-', $item[ 'codebar' ]);
        // for item which doesn't have a modifier
        $data[ 'RESTAURANT_PRODUCT_REAL_BARCODE' ]      =   $item[ 'codebar' ];
        if( @$barcode[1] ) {
            $data[ 'RESTAURANT_PRODUCT_REAL_BARCODE' ]  =   $barcode[1]; // after "-barcode-";
        }

        $data[ 'RESTAURANT_FOOD_NOTE' ]                 =   @$item[ 'restaurant_food_note' ] ?? '';
        $data[ 'RESTAURANT_FOOD_PRINTED' ]              =   @$item[ 'PRODUCT_SKIP_PRINT' ]=='no' ? 1:0;


        //$data[ 'RESTAURANT_FOOD_STATUS' ]               =   @$item[ 'restaurant_food_status' ] ?? '';
       // $data[ 'RESTAURANT_FOOD_MODIFIERS' ]            =   json_encode( @$item[ 'restaurant_food_modifiers' ] ) ?? '';


        $data[ 'RESTAURANT_FOOD_STATUS' ]               =   $item[ 'restaurant_food_status' ]==''?@$item[ 'RESTAURANT_FOOD_STATUS' ]?? '':@$item[ 'restaurant_food_status' ] ?? '';
        $data[ 'RESTAURANT_FOOD_MODIFIERS' ]            =   $item[ 'restaurant_food_modifiers' ]==''?@$item[ 'RESTAURANT_FOOD_MODIFIERS' ]?? '':json_encode( @$item[ 'restaurant_food_modifiers' ] ) ?? '';

        $data[ 'RESTAURANT_FOOD_ISSUE' ]                =   @$item[ 'restaurant_food_issue' ] ?? '';
        $data[ 'RESTAURANT_PRODUCT_CATEGORY' ]          =   @$item[ 'category_id' ] ?? '';

        return $data;
    }

    /**
     * Save Real order type
     * @param item
     * @return item
    **/

    public function post_order_item( $data, $item )
    {
        $barcode    =   explode( '-barcode-', $item[ 'codebar' ]);
        // for item which doesn't have a modifier
        $data[ 'RESTAURANT_PRODUCT_REAL_BARCODE' ]      =   $item[ 'codebar' ];
        if( @$barcode[1] ) {
            $data[ 'RESTAURANT_PRODUCT_REAL_BARCODE' ]  =   $barcode[1]; // after "-barcode-";
        }

        $data[ 'RESTAURANT_FOOD_NOTE' ]                 =   @$item[ 'restaurant_food_note' ] ?? '';
        $data[ 'RESTAURANT_FOOD_PRINTED' ]              =   @$item[ 'PRODUCT_SKIP_PRINT' ]=='no' ? 1:0;
        $data[ 'RESTAURANT_FOOD_STATUS' ]               =   @$item[ 'restaurant_food_status' ] ?? '';
        $data[ 'RESTAURANT_FOOD_MODIFIERS' ]            =   json_encode( @$item[ 'restaurant_food_modifiers' ] ) ?? '';
        $data[ 'RESTAURANT_FOOD_ISSUE' ]                =   @$item[ 'restaurant_food_issue' ] ?? '';
        $data[ 'RESTAURANT_PRODUCT_CATEGORY' ]          =   @$item[ 'category_id' ] ?? '';

        return $data;
    }

    /**
     * Allowed order for print
     * @param array order
     * @return array order type
     */
    public function allowed_order_for_print( $order_type )
    {
        $order_type[]   =   'nexo_order_devis';
        return $order_type;
    }

    /**
     * Put order response
     * @param array current response
     * @param array array of proceeced that
     */
    public function put_order_response( $response, $data ) 
    {
        $this->load->module_model( 'gastro', 'Nexo_Gastro_Tables_Models', 'gastro_model' );
        // a table might not always be provided. But in case it's provided, then we should return it
        $table                              =   $this->gastro_model->get_table_used( $response[ 'order_id' ] );
        if ( $table ) {
            $table                              =   $table[0];
            $response[ 'table_id' ]             =   $table[ 'ID' ];
        }

        $response[ 'restaurant_type' ]          =   $data[ 'current_order' ][ 'RESTAURANT_ORDER_TYPE' ];
        $response[ 'restaurant_status' ]        =   $data[ 'current_order' ][ 'RESTAURANT_ORDER_STATUS' ];
        
      
        $response[ 'request_type' ]             =   'PUT';
        
        if($data[ 'current_order' ][ 'RESTAURANT_ORDER_STATUS']) {
            
             $current_order          =    $this->db->where('CODE', $data[ 'current_order' ][ 'CODE' ])
            ->update( store_prefix() . 'nexo_commandes',['RESTAURANT_ORDER_STATUS'=>'pending']);
        

                       $this->db->where( 'ID',  $data[ 'current_order' ][ 'RESTAURANT_TABLE_ID' ] )
                        ->update( store_prefix() . 'nexo_restaurant_tables', [
                            'STATUS'                =>  'in_use'
                        ]);
            
          
            
        }  
        
    
     
        return $response;
    }

    /**
     * Post order response
     * @param array current response
     * @param array array of proceeced that
     */
    public function post_order_response( $response, $data ) 
    {
        $this->load->module_model( 'gastro', 'Nexo_Gastro_Tables_Models', 'gastro_model' );
        // a table might not always be provided. But in case it's provided, then we should return it
        $table                                  =   $this->gastro_model->get_table_used( $response[ 'order_id' ] );
        
        if ( $table ) {
            $table                              =   $table[0];
            $response[ 'table_id' ]             =   $table[ 'ID' ];
        }

        $response[ 'restaurant_type' ]          =   $data[ 'current_order' ][ 'RESTAURANT_ORDER_TYPE' ];
        $response[ 'restaurant_status' ]        =   $data[ 'current_order' ][ 'RESTAURANT_ORDER_STATUS' ];
        $response[ 'request_type' ]             =   'POST';
        
        if($data[ 'current_order' ][ 'RESTAURANT_ORDER_STATUS' ]=='hold'){
            
              $tables_history_unpaid = $this->db->select('*')->from( store_prefix() . 'nexo_restaurant_tables_relation_orders' )->join( store_prefix() . 'nexo_commandes', store_prefix() . 'nexo_restaurant_tables_relation_orders.REF_ORDER = ' . store_prefix() . 'nexo_commandes.ID' )
                ->where( store_prefix() . 'nexo_restaurant_tables_relation_orders.REF_TABLE', $table[ 'ID' ])
                ->where( store_prefix() . 'nexo_commandes.RESTAURANT_ORDER_STATUS!=','hold')
                ->where( store_prefix() . 'nexo_commandes.PAYMENT_TYPE','unpaid')->get()->result_array();



                if(empty($tables_history_unpaid)){    

                    
                    $this->db->where( 'ID',  $table[ 'ID' ])
                        ->update( store_prefix() . 'nexo_restaurant_tables', [
                            'STATUS'                =>  'available'
                        ]);

                }
        }

        return $response;
    }

    /**
     * Filter loaded order for modification
     * @param object order
     * @return object
     */
    public function loaded_order( $order )
    {
        if( ! $order ) {
            return $order;
        }
        
        $this->load->module_model( 'gastro', 'Nexo_Gastro_Tables_Models', 'gastro_model' );
        $table          =   get_instance()->gastro_model->get_table_used( $order[ 'order' ][0][ 'ORDER_ID' ] );
        $area           =   [];

        if( $table ) {
            $area           =   get_instance()->gastro_model->get_table_area( @$table[0][ 'ID' ] );
        }

        $order[ 'order' ][0][ 'USED_TABLE' ]    =   ( array ) @$table[0];
        $order[ 'order' ][0][ 'USED_AREA' ]     =   $area;
        return $order;
    }

    /**
     * Send order to kitchen directly from order list
     * @param object crud
     * @return void
     */
    public function grocery_row_actions_output( $filter, $row )
    {
        global $PageNow;
        if ( $PageNow === 'nexo/commandes/list' ) {
            $filter     =   '<li><a data-id="' . $row->ID . '" href="javascript:void(0)" class="fa fa-print print-to-kitchen"> ' . __( 'Print to kitchen', 'gastro' ) . '</a></li>';
        }
        return $filter;
    }

    /**
     * Register some script to handle print to kitchen from sale list
     * @param object crud object
     * @return object
     */
    public function nexo_commandes_loaded( $crud )
    {
        $this->events->add_action( 'dashboard_footer', function(){
            get_instance()->load->module_view( 'gastro', 'sales.footer-script' );
        });

        $crud->add_action(
            __( 'Staff History', 'gastro' ),
            '',
            '#',
            'fa fa-eye staff-history'
        );

        return $crud;
    }

    /**
     * Hide or display the discount button
     * @return view/null
     */
    public function cart_discount_button( $button ) 
    {
        if( User::can( 'gastro.view.discountbutton' ) ) {
            return $button;
        }
        return '';
    }

    /**
     * Used to hide "canceled items on the receipt"
     * @param array of items
     * @return array of item filtred
     */
    public function receipt_items( $items )
    {
        if( $items ) {
            $indexToSplice  =   [];
            foreach( $items as $index => &$item ) {
                if( $item[ 'RESTAURANT_FOOD_STATUS'] === 'canceled' ) {
                    $indexToSplice[]    =   $index;
                }
            }

            foreach( $indexToSplice as $index ) {
                unset( $items[ $index ] );
            }
        }
        return $items;
    }

    /**
     * Filter receipt template in order to add more tags
     * @param array
     * @return array of template tags
     */
    public function nexo_filter_receipt_template( $data ) 
    {
        $this->load->module_model( 'gastro', 'Nexo_Gastro_Tables_Models', 'gastro_model' );
        if ( $data ) {
            $order_id   =   $data[ 'order' ][ 'ORDER_ID' ];
            $table      =   $this->gastro_model->get_table_used( $order_id );

            switch( $data[ 'order' ][ 'RESTAURANT_ORDER_TYPE' ] ) {
                case 'delivery'     : $orderType = __( 'Delivery', 'gastro' );break;
                case 'dinein'       : $orderType = __( 'Dine In', 'gastro' );break;
                case 'takeaway'     : $orderType = __( 'Take Away', 'gastro' );break;
                default: $orderType = __( 'Regular', 'gastro' ); break;
            }
            
            if ( $table ) {
                $table_area     =   $this->gastro_model->get_table_area( $table[0][ 'ID' ] );
                $data[ 'template' ][ 'table_name' ]     =   @$table[0][ 'NAME' ];
                $data[ 'template' ][ 'area_name' ]      =   @$table_area[ 'NAME' ];
                $data[ 'template' ][ 'order_type' ]      =   $orderType;
            } else {
                $data[ 'template' ][ 'table_name' ]     =   __( 'N/A', 'gastro' );
                $data[ 'template' ][ 'area_name' ]      =   __( 'N/A', 'gastro' );
                $data[ 'template' ][ 'order_type' ]      =   $orderType;
            }

            /**
             * filter the order id in order to display the daily order
             * number
             */
            if ( preg_match( '/([0-9]+)-([0-9]+)/', $data[ 'order' ][ 'CODE' ], $codes ) ) {
                $data[ 'template' ][ 'daily_number' ]   =   $codes[2];
            } else {
                $data[ 'template' ][ 'daily_number' ]   =   __( 'N/A', 'gastro' );
            }

            return $data;
        }
    }

  public function nexo_filter_receipt_template1( $data ) 
    {
       
        return $data;
    }

    /**
     * Filter Dom tag
     * @param string
     * @return string
     */
    public function nexo_filter_invoice_dom_tag_list( $dom )
    {
        $dom    .=  
         '<h3>' . __( 'A2000 F&B POS Tags', 'gastro' ) . '</h3>' .
        __( '{table_name} to display the table name', 'gastro' ) . '<br>' . 
        __( '{order_type} to display the order type', 'gastro' ) . '<br>' . 
        __( '{daily_number} to display the daily number of an order', 'gastro' ) . '<br>' . 
        __( '{area_name} to display the area name', 'gastro' );

        return $dom;
    }

    /**
     * Nexo Filter items
     * @param array of item
     * @return array of the filtred item
     */
    public function nexo_order_filter_item( $item ) 
    { 
        if ( preg_match( '/(\w+)\-barcode\-(\w+)/', $item[ 'codebar' ], $result ) ) {
            $item[ 'codebar' ]    =   $result[2];
        }
        return $item;
    }

    /**
     * After submit order
     * @param array order details
     * @return array
     */
    public function after_submit_order( $details )
    {
        $this->load->module_model( 'gastro', 'Nexo_Gastro_Tables_Models', 'gastro_model' );

        if ( @$details[ 'data' ][ 'metas' ][ 'seat_used' ] !== null &&  $details[ 'current_order' ][0][ 'RESTAURANT_ORDER_STATUS' ] !== 'booking' ) {
            $result         =   get_instance()->gastro_model->table_status([
                'ORDER_ID'                  =>  $details[ 'current_order' ][0][ 'ID' ],
                'CURRENT_SEATS_USED'        =>  @$details[ 'data' ][ 'metas' ][ 'seat_used' ],
                'TABLE_ID'                  =>  @$details[ 'data' ][ 'metas' ][ 'table_id' ],
                'STATUS'                    =>  'in_use'
            ]);
        }

        return $details;
    }

    /**
     * Filter order posted from WooCommerce
     * @param array order data
     * @return array
     */
    public function woocommerce_post_order_data( $data )
    {
        /**
         * Lets set the 
         */
        $orderType  =   '';
        $tableId    =   0;
        $seatUsed   =   0;
        $metas      =   [];

        if ( isset( $data[ 'raw' ][ 'order' ][ 'meta_data' ] ) ) {
            foreach( $data[ 'raw' ][ 'order' ][ 'meta_data' ] as $meta ) {
                switch( $meta[ 'key' ] ) {
                    case 'nexostore_order_type':
                        $metas[ 'order_type' ]                          =   $meta[ 'value' ];
                        // $metas[ $meta[ 'key' ] ]                        =   $meta[ 'value' ];
                        $orderType                                      =   $meta[ 'value' ];
                        $data[ 'order' ][ 'RESTAURANT_ORDER_TYPE' ]     =   $meta[ 'value' ];
                    break;
                    case 'nexostore_table_id': 
                        $metas[ 'table_id' ]                            =   $meta[ 'value' ];
                        $tableId                                        =   $meta[ 'value' ];
                        $data[ 'order' ][ 'RESTAURANT_TABLE_ID' ]       =   $meta[ 'value' ];
                    break;
                    case 'nexostore_table_seats':
                        $metas[ 'seat_used' ]                           =   $meta[ 'value' ];
                        $seatUsed                                       =   $meta[ 'value' ];
                        $data[ 'order' ][ 'RESTAURANT_SEAT_USED']       =   $meta[ 'value' ];
                    break;
                    case 'nexostore_datetime':
                        $metas[ $meta[ 'key' ] ]                        =   $meta[ 'value' ];
                        ! empty( $meta[ 'value' ] ) ? $data[ 'order' ][ 'RESTAURANT_BOOKED_FOR' ]   =   $meta[ 'value' ] : null;
                    break;
                }
            }
        }

        file_put_contents( FCPATH . '/order.json', json_encode( $data[ 'raw' ][ 'order' ] ) );

        $data[ 'order' ][ 'RESTAURANT_ORDER_STATUS' ]       =   'booking';
        $data[ 'order' ][ 'STATUS' ]                        =   'pending';
        $data[ 'order' ][ 'RESTAURANT_ORDER_TYPE' ]         =   $orderType;
        $data[ 'order' ][ 'metas' ]                         =   array_merge( $data[ 'order' ][ 'metas' ], $metas );

        /**
         * Switching title
         */
        switch( $orderType ) {
            case 'dinein': 
                $data[ 'order' ][ 'TITRE' ]     =   __( 'Dine in Booking', 'gastro' );
            break;
            case 'takeaway': 
                $data[ 'order' ][ 'TITRE' ]     =   __( 'Takeaway Booking', 'gastro' );
            break;
            case 'delivery': 
                $data[ 'order' ][ 'TITRE' ]     =   __( 'Delivery Booking', 'gastro' );
            break;
        }

        return $data;
    }

    /**
     * Filter the items to catchs the modifiers
     * @param array item
     * @return array item
     */
    public function woocommerce_post_item_data( $item ) 
    {
        file_put_contents( FCPATH . '/items-' . $item[ 'raw' ][ 'sku' ] . '.json', json_encode( $item ) );

        $metas         =   [];

        if ( $item[ 'raw' ] && $item[ 'raw' ][ 'nexostore-modifiers' ] ) {

            /**
             * If the nexostore-modifiers is not an array
             * we should set that as an array so that we can loop that 
             * wether it's an array or not
             */
            $modifiers      =   is_array( $item[ 'raw' ][ 'nexostore-modifiers' ] ) ? $item[ 'raw' ][ 'nexostore-modifiers' ] : [ $item[ 'raw' ][ 'nexostore-modifiers' ] ];

            // file_put_contents( FCPATH . '/items-modifier-' . $item[ 'raw' ][ 'sku' ] . '.dump', print_r( $modifiers ) );
            // file_put_contents( FCPATH . '/items-modifier-' . $item[ 'raw' ][ 'sku' ] . '.json', json_encode( $modifiers ) );

            if( ! empty( $modifiers ) ) {
                foreach( $modifiers as $group ) {
                    foreach( $group[ 'modifiers' ] as $modifier ) {
                        $metas[]    =   [
                            'name'                  =>  $modifier[ 'name' ],
                            'selected'              =>  'yes', // all modifiers provided are already selected
                            'id'                    =>  $modifier[ 'gastro_id' ],
                            'price'                 =>  $modifier[ 'price' ],
                            'default'               =>  @$modifier[ 'default' ], // NexoStore should send this
                            'category'              =>  @$modifier[ 'gastro_group_id' ], // this should actually be the id of the group
                            'image'                 =>  $modifier[ 'thumbnail' ],
                            'group_forced'          =>  @$modifier[ 'group_forced' ], // NexoStore should send this
                            'group_multiselect'     =>  @$modifier[ 'group_multiselect' ], // NexoStore should send this
                            'group_name'            =>  $modifier[ 'group_name' ]
                        ];
                    }
                }
            }
        }

        /**
         * Let's save the modifier within the 
         * product metas, by merge array.
         */
        $item[ 'product' ]                  =   array_merge( $item[ 'product' ], [ 
            'restaurant_food_modifiers'     =>  $metas,
            'restaurant_food_note'          =>  '',
            'restaurant_food_printed'       =>  0,
            'restaurant_food_status'        =>  'not_ready',
            'restaurant_food_issue'         =>  ''
        ]);

        return $item;
    }
}



