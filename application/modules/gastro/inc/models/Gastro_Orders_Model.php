<?php

class Gastro_Orders_Model extends Tendoo_Module
{
    public function __construct()
    {
        parent::__construct();

        $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'order_model' );
    }

    public function changeMealsStatus( $meals, $status, $allMeals, $order_code )
    {
        $this->events->do_action( 'gastro_before_change_food_status', [
            'foods'         =>  $meals,
            'state'         =>  $status,
            'order_code'    =>  $order_code,
            'all_foods'     =>  $allMeals
        ]);

        foreach( $meals as $item_id ) {
            $this->db
            ->where( 'ID', $item_id )
            ->update( store_prefix() . 'nexo_commandes_produits', [
                'RESTAURANT_FOOD_STATUS'   =>    $status
            ]);
        }

        $order_foods     =   $this->db
            ->where( 'REF_COMMAND_CODE', $order_code )
            ->get( store_prefix() . 'nexo_commandes_produits' )
            ->result_array();

        $this->events->do_action( 'gastro_change_food_status', [
            'foods'         =>  $meals,
            'state'         =>  $status,
            'order_code'    =>  $order_code,
            'all_foods'     =>  $allMeals
        ]);

        if( $order_foods ) {
            
            $order_is_ready     =   [];
            $order_is_canceled  =   [];
            $order_all_food     =   $allMeals;

            foreach( $order_foods as $food ) {
                if( in_array( $food[ 'RESTAURANT_FOOD_STATUS' ], [ 'ready', 'collected' ] ) ) {
                    $order_is_ready[]   =   true;
                }

                if( in_array( $food[ 'RESTAURANT_FOOD_STATUS' ], [ 'denied', 'canceled', 'issue' ] )  ) {
                    $order_is_canceled[]   =   false;
                }
            }

            
            if( count( $order_is_ready ) == ( count( $order_foods ) - count( $order_is_canceled ) ) ) {
                $status     =   'ready';
            } else if( count( $order_is_canceled ) == count( $order_foods ) ) {
                $status     =   'denied';
            } else {
                if( count( $order_is_ready ) > 0 ) {
                    $status     =   'partially';
                } else {
                    $status     =   'ongoing';
                }
            }

            // update if it's ready
            $this->db->where( 'CODE', $order_code )
            ->update( store_prefix() . 'nexo_commandes', [
                'RESTAURANT_ORDER_STATUS'      =>   $status,
            ]);

            $this->events->do_action( 'gastro_change_order_status', [
                'status'        =>  $status,
                'order_code'    =>  $order_code,
                'is_ready'      =>  $order_is_ready,
                'is_canceled'   =>  $order_is_canceled
            ]);

            // if order is ready we should send a notification
            if( count( $order_is_ready ) == count( $order_foods ) ) {
                nexo_notices([
                    'user_id'       =>  User::id(),
                    'link'          =>  site_url([ 'dashboard', store_slug(), 'nexo', 'orders' ]),
                    'icon'          =>  'fa fa-cutlery',
                    'type'          =>  'text-success',
                    'message'       =>  sprintf( __( 'The order <strong>%s</strong> is ready', 'nexo' ), $order_code )
                ]);
            }
        }
    }

    public function getUnprintedOrderItems( $order_id )
    {
        $order  =   $this->order_model->get( $order_id );

        return $this->db->where( 'REF_COMMAND_CODE', $order[ 'CODE' ] )
            ->where( 'RESTAURANT_FOOD_PRINTED', '0' )
            ->get( store_prefix() . 'nexo_commandes_produits' )
            ->result_array();
    }

    public function getPrintedOrderItems( $order_id )
    {
        $order  =   $this->order_model->get( $order_id );

        return $this->db->where( 'REF_COMMAND_CODE', $order[ 'CODE' ] )
            ->where( 'RESTAURANT_FOOD_PRINTED', '1' )
            ->get( store_prefix() . 'nexo_commandes_produits' )
            ->result_array();
    }

    public function setAsPrinted( $order_product_id )
    {
        return $this->db->where( 'ID', $order_product_id )
            ->update( store_prefix() . 'nexo_commandes_produits', [
                'RESTAURANT_FOOD_PRINTED'   =>  1
            ]);
    }
}