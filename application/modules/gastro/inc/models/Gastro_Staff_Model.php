<?php
class Gastro_Staff_Model extends Tendoo_Module
{
    public function recordStaffAction( $order_id, $action, $user_id )
    {
        $user   =   is_int( $user_id ) ? User::get( $user_id ) : $user_id; // assume it can be array
        $data   =   [
            'AUTHOR'        =>  $user_id,
            'ACTION'        =>  $action,
            'REF_ORDER'     =>  $order_id,
            'DATE_CREATION' =>  date_now(),
        ];
        $this->db->insert( store_prefix() . 'nexo_restaurant_orders_staff_actions', $data );
        $data[ 'ID' ]   =   $this->db->insert_id();
        return [
            'status'    =>  'success',
            'message'   =>  __( 'The staff action has been registered', 'gastro' ),
            'data'      =>  $data
        ];
    }
    public function getStaffActions( $order_id )
    {
        return collect( $this->db->where( 'REF_ORDER', $order_id )
            ->get( store_prefix() . 'nexo_restaurant_orders_staff_actions' )
            ->result_array() )
            ->map( function( $action ) {
                $action[ 'user' ]           =   User::get( $action[ 'AUTHOR' ] );
                // $action[ 'user' ]->role     =   get_instance()->auth->get_user_groups( $action[ 'AUTHOR' ] )[0];
                return $action;
            });
    }

    public function deleteActionsOnOrder( $order_id )
    {
        $this->db->where( 'REF_ORDER', $order_id )
            ->delete( store_prefix() . 'nexo_restaurant_orders_staff_actions' );

        return [
            'status'    =>  'failed',
            'message'   =>  __( 'The staff actions has been deleted.', 'gastro' )
        ];
    }
} 