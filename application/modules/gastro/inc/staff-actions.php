<?php

class Nexo_Restaurant_Staff_Actions extends Tendoo_Module
{
    public function __construct()
    {
        parent::__construct();
    }

    public function recordNewItems( $order, $data )
    {   
        $this->load->module_model( 'gastro', 'Gastro_Staff_Model', 'staff_model' );

        extract( $data );
        /**
         * @param array order
         * @param array data
         */

        $this->staff_model->recordStaffAction(
            $order[ 'ID' ],
            sprintf( 
                __( 'Has added %s product(s).', 'gastro' ),
                count( $data[ 'ITEMS' ] )
            ),
            User::id()
        );
    }

    public function recordNewOrder( $details )
    {
        $this->load->module_model( 'gastro', 'Gastro_Staff_Model', 'staff_model' );

        $this->staff_model->recordStaffAction(
            $details[ 'current_order' ][0][ 'ID' ],
            __( 'Has created the order.', 'gastro' ),
            User::id()
        );

        return $details;
    }

    public function recordFoodStatusChange( $details )
    {
        extract( $details );
        /**
         * @param array foods
         * @param array state
         * @param array order_code
         * @param array all_foods
         */

        $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'orders_model' );
        $this->load->module_model( 'gastro', 'Gastro_Staff_Model', 'staff_model' );

        $order      =   $this->orders_model->get( $order_code, 'CODE' );

        if ( ! empty( $order ) ) {
            $this->staff_model->recordStaffAction(
                $order[ 'ID' ],
                sprintf( 
                    __( 'Has has changed %s meal(s) status to %s.', 'gastro' ),
                    count( $foods ),
                    $state
                ),
                User::id()
            );
        }
    }

    public function recordCollectOrder( $order )
    {
        $this->load->module_model( 'gastro', 'Gastro_Staff_Model', 'staff_model' );
        $this->staff_model->recordStaffAction(
            $order[ 'ID' ],
            __( 'Has collected the order.', 'gastro' ),
            User::id()
        );
    }

    public function recordStartCooking( $data ) 
    {
        extract( $data );
        /**
         * @param string order_code
         * @param array items id
         */

        $this->load->module_model( 'gastro', 'Gastro_Staff_Model', 'staff_model' );
        $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'orders_model' );

        $order      =   $this->orders_model->get( $order_code, 'CODE' );

        $this->staff_model->recordStaffAction(
            $order[ 'ID' ],
            sprintf( 
                __( 'Has started cooking %s item(s).', 'gastro' ),
                count( $items )
            ),
            User::id()
        );
    }

    public function recordMovedOrder( $data ) 
    {
        extract( $data );
        /**
         * @param int order_id
         * @param int previous_table_id
         * @param int new_table_id
         */

        $this->load->module_model( 'gastro', 'Gastro_Staff_Model', 'staff_model' );
        $this->load->module_model( 'gastro', 'Nexo_Gastro_Tables_Models', 'table_model' );

        $fromTable      =   $this->table_model->get_table( $previous_table_id );
        $toTable      =   $this->table_model->get_table( $new_table_id );

        $this->staff_model->recordStaffAction(
            $order_id,
            sprintf( 
                __( 'Has moved the order from %s to %s.', 'gastro' ),
                $fromTable[ 'NAME' ],
                $toTable[ 'NAME' ]
            ),
            User::id()
        );
    }

    public function recordCancelItem( $item, $order )
    {
        $this->load->module_model( 'gastro', 'Gastro_Staff_Model', 'staff_model' );

        $this->staff_model->recordStaffAction(
            $order[0][ 'ID' ],
            sprintf( 
                __( 'Has canceled the item %s.', 'gastro' ),
                $item[0][ 'NAME' ]
            ),
            User::id()
        );
    }

    public function recordPayment( $details )
    {
        extract( $details );
        /**
         * @param array order
         * @param int amount
         * @param string action
         * @param string reason
         */

        $this->load->module_model( 'gastro', 'Gastro_Staff_Model', 'staff_model' );
        $this->load->model( 'Nexo_Misc' );

        $this->staff_model->recordStaffAction(
            $order[ 'ID' ],
            sprintf( 
                __( 'Has saved a payment for %s.', 'gastro' ),
                $this->Nexo_Misc->cmoney_format( $amount )
            ),
            User::id()
        );
    }

    public function deleteStaffActionsOnOrder( $response, $order )
    {
        $this->staff_model->deleteActionsOnOrder( $order[ 'ID' ] );
    }
} 