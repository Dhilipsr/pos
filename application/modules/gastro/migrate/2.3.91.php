<?php
$this->load->model( 'Nexo_Stores' );

use Carbon\Carbon;

$stores         =   $this->Nexo_Stores->get();

array_unshift( $stores, [
    'ID'        =>  0
]);

foreach( $stores as $store ) {
    $store_prefix       =   $store[ 'ID' ] == 0 ? '' : 'store_' . $store[ 'ID' ] . '_';
    $columns            =   $this->db->list_fields( $store_prefix . 'nexo_articles' );

    if( ! in_array( 'PRODUCT_SKIP_COOKING', $columns ) ) {
        $this->db->query( 'ALTER TABLE `'. $this->db->dbprefix . $store_prefix .'nexo_articles` ADD `PRODUCT_SKIP_COOKING` varchar(3) NOT NULL AFTER `REF_MODIFIERS_GROUP`;');
    }
} 