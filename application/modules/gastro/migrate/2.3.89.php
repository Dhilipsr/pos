<?php
$this->load->model( 'Nexo_Stores' );

use Carbon\Carbon;

$stores         =   $this->Nexo_Stores->get();

array_unshift( $stores, [
    'ID'        =>  0
]);

foreach( $stores as $store ) {
    $store_prefix       =   $store[ 'ID' ] == 0 ? '' : 'store_' . $store[ 'ID' ] . '_';
    $columns            =   $this->db->list_fields( $store_prefix . 'nexo_commandes' );

    $this->db->query('CREATE TABLE IF NOT EXISTS `'. $this->db->dbprefix . $store_prefix .'nexo_restaurant_orders_staff_actions` (
        `ID` int(11) NOT NULL AUTO_INCREMENT,
        `AUTHOR` int(11) NOT NULL,
        `ACTION` varchar(200) NOT NULL,
        `REF_ORDER` int(11) NOT NULL,
        `DATE_CREATION` datetime NOT NULL,
        `DATE_MODIFICATION` datetime NOT NULL,
        PRIMARY KEY (`ID`)
    )');
} 