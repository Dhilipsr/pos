<?php
$this->load->model( 'Nexo_Stores' );

use Carbon\Carbon;

$stores         =   $this->Nexo_Stores->get();

array_unshift( $stores, [
    'ID'        =>  0
]);

foreach( $stores as $store ) {
    $store_prefix       =   $store[ 'ID' ] == 0 ? '' : 'store_' . $store[ 'ID' ] . '_';
    $columns            =   $this->db->list_fields( $store_prefix . 'nexo_commandes_produits' );

    if( ! in_array( 'RESTAURANT_FOOD_PRINTED', $columns ) ) {
        $this->db->query( 'ALTER TABLE `'. $this->db->dbprefix . $store_prefix .'nexo_commandes_produits` ADD `RESTAURANT_FOOD_PRINTED` INT(1) DEFAULT 0 NOT NULL AFTER `RESTAURANT_FOOD_STATUS`;');
    }
} 