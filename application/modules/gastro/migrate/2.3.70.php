<?php
$this->load->model( 'Nexo_Stores' );

use Carbon\Carbon;

$stores         =   $this->Nexo_Stores->get();

array_unshift( $stores, [
    'ID'        =>  0
]);

foreach( $stores as $store ) {
    $store_prefix       =   $store[ 'ID' ] == 0 ? '' : 'store_' . $store[ 'ID' ] . '_';
    $columns            =   $this->db->list_fields( $store_prefix . 'nexo_commandes' );

    if( ! in_array( 'RESTAURANT_TABLE_ID', $columns ) ) {
        $this->db->query( 'ALTER TABLE `'. $this->db->dbprefix . $store_prefix .'nexo_commandes` ADD `RESTAURANT_TABLE_ID` int(11) NOT NULL AFTER `RESTAURANT_ORDER_STATUS`;');
    }

    if( ! in_array( 'RESTAURANT_SEAT_USED', $columns ) ) {
        $this->db->query( 'ALTER TABLE `'. $this->db->dbprefix . $store_prefix .'nexo_commandes` ADD `RESTAURANT_SEAT_USED` int(11) NOT NULL AFTER `RESTAURANT_ORDER_STATUS`;');
    }

    $columns            =   $this->db->list_fields( $store_prefix . 'nexo_commandes_produits' );

    if( ! in_array( 'RESTAURANT_FOOD_NOTE', $columns ) ) {
        $this->db->query( 'ALTER TABLE `'. $this->db->dbprefix . $store_prefix .'nexo_commandes_produits` ADD `RESTAURANT_FOOD_NOTE` text NOT NULL AFTER `ID`;');
    }

    if( ! in_array( 'RESTAURANT_FOOD_STATUS', $columns ) ) {
        $this->db->query( 'ALTER TABLE `'. $this->db->dbprefix . $store_prefix .'nexo_commandes_produits` ADD `RESTAURANT_FOOD_STATUS` varchar(200) NOT NULL AFTER `ID`;');
    }

    if( ! in_array( 'RESTAURANT_FOOD_ISSUE', $columns ) ) {
        $this->db->query( 'ALTER TABLE `'. $this->db->dbprefix . $store_prefix .'nexo_commandes_produits` ADD `RESTAURANT_FOOD_ISSUE` varchar(200) NOT NULL AFTER `ID`;');
    }

    if( ! in_array( 'RESTAURANT_FOOD_MODIFIERS', $columns ) ) {
        $this->db->query( 'ALTER TABLE `'. $this->db->dbprefix . $store_prefix .'nexo_commandes_produits` ADD `RESTAURANT_FOOD_MODIFIERS` text NOT NULL AFTER `ID`;');
    }

    if( ! in_array( 'RESTAURANT_PRODUCT_CATEGORY', $columns ) ) {
        $this->db->query( 'ALTER TABLE `'. $this->db->dbprefix . $store_prefix .'nexo_commandes_produits` ADD `RESTAURANT_PRODUCT_CATEGORY` int(11) NOT NULL AFTER `ID`;');
    }
}

echo json_encode([
    'status'    =>  'success',
    'message'   =>  __( 'Migration has run' ),
    'data'      =>  [
        'url'       =>  site_url([ 'api', 'gastro', 'migration' ]),
        'title'     =>  __( 'Update Current Orders' ),
        'version'   =>  '2.3.70',
        'handle'    =>  'orders',
        'class'     =>  'update-products',
        'offset'    =>  0
    ]
]);

return false;