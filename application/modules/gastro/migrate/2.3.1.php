<?php

$this->load->model( 'Nexo_Stores' );

$stores         =   $this->Nexo_Stores->get();

array_unshift( $stores, [
    'ID'        =>  0
]);

foreach( $stores as $store ) {
}

$permissions        =   [];
$permissions[ 'gastro.view.paybutton' ]             =   __( 'View the button "Pay"', 'gastro' );
$permissions[ 'gastro.view.discountbutton' ]        =   __( 'View the discount "Button"', 'gastro' );
$permissions[ 'gastro.use.merge-meal' ]             =   __( 'View the discount "Button"', 'gastro' );

foreach( $permissions as $namespace => $perm ) {

    if( get_instance()->auth->get_perm_id( $namespace ) == null ) {
        get_instance()->auth->create_perm( 
            $namespace,
            $perm
        );
    }
    
    get_instance()->auth->allow_group( 'master', $namespace );
    get_instance()->auth->allow_group( 'admin', $namespace );
    get_instance()->auth->allow_group( 'store.manager', $namespace );
    get_instance()->auth->allow_group( 'store.demo', $namespace );
    get_instance()->auth->allow_group( 'gastro.waiter', $namespace );
}