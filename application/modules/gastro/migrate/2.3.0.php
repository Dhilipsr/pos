<?php

$this->load->model( 'Nexo_Stores' );

$stores         =   $this->Nexo_Stores->get();

array_unshift( $stores, [
    'ID'        =>  0
]);

foreach( $stores as $store ) {
}

if( ! get_instance()->auth->get_group_id( 'gastro.chief' ) ) {
    Group::create(
        'gastro.chief',
        __( 'Chief', 'gastro' ),
        true,
        __( 'This role can manage the kitchen', 'gastro' )
    );
}

if( ! get_instance()->auth->get_group_id( 'gastro.waiter' ) ) {
    Group::create(
        'gastro.waiter',
        __( 'Waiter', 'gastro' ),
        true,
        __( 'This role can manage the order', 'gastro' )
    );
}

$permissions        =   [];
$permissions[ 'gastro.view.tokitchen' ]         =   __( 'View the button "to kitchen"', 'gastro' );
$permissions[ 'gastro.use.waiter-screen' ]      =   __( 'Use the waiter screen', 'gastro' );
$permissions[ 'gastro.use.kitchen-screen' ]     =   __( 'Use the kitchen screen', 'gastro' );
$permissions[ 'gastro.use.merge-meal' ]         =   __( 'Use the merging meal feature', 'gastro' );

foreach( $permissions as $namespace => $perm ) {

    if( get_instance()->auth->get_perm_id( $namespace ) == null ) {
        get_instance()->auth->create_perm( 
            $namespace,
            $perm
        );
    }
    
    get_instance()->auth->allow_group( 'master', $namespace );
    get_instance()->auth->allow_group( 'admin', $namespace );
    get_instance()->auth->allow_group( 'store.manager', $namespace );
    get_instance()->auth->allow_group( 'store.demo', $namespace );
}

get_instance()->auth->allow_group( 'gastro.chief', 'gastro.use.kitchen-screen' );
get_instance()->auth->allow_group( 'gastro.waiter', 'gastro.use.waiter-screen' );
get_instance()->auth->allow_group( 'gastro.waiter', 'gastro.view.tokitchen' );