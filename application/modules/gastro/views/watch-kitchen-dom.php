<div class="row" ng-controller="watchRestaurantCTRL" ng-cloak="">
    <div class="<?php echo store_option( 'gastro_show_booked_at_kitchen', 'no' ) === 'yes' ? 'col-md-8 col-lg-9 col-sm-6  col-xs-12' : 'col-md-12 col-sm-12 col-xs-12';?>">
    <!--  <div class="container">   --> 
        <div class="row ">
        
            <div class=" col col-md-4 col-sm-12 col-xs-12 col-lg-3 kitchen-box " ng-repeat="( column_index, column ) in columns  | orderBy:randomSort">
                <div class="box box-solid {{ testOrderWaitingTime( order ) }} " id="{{order.CODE}}" ng-repeat="( order_index, order ) in column track by $index" ng-hide="hideOrder( order )"  >
                    <div class="box-header with-border">
                        <<?php echo store_option( 'kitchen_card_title', 'strong' );?> style="margin:0">
                            <span style="float:left;font-size: 20px;">#{{ order.CODE }}</span>

                            <span  ng-if="order.RESTAURANT_ORDER_TYPE == 'dinein'" style="float:left;margin-left:20%;font-size: 17px;display: none !important; ">TABLE {{ order.RESTAURANT_TABLE_ID }}</span>
                            <span style="float:right" ng-show="order.RESTAURANT_ORDER_TYPE =='dinein'">
                                <?php echo __( 'Dine In', 'gastro' );?>
                            </span>
                            <span style="float:right" ng-show="order.RESTAURANT_ORDER_TYPE == 'takeaway'">
                                <?php echo __( 'Take Away', 'gastro' );?>
                            </span>
                            <span style="float:right" ng-show="order.RESTAURANT_ORDER_TYPE == 'delivery'">
                                <?php echo __( 'Delivery', 'gastro' );?>
                            </span>
                        </<?php echo store_option( 'kitchen_card_title', 'strong' );?>>
                    </div>


                    
                        

                     <div  class="box-header with-border">
                           <strong><?php echo sprintf( __( 'By %s', 'gastro' ), '{{ ucwords( structuredUsers[ order.AUTHOR ].user_name ) }}' );?></strong> 
                           <!-- (<span am-time-ago="order.DATE_MOD  | amParse: 'YYYY-MM-DD HH:mm:ss' "></span>) -->
                        </<?php echo store_option( 'kitchen_card_title', 'strong' );?>>

                        <span class="pull-right">
                            <span class="order-status">{{ getOrderStatus( order ) }}</span>
                        </span>
                        
                    </div>

                
                    
                    <!-- <div class="box-header with-border">
                    
                        

                       
                      
                    </div> -->
                    <div ng-if="order.RESTAURANT_ORDER_TYPE == 'dinein'" class="box-header with-border text-center" style="font-size: 16px;">
                        <?php if( store_option( 'disable_meal_feature' ) != 'yes' ):?>
                        <strong><?php echo __( 'Area', 'gastro' );?></strong> : {{ 
                            structuredAreas[ structuredTables[ order.RESTAURANT_TABLE_ID ].REF_AREA ].NAME    
                        }} >
                        <?php endif;?>
                        <strong><?php echo __( 'Table', 'gastro' );?></strong> : {{ structuredTables[ order.RESTAURANT_TABLE_ID ].NAME }}
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="{{ order.meals.length > 1 ? 'col-md-6' : 'col-md-12' }} flex-fill" ng-repeat="( meal_code, meal ) in order.meals track by $index">
                                <ul class="list-group" style="margin-bottom:0">
                                    <!-- ng-if="categoryCheck( item )"  -->
                                    <!-- " -->
                                    <!-- <li class="list-group-item" ng-hide="meal_code == 'null'">
                                        <strong><?php echo __( 'Meal', 'gastro' );?></strong> : {{ meal_code }}
                                    </li> -->

                                    <li style="color:white;background-color:{{ testOrderWaitingTime( food.RESTAURANT_FOOD_STATUS ) }} ;font-size: <?php echo store_option( 'kitchen_item_font', 16 );?>px" ng-repeat="( food_index, food ) in meal track by $index" ng-click="selectItem( food )" class="info list-group-item {{ food.active == true ? 'active' : '' }}" >
                                        <span class="badge">{{ getFoodStatus(order,food.RESTAURANT_FOOD_STATUS ) }}</span>
                                        <span class="badge" style="color: red;background-color: white;" ng-if="food.isnew=='yes'">New item</span>
                                      
                                        <span style="width: 10%;">{{ food.DESIGN == null ? food.NAME : food.DESIGN }} {{ food.MEAL }} (x{{ food.QTE_ADDED }})</span> <br>
                                        <p class="restaurant-note" ng-show="modifier.default == 1" ng-repeat="modifier in food.RESTAURANT_FOOD_MODIFIERS track by $index" style="color: black;">
                                            + <strong>{{ modifier.group_name }}</strong> : {{ modifier.name }}
                                        </p>
                                        <p class="restaurant-note" ng-show="food.RESTAURANT_FOOD_NOTE != null && food.RESTAURANT_FOOD_NOTE != ''" style="color: black;"><strong><?php echo __( 'Note', 'gastro' );?></strong>: {{ food.RESTAURANT_FOOD_NOTE }}</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div ng-if="orders.length == 0" class="list-group">
                            <a href="#" class="list-group-item warning"><?php echo __( 'No order for this kitchen', 'gastro' );?></a>
                        </div>
                    </div>
                    <div class="box-footer" style="display: none !important;">
                        <div class="row" style="">
                            <div class="col-md-7">
                                <div class="btn-group">
                                    <!-- <div class="btn-group ">
                                        <button ng-click="selectAllItems( order )" class="btn btn-default btn-sm"><?php echo __( 'Select All', 'gastro' );?></button>
                                    </div>
                                    <div class="btn-group ">
                                        <button ng-click="unselectAllItems( order )" class="btn btn-default btn-sm"><?php echo __( 'Unselect All', 'gastro' );?></button>
                                    </div> -->
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="btn-group btn-group-justified">
        
                                    <!-- <div
                                        ng-show="ifAllSelectedItemsAre( 'not_ready', order )"
                                        ng-click="changeFoodState( order, 'in_preparation' )"
                                        class="btn-group ">
                                        <button class="btn btn-default"><?php echo __( 'Cook', 'gastro' );?></button>
                                    </div> -->
        
                                   <!--  <div
                                        ng-show="ifAllSelectedItemsAre( 'in_preparation', order )" 
                                        ng-click="changeFoodState( order, 'ready' )"
                                        class="btn-group ">
                                        <button class="btn btn-default"><?php echo __( 'Ready', 'gastro' );?></button>
                                    </div> -->
        
                                    <!--<div ng-show="ifAllSelectedItemsAre( 'in_preparation', order )" ng-click="changeFoodState( order, 'issue' )" class="btn-group ">
                                        <button class="btn btn-warning"><?php echo __( 'Issue', 'gastro' );?></button>
                                    </div>-->
        
                                    <!--<div ng-show="ifAllSelectedItemsAre( 'not_ready', order )" ng-click="changeFoodState( order, 'denied' )" class="btn-group ">
                                        <button class="btn btn-danger"><?php echo __( 'Unavailable', 'gastro' );?></button>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-xs-12 col-lg-12 col-sm-12" ng-show="columns.length == 0 || noValidOrder == true">
                <?php echo tendoo_info( __( 'No order has been placed for this kitchen.', 'gastro' ) );?>
            </div>
        </div>
       <!--  </div> -->
    </div>
    <div class="<?php echo store_option( 'gastro_show_booked_at_kitchen', 'no' ) === 'yes' ? 'col-md-4 col-lg-3 col-sm-6  col-xs-12' : 'hidden';?>">
        <div class="box box-solid box-info collapsed-box">
            <div class="box-header">
                <h3 class="box-title"><?php echo __( 'Booked Orders', 'gastro' );?></h3>
            </div>
        </div>
        <div class="booked-wrapper">
            <div ng-repeat="( index, order ) in bookedOrders" class="box box-success box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ order.CODE }} &mdash; {{ calculateTime( order ) }}</h3>
                </div>
                <div class="box-body" style="padding: 5px">
                    <div class="btn-group btn-group-justified">
                        <a ng-click="showDetails( order )" type="button" class="btn btn-primary" data-widget="collapse">
                            <i class="fa fa-eye"></i> <?php echo __( 'Details', 'gastro' );?>
                        </a>
                        <a ng-click="queueOrderToKitchen( order )" type="button" class="btn btn-primary" data-widget="collapse">
                            <i class="fa fa-check"></i> <?php echo __( 'Queue', 'gastro' );?>
                        </a>
                        <a ng-click="npsPrint( order )" type="button" class="btn btn-primary" data-widget="collapse">
                            <i class="fa fa-print"></i> <?php echo __( 'Receipt', 'gastro' );?>
                        </a>
                        <a ng-click="npsPrintKitchen( order )" type="button" class="btn btn-primary" data-widget="collapse">
                            <i class="fa fa-print"></i> <?php echo __( 'Kitchen', 'gastro' );?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style media="screen">
    .restaurant-note {
        border: solid 1px #d8d8d8;
        border-radius: 10px;
        padding: 5px 10px;
        margin: 5px 0;
        background: #F2F2F2;
    }

    .active .restaurant-note{
        color : #333;
    }

    .order-status {
        padding: 0 20px;
        text-align: center;
        border: solid 1px #1d5d7b;
        border-radius: 10px;
        background: #abe4ff;
        font-weight: 600;
        color: #333;
    }
    .kitchen-box .bg-primary {
    color: #fff;
    background-color: #337ab7 !important;
    }

    .kitchen-box .bg-success {
    background-color: #dff0d8 !important;
    }

    .kitchen-box .bg-info {
    background-color: #d9edf7 !important;
    }

    .kitchen-box .bg-warning {
    background-color: #fcf8e3 !important;
    }

    .kitchen-box .bg-danger {
    background-color: #f2dede !important;
    }

</style>
<style>

.box-body{
    min-height: 40px;
}
.content-wrapper {
    display: flex;
    flex-direction: column;
}
.content-wrapper .content {
    flex: 1 1 100%;
    display: flex;
    flex-direction: column;
    margin-left: 0;
    margin-right: 0;
}
.content-wrapper .content .row {
    flex: 1 1 100%;
    display: flex;
    height: 100%;
    flex-grow: 1;
}
.content-wrapper .content .row > .col-md-3 {
    flex-grow: 0;
    overflow-y: auto;
    display: flex;
    flex-direction: column;
}
.content-wrapper .content .row > .col-md-3 .booked-wrapper {
    display: flex;
    flex-direction: column;
    flex: 1 0 100%;
    flex-grow: 1;
    flex-shrink: 0;
    flex-basis: 0;
    overflow-y: auto;
    height: 100%;
    flex-basis: 0;
}
.main-sidebar{
     display: none !important;
}
.main-header{
    display: none !important;
}
.fixed .content-wrapper, .fixed .right-side{
    padding: unset !important;
}
.content-wrapper{
    margin: unset !important;
}
.main-footer{
    display: none !important;
}

</style>

<?php $this->events->do_action( 'gastro_kitchen_footer' );?>