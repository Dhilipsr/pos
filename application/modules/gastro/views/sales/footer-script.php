<?php $this->load->module_view( 'gastro', 'footer-swal' );?>
<script src="<?php echo module_url( 'gastro' ) . '/js/staff-history.js';?>"></script>
<script>
const GastroStaffHistoryData    =   {
    textDomain: {
        staffHistory : `<?php echo __( 'Staff History', 'gastro' );?>`,
        staffName : `<?php echo __( 'Name', 'gastro' );?>`,
        staffRole : `<?php echo __( 'Role', 'gastro' );?>`,
        action : `<?php echo __( 'Action', 'gastro' );?>`,
        unexpectedError : `<?php echo __( 'An unexpected error occured.', 'gastro' );?>`,
        noResults : `<?php echo __( 'Looks like there is no result for this order.', 'gastro' );?>`,
        date : `<?php echo __( 'Date', 'gastro' );?>`,
    },
    url : {
        staffHistory : `<?php echo site_url([ 'api', 'gastro', 'staff-actions', '{id}', store_get_param( '?' ) ]);?>`
    }
}
</script>
<script type="text/javascript">
$( document ).ready( function(){
    
    let bindSendToKitchen = function()  {
        setTimeout(() => {
        $( '.print-to-kitchen' ).bind( 'click', function(){
            $this   =   $( this );
                NexoAPI.showConfirm({
                    title       :   '<?php echo _s( 'Send this order to the kitchen ?', 'gastro' );?>',
                    message     :   '<?php echo _s( 'By proceeding to this will send all items again to the kitchen, even if those items are ready', 'gastro' );?>'
                }).then( function( action ){
                    if( action.value ) {
                        $.ajax({
                            url     :   '<?php echo site_url([ 'api', 'gastro', 'kitchens', 'print' ]);?>/' + $this.attr( 'data-id' ) + '?caching=false&app_code=<?php echo store_option( 'nexopos_app_code' ) . store_get_param( '&' );?>',
                            success     :   function( data ){
                                <?php if ( store_option( 'gastro_print_gateway', 'gcp' ) == 'nps' ):?>
                                $.ajax( '<?php echo store_option( 'nexo_print_server_url' );?>/api/print', {
                                    type  	:	'POST',
                                    data 	:	{
                                        'content' 	:	data,
                                        'printer'	:	'<?php echo store_option( 'printer_takeway' );?>'
                                    },
                                    dataType 	:	'json',
                                    success 	:	function( result ) {
                                        console.log( result );
                                    }
                                });
                                <?php endif;?>
                            }, 
                            headers     :   {
                                [ tendoo.rest.key ]     :   tendoo.rest.value
                            }
                        })
                    }
                });
            })        
        }, 3000 );

        $( '.staff-history' ).each( function() {
            if( $( this ).attr( 'bound' ) === undefined ) {
                $( this ).bind( 'click', function( e ) {
                    const textDomain    =   GastroStaffHistoryData.textDomain;
                    const id    =   $( this ).attr( 'data-item-id' );
                    swal({
                        title: textDomain.staffHistory,
                        html: `
                        <div id="staff-history-app" style="max-height: 400px; overflow-y: auto">
                            <table v-if="loaded && entries.length > 0" class="table table-condensed" style="font-size: 15px;text-align:justify;">
                                <thead>
                                    <tr>
                                        <th class="text-center">${textDomain.staffName}</th>
                                        <th class="text-center">${textDomain.action}</th>
                                        <th class="text-center">${textDomain.date}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr v-for="entry of entries">
                                        <th scope="row">{{ entry.user.name }}</th>
                                        <td>{{ entry.ACTION }}</td>
                                        <td>{{ entry.DATE_CREATION }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <div v-if="! loaded" id="loading">
                                <div class="overlay">
                                    <i class="fa fa-refresh fa-spin"></i>
                                </div>
                            </div>
                            <div v-if="loaded && entries.length === 0">
                                <h3>${textDomain.noResults}</h3>
                            </div>
                        </div>
                        `
                    });

                    switch( layout.is() ) {
                        case 'xs':
                        case 'sm':
                            $( '.swal2-container > div' ).css({ 'width': '80%' }); break;
                        case 'md':
                        case 'lg':
                        case 'xg':
                        default :
                            $( '.swal2-container > div' ).css({ 'width': '700px' }); break;
                    }

                    e.preventDefault();

                    new Vue({
                        el : '#staff-history-app',
                        data: {
                            entries : [],
                            loaded  :   false,
                            ...GastroStaffHistoryData
                        },
                        mounted() {
                            this.loadStaffHistoryFor( id );
                        },
                        methods: {
                            loadStaffHistoryFor( id ) {
                                HttpRequest.get( this.url.staffHistory.replace( '{id}', id ) )
                                    .then( result => {
                                        console.log( result.data );
                                        this.entries    =   result.data;
                                        this.loaded     =   true;
                                    })
                                    .catch( error => {
                                        NexoAPI.Toast()( error.response.data.message || this.textDomain.unexpectedError );
                                    })
                            }
                        }
                    })

                    return false;
                });
                $( this ).attr( 'bound', 'true' );
            }
        });
    }

    $( document ).ajaxComplete( function() {
        bindSendToKitchen();
    });

    bindSendToKitchen();
});
</script>