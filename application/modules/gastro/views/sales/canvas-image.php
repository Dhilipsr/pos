<script>
const url       =   '<?php echo store_option( 'nexo_print_server_url' );?>/api/print-base64';
var hasRunned   =   false;
$( document ).ready( function() {
    const trigger   =   () => {
        hasRunned   =   true;
        HttpRequest.get( '/api/gastro/test-print' ).then( result => {
            
            $( '#print-base64' ).remove();
            $( 'body' ).append( `<div id="print-base64">${result.data}</div>` );

            html2canvas( document.getElementById( 'print-base64' ), {
                logging: true
            }).then(function(canvas) {
                const image     =   canvas.toDataURL();
                HttpRequest.post( url, {
                    'base64' 	:	image,
                    'printer'	:	'<?php echo store_option( 'printer_takeway' );?>'
                }).then( result => {
                    hasRunned   =   false;
                }).catch( error => {
                    hasRunned   =   false;
                })
            }).catch( err => console.log( err ) );
        })
    }

    $( document ).keyup( function( e ) {
        if (e.which === 13 && hasRunned === false ) {
            trigger();
        }
    })
});
</script>