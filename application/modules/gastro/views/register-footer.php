<?php global $Options, $current_register;?>
<?php $this->load->module_view( 'gastro', 'footer-swal' );

$taxes_details =  $this->events->apply_filters( 'tax', array( 'nexo_order_devis' ) )['RATE'];
?>
<script>
const base64URL             =   `<?php echo store_option( 'nexo_print_server_url' );?>/api/print-base64`;
const printServerURL        = '<?php echo store_option( 'nexo_print_server_url' );?>';
var GastroVars      =   {
    reservationPattern      :   <?php echo json_encode( ( array ) explode( ',', @$Options[ store_prefix() . 'reservation_pattern' ] ) );?>,
    cancelURL               :   '<?php echo site_url([ 'api', 'gastro', 'tables', 'cancel-item', store_get_param( '?' ) ]);?>',
    tables_get_all          :   '<?php echo site_url([ 'api', 'gastro', 'tables_get_all' ]);?>',
    serverDate              :   '<?php echo date_now( 'Y-m-d h:i:s' );?>',
    kitchenScreenDisabled   :   <?php echo store_option( 'disable_kitchen_screen', 'no' ) === 'yes' ? 'true' : 'false';?>,
    disableAreaRooms        :   <?php echo store_option( 'disable_area_rooms' ) == 'yes' ? 'true' : 'false';?>,
    messages    :   {
        unknownStatus           :   '<?php echo _s( 'Unknow Status', 'gastro' );?>',
        not_ready               :   '<?php echo _s( 'Not Ready', 'gastro' );?>',
        ready                   :   '<?php echo _s( 'Ready', 'gastro' );?>',
        in_preparation          :   '<?php echo _s( 'On Going', 'gastro' );?>',
        canceled                :   '<?php echo _s( 'Canceled', 'gastro' );?>',
        cancelItemMessage       :   '<?php echo _s ( 'Would you like to cancel this item', 'gastro' );?>',
        freeTable               :   '<?php echo _s( 'Setting this table as free, will change the table status to available. This action can\'t be undone.', 'gastro' );?>'
    },
    canCancelItem       :   <?php echo User::can( 'gastro.view.cancel-button' ) ? 'true' : 'false';?>,
    base64Print         :   '<?php echo store_option( 'nps_print_base64' );?>',
}
</script>
<script type="text/javascript">
    tendooApp.filter('capitalize', function() {
        return function(input) {
        return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
        }
    });

    tendooApp.directive("masonry", function () {
        var NGREPEAT_SOURCE_RE = '<!-- ngRepeat: ((.*) in ((.*?)( track by (.*))?)) -->';
        return {
            compile: function(element, attrs) {
                // auto add animation to brick element
                var animation = attrs.ngAnimate || "'masonry'";
                var $brick = element.find( '[masonry-item]');
                $brick.attr("ng-animate", animation);
                
                // generate item selector (exclude leaving items)
                var type = $brick.prop('tagName');
                var itemSelector = type+"[masonry-item]:not([class$='-leave-active'])";
                
                return function (scope, element, attrs) {
                    var options = angular.extend({
                        itemSelector: itemSelector
                    }, scope.$eval(attrs.masonry));

                    
                    // try to infer model from ngRepeat
                    if (!options.model) { 
                        var ngRepeatMatch = element.html().match(NGREPEAT_SOURCE_RE);
                        if (ngRepeatMatch) {
                            options.model = ngRepeatMatch[4];
                        }
                    }
                    
                    // initial animation
                    element.addClass('masonry');
                    
                    // Wait inside directives to render
                    setTimeout(function () {
                        element.masonry(options);
                        
                        element.on("$destroy", function () {
                            element.masonry('destroy')
                        });
                        
                        if (options.model) {
                            scope.$apply(function() {
                                scope.$watchCollection(options.model, function (_new, _old) {
                                    if(_new == _old) return;
                                    
                                    // Wait inside directives to render
                                    setTimeout(function () {
                                        element.masonry("reloadItems");
                                        element.masonry(options);
                                    });
                                });
                            });
                        }
                    });
                };
            }
        };
    });



    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    var selectTableCTRL     =   function( $compile, $scope, $timeout, $http, $interval, $rootScope, $filter ) {
        $scope.userCanCancel            =   GastroVars.canCancelItem;
        $scope.isAreaRoomsDisabled      =   GastroVars.disableAreaRooms;
        $scope.spinner                  =   {}
        $scope.areas                    =   [];
        $scope.tables                   =   [];
        $scope.tables_all               =   [];
        $scope.selectedTable            =   false;
        $scope.sessionOrder             =   false;
        $scope.roomHeaderHeight         =   0;
        $scope.hideSideKeys             =   true;
        $scope.serverDate               =   moment( GastroVars.serverDate );
        $scope.showHistory              =   false;
        $scope.wasOpenFromTableHistory  =   false;
        $scope.canEditOrderType         =   false; // is set only true when we're editing an order
        $scope.tendoo                   =   {};
        $scope.tendoo.date              =   tendoo.date;
        $scope.kitchenScreenDisabled    =   GastroVars.kitchenScreenDisabled;

        $scope.timeInterval         =   <?php  echo 30000;?>;

        /**
         * Init time counter for tables timer
         */
        $interval( () => {
            $scope.serverDate.add( 1, 's' );

        }, 1000 );

      

        $scope.hideButton               =   {
            dot             :   true
        }

         $rootScope.$on("Call_table_refresh",function(table_id){
            $scope.table_refresh(table_id);
        });

        /**
         * Close history
         * @return void
         */
        $scope.closeHistory             =   function(){
            $scope.showHistory      =   false;
            $scope.sessionOrder     =   false;
            $scope.sessions         =   {};
        }

        $scope.tables_get_all  = function(){

             $http.get( GastroVars.tables_get_all,{
                        headers     :   {
                            [ tendoo.rest.key ]     :   tendoo.rest.value
                        }
                    }).then( result => {

                        $scope.tables_all = result.data;
                    });

        }
        
        $scope.paylater = function(){

    
        // if( v2Checkout.isCartEmpty() ) {
        //     NexoAPI.Toast()( '<?php echo _s( 'Vous ne pouvez pas payer une commande sans article. Veuillez ajouter au moins un article', 'nexo' );?>' );
        //     //NexoAPI.events.doAction( 'close_paybox', $scope );
        //     return false;
        // }
        // if( v2Checkout.CartItems.length > 0 ) {
        //        // $scope.sendToKitchen();

        //     // var scope = angular.element(document.getElementById('takeaway')).scope();
        //     $scope.sendToKitchen();
        // }
    }

        

        /**
         * Cancel Item
         * @param return void
         */
        $scope.cancelItem           =   function( details ) {
            let item                =   details.item;
            NexoAPI.showConfirm({
                message     :   GastroVars.messages.cancelItemMessage
            }).then( action => {
                if( action.value ) {
                    $http.post( GastroVars.cancelURL, {
                        item_id     :   item.ITEM_ID
                    }, {
                        headers     :   {
                            [ tendoo.rest.key ]     :   tendoo.rest.value
                        }
                    }).then( result => {
                        item.restaurant_food_status     =   result.data.restaurant_food_status;
                        let order           =   $scope.sessions[ details.session_key ].orders[ details.order_index ];
                            order.TOTAL     =   parseFloat( order.TOTAL ) - parseFloat( item.PRIX_TOTAL );

                        /**
                         * Since we're watching this variable in order to load the history
                         * updating the value might be enough to reload the history
                         */
                        $scope.showHistory  =   false;
                        $timeout(() => {
                            $scope.showHistory  = true;
                        }, 0 );

                        NexoAPI.events.doAction( 'gastro_canceled_item', { item });
                    });
                }
            });
        }

        $scope.wrapperHeight            =   $scope.windowHeight - ( ( 56 * 2 ) + 30 );
        $scope.reservationPattern       =   GastroVars.reservationPattern;

        /**
         * Quick shortcut to print orders
         * @param object order details
         */
        $scope.printReceipt         =   function( order ){
            $( '#receipt-wrapper' ).remove();
            $( 'body' ).append( '<iframe id="receipt-wrapper" style="visibility:hidden;height:0px;width:0px;position:absolute;top:0;" src="<?php echo dashboard_url([ 'orders', 'receipt' ]);?>/' + order.ORDER_ID + '?refresh=true&autoprint=true"></iframe>' );
        }

        /**
         * Nexo Print Server
         * @return void
         */
        $scope.npsPrint         =   function( order ) {
            // $.ajax( '<?php echo dashboard_url([ 'local-print' ]);?>' + '/' + order.ORDER_ID, {
            //     success  :   function( printResult ) {
            //         $.ajax( '<?php echo store_option( 'nexo_print_server_url' );?>/api/print', {
            //             type     :   'POST',
            //             data     :   {
            //                 'content'    :   printResult,
            //                 'printer'    :   '<?php echo store_option( 'nexo_pos_printer' );?>'
            //             },
            //             dataType     :   'json',
            //             success  :   function( result ) {
            //                 console.log( result );
            //             },
            //             error       :   ( error ) => {
            //                 swal({
            //                     title: `<?php echo __( 'An error occured', 'gastro' );?>`,
            //                     text: `<?php echo __( 'Nexo Print Server can\'t be reached or has returned an unexpected error.', 'gastro' );?>`,
            //                     type: 'error'
            //                 });
            //             }
            //         });
            //     }, 
            //     error : () => {
            //         swal({
            //             title: `<?php echo __( 'An error occured', 'gastro' );?>`,
            //             text: `<?php echo __( 'We were not able to print that order. The server has not returned  a valid response', 'gastro' );?>`,
            //             type: 'error'
            //         });
            //     }
            // });
                LocalPrintURL     =   '<?php echo site_url([ 'api', 'gastro', 'localprint']);?>/';
                var posPrinter='<?php echo store_option( 'nexo_pos_printer' );?>';

               if(posPrinter!="") {
                    if(posPrinter!="Choose an option") {



                        if(printServerURL==''){
                            var host_url = 'http://'+'<?php echo $_SERVER['HTTP_HOST'];?>'+':1000';

                        } else {

                            var host_url = printServerURL;
                        }

                        //var host_url = '<?php echo $_SERVER['HTTP_HOST'];?>';
                        var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
                        var theUrl = host_url+"/sendsampleregister";
                        xmlhttp.onreadystatechange = function() {
                            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                                console.log(xmlhttp.responseText);
                            }
                        }

                        xmlhttp.open("POST", theUrl);
                        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                        xmlhttp.send(JSON.stringify({'URL'   :   LocalPrintURL + '' + order.ORDER_ID+'?refresh=true','PRINTER'   :   posPrinter}));

                    }

                }
        }

        /**
         * Register Print NPS
         * @return void
         */
        $scope.registerPrint    =   function( order ) {
            <?php if ( empty( $current_register[ 'ASSIGNED_PRINTER' ] ) || ! filter_var( $current_register[ 'NPS_URL' ], FILTER_VALIDATE_URL ) ):?>
                NexoAPI.Notify().warning(
                    `<?php echo __( 'Unable to print', 'gastro' );?>`,
                    `<?php echo __( 'No printer has been assigned to the cash register or the URL to the Print Server is not valid.', 'gastro' );?>`
                );
            <?php else:?>
            // $.ajax( '<?php echo dashboard_url([ 'local-print' ]);?>' + '/' + order.ORDER_ID, {
            //     success  :   function( printResult ) {
            //         $.ajax( '<?php echo $current_register[ 'NPS_URL' ];?>/api/print', {
            //             type     :   'POST',
            //             data     :   {
            //                 'content'    :   printResult,
            //                 'printer'    :   '<?php echo $current_register[ 'ASSIGNED_PRINTER' ];?>'
            //             },
            //             dataType     :   'json',
            //             success  :   function( result ) {
            //                 NexoAPI.Toast()( `<?php echo __( 'Print Job submitted', 'gastro' );?>` );
            //             },
            //             error        :   () => {
            //                 NexoAPI.Notify().warning(
            //                     `<?php echo __( 'Unable to print', 'gastro' );?>`,
            //                     `<?php echo __( 'NexoPOS has not been able to connect to the Print Server  or this latest has returned an unexpected error.', 'gastro' );?>`
            //                 );
            //             }
            //         });
            //     }
            // });

            if(registerPrinter!="") {
                    if(registerPrinter!="Choose an option") {
                       if(printServerURL==''){
                            var host_url = 'http://'+'<?php echo $_SERVER['HTTP_HOST'];?>'+':1000';

                        } else {

                            var host_url = printServerURL;
                        }
                        var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
                        var theUrl = host_url+"/sendsampleregister";
                        xmlhttp.onreadystatechange = function() {
                            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                                console.log(xmlhttp.responseText);
                            }
                        }

                        xmlhttp.open("POST", theUrl);
                        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                        xmlhttp.send(JSON.stringify({'URL'   :   LocalPrintURL + '' + returned.order_id+'?refresh=true','PRINTER'   :   registerPrinter}));

                    }

            }
            <?php endif;?>
        }

        /**
         * Compare AMount
         * @param numeric first amount
         * @param string operation
         * @param numeric last amount
         * @return boolean
        **/

        $scope.compareAmount        =   function( first, operation, last ) {
            if( operation == '<' ) {
                return parseFloat( first ) < parseFloat( last );
            } else {
                return parseFloat( first ) > parseFloat( last );
            }
            return false;
        }

        /**
         * Get food status
         * @param string food
         * @return string
         */
        $scope.getFoodStatus    =   function( status ) {
            if ([ 'not_ready', 'ready', 'canceled', 'in_preparation' ].includes( status ) ) {
                return GastroVars.messages[ status ];
            }
            return GastroVars.messages.unknownStatus;
        }

        /**
         * Parse Modifier Json
         * @param string modifier string
         * @return object
        **/

        $scope.jsonParse            =   function( string ) {
            if( typeof string == 'string' ) {
                return JSON.parse( string );
            }
            return string;
        }

        /**$scope.selectedTable.
         *  check selecting table action
         *  @param string action
         *  @return void
        **/

        $scope.checkSelectingTableAction    =   function( action ) {
            if( action == true ) {
                if( $scope.selectedTable.STATUS != 'available' && $scope.selectedTable.CURRENT_SEATS_USED == '0' ) {
                    NexoAPI.Bootbox().alert( '<?php echo _s( 'You must select a table available.', 'gastro' );?>' );
                    return false;
                }

                if( $scope.seatToUse == 0  || angular.isUndefined( $scope.seatToUse ) && $scope.showHistory == false ) {
                    // NexoAPI.Bootbox().alert( '<?php echo _s( 'You must set a used seats. You can set used seats only for available tables.', 'gastro' );?>' );
                    return false;
                }

                if( $scope.isAreaRoomsDisabled ) { 
                    v2Checkout.CartMetas        =   _.extend( v2Checkout.CartMetas, {
                        table_id            :   $scope.selectedTable.ID || $scope.selectedTable.TABLE_ID,
                        room_id             :   0,
                        area_id             :   0,
                        seat_used           :   $scope.seatToUse > parseInt( $scope.selectedTable.MAX_SEATS ) ? $scope.selectedTable.MAX_SEATS  : $scope.seatToUse
                    });
                } else {
                    v2Checkout.CartMetas        =   _.extend( v2Checkout.CartMetas, {
                        table_id            :   $scope.selectedTable.TABLE_ID,
                        area_id             :   $scope.selectedArea.AREA_ID,
                        seat_used           :   $scope.seatToUse > parseInt( $scope.selectedTable.MAX_SEATS ) ? $scope.selectedTable.MAX_SEATS  : $scope.seatToUse
                    });
                }
                
                // restore visible buttons
                angular.element( '[ng-click="openPayBox()"]' ).closest( '.btn-group' ).show();
                angular.element( '[ng-click="openSaveBox()"]' ).closest( '.btn-group' ).show();
                
            } else {
                $scope.selectedTable    =   false;
            }
        }

        /**
        * GetModifier PRice
        * @param string json
        * @return numeric amount
        **/

        $scope.getModifierPrices        =   function( json ) {
            let total           =   0;
            if( json ) {
                let modifiers       =   $scope.jsonParse( json ) || false;
                if( modifiers !== false ) {
                    modifiers.forEach( entry => {
                        total       +=  parseFloat( entry.price );
                    })
                }
            }

            return total;
        }

        /**
         *  Get a class when a table is selected
         *  @param
         *  @return
        **/

        $scope.tableSelectedClass       =   ( selectedTable ) => {
            return selectedTable == false ? 'btn-default' :
                selectedTable.STATUS == 'in_use' ? 'btn-default' :  'btn-success';
        }

        /**
         * checkDate
         * @return valid date or current server date
        **/

        $scope.checkDate                =   function( date ) {
            if( moment( date ).isValid() ) {
                return date;
            } else {
                return moment( date ).format( 'YYYY-MM-DD HH:mm:ss' );
            }
        }

        /**
         *  Open table Selection
         *  @param void
         *  @return void
        **/

        $scope.openTableSelection       =       function() {
            $scope.seatToUse                =   0;
            $scope.areas                    =   [];
            $scope.tables                   =   [];
            $scope.selectedArea             =   false;
            $scope.selectedTable            =   false;


            NexoAPI.Bootbox().confirm({
                message         :   '<div class="table-selection"><restaurant-rooms rooms="rooms"/></restaurant-rooms></div>',
                title           :   '<?php echo _s( 'Select a Table', 'gastro' );?>',
                buttons: {
                    confirm: {
                        label: '<?php echo _s( 'Confirm', 'gastro' );?>',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: '<?php echo _s( 'Close', 'gastro' );?>',
                        className: 'btn-default'
                    }
                },
                callback        :   function( action ) {
                    if( 
                        ! action &&  
                        typeof v2Checkout.CartMetas.table_id == 'undefined' && 
                        typeof v2Checkout.CartMetas.seat_used == 'undefined' 
                    ) {
                        // if we're canceling table selection the order type is cancel as well
                        $scope.selectedOrderType            =   false;
                        $scope.sessionOrder                 =   false;
                    }

                    $scope.showHistory      =   false;
                    return $scope.checkSelectingTableAction( action );

                },
                className       :   'table-selection-box',
                animate         :   false
            });

            $scope.windowHeight             =   window.innerHeight;
            $scope.wrapperHeight            =   $scope.windowHeight - (  20 );

            $timeout( function(){
                angular.element( '.table-selection-box .modal-dialog' ).css( 'width', '98%' );
                // angular.element( '.table-selection-box .modal-header' ).css( 'display', 'none' );
                angular.element( '.table-selection-box .modal-body' ).css( 'padding-top', '0px' );
                angular.element( '.table-selection-box .modal-body' ).css( 'padding-bottom', '0px' );
                angular.element( '.table-selection-box .modal-body' ).css( 'padding-left', '0px' );
                angular.element( '.table-selection-box .modal-body' ).css( 'padding-right', '0px' );
                angular.element( '.table-selection-box .modal-body' ).css( 'height', $scope.wrapperHeight );
                angular.element( '.table-selection-box .modal-body' ).css( 'overflow-x', 'hidden' );
            }, 10 );

            setTimeout( function(){
                
                /**
                * When the Rooms and Area are disabled. Just load the tables quickly
                **/

                if( $scope.isAreaRoomsDisabled ) { 
                    $scope.loadTables();
                } else {
                    $scope.loadRoomAreas();
                }

                $( '.table-selection' ).html( $compile( $( '.table-selection').html() )( $scope ) );
               $( '.table-selection-box [data-bb-handler="cancel"]' ).attr( 'ng-click', 'cancelTableSelection()' );
                $( '.table-selection-box .modal-footer' ).html( $compile( $( '.table-selection-box .modal-footer' ).html() )( $scope ) );
                $( '.table-selection-box [data-bb-handler="confirm"]' ).hide();
                $( '.table-selection-box [data-bb-handler="cancel"]' ).hide();
                 $( '.table-selection-box .modal-footer' ).hide();
                 $( '.table-selection-box .modal-header' ).hide();
            }, 30 );
        }

        /**
         * Is a table selected
         * @return bool
        **/

        $scope.isTableSelected          =       function(){
            if ( ( $scope.selectedTable.STATUS == 'in_use' || $scope.selectedTable.STATUS == 'available' )  && $scope.showHistory == false ) {
                return true;
            }
            return false;
        }

        /**
         *  Open checkout to proceed to payment for a specific order
         * @param object order
         * @return void
        **/

        $scope.openCheckout             =   function( order ) {

            // we asume all order opened from this function are dinein orders
            // since if we cancel or set an item as available, the order type is updated. We're then refreshing
            // the order type with the line.
            $scope.enableOrderType( 'dinein' );
         
            v2Checkout.emptyCartItemTable();
            v2Checkout.CartItems            =   order.items;
           

            _.each( v2Checkout.CartItems, function( value, key ) {
                value.QTE_ADDED                         =   value.QUANTITE;
                value.restaurant_food_modifiers         =   value.RESTAURANT_FOOD_MODIFIERS !== 'null' || JSON.parse( value.RESTAURANT_FOOD_MODIFIERS ) !== null ? JSON.parse( value.RESTAURANT_FOOD_MODIFIERS ) : [];
                value.restaurant_food_status            =   value.RESTAURANT_FOOD_STATUS || '';
                value.restaurant_food_note              =   value.RESTAURANT_FOOD_NOTE || '';
                value.restaurant_food_issue             =   value.RESTAURANT_FOOD_ISSUE || '';
                value.restaurant_food_printed           =   value.RESTAURANT_FOOD_PRINTED || 0;
            });

            // @added CartRemisePercent
            // @since 2.9.6

            if( order.REMISE_TYPE != '' ) {
                v2Checkout.CartRemiseType               =   order.REMISE_TYPE;
                v2Checkout.CartRemise                   =   NexoAPI.ParseFloat( order.REMISE );
                v2Checkout.CartRemisePercent            =   NexoAPI.ParseFloat( order.REMISE_PERCENT );
                v2Checkout.CartRemiseEnabled            =   true;
            }

            if( parseFloat( order.GROUP_DISCOUNT ) > 0 ) {
                v2Checkout.CartGroupDiscount            =   parseFloat( order.GROUP_DISCOUNT ); // final amount
                v2Checkout.CartGroupDiscountAmount      =   parseFloat( order.GROUP_DISCOUNT ); // Amount set on each group
                v2Checkout.CartGroupDiscountType        =   'amount'; // Discount type
                v2Checkout.CartGroupDiscountEnabled     =   true;
            }

            v2Checkout.CartCustomerID           =   order.REF_CLIENT;
            // @since 2.7.3
            v2Checkout.CartNote                 =   order.DESCRIPTION;
            v2Checkout.CartTitle                =   order.TITRE;

            // @since 3.1.2
            v2Checkout.CartShipping                 =   parseFloat( order.SHIPPING_AMOUNT );
            $scope.price                            =   v2Checkout.CartShipping; // for shipping directive
            $( '.cart-shipping-amount' ).html( $filter( 'moneyFormat' )( $scope.price ) );

            // Restore Custom Ristourne
            v2Checkout.restoreCustomRistourne();

            // Refresh Cart
            // Reset Cart state
            v2Checkout.buildCartItemTable();
            v2Checkout.refreshCart();
            v2Checkout.refreshCartValues();
            v2Checkout.ProcessURL               =   "<?php echo site_url(array( 'rest', 'nexo', 'order', User::id() ));?>" + '/' + order.ORDER_ID + "?store_id=<?php echo get_store_id();?>&from-pos-waiter-screen=true";
            v2Checkout.ProcessType              =   'PUT';
            v2Checkout.CartPayments             =   order.payments.map( payment => {
                return {
                    namespace : payment.PAYMENT_TYPE,
                    amount : parseFloat( payment.MONTANT ),
                    meta : { readOnly: true }
                }
            });

            // // Restore Shipping
            // // @since 3.1
            _.each( order.shipping, ( value, key ) => {
                $scope[ key ]   =   value;
            });

            $scope.wasOpenFromTableHistory      =   true;

            if ( NexoAPI.events.applyFilters( 'gastro_open_payment_box', true, $scope ) ) {
                $rootScope.$emit( 'payBox.openPayBox' );
            }
        }

        /**
         * Select Order Type
         * @param void
        **/

        $scope.types               =   []
        
        <?php if( store_option( 'disable_delivery' ) != 'yes' ):?>
        $scope.types.push({
            namespace       :   'delivery',
            text            :   '<?php echo _s( 'Delivery', 'gastro' );?>'
        });
        <?php endif;?>
        
        <?php if( store_option( 'disable_dinein' ) != 'yes' ):?>
        $scope.types.push({
            namespace       :   'dinein',
            text            :   '<?php echo _s( 'Dine In', 'gastro' );?>'
        });
        <?php endif;?>
        
        <?php if( store_option( 'disable_takeaway' ) != 'yes' ):?>
        $scope.types.push({
            namespace       :   'takeaway',
            text            :   '<?php echo _s( 'Take Away', 'gastro' );?>'
        });
        <?php endif;?>

        <?php if( store_option( 'disable_readyorders' ) != 'yes' ):?>
        $scope.types.push({
            namespace       :   'readyorders',
            text            :   '<?php echo _s( 'Ready Orders', 'gastro' );?>'
        });
        <?php endif;?>

        <?php if( store_option( 'disable_pendingorders' ) != 'yes' ):?>
        $scope.types.push({
            namespace       :   'pendingorders',
            text            :   '<?php echo _s( 'Pending Orders', 'gastro' );?>'
        });
        <?php endif;?>

        <?php if( store_option( 'disable_pos_waiter' ) != 'yes' ):?>
        $scope.types.push({
            namespace       :   'waiter',
            text            :   '<?php echo _s( 'Waiters', 'gastro' );?>'
        });
        <?php endif;?>

        <?php if( store_option( 'disable_saleslist' ) != 'yes' ):?>
        $scope.types.push({
            namespace       :   'return',
            text            :   '<?php echo _s( 'Sales List', 'gastro' );?>'
        });
        <?php endif;?>

       

        $scope.types.push({
            namespace       :   'quickbill',
            text            :   '<?php echo _s( 'Quick bill', 'gastro' );?>'
        });

        $scope.noOrderTypeOption        =   function(){
            NexoAPI.Bootbox().confirm( '<?php echo _s( 'You can\'t proceed to sales if there is no order type available. Please contact the manager. <br>Would you like to go back ?', 'gastro' );?>', function( action ) {
                if( action ) {
                    document.location   =   '<?php echo site_url([ 'dashboard', store_slug(), 'nexo', 'commandes', 'lists' ]);?>';
                } else {
                    $scope.noOrderTypeOption();
                }
            }); 
        }

        /**
         * Open history
         * @return void
        **/

        $scope.openHistory              =   function(){
            $scope.showHistory          =   true;
        }

        /**
         *  Load Room
         *  @param int room id
         *  @return void
        **/

        $scope.loadRoomAreas                =   function() {
            $http.get( '<?php echo site_url([ 'api', 'gastro', 'areas', store_get_param( '?' ) ]);?>', {
                headers         :   {
                    '<?php echo $this->config->item('rest_key_name');?>'    :   '<?php echo @$Options[ 'rest_key' ];?>'
                }
            }).then(function( returned ) {
                $scope.spinner[ 'areas' ]     =   false;
                $scope.areas                    =   returned.data;
                // Load first room tables
                $scope.loadTables( $scope.areas[0] );
            });
        }

        /**
         *  Load table
         *  @param object areas
         *  @return void
        **/

        $scope.loadTables               =   function( area ) {
            return new Promise( ( resolve, reject ) => {
                _.each( $scope.areas, function( _area ) {
                    _area.active                 =   false;
                });

                if( $scope.isAreaRoomsDisabled ) {
                    link    =   '<?php echo site_url([ 'api', 'gastro', 'tables' ]);?>';
                } else {
                    link    =   '<?php echo site_url([ 'api', 'gastro', 'tables', 'area' ]);?>' + '/' + area.ID;
                }

                if( typeof area != 'undefined' ) {
                    $scope.selectedArea             =   area;
                    area.active                     =   true;
                }
                
                $scope.spinner[ 'tables' ]      =   true;

                $http.get( link + '<?php echo store_get_param( '?' );?>', {
                    headers         :   {
                        '<?php echo $this->config->item('rest_key_name');?>'    :   '<?php echo @$Options[ 'rest_key' ];?>'
                    }
                }).then(function( returned ) {
                    $scope.spinner[ 'tables' ]          =   false;
                    $scope.tables                       =   returned.data;
                    $rootScope.$emit( 'tables-has-loaded', returned.data );
                });
            });
        }

        /**
         *  get table timer
         *  @param table
         *  @return string
        **/

        $scope.getTimer         =   ( since )   =>  {
            if( since != '0000-00-00 00:00:00' ) {
                let now     =   $scope.serverDate.format( 'YYYY-MM-DD HH:mm:ss' );
                let then    =   since;

                var ms = moment( tendoo.now() ).diff( moment( then ) );
                var d = moment.duration(ms);
                var s = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");

                return s;
            } else {
                return '--:--:--';
            }
        }

        /** 
         * New Order
         * Just place a new order over a selected table
         * @return void
        **/

        $scope.addNewItem             =   function( order ){

          /*  if( $scope.isAreaRoomsDisabled ) { 
                v2Checkout.CartMetas        =   _.extend( v2Checkout.CartMetas, {
                    table_id            :   $scope.selectedTable.ID,
                    room_id             :   0,
                    area_id             :   0,
                    seat_used           :   $scope.selectedTable.CURRENT_SEATS_USED,
                    add_on              :   order.REF_ORDER
                });
            } else {
                v2Checkout.CartMetas        =   _.extend( v2Checkout.CartMetas, {
                    table_id            :   $scope.selectedTable.TABLE_ID,
                    area_id             :   $scope.selectedArea.AREA_ID,
                    seat_used           :   $scope.selectedTable.CURRENT_SEATS_USED,
                    add_on              :   order.REF_ORDER
                });
            }*/
            

            $scope.seatToUse        =   $scope.selectedTable.CURRENT_SEATS_USED;
            angular.element( '.table-selection-box [data-bb-handler="confirm"]' ).trigger( 'click' );
           // $scope.toggleConflictingButton( 'hide' );
            $('#dinein').addClass('show-tables-button btn-primary btn-no-border');
           $('#payerbutton').hide();

            /**
             * If the cart is not empty, let's
             * ask if the user would like to proceed
             */
            if( v2Checkout.CartItems.length > 0 ) {
                $scope.sendToKitchen();
            }
        }

        $scope.addNewItem1             =   function( order ){

            // if( $scope.isAreaRoomsDisabled ) { 
            //     v2Checkout.CartMetas        =   _.extend( v2Checkout.CartMetas, {
            //         table_id            :   $scope.selectedTable.ID,
            //         room_id             :   0,
            //         area_id             :   0,
            //         seat_used           :   $scope.selectedTable.CURRENT_SEATS_USED,
            //         add_on              :   order.REF_ORDER
            //     });
            // } else {
                v2Checkout.CartMetas        =   _.extend( v2Checkout.CartMetas, {
                    table_id            :   $scope.selectedTable.TABLE_ID,
                    area_id             :   $scope.selectedArea.AREA_ID,
                    seat_used           :   $scope.selectedTable.CURRENT_SEATS_USED,
                    add_on              :   order.REF_ORDER
                });
            //}
            
         

            $scope.seatToUse        =   $scope.selectedTable.CURRENT_SEATS_USED;
            angular.element( '.table-selection-box [data-bb-handler="confirm"]' ).trigger( 'click' );
             $('#dinein').addClass('show-tables-button btn-primary btn-no-border');
             $('#payerbutton').hide();

             // $('#payerbutton').hide();
            //$scope.toggleConflictingButton( 'hide' );

            /**
             * If the cart is not empty, let's
             * ask if the user would like to proceed
             */
            if( v2Checkout.CartItems.length > 0 ) {
                $scope.sendToKitchen();
            }
        }

        /**
         * Hide button which might cause bugs
         * @return void
         */
        $scope.toggleConflictingButton      =   function( action ) {
            if( action === 'hide' ) {
                angular.element( '[ng-click="openTables()"]' ).hide();
                angular.element( '[ng-click="switchOrderType( \'takeaway\' )"]' ).hide();
                angular.element( '[ng-click="switchOrderType( \'delivery\' )"]' ).hide();
               
               
                angular.element( '[ng-click="openHistoryBox()"]' ).hide();
                angular.element( '[ng-click="openPayBox()"]' ).closest( '.btn-group' ).hide();
                angular.element( '[ng-click="openSaveBox()"]' ).closest( '.btn-group' ).hide();
            } else {
                angular.element( '[ng-click="openTables()"]' ).show();
                angular.element( '[ng-click="switchOrderType( \'takeaway\' )"]' ).show();
                angular.element( '[ng-click="switchOrderType( \'delivery\' )"]' ).show();
                angular.element( '[ng-click="openHistoryBox()"]' ).show();
                angular.element( '[ng-click="openPayBox()"]' ).closest( '.btn-group' ).show();
                angular.element( '[ng-click="openSaveBox()"]' ).closest( '.btn-group' ).show();
            }
        }

        /** 
         * Switch Order Type
         * @param string order type
         * @return void
        **/

        $scope.realOrderType            =   [
            'dinein',
            'delivery',
            'takeaway',
            'booking',
            'quickbill',
        ];

        /**
         * Set as astive
         * @param string
         * @return void
         */
        $scope.enableOrderType              =   function( typeNamespace ) {
            _.each( $scope.types, function( type ) {
                if( type.namespace == typeNamespace ) {
                    $scope.selectedOrderType    =   type;
                }
            });

           
            
            <?php   if( store_option( 'disable_delivery' ) == 'yes' ):?>
    

            $('#delivery').hide();
                
            <?php endif;?>
            
            <?php if( store_option( 'disable_dinein' ) == 'yes' ):?>
      
             $('#dinein').hide();
            <?php endif;?>
            
            <?php if( store_option( 'disable_takeaway' ) == 'yes' ):?>
            
             $('#takeaway').hide();
           
            <?php endif; ?>

        }

        $scope.switchOrderType              =   function( order_type = null ){
            
            
            // return false;
            
              <?php   if( store_option( 'disable_delivery' ) == 'yes' ):?>
    
    
                $('#delivery').hide();
                    
                <?php endif;?>
                
                <?php if( store_option( 'disable_dinein' ) == 'yes' ):?>
          
                 $('#dinein').hide();
                <?php endif;?>
                
                <?php if( store_option( 'disable_takeaway' ) == 'yes' ):?>
                
                 $('#takeaway').hide();
               
                <?php endif; ?>

            // cancel seleced Table
            $scope.selectedTable            =   false;







            // check if there is one selected
            var selected        =   false;
            _.each( $scope.types, function( type ) {
                if( _.indexOf( $scope.realOrderType, type.namespace ) != -1 ) {
                    if( order_type != null ) {



            




                        if( type.namespace == order_type ) {
                            selected    =   true;
                            $scope.selectedOrderType    =   type;
                        } 
                    } else {
                        if( type.active ) {
                            selected    =   true;
                            $scope.selectedOrderType    =   type;
                        }
                    }

                // for any custom action that we add on the order type array
                } else {
                    if( type.active ) {
                        if( type.namespace == 'return' ) {
                            document.location   =   '<?php echo dashboard_url([ 'orders' ]);?>';
                        } else if( type.namespace == 'readyorders' ) {
                            bootbox.hideAll();
                            $rootScope.$broadcast( 'open-ready-orders' );
                        } else if( type.namespace == 'pendingorders' ) {
                            $rootScope.$broadcast( 'open-history-box' );
                        } else if( type.namespace == 'waiter' ) {
                            $rootScope.$broadcast( 'open-waiter-screen' );
                        }
                        type.active     =   false;
                    }
                }
            });

            if( ! selected ) {
                // NexoAPI.Toast()( '<?php echo _s( 'You must select an order type', 'gastro' );?>' );
                return false;
            }

           //  console.log( $scope.selectedOrderType);
            v2Checkout.CartSelectedOrderType    =   $scope.selectedOrderType;


           // console.log($scope.selectedOrderType.namespace);
              if($scope.selectedOrderType.namespace=='takeaway'){

                $('.payer_payment').text('Pay confirm');

                $('.payer').text('Pay later');

                 $('.paylater').show(); 

                 $('#paylater').show();


                  // console.log('paylater show');

               } else {

               

                // console.log('paylater hide');
                $('.paylater').hide();
                $('#paylater').hide();
                $('.payer_payment').text('Pay');


               }



             if( $scope.selectedOrderType.namespace == 'dinein' ) {
                bootbox.hideAll();
               
                $scope.openTableSelection();
                $( '[ng-click="openDelivery()"]' ).hide();
                $( '.sendToKitchenButtonWrapper' ).show();
                 $( '.payerbutton' ).hide();
            
                $( '#cart-details > tfoot.hidden-xs.hidden-sm.hidden-md > tr:nth-child(3)' ).hide();
                $('#quickbill').removeClass('show-tables-button btn-primary btn-no-border');
                 $('#quickbill').css('color','black');
                $( '#hold' ).show();
            } else if( $scope.selectedOrderType.namespace == 'delivery' ) {
                bootbox.hideAll();
                
                $( '.payerbutton' ).show();
                $( '.sendToKitchenButtonWrapper' ).hide();
                $( '#cart-details > tfoot.hidden-xs.hidden-sm.hidden-md > tr:nth-child(3)' ).show();
                $( '[ng-click="openDelivery()"]' ).show();
                $('#quickbill').removeClass('show-tables-button btn-primary btn-no-border');
                 $('#quickbill').css('color','black');
                 $( '#hold' ).show();
            } else if( $scope.selectedOrderType.namespace == 'takeaway' ) {
                bootbox.hideAll();
                $( '.payerbutton' ).show();
                 $( '.sendToKitchenButtonWrapper' ).hide();
                $( '[ng-click="openDelivery()"]' ).hide();
                $( '#cart-details > tfoot.hidden-xs.hidden-sm.hidden-md > tr:nth-child(3)' ).hide();
                $('#quickbill').removeClass('show-tables-button btn-primary btn-no-border');
                 $('#quickbill').css('color','black');
                $( '#hold' ).show();
            } else if( $scope.selectedOrderType.namespace == 'quickbill' ) {
                bootbox.hideAll();
                $( '.payerbutton' ).show();
                 $( '.sendToKitchenButtonWrapper' ).hide();
                $( '[ng-click="openDelivery()"]' ).hide();
                $( '#cart-details > tfoot.hidden-xs.hidden-sm.hidden-md > tr:nth-child(3)' ).hide();
                $('#quickbill').addClass('show-tables-button btn-primary btn-no-border');
               
                $('#quickbill').css('color','white');
                $( '#hold' ).hide();
            } else if( $scope.selectedOrderType.namespace == 'booking' ) {
                $( '.payerbutton' ).show();
                 $('#quickbill').css('color','black');
                bootbox.hideAll();
                bootbox.confirm({

                    title: "<?php echo _s( 'Booking Management', 'gastro' );?>",
                    message: '<div class="booking-wrapper" style="height:300px"><booking-ui></booking-ui></div>',
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> <?php echo _s( 'Cancel', 'gastro' );?>'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> <?php echo _s( 'Add the booking', 'gastro' );?>'
                        }
                    },
                    callback: function (result) {
                        // 
                    },
                    className       :   'booking-box'
                });

                $scope.windowHeight				=	window.innerHeight;
                $scope.wrapperHeight			=	$scope.windowHeight - ( ( 56 * 2 ) + 30 );

                $timeout( function(){
                    angular.element( '.booking-box .modal-dialog' ).css( 'width', '98%' );
                    angular.element( '.booking-box .modal-body' ).css( 'height', $scope.wrapperHeight );
                    angular.element( '.booking-box .modal-body' ).css( 'overflow-x', 'hidden' );
                }, 200 );

                $( '.booking-wrapper' ).html( $compile( $( '.booking-wrapper').html() )( $scope ) );
                $scope.loadRoomAreas();
            }  
            v2Checkout.fixHeight();
        }

        /**
         * masonry orders
         * @return object
        **/

        $scope.masonry                      =   function( orders ) {
            let totalColumn;
            if( window.screen.width <= 320 ) {
                totalColumn         =   1;
            } else if( window.screen.width > 320 && window.screen.width <= 720 ) {
                totalColumn         =   2;
            } else {
                totalColumn         =   3;
            }

            for( let i = 0; i < totalColumn; i++ ) {
                
            }
        }

        /**
         * Set as Served
         * @param int order id
         * @return void
        **/

        $scope.setAsServed          =   function( order_id ) {
            NexoAPI.Bootbox().confirm( '<?php echo _s( 'Would you like to set this order has served ?', 'gastro' );?>', function( action ){
                if( action ) {
                    $http.post( '<?php echo site_url([ 'api', 'gastro', 'tables', 'serve', store_get_param( '?' ) ]);?>', {
                        order_id
                    }, {
                        headers         :   {
                            '<?php echo $this->config->item('rest_key_name');?>'    :   '<?php echo @$Options[ 'rest_key' ];?>'
                        }
                    }).then(function( returned ) {
                        $rootScope.$broadcast( 'food-served', returned.data );

                        if( $( '.table-selection-box' ).length > 1 ) {
                            $scope.showHistory      =   false;
                            // to force refresh
                            $timeout(function(){
                                $scope.showHistory  =   true;
                            }, 100 );
                        }
                    }, function( returned ){
                        $rootScope.$broadcast( 'food-no-served', returned.data );
                    });
                }
            });
        }

        $scope.paylater = function(){

        
            if( v2Checkout.isCartEmpty() ) {
                NexoAPI.Toast()( '<?php echo _s( 'Vous ne pouvez pas payer une commande sans article. Veuillez ajouter au moins un article', 'nexo' );?>' );
                //NexoAPI.events.doAction( 'close_paybox', $scope );
                return false;
            }
            if( v2Checkout.CartItems.length > 0 ) {
                   // $scope.sendToKitchen();

                //var scope = angular.element(document.getElementById('takeaway')).scope();
                $scope.sendToKitchen();
            }
        }

        /**
        * Keyboard Input
        **/

        $scope.keyboardInput        =   function( char, field, add ) {


            //alert('hi');

          //  angular.element( '[ng-click="openPayBox()"]' ).closest( '.btn-group' ).show();
            
            if( typeof $scope.seatToUse ==  'undefined' ) {
                $scope.seatToUse    =   ''; // reset paid amount
            }

            if( $scope.seatToUse    ==  0 ) {
                $scope.seatToUse    =   '';
            }

            if( char == 'clear' ) {
                $scope.seatToUse    =   '';
            } else if( char == '.' ) {
                $scope.seatToUse    +=  '.';
            } else if( char == 'back' ) {
                $scope.seatToUse    =   $scope.seatToUse.substr( 0, $scope.seatToUse.length - 1 );
            } else if( typeof char == 'number' ) {
                if( add ) {
                    $scope.seatToUse    =   $scope.seatToUse == '' ? 0 : $scope.seatToUse;
                    $scope.seatToUse    =   parseFloat( $scope.seatToUse ) + parseFloat( char );
                } else {
                    $scope.seatToUse    =   $scope.seatToUse + '' + char;
                }
            }

            $scope.seatToUse    =   $scope.seatToUse == '' ? 0 : parseInt( $scope.seatToUse );
            $scope.seatToUse    =   ( $scope.seatToUse > parseInt( $scope.selectedTable.MAX_SEATS ) ) ? $scope.selectedTable.MAX_SEATS  :  $scope.seatToUse;
            
            // Since order type might be refreshed, when we're paying an order or settnig a table as available
            // we should then confirm the order type when a table is selected.
            // selectedTable is always refreshed everytime we're selecting a table.
            $scope.enableOrderType( 'dinein' );

            // to avoid several click. The table popup is closed.
            $( '.table-selection-box' ).find( '[data-bb-handler="confirm"]' ).trigger( 'click' );

            /**
             * Let's check if the script as for faster submittion
             */
            if ( $scope.submitTo == 'sendToKitchen' ) {
                $scope.sendToKitchen();
            } else if ( $scope.submitTo == 'openPayBox' && NexoAPI.events.applyFilters( 'gastro_open_payment_box', true, $scope ) ) {
                $rootScope.$emit( 'payBox.openPayBox' );
            }
            $('#payerbutton').hide();
            // delete the variable
            delete $scope.submitTo;
        };

        /**
         * Change order type from outside
         */
        $rootScope.$on( 'gastro.orderType', ( params, orderType ) => {
            $scope.enableOrderType( orderType );
        });

        /**
         * Duration for a specific session
         */
        $scope.duration         =   function( starts, ends ) {
            let startMoment     =   moment( starts );
            let endMoment       =   moment( ends );
            return startMoment.locale( 'en' ).from( endMoment );
            // let remaining       =    - moment( session.ends ).duration().asMilliseconds();
            // return moment.utc( moment.unix( remaining ).duration().asMilliseconds() ).format("HH:mm:ss");
        }


        /**
         *  Select Table
         *  @param object table
         *  @return void
        **/

        $scope.selectTable              =   function( table ) {
            
              <?php   if( store_option( 'disable_delivery' ) == 'yes' ):?>
    
    
                $('#delivery').hide();
                    
                <?php endif;?>
                
                <?php if( store_option( 'disable_dinein' ) == 'yes' ):?>
          
                 $('#dinein').hide();
                <?php endif;?>
                
                <?php if( store_option( 'disable_takeaway' ) == 'yes' ):?>
                
                 $('#takeaway').hide();
               
                  <?php endif; ?>

            // if table is in use, just show his history
                 $scope.tables_get_all();
            //console.log(table);
              //  $scope.selectedTable        =   false;
                
                if( table.STATUS == 'in_use' ) {
                    $scope.showHistory      =   true;
                    $scope.selectedTable        =   table;

                } else {
                
                    $scope.seatToUse            =   0;
                    $scope.selectedTable        =   table;

                    // Unselect active on all tables
                    $scope.tables.forEach( _table => {
                        _table.active   =   false;
                    })

                    table.active    =   true;

                    var scope = angular.element(document.getElementById('orderone')).scope();
                   scope.keyinput( 1,'inputName' );

            }

          //  alert('hi');


         // angular.element('#orderone').trigger('click');

          //angular.element('#orderone').triggerHandler('click');

          // var scope = angular.element(document.getElementById('orderone')).scope();
           // scope.keyinput( 1,'inputName' );


        }

        /**
         *  Cancel Table Selection
         *  @param
         *  @return
        **/

        $scope.cancelTableSelection     =   function(){
            $scope.selectedTable        =   false;
            $scope.showHistory          =   false;
            // Unselect active on all tables
            _.each( $scope.tables, function( table ){
                table.active    =   false;
            });
          //  $scope.selectedOrderType    =   false;
        }

        /**
         *  Get Table Color Status
         *  @param object table
         *  @return string color
        **/

        $scope.getTableColorStatus      =   function( table ) {
            if( table.active && table.STATUS == 'out_of_use' ) {
                return 'table-out-of-use';
            } else if( table.active && table.STATUS == 'available' ) {
                return 'table-selected';
            } else if( table.active && table.STATUS == 'reserved' ) {
                return 'table-reserved';
            } else if( table.active ) {
                return 'table-in-use';
            }
            return '';
        }

        /**
         * Listen on OpenSelectOrderType
         * @return void
        **/

        $rootScope.$on( 'open-table-selection', function(){
            $scope.openTableSelection();
        });

        $rootScope.$on( 'filter-selected-order-type', function( evt, orderType ) {
            $scope.selectedOrderType        =   orderType;    
        });

        $rootScope.$on( 'serve-order', function( event, $order_id ) {
            $scope.setAsServed( $order_id );
        });

        /**
         *  Set A table available
         *  @param object table
         *  @return void
        **/

        $scope.setAvailable         =   function( selectedTable ) {
            var link    =   '<?php echo site_url([ 'api', 'gastro', 'tables', 'status' ]);?>/' +
                ( selectedTable.TABLE_ID || selectedTable.ID );

            /**
             * @todo We might need to retreive table orders and check if it can be set to available
             */

            $http.put(
                link +  '<?php echo store_get_param( '?' );?>', {
                CURRENT_SEATS_USED      :   0,
                STATUS                  :   'available'
            }, {
                headers         :   {
                    '<?php echo $this->config->item('rest_key_name');?>'    :   '<?php echo @$Options[ 'rest_key' ];?>'
                }
            }).then(function(){
                if( $scope.isAreaRoomsDisabled ) {
                    $scope.loadTables();
                } else {
                    _.each( $scope.areas, function( area ) {
                        // Refresh Area table
                        if( area.active ) {
                            $scope.loadTables( area );
                        }
                    });
                }
                $scope.showHistory      =   false;
                $scope.cancelTableSelection();
            });
        }

        /**
         * Free The table
         * if all items has been canceled
         */
        $scope.freeTable    =   function( order ) {

            NexoAPI.showConfirm({
                message     :   GastroVars.messages.freeTable
            }).then( action => {
                if( action.value ) {
                    $http.post( '<?php echo site_url([ 'api', 'gastro', 'tables', 'free-table', store_get_param( '?' ) ]);?>', {
                        order     :   order
                    }, {
                        headers     :   {
                            [ tendoo.rest.key ]     :   tendoo.rest.value
                        }
                    }).then( result => {

                        $scope.closeHistory();

                        if( $scope.isAreaRoomsDisabled ) { 
                            $scope.loadTables();
                        } else {
                            $scope.loadRoomAreas();
                        }
                    });
                }
            });
        }

        /**
         * Send a current order to the kitchen
         * @param void
         * @return void
        **/

        $scope.sendToKitchen    =   function(){
            if( v2Checkout.isCartEmpty() ) {
                return NexoAPI.Toast()( '<?php echo _s( 'The Cart is empty.', 'gastro' );?>' );
            }
            
            if( typeof $scope.selectedOrderType === 'undefined' || [ 'dinein', 'takeaway', 'delivery' ].indexOf( $scope.selectedOrderType.namespace ) == -1 ) {
                //return $scope.askForOrderType( 'sendToKitchen' );
                return NexoAPI.Toast()( '<?php echo _s( 'Please Select Order type.', 'gastro' );?>' );
            }

            if ( ! NexoAPI.events.applyFilters( 'gastro_send_to_kitchen', true, $scope ) ) {
                return false;
            }

            NexoAPI.showConfirm({
                message     :   '<?php echo _s( 'Would you like to send that order to the kitchen ?', 'gastro' );?>',
            }).then( ( result ) => {
                if( result.value ) {
                    v2Checkout.cartSubmitOrder( 'cash', {
                        allow_printing: true,
                        saving_order: false
                    });
                }
            });
        }

        /**
         * Set Order Title
         * @return void
         */
        $scope.setCartTitle         =   function( order_details ){

            /**
             * If the title is yet provided. Then skip this
             */
            if ( order_details.TITRE === undefined || order_details.TITRE.length === 0 ) {
                if( $scope.selectedTable == false ) {
                    if ( $scope.selectedOrderType.namespace === 'delivery' ) {
                        order_details.TITRE   =   '<?php echo __( 'Delivery', 'gastro' );?>';
                    } else if($scope.selectedOrderType.namespace === 'takeaway') {
                        order_details.TITRE   =   '<?php echo __( 'Take Away', 'gastro' );?>';
                    } else {
                          order_details.TITRE   =   '<?php echo __( 'Quick Bill', 'gastro' );?>';
                    }                                        
                } else {
                    order_details.TITRE    =   ( $scope.selectedArea.NAME ?  $scope.selectedArea.NAME + ' > ' : `<?php echo __( 'Dine in : ', 'gastro' );?>` ) + ( $scope.selectedTable.TABLE_NAME || $scope.selectedTable.NAME );
                }
            }
            return order_details;
        }

        /**
         * Watch Select Order History
        **/



        $scope.table_refresh = function() {
            
               // if( newValue ) {
               // $scope.spinner[ 'tableHistory' ]        =   true;
                $scope.historyHasLoaded                 =   false;
                $http.get( '<?php echo site_url([ 'api', 'gastro', 'tables', 'history' ]);?>/' + ( $scope.selectedTable.TABLE_ID || $scope.selectedTable.ID ) +  '<?php echo store_get_param( '?' );?>' ,{
                    headers         :   {
                        '<?php echo $this->config->item('rest_key_name');?>'    :   '<?php echo @$Options[ 'rest_key' ];?>'
                    }
                }).then( ( returned ) => {
                  //  $scope.spinner[ 'tableHistory' ]        =   false;
                    $scope.historyHasLoaded                 =   true;
                    $scope.treatTableOrderHistory( returned.data );
                    $scope.table_history();
                });
            //} else {
               // $scope.historyHasLoaded  =   false;
               // $scope.sessions          =   {};
           // }
              
        }



        $scope.$watch( 'showHistory', function( newValue, oldValue  ) {
            if( newValue ) {
                $scope.spinner[ 'tableHistory' ]        =   true;
                $scope.historyHasLoaded                 =   false;
                $http.get( '<?php echo site_url([ 'api', 'gastro', 'tables', 'history' ]);?>/' + ( $scope.selectedTable.TABLE_ID || $scope.selectedTable.ID ) +  '<?php echo store_get_param( '?' );?>' ,{
                    headers         :   {
                        '<?php echo $this->config->item('rest_key_name');?>'    :   '<?php echo @$Options[ 'rest_key' ];?>'
                    }
                }).then( ( returned ) => {
                    $scope.spinner[ 'tableHistory' ]        =   false;
                    $scope.historyHasLoaded                 =   true;
                    $scope.treatTableOrderHistory( returned.data );
                });
            } else {
                $scope.historyHasLoaded  =   false;
                $scope.sessions          =   {};
            }   

             $scope.table_history();
           

        });

        $scope.table_history           =   function( ) {
        // timeInterval = 0;
            // $timeout( function(){

            //           //console.log(table_id);
            //     if($scope.selectedTable.TABLE_ID!=undefined ){
            //             $scope.table_refresh();
            //     }
            //     //$scope.fetchOrders( () => {
            //      //   $scope.table_refresh();
            //     //});
            // }, $scope.timeInterval  );

        }


        $scope.$watch( 'selectedOrderType', () => {
            v2Checkout.CartSelectedOrderType    =   $scope.selectedOrderType;
        });

        /**
         * Parse Table order history
         * @param object order
         * @return object
        **/
        
        $scope.treatTableOrderHistory           =   function( raw ) {
            $scope.sessions     =   {};
            $scope.states       =   {};
            raw.forEach( ( order, index ) => {

                /**
                 * Let's count the canceled items
                 */
                $scope.states[ order.CODE ]     =   {};

                order.items.forEach( item => {
                    if ( $scope.states[ order.CODE ][ item.restaurant_food_status ] === undefined ) {
                        $scope.states[ order.CODE ][ item.restaurant_food_status ] = 1;
                    } else {
                        $scope.states[ order.CODE ][ item.restaurant_food_status ]++;
                    }
                });  

                // if the order has canceled items and the canceled match the number of items
                if ( 
                    $scope.states[ order.CODE ].canceled !== undefined && 
                    $scope.states[ order.CODE ].canceled === order.items.length && 
                    order.SESSION_ENDS === '0000-00-00 00:00:00' && 
                    order.TYPE !== 'nexo_order_comptant'
                ) {
                    order.$showFreeTable    =   true;
                } else {
                    order.$showFreeTable    =   false;
                }

                /**
                 * We might need to refresh
                 * the session so that while canceling the order everything is updated
                 * as well
                 */
                if( typeof $scope.sessions[ 'session-' + order.SESSION_ID ] == 'undefined' ) {
                    $scope.sessions[ 'session-' + order.SESSION_ID ]    =   {
                        starts      :   order.SESSION_STARTS,
                        ends        :   order.SESSION_ENDS,
                        orders      :   []
                    }
                }

                if( index == 0 ) {
                    $scope.sessionOrder         =   order;  
                }
                
                $scope.sessions[ 'session-' + order.SESSION_ID ].orders.push( order );
            });
        }

        /**
         * Select Order Type
         * @param void
         * @return void
         */
        $scope.selectOrderType  =   function( type, action ) {
            // switch to the order type 
            $scope.types.forEach( _type => {
                _type.selected  =   false;
            });
            type.selected       =   true;

            swal.close();
            
            $scope.switchOrderType( type.namespace );

            /**
             * Only if the order type is delivery or takeaway we can performn an action
             */
            if ( [ 'delivery', 'takeaway' ].indexOf( type.namespace ) != -1 ) {
                if ( action == 'sendToKitchen' ) {
                    $scope.sendToKitchen();
                } else if ( action == 'openPayBox' && NexoAPI.events.applyFilters( 'gastro_open_payment_box', true, $scope ) ) {
                    $rootScope.$emit( 'payBox.openPayBox' );
                }
            } else {
                $scope.submitTo         =   action;
            }

            /**
             * This trigger an event when the user
             * has selected the order type
             */
            NexoAPI.events.doAction( 'gastro_order_type_selected', type );
        }

        /**
         * Ask For Order Type
         */
        $scope.askForOrderType  =   function( action ) {
		 return NexoAPI.Toast()( '<?php echo _s( 'Please Select Order type.', 'gastro' );?>' );
        }

        /**
         * Checks whether an object is empty
         * @param object
         * @return bool
        **/

        $scope.isEmptyObject        =   function( object ) {
            return _.keys( object ).length == 0;
        }

        /**
         * Open table
         * @return void
        **/



        $scope.openTables           =   function(){

            $scope.switchOrderType( 'dinein' );


             /*$scope.hideme = true;*/

          //  ['ng-controller="payBox"'].hide();

        }

        $( '.delivery-button' ).replaceWith( 
            $compile( $( '.delivery-button' )[0].outerHTML )( $scope ) 
        );
        $( '.takeaway-button' ).replaceWith( 
            $compile( $( '.takeaway-button' )[0].outerHTML )( $scope ) 
        );

        /**
         * Register NexoPOS filters
            **/
        NexoAPI.events.addFilter( 'item_loaded', ( item ) => {
            item.restaurant_food_note       =   '';
            item.restaurant_food_printed    =   0;
         

              if(item.PRIX_PROMOTIONEL!=0){
                
                 item.default_item_price    =   item.PRIX_PROMOTIONEL;

            } else {

                item.default_item_price    =   item.PRIX_DE_VENTE_TTC;
            }
            item.restaurant_food_status     =   'not_ready';
            return item;
        })

        NexoAPI.events.removeFilter( 'cart_before_item_name' );

        NexoAPI.events.addFilter( 'cart_before_item_name', function( item_name ) {
            return '<a class="btn btn-sm btn-default restaurant_item_note" href="javascript:void(0)" style="vertical-align:inherit;margin-right:10px;float:left;padding:19% 30% !important;"><i class="fa fa-edit"></i></a> ' + item_name;
        });

        NexoAPI.events.addAction( 'cart_refreshed', function(){
            $( '.restaurant_item_note' ).bind( 'click', function() {
                var item_barcode     =   $( this ).closest( '[cart-item]').attr( 'data-item-barcode');
                var dom             =
                '<div class="form-group">' +
                  '<label for=""></label>' +
                  '<textarea type="text" class="form-control item_note_textarea" id="" placeholder=""/>' +
                  '<p class="help-block">Help text here.</p>' +
                '</div>';

                var item    =   v2Checkout.getItem( item_barcode );

                if( typeof item.metas == 'undefined' ) {
                    item.metas    =   new Object;
                }

                NexoAPI.Bootbox().confirm( '<?php echo _s( 'Add a note to this item', 'gastro' );?>' + dom, function( action ) {
                    item.restaurant_food_note       =   $( '.item_note_textarea' ).val();
                });

                if( angular.isDefined( item.restaurant_food_note ) ) {
                    $( '.item_note_textarea' ).val( item.restaurant_food_note )
                }
            });
        });

        /**
         * If the order is delivery, invite the use to input delivery charges
        **/

        NexoAPI.events.addFilter( 'openPayBox', ( filter ) => {
            if( $scope.selectedOrderType == undefined || $scope.selectedOrderType == false ) {
                $scope.askForOrderType( 'openPayBox' );
                return false;
            }
            return filter;
        });
        

        // When the order is submited, we just change the selected table status
        NexoAPI.events.addFilter( 'test_order_type', function( returned ) {
            if( returned[1].order_type === 'nexo_order_devis' ) {
                returned[0]     =   <?php echo store_option( 'gastro_print_sale_receipt', 'yes' ) === 'yes' ? 'true' : 'false';?>;
            }

            const order     =   returned[1];

            if( $scope.isAreaRoomsDisabled ) {
                var link        =   '<?php echo site_url([ 'api', 'gastro', 'tables', 'status' ]);?>/' +
                $scope.selectedTable.ID;
            } else {
                var link        =   '<?php echo site_url([ 'api', 'gastro', 'tables', 'status' ]);?>/' +
                $scope.selectedTable.TABLE_ID;
            }

            // If the order is a dinein and a complete order, then set the table as available.
            if( order.restaurant_type == 'dinein' && order.order_type == 'nexo_order_comptant' && order.request_type == 'PUT' ) {
                // if a table id is returned by the server.
                if ( order.table_id != undefined && <?php echo store_option( 'gastro_freed_tables', 'no' ) === 'yes' ? 'true' : 'false';?> ) {
                    $scope.setAvailable({
                        ID          :   order.table_id,
                        TABLE_ID    :   order.table_id
                    });
                }
            } else if( 
                order.restaurant_type == 'dinein' && (
                    order.order_type == 'nexo_order_devis' || // if an order is a quote order
                    ( 
                        order.order_type == 'nexo_order_comptant' && order.request_type == 'POST' 
                    ) // or if an order is paid and has been submited for the first time.
                )
            ) {
                // if the order is a dinein and the order is not complete. Then update the table status
                // $http.put( link +  '<?php echo store_get_param( '?' );?>', {
                //     CURRENT_SEATS_USED      :   $scope.seatToUse,
                //     STATUS                  :   'in_use',
                //     ORDER_ID                :   order.order_id,
                //     ORDER_TYPE              :   order.order_type
                // },{
                //     headers          :   {
                //         '<?php echo $this->config->item('rest_key_name');?>' :   '<?php echo @$Options[ 'rest_key' ];?>'
                //     }
                // }).then( ( response ) => {
                    
                // });

                if ( order.order_type === 'nexo_order_comptant' && <?php echo store_option( 'gastro_freed_tables', 'no' ) === 'yes' ? 'true' : 'false';?> ) {
                    $scope.setAvailable({
                        ID          :   order.table_id,
                        TABLE_ID    :   order.table_id
                    });
                }

                $rootScope.$on( 'tables-has-loaded', function( response ){
                    // trigger selected table
                    // since the hook "reset_cart" delete everything
                    _.each( $scope.tables, ( table ) => {
                        if( table.TABLE_ID === response.table_id ) {
                            $scope.selectTable( table );
                        }
                    });      
                })                
            } else if ( order.code === 'item_added' ) {
                /**
                 * This helps to track
                 * wether we're adding new items
                 * or not
                 */
                $scope.printOrder( order );
            }

            return returned;
        });

        NexoAPI.events.addAction( 'nexo_print_complete', function( data ){
            const order     =   data.returned;
            
            /**
             * only if the order is pending
             * it could be printed again
             */
            if ( order.restaurant_status === 'pending' || 
                /**
                 * this should make it print
                 * when : 
                 * -> the kitchen screen is disabled 
                 * -> the order is unpaid
                 * -> the order is ready
                 */
                ( $scope.kitchenScreenDisabled && order.restaurant_status === 'ready' && order.order_type === 'nexo_order_devis' ) ) {
                $scope.printOrder( order );
    
                $scope.sessionOrder         =   false;
                $scope.selectedOrderType    =   false;
                $scope.selectedTable        =   false;
            }

            return order;
        });

        /**
         * submit an order to a table as a new order
         */
        $scope.newOrder             =   function() {
            if ( v2Checkout.CartItems.length === 0 ) {
                return swal({
                    title: `<?php echo __( 'Unable to proceed', 'gastro' );?>`,
                    text: `<?php echo __( 'The order can\'t been placed since no items has been added to the cart.', 'gastro' );?>`
                });
            }

            swal({
                title: `<?php echo __( 'Confirm Your Action', 'gastro' );?>`,
                text: `<?php echo __( 'Would you like to place this order as a new order on the current table ?', 'gastro' );?>`,
                showCancelButton: true
            }).then( action => {
                if ( action.value ) {
                    HttpRequest.post(
                        `<?php echo site_url([ 'api', 'gastro', 'push-order' ]);?>`,
                        {
                            table: $scope.selectedTable,
                            items: v2Checkout.CartItems,
                            customer: v2Checkout.CartCustomerID
                        }
                    ).then( result => {
                        console.log( result );
                    }).catch( error => {
                        console.log( error );
                    })
                }
            })
        }

        /**
         * Print Order
         */
        $scope.printOrder               =   function( order ) {
            /**
             * We should be printing only if the order is submited once or when 
             * we're adding new item on dine in order
             */
            let singlePrintUrl, multiplePrintURL;
            
            if ( GastroVars.base64Print === 'yes' ) {
                singlePrintURL      =   '<?php echo site_url([ 'api', 'gastro', 'kitchens', 'base64-single-print' ]);?>/';
                mutiplePrintURL     =   '<?php echo site_url([ 'api', 'gastro', 'kitchens', 'base64-splitted-print' ]);?>/';
            } else {
                <?php if ( store_option( 'gastro_print_gateway', 'gcp' ) == 'gcp' ):?>

                singlePrintURL      =   '<?php echo site_url([ 'api', 'gastro', 'kitchens', 'print' ]);?>/';
                mutiplePrintURL     =   '<?php echo site_url([ 'api', 'gastro', 'kitchens', 'split-print' ]);?>/';
                <?php else:?>
                singlePrintURL      =   '<?php echo site_url([ 'api', 'gastro', 'kitchens', 'nps-single-print' ]);?>/';
                mutiplePrintURL     =   '<?php echo site_url([ 'api', 'gastro', 'kitchens', 'nps-splitted-print' ]);?>/';
                <?php endif;?>

                kitchens_get     =   '<?php echo site_url([ 'api', 'gastro', 'kitchens', 'getkitchen' ]);?>';
                kitchens_equal = '<?php echo site_url([ 'api', 'gastro', 'kitchens', 'kitchens_products' ]);?>';
            }

            if ( order[ 'restaurant_status' ] == 'pending' || $scope.kitchenScreenDisabled || order[ 'code' ] == 'item_added' ) {

               // console.log(get_option( store_prefix() . 'disable_kitchen_print' ));
                <?php if( get_option( store_prefix() . 'disable_kitchen_print' ) == 'no' ) { ?>
                    <?php if( get_option( store_prefix() . 'printing_option' ) == 'kitchen_printers'):?>


                        $http.get( kitchens_get ,{
                            headers         :   {
                                '<?php echo $this->config->item('rest_key_name');?>'    :   '<?php echo @$Options[ 'rest_key' ];?>'
                            }
                        }).then( ( returned ) => {
                            
                           /// console.log(returned.data);

                           // array_data= [];

                            returned.data.map( data => {
                                  
                                const url   =   mutiplePrintURL + order.order_id +'/'+data.ID +'<?php echo store_get_param( '?' );?>&app_code=<?php echo store_option( 'nexopos_app_code' );?>';
                                    
                                var delivery_kitchen = '<?php echo get_option( store_prefix() . 'delivery_kitchen' );?>';

                                var takeaway_kitchen ='<?php echo get_option( store_prefix() . 'takeaway_kitchen' );?>';

                                var printer_takeway = '<?php echo store_option( 'printer_takeway' );?>';

                                var kitchens_printer = '';

                              

                                    $http.get( kitchens_equal+'/'+data.ID+'/'+order.order_id ,{
                                        headers         :   {
                                            '<?php echo $this->config->item('rest_key_name');?>'    :   '<?php echo @$Options[ 'rest_key' ];?>'
                                        }
                                    }).then( ( returned ) => {


                                           kitchens_printer ='';
                                           if(data.Printer_name!=''){
                                                     
                                             kitchens_printer = data.Printer_name;

                                           } else if(printer_takeway!='') {
                                             kitchens_printer = printer_takeway;
                                           } else {

                                             kitchens_printer ='';
                                           }



                                        if(returned.data==1){

                                            const url   =   mutiplePrintURL + order.order_id +'/'+data.ID +'<?php echo store_get_param( '?' );?>&app_code=<?php echo store_option( 'nexopos_app_code' );?>';
                                            

                                               
                                             if(kitchens_printer!='Choose an option'&&kitchens_printer!='') {


                                                if(printServerURL==''){
                                                    var host_url = 'http://'+'<?php echo $_SERVER['HTTP_HOST'];?>'+':1000';

                                                } else {

                                                    var host_url = printServerURL;
                                                }

                                                //var host_url = '<?php //echo $_SERVER['HTTP_HOST'];?>';


                                                var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
                                                var theUrl = host_url+"/sendsample";
                                                xmlhttp.onreadystatechange = function() {
                                                    if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                                                        console,log(xmlhttp.responseText);
                                                    }
                                                }

                                                xmlhttp.open("POST", theUrl);
                                                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                                                xmlhttp.send(JSON.stringify({'URL'   :   url + '' + returned.order_id,'PRINTER'   :   kitchens_printer}));  

                                            }



                                        } 
                                       
                                    });
                                    // $scope.tables_get_all();
                                     console.log( 'has finished kitchen receipt' );
                                     if(order.restaurant_type=='dinein'){

                                            var scope = angular.element(document.getElementById('dinein')).scope();
                                            let myPromise = new Promise(function(myResolve, myReject) {
                                            // "Producing Code" (May take some time)
                                            scope.openTables();
                                           
                                            myResolve(); // when successful
                                            
                                            });

                                         
                                            myPromise.then(
                                            function() { 
                                                let myPromise = new Promise(function(myResolve, myReject) {
                                                    // "Producing Code" (May take some time)
                                                    
                                                        _.each( $scope.tables_all, ( table ) => {                    
                                                                    if( table.TABLE_ID === order.table_id ) {
                                                                        $scope.selectTable( table );
                                                                        // console.log(table);
                                                                       
                                                                    }
                                                                });
                                                 
                                                    myResolve();
                                                });

                                                myPromise.then(

                                                    function() { 

                                                    },
                                                    function(error){
                                                        scope.openTables();
                                                       
                                                    });
                                                    
                                                
                                             },
                                            function(error) {

                                               
                                                
                                             }
                                            );

                                         

                                        } else if (order.restaurant_type=='delivery') {

                                          
                                            var scope = angular.element(document.getElementById('delivery')).scope();
                                            scope.switchOrderType( 'delivery' );

                                        } else {

                                            
                                            var scope = angular.element(document.getElementById('takeaway')).scope();
                                            scope.switchOrderType( 'takeaway' );

                                            
                                        }
                                        
                                            <?php   if( store_option( 'disable_delivery' ) == 'yes' ):?>
    
    
                                            $('#delivery').hide();
                                                
                                            <?php endif;?>
                                            
                                            <?php if( store_option( 'disable_dinein' ) == 'yes' ):?>
                                      
                                             $('#dinein').hide();
                                            <?php endif;?>
                                            
                                            <?php if( store_option( 'disable_takeaway' ) == 'yes' ):?>
                                            
                                             $('#takeaway').hide();
                                           
                                              <?php endif; ?>

                               // }


                                 //array_data.push(data.REF_CATEGORY);

                                     
                            });
                             // var ar = data.REF_CATEGORY.split(',');
                             // cleanArray = ar.filter(function () { return true });
                             //console.log(ar);
                            //  var filtered = ar.filter(function (el) {
                            //   return el != null;
                            // });

                               // $http.get( kitchens_equal+'/'+data.ID+'/'+order.order_id ,{
                               //      headers         :   {
                               //          '<?php echo $this->config->item('rest_key_name');?>'    :   '<?php echo @$Options[ 'rest_key' ];?>'
                               //      }
                               //  }).then( ( returned ) => {

                               //      console.log(returned);
                               //  });

                           // console.log(array_data);


                        });



                        <?php $kitchens    =  $this->db->get( store_prefix() . 'nexo_restaurant_kitchens' )->result_array();?>
                        const url   =   mutiplePrintURL + order.order_id + '/1<?php echo store_get_param( '?' );?>&app_code=<?php echo store_option( 'nexopos_app_code' );?>';
                        //const url_1   =   mutiplePrintURL + order.order_id +  '<?php echo store_get_param( '?' );?>&app_code=<?php echo store_option( 'nexopos_app_code' );?>';
                        console.log(url);
                        // HttpRequest.get( url ).then( async ({data}) => {
                        //     <?php if ( store_option( 'gastro_print_gateway', 'gcp' ) == 'nps' ):?>
                        //     if ( data.length === undefined ) { // if length is defined, it means that here is an error [{ status : 'failed', message: 'No new item to print' }]
                        //         if ( data.receipts === undefined ) {
                        //             return swal({
                        //                 title: '<?php echo __( 'Gastro Misconfiguration Error', 'gastro' );?>',
                        //                 text: `<?php echo __( 'It seems like Gastro is not configured correctly. We\'ve been expecting a split printing result, 
                        //                 but a normal result has been returned. Make sure you\'ve correctly configured "Print Option".' );?>`,
                        //                 type: 'error'
                        //             })
                        //         } else {
                        //             for( let index = 0; index < data.receipts.length; index++ ) {
                        //                 const receipt   =   data.receipts[ index ];
                        //                 const result    =   await new Promise( ( resolve, reject ) => {
                        //                     if ( GastroVars.base64Print === 'yes' ) {
                        //                         $( '#gastro-print-base64' ).remove();
                        //                         $( 'body' ).append( `<div id="gastro-print-base64" style="width:580px">${receipt.content}</div>` );

                        //                         html2canvas( document.getElementById( 'gastro-print-base64' ), {
                        //                             logging: true
                        //                         }).then(function(canvas) {
                        //                             const image     =   canvas.toDataURL();
                        //                             HttpRequest.post( base64URL, {
                        //                                 'base64'     :   image,
                        //                                 'printer'    :   receipt.printer
                        //                             }).then( results => {
                        //                                 $( '#gastro-print-base64' ).remove();
                        //                                 resolve( results );
                        //                             }).catch( error => {
                        //                                 $( '#gastro-print-base64' ).remove();
                        //                                 reject( error )
                        //                             })
                        //                         }).catch( err => console.log( err ) );
                        //                     } else {
                        //                         $.ajax( '<?php echo store_option( 'nexo_print_server_url' );?>/api/print', {
                        //                             type     :   'POST',
                        //                             data     :   {
                        //                                 'content'    :   receipt.content,
                        //                                 'printer'    :   receipt.printer
                        //                             },
                        //                             dataType     :   'json',
                        //                             success  :   function( result ) {
                        //                                 console.log( 'has finished kitchen receipt' );
                        //                                 resolve( result );
                        //                             }
                        //                         });
                        //                     }
                        //                 });
                        //             }
                        //         }
                        //     }
                        //     <?php endif;?>
                        // }).catch( result => {
                        //     return swal({
                        //         title: '<?php echo __( 'Gastro Misconfiguration Error', 'gastro' );?>',
                        //         html: `<?php echo __( 'It seems like the order could\'nt been send to the kitchen because :', 'gastro' );?> <br>
                        //         <?php echo __( '<ul class="text-left">
                        //             <li>Either the kitchen doesn\'nt handle the item category</li>
                        //             <li>No printer is assigned to the kitchen</li>
                        //         </ul>', 'gastro' );?>
                        //         <?php echo __( 'Solving these points could make the printing works.', 'gastro' );?>`,
                        //         type: 'error'
                        //     });
                        // });
                    <?php elseif( store_option( 'printing_option' ) == 'single_printer' ):?>


                        // $http.get( singlePrintURL + order.order_id +  '<?php echo store_get_param( '?' );?>&app_code=<?php echo store_option( 'nexopos_app_code' );?>',{
                        //     headers          :   {
                        //         '<?php echo $this->config->item('rest_key_name');?>' :   '<?php echo @$Options[ 'rest_key' ];?>'
                        //     }
                        // }).then( ( returned ) => {
                        //     <?php if ( store_option( 'gastro_print_gateway', 'gcp' ) == 'nps' ):?>
                        //     if ( typeof returned.data != 'string' ) {
                        //         return swal({
                        //             title: '<?php echo __( 'Gastro Misconfiguration Error', 'gastro' );?>',
                        //             text: `<?php echo __( 'It seems like Gastro is not configured correctly. We\'ve been expecting a single printing result, 
                        //             but a split result has been returned. Make sure you\'ve correctly configured "Print Option".' );?>`,
                        //             type: 'error'
                        //         })
                        //     } else {
                        //         if ( GastroVars.base64Print === 'yes' ) {
                        //             $( '#gastro-print-base64' ).remove();
                        //             $( 'body' ).append( `<div id="gastro-print-base64" style="width:580px">${returned.data}</div>` );

                        //             html2canvas( document.getElementById( 'gastro-print-base64' ), {
                        //                 logging: true
                        //             }).then(function(canvas) {
                        //                 const image     =   canvas.toDataURL();
                        //                 HttpRequest.post( base64URL, {
                        //                     'base64'     :   image,
                        //                     'printer'    :   '<?php echo store_option( 'printer_takeway' );?>'
                        //                 }).then( results => {
                        //                     $( '#gastro-print-base64' ).remove();
                        //                 }).catch( error => {
                        //                     $( '#gastro-print-base64' ).remove();
                        //                 })
                        //             }).catch( err => console.log( err ) );
                        //         } else {
                        //             $.ajax( '<?php echo store_option( 'nexo_print_server_url' );?>/api/print', {
                        //                 type     :   'POST',
                        //                 data     :   {
                        //                     'content'    :   returned.data,
                        //                     'printer'    :   '<?php echo store_option( 'printer_takeway' );?>'
                        //                 },
                        //                 dataType     :   'json',
                        //                 success  :   function( result ) {
                        //                     console.log( result );
                        //                 }
                        //             });
                        //         }
                        //     }
                        //     <?php endif;?>
                        // });
                                  
                                const url_single   =   singlePrintURL + order.order_id +'/<?php echo store_get_param( '?' );?>&app_code=<?php echo store_option( 'nexopos_app_code' );?>';
                               

                                var printer_takeway = '<?php echo store_option( 'printer_takeway' );?>';

                                    if(printServerURL==''){
                                        var host_url = 'http://'+'<?php echo $_SERVER['HTTP_HOST'];?>'+':1000';

                                    } else {

                                        var host_url = printServerURL;
                                    }
                                
                                    var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
                                    var theUrl = host_url+"/sendsample";
                                    xmlhttp.onreadystatechange = function() {
                                        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                                            console.log(xmlhttp.responseText);
                                        }
                                    }

                                    xmlhttp.open("POST", theUrl);
                                    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                                    xmlhttp.send(JSON.stringify({'URL'   :   url_single + '' + returned.order_id,'PRINTER'   :   printer_takeway}));  

                                                

                                             
                                     console.log( 'has finished kitchen receipt' );
                                     
                                     if(order.restaurant_type=='dinein'){

                                            var scope = angular.element(document.getElementById('dinein')).scope();
                                            let myPromise = new Promise(function(myResolve, myReject) {
                                            // "Producing Code" (May take some time)
                                            scope.openTables();
                                           
                                            myResolve(); // when successful
                                            
                                            });

                                         
                                            myPromise.then(
                                            function() { 
                                                let myPromise = new Promise(function(myResolve, myReject) {
                                                    // "Producing Code" (May take some time)
                                                    
                                                        _.each( $scope.tables_all, ( table ) => {                    
                                                                    if( table.TABLE_ID === order.table_id ) {
                                                                        $scope.selectTable( table );
                                                                        // console.log(table);
                                                                       
                                                                    }
                                                                });
                                                 
                                                    myResolve();
                                                });

                                                myPromise.then(

                                                    function() { 

                                                    },
                                                    function(error){
                                                        scope.openTables();
                                                       
                                                    });
                                                    
                                                
                                             },
                                            function(error) {

                                               
                                                
                                             }
                                            );

                                        } else if (order.restaurant_type=='delivery') {

                                          
                                            var scope = angular.element(document.getElementById('delivery')).scope();
                                            scope.switchOrderType( 'delivery' );

                                        } else {

                                            
                                            var scope = angular.element(document.getElementById('takeaway')).scope();
                                            scope.switchOrderType( 'takeaway' );

                                            
                                        }
                                        
                                           <?php   if( store_option( 'disable_delivery' ) == 'yes' ):?>
    
    
                                            $('#delivery').hide();
                                                
                                            <?php endif;?>
                                            
                                            <?php if( store_option( 'disable_dinein' ) == 'yes' ):?>
                                      
                                             $('#dinein').hide();
                                            <?php endif;?>
                                            
                                            <?php if( store_option( 'disable_takeaway' ) == 'yes' ):?>
                                            
                                             $('#takeaway').hide();
                                           
                                              <?php endif; ?>
                                
                    <?php endif;?>
                <?php } else {?>
                        
                         if(order.restaurant_type=='dinein'){

                                var scope = angular.element(document.getElementById('dinein')).scope();
                                 let myPromise = new Promise(function(myResolve, myReject) {
                                // "Producing Code" (May take some time)
                                scope.openTables();
                               
                                myResolve(); // when successful
                                
                                });

                             
                                myPromise.then(
                                function() { 
                                    let myPromise = new Promise(function(myResolve, myReject) {
                                        // "Producing Code" (May take some time)
                                        
                                            _.each( $scope.tables_all, ( table ) => {                    
                                                        if( table.TABLE_ID === order.table_id ) {
                                                            $scope.selectTable( table );
                                                            // console.log(table);
                                                           
                                                        }
                                                    });
                                     
                                        myResolve();
                                    });

                                    myPromise.then(

                                        function() { 

                                        },
                                        function(error){
                                            scope.openTables();
                                           
                                        });
                                        
                                    
                                 },
                                function(error) {

                                   
                                    
                                 }
                                );

                                

                            } else if (order.restaurant_type=='delivery') {

                              
                                var scope = angular.element(document.getElementById('delivery')).scope();
                                scope.switchOrderType( 'delivery' );

                            } else {

                                
                                var scope = angular.element(document.getElementById('takeaway')).scope();
                                scope.switchOrderType( 'takeaway' );

                                
                            }
                <?php } ?>
            }
        }

        /**
         * Prevent submitting order while order type is not provided.
         */
        NexoAPI.events.addFilter( 'nexo_open_save_box', function( something ) {
            if ( $scope.selectedOrderType == undefined || $scope.selectedOrderType == false ) {
                NexoAPI.showError({
                    message     :   '<?php echo _s( 'You should define an order type before saving that order', 'gastro' );?>'
                });
                return false;
            }
            return something;
        })

        // All this works only if we're making a new order
        NexoAPI.events.addFilter( 'before_submit_order', function( data ){
            // only when pay box is on
            // if( order_details.SOMME_PERCU < order_details.TOTAL && $( '.paxbox-box' ).length > 0 ) {
            //     NexoAPI.Bootbox().alert( '<?php echo _s( 'Incomplete order aren\'t allowed', 'gastro' );?>' );
            //     return {}
            // }

            if( [ 'POST', 'PUT' ].indexOf( v2Checkout.ProcessType ) !== -1 ) {
                if ( v2Checkout.ProcessType === 'POST' ) {
                    $scope.setCartTitle( data.order_details );
    
    


               if($scope.selectedOrderType.namespace!='quickbill'){
                    // no table has been selected
                   
                    data.order_details.ITEMS.forEach( ( item ) => {
                      
                        item.restaurant_food_status     =   'not_ready';
                        item.restaurant_food_issue      =   '';
                        item.restaurant_food_printed    =   item.restaurant_food_printed || 0;
                        item.restaurant_food_modifiers  =   item.restaurant_food_modifiers || {};
                         item.restaurant_food_printer =   item.PRODUCT_SKIP_PRINT;
                        /**
                        * We want this item stock to be 
                        * handled. Because, inline item stock
                        * is ignored by default.
                        */
                        item.inline                         =   false;          

                        if( angular.isUndefined( item.restaurant_food_note ) ) {
                            item.restaurant_food_note         =   '';
                        }

                        return item;
                    });

                } else {


                      data.order_details.ITEMS.forEach( ( item ) => {
                          
                        item.restaurant_food_status     =   'ready';
                        item.restaurant_food_issue      =   '';
                        item.restaurant_food_printed    =   item.restaurant_food_printed || 0;
                        item.restaurant_food_modifiers  =   item.restaurant_food_modifiers || {};
                        item.restaurant_food_printer =   item.PRODUCT_SKIP_PRINT;

                        /**
                        * We want this item stock to be 
                        * handled. Because, inline item stock
                        * is ignored by default.
                        */
                        item.inline                         =   false;          

                        if( angular.isUndefined( item.restaurant_food_note ) ) {
                            item.restaurant_food_note         =   '';
                        }

                        return item;
                    });

                }
                    
                    /**
                    * If we're processing an order for the first time, we can set order type and order status
                    * these informations can't be set when we're editing an order
                    */
                    if ( data.saving_order ) {
                        data.order_details[ 'RESTAURANT_ORDER_TYPE' ]       =   $scope.selectedOrderType.namespace;
                        data.order_details[ 'RESTAURANT_ORDER_STATUS' ]     =   'hold';
                    } else {
                        if( $scope.selectedOrderType.namespace == 'delivery' ) {
                            <?php if( store_option( 'disable_kitchen_screen' ) === 'yes' ):?>
                            data.order_details[ 'RESTAURANT_ORDER_TYPE' ]    =   'delivery';
                            data.order_details[ 'RESTAURANT_ORDER_STATUS' ]  =   'ready';
                            <?php else:?>
                            data.order_details[ 'RESTAURANT_ORDER_TYPE' ]    =   'delivery';
                            data.order_details[ 'RESTAURANT_ORDER_STATUS' ]  =   'pending';
                            <?php endif;?>
                        } else if( $scope.selectedOrderType.namespace == 'takeaway' ) {
                            <?php if( store_option( 'disable_kitchen_screen' ) === 'yes' ):?>
                            data.order_details[ 'RESTAURANT_ORDER_TYPE' ]    =   'takeaway';
                            data.order_details[ 'RESTAURANT_ORDER_STATUS' ]  =   'ready';
                            <?php else:?>
                            data.order_details[ 'RESTAURANT_ORDER_TYPE' ]    =   'takeaway';
                            data.order_details[ 'RESTAURANT_ORDER_STATUS' ]  =   'pending';
                            <?php endif;?>

                         } else if( $scope.selectedOrderType.namespace == 'quickbill' ) {
                        
                            data.order_details[ 'RESTAURANT_ORDER_TYPE' ]    =   'quickbill';
                            data.order_details[ 'RESTAURANT_ORDER_STATUS' ]  =   'ready';
                           
                        
                        } else if( $scope.selectedOrderType.namespace == 'dinein' ) {
                            <?php if( store_option( 'disable_kitchen_screen' ) === 'yes' ):?>
                            data.order_details[ 'RESTAURANT_ORDER_TYPE' ]    =   'dinein';
                            data.order_details[ 'RESTAURANT_ORDER_STATUS' ]  =   'ready';
                            <?php else:?>
                            data.order_details[ 'RESTAURANT_ORDER_TYPE' ]    =   'dinein';
                            data.order_details[ 'RESTAURANT_ORDER_STATUS' ]  =   'pending';
                            <?php endif;?>
                        }
                    }
                }
            }
            
            return data;
        });

        /**
         * This will add a new button on the paybox
        **/

        if( $( '.sendToKitchenButton' ).length > 0 ) {
            $( '.sendToKitchenButton' ).replaceWith( $compile( $( '.sendToKitchenButton' )[0].outerHTML )( $scope ) )
        }
        
        /**
         * Implementing To kitchen button
         */

        NexoAPI.events.addAction( 'reset_cart', function() {
            
            if( angular.element( '.table-selection-box' ).length == 0 ) {
                $scope.selectedTable            =   false;
                $scope.selectedArea             =   false;
            }

            $scope.wasOpenFromTableHistory  =   false;

            // if table selection is not already open
            if( angular.element( '.table-selection-box' ).length == 0 ) {
                if( typeof $scope.selectedOrderType != 'undefined' ) {
                    $scope.toggleConflictingButton( 'show' );
                    $timeout( function(){
                        if( $scope.selectedOrderType.namespace == 'dinein' ) {

                              // angular.element( '[ng-controller="payBox"]' ).hide();

                              // angular.element($('.payerbutton')).hidegrid = true;
           // $('.payerbutton').hide();
                            $scope.openTableSelection();


                        }
                    }, 500 );        
                }
            }
            $scope.modifiers    =   [];
        });

        setInterval( () => {
            $http.get( '<?php echo site_url([ 'api', 'gastro', 'kitchens', 'google-refresh', store_get_param( '?' ) ]);?>' + '&app_code=<?php echo @$Options[ store_prefix() . 'nexopos_app_code' ];?>' ,{
                headers         :   {
                    '<?php echo $this->config->item('rest_key_name');?>'    :   '<?php echo @$Options[ 'rest_key' ];?>'
                }
            }).then( ( returned ) => {
                console.log( 'google-refresh' );
            });
        }, 3400 * 1000 );
        
        /**
        * Update order type when an order is being open
        **/

        NexoAPI.events.addFilter( 'override_open_order', function({ order_details, proceed }) {
            
            
            
            
            
            _.each( $scope.types, function( type, index ) {
                if(

                _.indexOf( [ 
                    'takeaway', 
                    'dinein', 
                    'delivery' 
                ], order_details.order.RESTAURANT_ORDER_TYPE ) != -1 && 

                _.indexOf( [ 
                    'takeaway', 
                    'dinein', 
                    'delivery' 
                ], type.namespace ) != -1 ) {
                    if( order_details.order.RESTAURANT_ORDER_TYPE == type.namespace ) {
                        
                        
                        <?php   if( store_option( 'disable_delivery' ) == 'yes' ):?>
    

                        $('#delivery').hide();
                            
                        <?php endif;?>
                        
                        <?php if( store_option( 'disable_dinein' ) == 'yes' ):?>
                  
                         $('#dinein').hide();
                        <?php endif;?>
                        
                        <?php if( store_option( 'disable_takeaway' ) == 'yes' ):?>
                        
                         $('#takeaway').hide();
                       
                          <?php endif; ?>

                                              if(type.namespace=='takeaway'){
                          

                        $('#payerbutton').show();
                        $('#paylater').show();

                        $('.sendToKitchenButtonWrapper').hide();
                        
                         if(order_details.order.RESTAURANT_ORDER_STATUS=='hold'){
                        
                         NexoAPI.Bootbox().alert('You are retrieving the order from takeaway . You cannot change order type. need change order type please click to the retreive and cancel it');
                         
                         }
        
                    } else if(type.namespace=='delivery'){
                         $('#payerbutton').show();
                         $('#paylater').hide();

                         $('.sendToKitchenButtonWrapper').hide();
                        if(order_details.order.RESTAURANT_ORDER_STATUS=='hold'){
                          NexoAPI.Bootbox().alert('You are retrieving the order from delivery . You cannot change order type. need change order type please click to the retreive and cancel it');
                          
                        }
                        
                    }else {
                        
                     
                     
                    if(order_details.order.RESTAURANT_ORDER_STATUS=='hold'){
                        
                        var tables_all_new = $scope.tables_get_all();
                        
                       // console.log(123212);
                     //   console.log($scope.tables_all);
                        //  if(order_details.order.RESTAURANT_ORDER_STATUS=='hold'){
                       
                       //  console.log(tables_all_new);
                    //    console.log(order_details.order.RESTAURANT_TABLE_ID);
                        
                        _.each( $scope.tables_all, ( table ) => {                    
                            if( table.TABLE_ID == order_details.order.RESTAURANT_TABLE_ID ) {
                                var table_no = table.TABLE_NAME;
                               
                               
                            }
                        });
                        
                        NexoAPI.Bootbox().alert('You are retrieving the order from table '+order_details.order.RESTAURANT_TABLE_ID+'.You cannot change table and order type.');
                        console.log(order_details.order.RESTAURANT_TABLE_ID);
                  
                        
                    }
                            
                        $('#payerbutton').hide();
                        $('#paylater').hide();
                        $('.sendToKitchenButtonWrapper').show();
                    }

                    
                        $scope.selectedOrderType    =   type;
                        type.active                 =   true;
                    } else {
                        type.active                 =   false;
                    }
                }
            });

            // 
            // $scope.switchOrderType();

            // an error may occur if the selected order type is not yet supproted by the system

            return {order_details, proceed};
        });

        /**
         * Refresh modfiiers
        **/

        // NexoAPI.events.addAction( 'cart_refreshed', () => {
        //     // refresh item modifiers
        //     _.each( v2Checkout.CartItems, ( item, index ) => {
        //         if( typeof item.restaurant_food_modifiers != 'undefined' ) {
        //             if( typeof item.restaurant_food_modifiers !== 'undefined' ) {
        //                 let modifiersLabels     =   '';
        //                 let modifiers           =   typeof item.restaurant_food_modifiers == 'string' ? JSON.parse( item.restaurant_food_modifiers ) : item.restaurant_food_modifiers;
        //                 _.each( modifiers, ( modifier ) => {
        //                     if( parseInt( modifier.default ) == 1 ) { // means if the modifiers is active
        //                         modifiersLabels     +=  '<span class="label label-default"> + ' + modifier.group_name + ' : ' + modifier.name + '</span> &mdash; ' + NexoAPI.DisplayMoney( modifier.price ) + '<br>';

                             
                                     
        //                         console.log(item);

                             

        //                          // modifiersLabels     += '<ul class="childs"><li><span>Tea</span></li><li><span>Coffee</span></li><li><span>test</span></li></ul></div>';

        //                        //console.log(modifier);   

        //                     }
        //                 });

        //                 $( '[cart-item-id="'+ index + '"] .item-name' ).after( modifiersLabels );
        //             }
        //         }
        //     }); 

          


        // });

      NexoAPI.events.addAction( 'cart_refreshed', () => {
            // refresh item modifiers
            _.each( v2Checkout.CartItems, ( item, index ) => {

                // console.log();
                // console.log(2345);
                if( typeof item.restaurant_food_modifiers != 'undefined' ) {
                    if( typeof item.restaurant_food_modifiers !== 'undefined'&&item.restaurant_food_modifiers!='') {
                        
                        

                        let modifiersLabels     =   '';
                        let modifiers           =   typeof item.restaurant_food_modifiers == 'string' ? JSON.parse( item.restaurant_food_modifiers ) : item.restaurant_food_modifiers;


                        let lsmodifer = modifiers;
                       
                        let modili = '';
                        
                       
                        
                         modili= '<button id="btnmodifier" data-id="'+index+'" data-value="'+item.REF_MODIFIERS_GROUP+'" class="btn btn-default btnmodifier" style="position: relative;border-radius: 3px;border: #fff !important;width: 45px;padding: 2% 6%; margin-left: 26%;margin-top: -3px;"><i class="fa fa-bars" style="    font-size: 11px; padding: 35%;"></i></button>';
                        $( '#Modifieritem_'+index ).html( modili );



                         modifiersLabels='<div>'; 
                        _.each( modifiers, ( modifier ) => {
                            modiferprize = 0;
                            if( parseInt( modifier.default ) == 1 ) { // means if the modifiers is active
                                modiferprize = parseFloat(modiferprize)+parseFloat(modifier.price);
                               
                                 modifiersLabels+=  '<span class="label label-default pointer" onclick="toggle(\'navbar_'+index+'\',\'TLink_'+index+'\')"> + ' + modifier.group_name + ' : ' + modifier.name + '</span> &mdash; ' + NexoAPI.DisplayMoney( modiferprize ) + '<br>';
                            }
                               
                            
                        });
                         modifiersLabels+='</div>'; 



                        $( '[cart-item-id="'+ index + '"] .item-name' ).after( modifiersLabels );
                    }
                }
            }); 

          


        });




      $scope.get_modifiers1        =   function(item_index ,group_id ) {
          
                    var group_id_new = group_id.split(',');
                 
                    var res= group_id_new.join('&');
                    
                    var group_id = 11;
                    
                    $http.get(
                        '<?php echo site_url( array( 'api', 'gastro', 'modifiers', 'by-group' ) );?>' + '/' +
                        group_id + '?<?php echo store_get_param( null );?>'+res,
                    {
                        headers         :   {
                            '<?php echo $this->config->item('rest_key_name');?>'    :   '<?php echo @$Options[ 'rest_key' ];?>'
                        }
                    }).then(function( response ){
                        
                        $scope.modifiers    =   response.data;
                        if ( $scope.modifiers.length > 0 ) {
                            $scope.group_name   =   $scope.modifiers[0].group_name;



                    $( '.modal-header').html(
                                '<?php echo __( 'Choose a modifier for : <strong>#s<strong>', 'gastro' );?>'.replace( '#s', $scope.group_name )
                            )
            
                    $( '.modal-dialog').css('width','44%');
    
    

                        modili = '<div class="row">';

                        modili+= '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">';


                            //modili= '<div  style="display: table;" id="navbar_1" class="row navbar modinavstart ';
                            _.each(  v2Checkout.CartItems[item_index].restaurant_food_modifiers, function( value, key ) {

                               // i = 0;
                            if( parseInt( value.default ) == 1 ) { 
                            modili += '<div class="col-lg-3 col-md-3 col-xs-4 modifi modify-index-'+item_index+'-key-'+key+' modifiers-item-active" attr-index="'+item_index+'" attr-modifierval="'+key+'" style=";padding:3px;border-right:solid 1px#DEDEDE;border-bottom: solid 1px #DEDEDE;"><div>';
                              //   i++;

                               modiliimage = value.image == '' ? '<?php echo module_url('nexo' ) . '/images/default.png';?>': '<?php echo get_store_upload_url() . 'items-images/';?>"' + value.image ;

                               //modilfierprice =;

                               modilfiername= value.name


                           amount =  '<span>{{ '+value.price+' | moneyFormat }}</span>';
                                 modili+= '<img data-original="http://localhost/New_Office/restaurant_pos_webapp/public/upload/items-images/d5828-main-qimg-2d84d946a4be9c31151f5d24d246c0bb.gif" style="display: block; width: 100%; min-height: 153px; max-height: 141px;" class="img-responsive img-rounded lazy" src='+modiliimage+'><div class="caption text-center" style="padding: 2px;overflow: hidden;position: absolute;bottom: 15px;z-index: 99999;width: 95%;background: #ffffffc9;"><strong class="item-grid-title">'+modilfiername +'</strong><br><span class="align-center">'+  NexoAPI.DisplayMoney( amount )+'</span></div>';
                               

                                 modili+= '</div></div>';

                              } else {

                               modili += '<div class="col-lg-3 col-md-3 col-xs-4 modifi modify-index-'+item_index+'-key-'+key+' modifiers-item-unactive" attr-index="'+item_index+'" attr-modifierval="'+key+'" style=";padding:3px;border-right:solid 1px#DEDEDE;border-bottom: solid 1px #DEDEDE;"><div>';
                              //   i++;

                               modiliimage = value.image == '' ? '<?php echo module_url('nexo' ) . '/images/default.png';?>': '<?php echo get_store_upload_url() . 'items-images/';?>"' + value.image ;

                               //modilfierprice =;

                               modilfiername= value.name


                           amount =  '<span>{{ '+value.price+' | moneyFormat }}</span>';
                                 modili+= '<img data-original="http://localhost/New_Office/restaurant_pos_webapp/public/upload/items-images/d5828-main-qimg-2d84d946a4be9c31151f5d24d246c0bb.gif" style="display: block; width: 100%; min-height: 153px; max-height: 141px;" class="img-responsive img-rounded lazy" src='+modiliimage+'><div class="caption text-center" style="padding: 2px;overflow: hidden;position: absolute;bottom: 15px;z-index: 99999;width: 95%;background: #ffffffc9;"><strong class="item-grid-title">'+modilfiername +'</strong><br><span class="align-center">'+  NexoAPI.DisplayMoney( value.price )+'</span></div>';
                                
                             

                                 modili+= '</div></div>';

                             }


                                
                                
                            });

                          

                            modili+= '</div>';
                            modili+= '</div>';

                           

                          $('.modal-body').html( modili );

                         
                      }
                   });
                
                }




        $scope.runModifierPromise       =   function({ ids, index, increase, quantity, item, modifierIndex, proceed }) {
            
            
            let modifierPromise     =   new Promise( ( resolve, reject ) => {
               // it will be used on the modifiers directive
                if( ids.length > index ) {
                    resolve({ ids, index, increase, quantity, item, modifierIndex,modifierIndex,proceed });
                } else {
                    reject();
                }
            });
    
          
            modifierPromise.then( ({ ids, index, modifierIndex, increase = false, quantity = 1, proceed, item }) => {
                // modifier popup
                if( $( '.modifier-popup' ).length > 0 ) {
                    return false;
                }
                $scope.Modifier_group_data='';
                $scope.Modifier_group_data = item.Modifier_group_data;
                NexoAPI.Bootbox().confirm({
                    message     :    '<modifiers ' 
                    + 'id="' + 'modifiers-' + modifierIndex
                    + '" increase="' + ( increase ) 
                    + '" qte="' + quantity 
                    + '" ' + ( index != null ? 'index="'+ index + '"' : '' ) 
                    + ' barcode="' + item.CODEBAR 
                    + '" item="' + ( ids ) 
                    + '" item_data="' + ( item ) 
                    + '" modifier-index="' + modifierIndex
                    + '" modifier-groups-length="' + ids.length + '"></modifiers>',
                    title       :   '<?php echo _s( 'Please select a modifier', 'gastro' );?>',
                    buttons: {
                        confirm: {
                            label: '<?php echo _s( 'Add modifiers', 'gastro' );?>',
                            className: 'btn-success btn-lg'
                        },
                        cancel: {
                            label: '<?php echo _s( 'Cancel', 'gastro' );?>',
                            className: 'btn-default btn-lg'
                        }
                    },
                    callback: function (result) {
                        if( result ) {
                               //$scope.runModifierPromise({ ids, index, modifierIndex : modifierIndex+0 , increase, quantity, proceed, item });
                            //}
                        } else {
                            $rootScope.$emit( 'close.modifierBox', { ids, index, modifierIndex : modifierIndex+0,increase, quantity, proceed, item })
                        }
                    },
                    animate: false,
                    className   :   'modifier-popup'
                });
                 $('body').addClass('test');
                 $('body').css('padding-right','0');
                $( '#modifiers-' + modifierIndex ).replaceWith( $compile( $( '#modifiers-' + modifierIndex )[0].outerHTML )( $scope ) );
                $( '#modifiers-' + modifierIndex ).closest( '.bootbox' ).addClass( 'modifiers-box' );
                $( '.modal-body' ).css({
                    'overflow-y'    :   'scroll',
                    'height'        :   window.innerHeight - 200,
                });
                $( '#modifiers-' + modifierIndex ).closest( '.modal-dialog' ).addClass( 'animated fadeIn' );
                $( '.bootbox-close-button.close' ).remove(); 
                $('body').removeClass('modal-open');
                $('body').css('padding','none');
                $('.modal-backdrop').remove();
            });
        }

        NexoAPI.events.addFilter( 'override_add_item', ({ item, proceed, quantity, increase = true, index = null }) => {
            
            
            if( item.REF_MODIFIERS_GROUP != '' && item.REF_MODIFIERS_GROUP != '0' && index == null ) { // && ( ( $scope.isCombo && $scope.comboActive ) || ! $scope.isCombo )
                $scope.currentItem      =   item;
                let ids                 =   item.REF_MODIFIERS_GROUP;
                
                var group_id_new = ids.split(',');
                 
                ids = group_id_new.join('&');
                
                //index = 1;
                let modifierIndex       =   0;
                //ids                     =   ids.filter( i => i.length !== 0 ); // this filter empty ids
                $scope.runModifierPromise({ ids, index, modifierIndex, increase, quantity, proceed, item });  
                return { item, proceed : true };
            } //  if _item support modfiiers

            return { item, proceed, increase, index };
        }, 99 );

        NexoAPI.events.addFilter( 'nexo_pos_barcode_attribute', ( barcode, { item, parent, index } ) => {
            return barcode;
        });

        NexoAPI.events.addAction( 'reset_cart', function(){
            $scope.modifiers    =   [];
        });

        NexoAPI.events.addAction( 'pos_load_order', function( data ){
            $scope.selectedTable    =   data.order[0].USED_TABLE; 
            $scope.selectedArea     =   data.order[0].USED_AREA;

            if( data.order[0].RESTAURANT_ORDER_TYPE == 'dinein' ) {
                let order_type      =   data.order[0].RESTAURANT_ORDER_TYPE;
                _.each( $scope.types, function( type ) {
                    if( _.indexOf( $scope.realOrderType, type.namespace ) != -1 ) {
                        if( order_type != null ) {
                            if( type.namespace == order_type ) {
                                selected    =   true;
                                $scope.selectedOrderType    =   type;
                            } 
                        } else {
                            if( type.active ) {
                                selected    =   true;
                                $scope.selectedOrderType    =   type;
                            }
                        }
                    // for any custom action that we add on the order type array
                    } else {
                        if( type.active ) {
                            if( type.namespace == 'return' ) {
                                document.location   =   '<?php echo dashboard_url([ 'orders' ]);?>';
                            } else if( type.namespace == 'readyorders' ) {
                                bootbox.hideAll();
                                $rootScope.$broadcast( 'open-ready-orders' );
                            } else if( type.namespace == 'pendingorders' ) {
                                $rootScope.$broadcast( 'open-history-box' );
                            } else if( type.namespace == 'waiter' ) {
                                $rootScope.$broadcast( 'open-waiter-screen' );
                            }
                            type.active     =   false;
                        }
                    }
                });
            } else {
                $scope.switchOrderType( data.order[0].RESTAURANT_ORDER_TYPE );
            }
        })
        /** 
         * Reset SelectedOrderType
        **/

        NexoAPI.events.addAction( 'order_history_cart_busy', function(){
            $scope.selectedOrderType            =   false;
        });

        // if we're editing an order, let set the order type selected
        // if( v2Checkout.ProcessType == 'PUT' ) {
        //     var orderType   =   v2Checkout.CartType.split( '_' );
        //     $scope.switchOrderType( orderType[2] );
        // }

        /**
         * Closing Order Type Selection in specific cases
        **/

        NexoAPI.events.addAction( 'open_order_on_pos', function(){
            v2Checkout.CartItems.forEach( item => {
                item.restaurant_food_modifiers  =   JSON.parse( item.RESTAURANT_FOOD_MODIFIERS ) || [];
                item.restaurant_food_status     =   item.RESTAURANT_FOOD_STATUS;
                item.restaurant_food_note       =   item.RESTAURANT_FOOD_NOTE;
                item.restaurant_food_issue      =   item.RESTAURANT_FOOD_ISSUE;

                $scope.computeItemsModifiers( item );
            });
        });

        $scope.computeItemsModifiers            =   function( item ) {
            let total   =   0;

           // console.log(item);
            //console.log(2732);

         


            item.restaurant_food_modifiers.forEach( modifier => {

               if( parseInt( modifier.default ) == 1 ) {
                   // if ( modifier.group_update_price === '1' ) {
                      //  item.PRIX_DE_VENTE          =   parseFloat( modifier.price );  
                      //  item.PRIX_DE_VENTE_TTC      =   parseFloat( modifier.price );  
                       // item.PRIX_DE_VENTE_BRUT     =   parseFloat( modifier.price );  
                   // } else {
                       total   +=  parseFloat( modifier.price );
                    //}

               }
            });
            //19 DEC 2020 Tax with modifier in hold orders
            // item.default_item_price = parseFloat( item.PRIX );
            // //console.log(2685);
            // item.PRIX_BRUT          =  parseFloat( item.PRIX_BRUT ) + total;
            // item.PRIX_DE_VENTE          =  parseFloat( item.PRIX_BRUT ) + total;
            // item.PRIX_DE_VENTE_TTC      =  parseFloat( item.PRIX_BRUT ) + total;
            // item.PRIX_DE_VENTE_BRUT     =  parseFloat( item.PRIX_BRUT ) + total;


            var rate = parseFloat("<?php echo $taxes_details;?>");
            //console
            item.default_item_price = parseFloat( item.PRIX )+(parseFloat(item.QTE_ADDED))*(parseFloat( item.PRIX) * rate)/ 100;
            //console.log(2685);
            item.PRIX_BRUT          =  parseFloat( item.PRIX_BRUT );
            item.PRIX_DE_VENTE          =  parseFloat( item.PRIX_DE_VENTE );
            item.PRIX_DE_VENTE_TTC      =  parseFloat( item.PRIX_BRUT ) ;
            item.PRIX_DE_VENTE_BRUT     =  parseFloat( item.PRIX_DE_VENTE_BRUT )+total;


            //console.log( item.PRIX_DE_VENTE );

        }

        NexoAPI.events.addAction( 'close_paybox', function(){
            if( $scope.wasOpenFromTableHistory ) {
                $scope.wasOpenFromTableHistory = false;
            }

            // if we close the paybox, then just restore default informations
            if( v2Checkout.ProcessType == 'PUT' && $( '.table-selection-box' ).length > 0 ) {
                v2Checkout.resetCart();
                v2Checkout.ProcessURL       =   "<?php echo site_url(array( 'rest', 'nexo', 'order', User::id() ));?>?store_id=<?php echo get_store_id();?>";
                v2Checkout.ProcessType      =   'POST';
            }
        });
    }

    selectTableCTRL.$inject =   [ '$compile', '$scope', '$timeout', '$http', '$interval', '$rootScope', '$filter' ];
    const selectTableAngularController      =   tendooApp.controller( 'selectTableCTRL', selectTableCTRL );



   
  


/*ngular.element(document).ready(function() {
    angular.element(".dinein-button").trigger("openTables"); 
});
*/

 window.onload = function() {
            // alert("Window is loaded");
            // $scope().openTables();

             var elem = document.getElementById("fullscreenmodal");

            elem.onclick = function() {
                // req = elem.requestFullScreen || elem.webkitRequestFullScreen || elem.mozRequestFullScreen;
                // req.call(elem);
                var scope_fullscreen = angular.element(document.getElementById('Fullscreen')).scope();
                scope_fullscreen.openFullScreen();
                fullscreenss();
            }
             var scope = angular.element(document.getElementById('takeaway')).scope();
             scope.switchOrderType( 'takeaway' );

             <?php if( store_option( 'ready_orders' ) == 'yes' ): ?>
                
                var scope_ready = angular.element(document.getElementById('readyorders')).scope();
                scope_ready.fetchOrders();
              
              <?php endif; ?>
              
             
              <?php if( store_option( 'bill_orders' ) == 'yes' ): ?>
                
                    var scope_ready = angular.element(document.getElementById('billorders')).scope();
                    scope_ready.fetchOrders();
              
              <?php endif; ?>

             // var scope_fullscreen = angular.element(document.getElementById('Fullscreen')).scope();
             // scope_ready.openFullScreen();
          

            
            
        //  
        
            // var elem = document.documentElement; 
             //if (elem.requestFullscreen) { 
              //  elem.requestFullscreen() 
            //}
           // console.log(scope);  
            /*  $scope.openTables           =   function(){

                alert("Window is loaded");
                    $scope.switchOrderType( 'dinein' );


                     $scope.hideme = true;

                  //  ['ng-controller="payBox"'].hide();

                }*/
             //$scope.openTableSelection();
          }



  
       //        });
  


</script>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width: 44%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Modal</h4>
      </div>
      <div class="modal-body">
        <!-- Modal content -->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" style="padding: 17px 35px;    font-size: 19px;">Close</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="fullscreenmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width: 100%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel"></h4>
      </div>
      <div class="modal-body">
        <!-- Modal content -->
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" style="padding: 17px 35px;    font-size: 19px;">Close</button>
      </div> -->
    </div>
  </div>
</div>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.1/angular.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/angular-filter/0.5.4/angular-filter.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.1/angular-sanitize.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/2.5.0/ui-bootstrap-tpls.js'></script> -->
<style type="text/css">
    .childs{
        display: none;s
    }
    .table-animation {
        padding:5px 0;
    }
    .table-animation:hover {
        background: #FFF;
        box-shadow: inset 5px 5px 100px #EEE;
        cursor: pointer;
    }
    .table-selected {
        background:#00a65a;
        color:#FFF;
    }
    .table-selected, .table-selected:hover {
        box-shadow: -5px 5px 4px 1px #bfbfbf;
    }
    .table-out-of-use:hover, .table-out-of-use {
        box-shadow: inset 5px 5px 100px #f7bdbd;
    }
    .table-in-use:hover, .table-in-use {
        box-shadow: inset 5px 5px 100px #c1f7bd;
    }
    .table-reserved:hover, .table-reserved {
        box-shadow: inset 5px 5px 100px #ffef9f;
    }
    .timer {
        border-radius: 5px;
        background: #666;
        padding: 5px;
        color : #FFF;
        display: inline-block;
    }
    .products-list {
        list-style: none;
        margin: 0;
        padding: 0;
        padding-right: 15px;
        border-right: 1px #f4f4f4 solid;
        border-top: 1px #F4F4F4 solid;
    }
    .modifier-class {
        border: solid 1px #e8e8e8;
        padding: 2px 5px;
        display: block;
        border-radius: 20px;
        line-height: 16px;
    }
    .btn-no-border {
        border: solid 1px #367fa9 !important;
    }
</style>
<style type="text/css" media="screen">
.navbar li, #navbar2 li {
  display: inline;
  list-style-type: none;
  padding-right: 20px;

}
.TLINK{
    display: none;
}
.pointer{
    cursor: pointer;
}
.modinavstart{
    margin-left: -42px;
    height:unset !important
    text-align: center;
}
div#cart-table-body tr {
    font-size: medium;
}
.bootbox.modal.modifier-popup.in.modifiers-box {
    z-index: -100;
    opacity: 0;
    animation-name: none;

}
.bootstrap-select.btn-group .dropdown-menu {
    min-width: 100%;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
    top: 60px;
}
.modifi{
    padding: 2% 0;
}
.modinavstart{
    height: unset !important;
    display: table;
}
.modifiers-item-active {
    box-shadow: inset 0px 0px 60px 0px #c1d3fd;
}.
.modifiers-item-unactive{
     box-shadow:unset;

}
</style>


<script type="text/javascript">




  function toggle(id, id2) {
   // alert(id);
    var n = document.getElementById(id);
    if (n.style.display != 'none') {
        n.style.display = 'none';
        document.getElementById(id2).setAttribute('aria-expanded', 'false');
    }else{
        n.style.display = '';
        document.getElementById(id2).setAttribute('aria-expanded', 'true');
    }
  }

  $(document).on("click",".modifi",function() {

            itemindex = $(this).attr("attr-index");

            changemodi = $(this).attr("attr-modifierval");


            if(v2Checkout.CartItems[itemindex].restaurant_food_modifiers[changemodi].default=="0"){
                v2Checkout.CartItems[itemindex].restaurant_food_modifiers[changemodi].default = "1";
                $('.modify-index-'+itemindex+'-key-'+changemodi).removeClass('modifiers-item-unactive');
                $('.modify-index-'+itemindex+'-key-'+changemodi).addClass('modifiers-item-active');

            } else{
                v2Checkout.CartItems[itemindex].restaurant_food_modifiers[changemodi].default = "0";
                $('.modify-index-'+itemindex+'-key-'+changemodi).removeClass('modifiers-item-active');
                $('.modify-index-'+itemindex+'-key-'+changemodi).addClass('modifiers-item-unactive');


                
            }


            var modifiervalue=0;
            var unselectmodifiervalue=0;
            _.each( v2Checkout.CartItems[itemindex].restaurant_food_modifiers, function( datavalue, datakey ) {
               
              // var modifiervalue; 
                //console.log(datavalue.default);
              //  console.log(datakey);
              
                if(datavalue.default==1){
                    // $scope.addition = parseInt(num1) + parseInt(num2);
                  modifiervalue+=parseFloat(datavalue.price);

                } else {
                    unselectmodifiervalue+=parseFloat(datavalue.price);
                }
            });
             //    console.log(modifiervalue);
             // console.log(unselectmodifiervalue);
            //v2Checkout.CartItems[itemindex].modifiersPrice = modifiervalue;

            var totalamout_per_item = parseFloat(v2Checkout.CartItems[itemindex].default_item_price)+parseFloat(modifiervalue);

             if(v2Checkout.CartItems[itemindex].PROMO_ENABLED){
                // var totalamout_per_item = parseFloat(v2Checkout.CartItems[itemindex].default_item_price)+parseFloat(modifiervalue);

                 v2Checkout.CartItems[itemindex].PRIX_PROMOTIONEL = parseFloat(totalamout_per_item);

             } else {

                 v2Checkout.CartItems[itemindex].PRIX_DE_VENTE_TTC = parseFloat(totalamout_per_item);


                 	//19 Dec 20 changes for tax with modifier //PRIX_DE_VENTE_BRUT --- Modifier amount with default item price 
                 
                 v2Checkout.CartItems[itemindex].PRIX_DE_VENTE_BRUT = parseFloat(v2Checkout.CartItems[itemindex].PRIX_DE_VENTE)+parseFloat(modifiervalue);

             }


           // v2Checkout.CartItems[itemindex].PRIX_DE_VENTE_TTC = parseFloat(totalamout_per_item); 
           // v2Checkout.CartItems[itemindex].PRIX_DE_VENTE_TTC=v2Checkout.CartItems[itemindex].PRIX_DE_VENTE_TTC+(v2Checkout.CartItems[itemindex].modifiersPrice-unselectmodifiervalue);
           
           // console.log(modifiervalue);

         v2Checkout.refreshCartValues();

        v2Checkout.buildCartItemTable();


            //$("#cart-details-wrapper").load(location.href + " #cart-details-wrapper");


            //$("#cart-table-body").load(location.href + " #cart-table-body"); 
           // $("#cart-details-wrapper").load(location.href+" #cart-details-wrapper>*","");
            //console.log(unselectmodifiervalue);

          // console.log(v2Checkout.CartItems[itemindex]);
            
    });

$('#fullscreenmodal').modal('show');

 $(document).on("click","#btnmodifier",function() {

    console.log('dinein');

    var scope = angular.element(document.getElementById('dinein')).scope();

    scope.get_modifiers1($(this).data("id"),$(this).data("value"));

    $('#myModal').modal('show');
   // alert('hu');
 });

 function fullscreenss()
{
    if((window.fullScreen) ||
       (window.innerWidth == screen.width && window.innerHeight == screen.height)) {
       /* alert("asd");*/
    } else {
        $("#fullscreenmodal").modal('hide');
    }
}


</script>



<style>
    #fullscreenmodal .modal-dialogue {
    width:100%;
}
#fullscreenmodal .modal-content {
    height:729px;
    opacity:0.5;
}
@media (min-width:700px)
{
    #fullscreenmodal .modal-content {
        height:1002px;
    }
}
@media (min-width:400px)
{
    #fullscreenmodal .modal-content {
        height:710px;
    }
}
</style>

