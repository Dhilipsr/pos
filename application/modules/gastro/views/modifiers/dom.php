<div class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
  <div class="col-lg-3 col-md-3 col-xs-4 modifiers-item" ng-click="select( modifier )" ng-class="{ 'active' : modifier.default == 1 }"
      ng-repeat="modifier in modifiers" style=";padding:3px; border-right: solid 1px #DEDEDE;border-bottom: solid 1px #DEDEDE;">
     <div>

      
      <!--  <img data-original="http://localhost/New_Office/restaurant_pos_webapp/public/upload/items-images/d5828-main-qimg-2d84d946a4be9c31151f5d24d246c0bb.gif" style="display: block; width: 100%; min-height: 153px; max-height: 141px;" class="img-responsive img-rounded lazy" src="{{ modifier.image == '' ? 
       '<?php echo module_url( 'nexo' ) . '/images/default.png';?>' : 
       '<?php echo get_store_upload_url() . 'items-images/';?>' + modifier.image 
      }}"> -->
       <div class="caption text-center" style="padding: 2px;overflow: hidden;position: absolute;bottom: 15px;z-index: 99999;width: 95%;background: #ffffffc9;">
         <strong class="item-grid-title">{{ modifier.name }}</strong>
        <br>

     <span class="align-center">{{ modifier.price | moneyFormat }}</span>
    </div>

   
      </div>
  </div>
  <div class="col-md-12" ng-show="modifiers.length == 0">
   <?php echo __( 'Loading Modifiers...', 'gastro' );?>
  </div>
 </div>
</div>
<style type="text/css">
  [data-bb-handler~=cancel] {
    margin-right: 18px;
    width: 12%;
    font-size: 80%;
    }
</style>

