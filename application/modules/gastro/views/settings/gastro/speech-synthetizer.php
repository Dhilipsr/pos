<?php
$this->Gui->add_item( array(
    'type'          =>    'dom',
    'content'       =>    '<h3>' . __( 'Speech Synthesizer', 'gastro' ) . '</h3>'
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'enable_kitchen_synthesizer',
    'options'     =>  [
        ''      =>  __( 'Choose a value', 'gastro' ),
        'yes'   =>  __( 'yes', 'gastro' ),
        'no'    =>  __( 'No', 'gastro' )
    ],    
    'label' =>   __( 'Enable Kitchen Synthesizer', 'gastro' ),
    'description' =>   __( 'The kitchen view will receive a vocal notice when an order is placed. <a href="https://developer.mozilla.org/fr/docs/Web/API/Window/speechSynthesis#Browser_compatibility">Your browser need to be compatible with</a>.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type'          =>    'dom',
    'content'       =>    $this->load->module_view( 'gastro', 'synthesis.settings-wrapper', null, true )
), 'gastro-settings', 1 );
