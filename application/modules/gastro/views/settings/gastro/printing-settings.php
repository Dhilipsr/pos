<?php
global $Options;
$this->load->module_model( 'gastro', 'Nexo_Restaurant_Kitchens', 'kitchens_model' );
$kitchens           =   get_instance()->kitchens_model->get();
foreach( $kitchens as $kitchen ) {
    $kitchens_options[ $kitchen[ 'ID' ] ]   =   $kitchen[ 'NAME' ];
}
$this->Gui->add_item( array(
    'type'          =>    'dom',
    'content'       =>    '<h3>' . __( 'Printing Settings', 'gastro' ) . '</h3>'
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'gastro_print_gateway',
    'options'     =>  [
        'gcp'   =>  __( 'Google Cloud Print', 'gastro' ),
        'nps'   =>  __( 'A2000 Print Server', 'gastro' ),
    ],    
    'label' =>   __( 'Prints Gateway', 'gastro' ),
    'description' =>   __( 'Select which gateway will be used to print order to the kitchen. Default : Google Cloud Print. Don\'t forget to save the settings.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'printing_option',
    'options'     =>  [
        0           =>  __( 'Please select an option', 'gastro' ),
        'kitchen_printers'      =>  __( 'Kitchen Printers', 'gastro' ),
        'single_printer'        =>  __( 'Single Printer', 'gastro' )
    ],    
    'label' =>   __( 'Print Option (Default: Single Printer)', 'gastro' ),
    'description' =>   __( 'You can choose whether you would like to use the printers assigned to each kitchen or you can use a single printer for all placed orders. Settings need to be saved if you want it to apply to the system.', 'gastro' )
), 'gastro-settings', 1 );

/**
 * When Google Cloud Print is enabled
 */

if ( store_option( 'gastro_print_gateway', 'gcp' ) == 'gcp' ) {

    $this->Gui->add_item( array(
        'type'              =>    'text',
        'name'              =>	store_prefix() . 'restaurant_envato_licence',
        'label'             =>   __( 'Envato Licence', 'gastro' ),
        'description'       =>   __( 'Enter your envato licence here for NexoPOS Restaurant Extension. If that field is not set, the cloud print may not work.', 'gastro' ),
        'placeholder'       =>   __( 'Envato Licence', 'gastro' )
    ), 'gastro-settings', 1 );
    
    $this->Gui->add_item( array(
        'type'          =>  'text',
        'name'          =>	store_prefix() . 'printer_gpc_proxy',
        'label'         =>  __( 'Printer Proxy', 'gastro' ),
        'description'   =>  __( 'Learn how to get the printer proxy <a href="https://nexopos.com/how-to-get-the-printer-proxy">here</a>. If that field is not set, the cloud print may not work.', 'gastro' ),
        'placeholder'   =>  __( 'Printer Proxy', 'gastro' )
    ), 'gastro-settings', 1 );

    // Add Print List
    if( ! empty( @$Options[ store_prefix() . 'nexopos_app_code' ] ) && ! empty( $Options[ store_prefix() . 'printer_gpc_proxy' ] ) ) {
        
        $curl_raw           =   $this->curl->get( module_config( 'nexo', 'nexo.store_url' ) . '/api/gcp/printers?app_code=' . $Options[ store_prefix() . 'nexopos_app_code' ] );
        $printers           =   json_decode( $curl_raw, true );
        $printers_options   =   [];
        
        // turn Raw to options
        foreach( ( array ) $printers[ 'printers' ] as $printer ) {
            $printers_options[ $printer[ 'id' ] ]   =   $printer[ 'displayName' ];
        }

        if( @$printers[ 'success' ] == true ) {

            $this->Gui->add_item( array(
                'type'          =>  'select',
                'name'          =>	store_prefix() . 'printer_takeway',
                'label'         =>  __( 'Default Printer', 'gastro' ),
                'description'   =>  __( 'Select a printer used by default for all places where a printer can\'t be selected.', 'gastro' ),
                'options'       =>  $printers_options
            ), 'gastro-settings', 1 );
            
            if ( store_option( 'printing_option', 'single_printer' ) == 'kitchen_printers' ) {
                $this->Gui->add_item( array(
                    'type'          =>    'dom',
                    'content'       =>    '<h3>' . __( 'Printers for kitchens', 'gastro' ) . '</h3>'
                ), 'gastro-settings', 1 );

                if( count( $kitchens ) == 0 ) {
                    $this->Gui->add_item( array(
                        'type'          =>    'dom',
                        'content'       =>    tendoo_info( __( 'You don\'t have a kitchen to setup the printer', 'gastro' )  )
                    ), 'gastro-settings', 1 );
                } else {
                    foreach( $kitchens as $kitchen ) {
                        $this->Gui->add_item( array(
                            'type'          =>  'select',
                            'name'          =>	store_prefix() . 'printer_kitchen_' . $kitchen[ 'ID' ],
                            'label'         =>  sprintf( __( 'Kitchen : %s', 'gastro' ), $kitchen[ 'NAME' ] ),
                            'description'   =>  sprintf( __( 'Select a printer to a specific kitchen : %s', 'gastro' ), $kitchen[ 'NAME' ] ),
                            'options'       =>  $printers_options
                        ), 'gastro-settings', 1 );
                    }
                }
    
            }

            if( store_option( 'gastr_show_booked_at_kitchen', 'no' ) === 'yes' ) {
                $this->Gui->add_item( array(
                    'type'          =>  'select',
                    'name'          =>	store_prefix() . 'printer_nexostore',
                    'label'         =>  __( 'Online Order Printer', 'gastro' ),
                    'description'   =>  __( 'Select a printer for a online order.', 'gastro' ),
                    'options'       =>  $printers_options
                ), 'gastro-settings', 1 );
            }

        } else {
            $this->Gui->add_item( array(
                'type'          =>    'dom',
                'content'       =>    tendoo_info( __( 'Unable to retreive printers from your Google Account.', 'gastro' )  )
            ), 'gastro-settings', 1 );
        }
    }

    if( empty( @$Options[ store_prefix() . 'nexopos_app_code' ] ) ) {
        $this->Gui->add_item( array(
            'type'          =>    'dom',
            'content'       =>    $this->load->module_view( 'gastro', 'login-btn', null, true )
        ), 'gastro-settings', 1 );
    }

    if( ! empty( @$Options[ store_prefix() . 'nexopos_app_code' ] ) ) {
        $this->Gui->add_item( array(
            'type'          =>    'dom',
            'content'       =>    $this->load->module_view( 'gastro', 'revoke-btn', null, true )
        ), 'gastro-settings', 1 );
    }
} else if ( store_option( 'gastro_print_gateway', 'gcp' ) == 'nps' ) {
    /**
     * When Nexo Print Server is enabled
     */
    $this->Gui->add_item( array(
        'type'          =>    'dom',
        'content'       =>    '<h3>' . __( 'Printers for kitchens', 'gastro' ) . '</h3>' .
        '<p>' . sprintf( __( 'Make sure the A2000 F&B POS Print Server is correctly configured on the <a href="%s">checkout settings</a>.' , 'gastro' ), dashboard_url([ 'settings', 'checkout' ]) ) . '</p>'
    ), 'gastro-settings', 1 );

    $this->Gui->add_item( array(
        'type'          =>  'select',
        'name'          =>	store_prefix() . 'printer_takeway',
        'label'         =>  __( 'Default Printer', 'gastro' ),
        'description'   =>  __( 'Select a printer used by default for all places where a printer can\'t be selected.', 'gastro' ),
        'options'       =>  [
            ''          =>  __( 'Choose a printer', 'gastro' )
        ]
    ), 'gastro-settings', 1 );

    $this->Gui->add_item( array(
        'type'          =>    'dom',
        'content'       =>    $this->load->module_view( 'gastro', 'settings.print-script', [
            'option'    =>    'printer_takeway' // no need to add store_prefix() since we're already calling the store_option function
        ], true )
    ), 'gastro-settings', 1 );

    if ( store_option( 'printing_option', 'single_printer' ) == 'kitchen_printers' ) {
        if( count( $kitchens ) == 0 ) {
            $this->Gui->add_item( array(
                'type'          =>    'dom',
                'content'       =>    tendoo_info( __( 'You don\'t have a kitchen to setup the printer', 'gastro' )  )
            ), 'gastro-settings', 1 );
        } else {
            foreach( $kitchens as $kitchen ) {
                $this->Gui->add_item( array(
                    'type'          =>  'select',
                    'name'          =>	store_prefix() . 'printer_kitchen_' . $kitchen[ 'ID' ],
                    'label'         =>  sprintf( __( 'Kitchen : %s', 'gastro' ), $kitchen[ 'NAME' ] ),
                    'description'   =>  sprintf( __( 'Select a printer to a specific kitchen : %s', 'gastro' ), $kitchen[ 'NAME' ] ),
                    'options'       =>  [
                        ''          =>  __( 'Choose a printer', 'gastro' )
                    ]
                ), 'gastro-settings', 1 );

                $this->Gui->add_item( array(
                    'type'          =>    'dom',
                    'content'       =>    $this->load->module_view( 'gastro', 'settings.print-script', [
                        'option'    =>    'printer_kitchen_' . $kitchen[ 'ID' ] // // no need to add store_prefix() since we're already calling the store_option function
                    ], true )
                ), 'gastro-settings', 1 );
            }
        }

    }
}