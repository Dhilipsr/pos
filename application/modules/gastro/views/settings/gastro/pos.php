<?php
$this->Gui->add_item( array(
    'type'          =>    'dom',
    'content'       =>    '<h3>' . __( 'POS Settings', 'gastro' ) . '</h3>' .
    '<p>' . __( 'Group all features that is used on the point of sale of NexoPOS', 'gastro' ) . '</p>'
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type'          =>    'dom',
    'content'       =>    '<h4>' . __( 'Ready Orders', 'gastro' ) . '</h4>'
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'gastro_display_only_unpaid_ready_orders',
    'options'     =>  [
        ''      =>  __( 'Choose a value', 'gastro' ),
        'yes'   =>  __( 'yes', 'gastro' ),
        'no'    =>  __( 'No', 'gastro' )
    ],    
    'label' =>   __( 'Show Only Unpaid Orders', 'gastro' ),
    'description' =>   __( 'Display only unpaid orders on the ready order popup on the POS.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'gastro_disable_orders_fetch',
    'options'     =>  [
        ''      =>  __( 'Choose a value', 'gastro' ),
        'yes'   =>  __( 'yes', 'gastro' ),
        'no'    =>  __( 'No', 'gastro' )
    ],    
    'label' =>   __( 'Disable Orders Fetch', 'gastro' ),
    'description' =>   __( 'You\'ll no longer be notified automatically from the POS when a new order is ready. However, this should reduce significaly the server load.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'gastro_freed_tables',
    'options'     =>  [
        ''      =>  __( 'Choose a value', 'gastro' ),
        'yes'   =>  __( 'yes', 'gastro' ),
        'no'    =>  __( 'No', 'gastro' )
    ],    
    'label' =>   __( 'Freed Table', 'gastro' ),
    'description' =>   __( 'This will define whether a table should be set available once the order over has been paid.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item([
    'type'      =>  'select',
    'name'      =>  store_prefix() . 'gastro_order_refresh_interval',
    'label'     =>  __( 'Refresh Order Interval', 'gastro' ),
    'description'   =>  __( 'This option let you define how often Gastro should retreive ready orders', 'gastro' ),
    'options'   =>  [
        '5000'     =>     __( '5 seconds', 'gastro' ),
        '10000'    =>  __( '10 seconds', 'gastro' ),
        '15000'    =>  __( '15 seconds', 'gastro' ),
        '20000'    =>  __( '20 seconds', 'gastro' ),
        '25000'    =>  __( '25 seconds', 'gastro' ),
        '30000'    =>  __( '30 seconds', 'gastro' ),
    ]
], 'gastro-settings', 1 );
