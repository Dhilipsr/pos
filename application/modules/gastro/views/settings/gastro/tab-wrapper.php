<?php
$this->Gui->add_item([
    'type'  =>  'dom',
    'content'   =>  $this->load->module_view( 'gastro', 'settings.gastro.tabs-header', compact( 'tabs', 'activeTab' ), true )
], 'gastro-settings', 1);
$filePath   =   dirname( __FILE__ ) . DIRECTORY_SEPARATOR . $activeTab . '.php';
if ( is_file( $filePath ) ) {
    include_once( $filePath );
} else {
    echo '<br>';
    echo tendoo_error( sprintf( __( 'Unable to find the output file" %s"', 'gastro' ), $filePath ) );
}
?>