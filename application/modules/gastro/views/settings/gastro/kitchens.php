<?php
$this->load->module_model( 'gastro', 'Nexo_Restaurant_Kitchens', 'kitchens_model' );
$kitchens           =   get_instance()->kitchens_model->get();

$kitchens_options   =   ['0'=>'None'];
foreach( $kitchens as $kitchen ) {
    $kitchens_options[ $kitchen[ 'ID' ] ]   =   $kitchen[ 'NAME' ];
}

$this->Gui->add_item( array(
    'type'          =>    'dom',
    'content'       =>    '<h3>' . __( 'Kitchen Settings', 'gastro' ) . '</h3>'
), 'gastro-settings', 1 );

$this->Gui->add_item(array(
    'type'        =>    'text',
    'label'        =>    __('Kitchen Refresh Interval', 'gastro'),
    'name'        =>    store_prefix() . 'refreshing_seconds',
    'placeholder'    =>    __( 'Set refresh in seconds', 'gastro' ),
    'description'   =>  __( 'After how many time (seconds) the order should be refreshed on the kitchen.')
), 'gastro-settings', 1 );

$this->Gui->add_item(array(
    'type'              =>    'select',
    'options'           =>  [
        'all'           =>  __( 'Show All', 'gastro' ),
        'paid'          =>  __( 'Only Paid', 'gastro' ),
        'from_unpaid'   =>  __( 'Unpaid & Paid', 'gastro' ),
    ],
    'label'             =>    __('Show Order By Status', 'gastro'),
    'name'              =>    store_prefix() . 'order_status_shown',
    'placeholder'       =>    __( 'By default : All', 'gastro' ),
    'description'       =>    __( 'The order with the following status will be shown at the kitchen.')
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'takeaway_kitchen',
    'options'     =>  $kitchens_options,    
    'label' =>   __( 'Take Away Kitchen', 'gastro' ),
    'description' =>   __( 'All take away order will be send to that kitchen.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>   store_prefix() . 'delivery_kitchen',
    'options'     =>  $kitchens_options,    
    'label' =>   __( 'Delivery Kitchen', 'gastro' ),
    'description' =>   __( 'All Delivery order will be send to that kitchen.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'gastro_kitchen_sort',
    'options'     =>  [
        'new_to_old'    =>  __( 'New to Old orders', 'gastro' ),
        'old_to_new'    =>  __( 'Old to New orders', 'gastro' ),
    ],    
    'label' =>   __( 'Sort Orders', 'gastro' ),
    'description' =>   __( 'Changes the way orders appear at the kitchen.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'kitchen_card_title',
    'options'       =>  [
        'h1'        =>  __( 'HeadLine 1', 'gastro' ),
        'h2'        =>  __( 'HeadLine 2', 'gastro' ),
        'h3'        =>  __( 'HeadLine 3', 'gastro' ),
        'h4'        =>  __( 'HeadLine 4', 'gastro' ),
        'strong'    =>  __( 'Bold', 'gastro' ),
    ],    
    'label' =>   __( 'Kitchen Card Title', 'gastro' ),
    'description' =>   __( 'Where "Headline 1" is the biggest title and "Bold" not that bigger.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item( array(
    'type' =>    'select',
    'name' =>	store_prefix() . 'kitchen_item_font',
    'options'       =>  [
        '11'    =>  __( '11 px', 'gastro' ),
        '12'    =>  __( '12 px', 'gastro' ),
        '13'    =>  __( '13 px', 'gastro' ),
        '14'    =>  __( '14 px', 'gastro' ),
        '15'    =>  __( '15 px', 'gastro' ),
        '16'    =>  __( '16 px', 'gastro' ),
        '17'    =>  __( '17 px', 'gastro' ),
        '18'    =>  __( '18 px', 'gastro' ),
        '19'    =>  __( '19 px', 'gastro' ),
        '20'    =>  __( '20 px', 'gastro' ),
    ],    
    'label' =>   __( 'Kitchen Item Font Size', 'gastro' ),
    'description' =>   __( 'Select the font to use for the kitchen item list.', 'gastro' )
), 'gastro-settings', 1 );