<?php
/**
 * Timing Settings For Gastro
 */
$this->Gui->add_item( array(
    'type'          =>    'dom',
    'content'       =>    '<h4>' . __( 'Kitchen Order Alert Pattern', 'gastro' ) . '</h4>' .
    '<p>' . __( 'All alert pattern must be correctly filled, otherwise it will be disabled. An order cannot be considered as Too Late before an order is considered as "Fresh". The minutes set must follow this rule : Fresh Order < Late Order < Too Later Order. All placeholder value will be used by default.', 'gastro' ) . '</p>'
), 'gastro-settings', 1 );

$this->Gui->add_item(array(
    'type'        =>    'text',
    'label'        =>    __('Fresh Order (minutes)', 'gastro'),
    'name'        =>    store_prefix() . 'fresh_order_min',
    'placeholder'    =>    __( 'For example : 10', 'gastro' ),
    'description'    =>    __( 'an order is considered as fresh when it has been published during a specific amount of minutes.', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item(array(
    'type'        =>    'text',
    'label'        =>    __('Late Order (minutes)', 'gastro'),
    'name'        =>    store_prefix() . 'late_order_min',
    'placeholder'    =>    __( 'For example : 20', 'gastro' ),
    'description'    =>    __( 'An order is considered as long when it has been published after a specific amount of minutes', 'gastro' )
), 'gastro-settings', 1 );

$this->Gui->add_item(array(
    'type'        =>    'text',
    'label'        =>    __('Too Late Order (minutes)', 'gastro'),
    'name'        =>    store_prefix() . 'too_late_order_min',
    'placeholder'    =>    __( 'For example : 30', 'gastro' ),
    'description'    =>    __( 'An order is considered as too late when it has been published after a specific amount of minutes.', 'gastro' )
), 'gastro-settings', 1 );

$color_options          =   [
    'box-default'    =>  __( 'Green', 'gastro' ),
    'bg-info box-info'       =>  __( 'Blue', 'gastro'),
    'bg-warning box-warning'       =>  __( 'Orange', 'gastro'),
    'bg-danger box-danger'       =>  __( 'Red', 'gastro'),
    'bg-success box-success'       =>  __( 'Green', 'gastro'),
];

$this->Gui->add_item(array(
    'type'        =>    'select',
    'options'       =>  $color_options,
    'label'        =>    __('Fresh Order Theme', 'gastro'),
    'placeholder'    =>    __( 'For example : #FFF', 'gastro' ),
    'name'        =>    store_prefix() . 'fresh_order_color',
    'description'    =>    __( 'Select a theme for this alert pattern.')
), 'gastro-settings', 1 );

$this->Gui->add_item(array(
    'type'        =>    'select',
    'options'       =>  $color_options,
    'label'        =>    __('Late Order Theme', 'gastro'),
    'name'        =>    store_prefix() . 'late_order_color',
    'placeholder'    =>    __( 'For example : #F5A4A4', 'gastro' ),
    'description'    =>    __( 'Select a theme for this alert pattern.')
), 'gastro-settings', 1 );

$this->Gui->add_item(array(
    'type'        =>    'select',
    'options'       =>  $color_options,
    'label'        =>    __('Too Late Order Theme', 'gastro'),
    'name'        =>    store_prefix() . 'too_late_order_color',
    'placeholder'    =>    __( 'For example : #DD1414', 'gastro' ),
    'description'    =>    __( 'Select a theme for this alert pattern.')
), 'gastro-settings', 1 );