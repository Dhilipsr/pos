<script>
$( document ).ready(function(){
	//<?php //echo store_option( 'nexo_print_server_url' );?>/api/printers


	<?php $URL = site_url([ 'api', 'gastro', 'kitchens', 'getprinters' ]);?>
    $.ajax( 
    	{
    	url:'<?php echo $URL;?>', 
    	headers: {"X-API-KEY": "9zOW3T3ZmBCYQWVDbrVWcFa4mApMrOZGa91mzUxr"},
    	method:'GET',
        success    :   function( result ) {
        	//console.log(result);
            $( '[name="<?php echo store_prefix();?>nexo_pos_printer"]' ).html( '<option><?php echo __( 'Choisir une option', 'nexo' );?></option>' );
             result.forEach( printer => {
                let selected    =   printer.name ==  '<?php echo store_option( $option );?>' ? 'selected="selected"' : null;
                $( '[name="<?php echo store_prefix() . $option;?>"]' ).append( `<option ${selected} value="${printer.name}">${printer.name}</option>` );
            });
        }
    })

})
</script>