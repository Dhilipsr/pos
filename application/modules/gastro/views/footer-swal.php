<script>

NexoAPI.showInfo     =   function({ message, title = '<?php echo _s( 'Warning', 'gastro' );?>', timer = undefined }) {
    swal({ text : message, title, type : 'info', timer });
}

NexoAPI.showError    =  function({ message, title = '<?php echo _s( 'An error occured', 'gastro' );?>', timer = undefined }) {
    swal({ text : message, title, type : 'error', timer })
}

NexoAPI.showConfirm     =   function({ message, title = '<?php echo _s( 'Would you like to proceed ?', 'gastro' );?>' }) {
    return swal({
        title,
        type                :   'warning',
        text                :   message,
        showCancelButton    :   true,
        confirmButtonText   :   '<?php echo _s( 'Yes', 'gastro' );?>',
        cancelButtonText    :   '<?php echo _s( 'No', 'gastro' );?>',
    })
}
</script>