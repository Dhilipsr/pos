<?php 
$this->load->model( 'Nexo_Misc' );
$this->cache        =   new CI_Cache(array( 'adapter' => 'file', 'backup' => 'file', 'key_prefix'    =>    'gastro_nps_print_status_' . store_prefix() ));
$printed_items      =   ! $this->cache->get( 'nps_order_' . $order[ 'ID' ] . '_kitchen_' . @$kitchen[ 'ID' ] ) ? []   :   $this->cache->get( 'nps_order_' . $order[ 'ID' ] . '_kitchen_' . @$kitchen[ 'ID' ] );
echo '<?xml version="1.0" encoding="UTF-8"? >';?>
<document>

        <?php 

        $data = explode(' ',  store_option( 'site_name' ));


        $first_str = substr($order['CODE'],0,2);

        $second_str = substr($order['CODE'],2,2);

        $third_str = substr($order['CODE'],4,2);
        
        $four_str = substr($order['CODE'],6);

          ?>

 <?php
        if( $order ) {
            switch( $order[ 'RESTAURANT_ORDER_TYPE' ] ) {
                case 'dinein' :  $type  =   __('Dinein', 'gastro' ); break;
                case 'takeaway' :  $type  =   __('TakeAway', 'gastro' ); break;
                case 'delivery' :  $type  =   __('Delivery', 'gastro' ); break;
                default: $type  =   __( 'Unknow Order Type', 'gastro' ); break;
            }
        }
        ?>


     <!-- <h2 style="font-family:courier;font-size: 20px;">&nbsp&nbsp&nbsp&nbsp<?php echo $data[0];?></h2> -->
     <h3 style="font-family:courier;font-size: 20px;">&nbsp&nbsp<?php echo 'Reprint';?></h2>
    <h2 style="font-family:courier;font-size: 20px;">&nbsp&nbsp<?php echo $kitchen[ 'NAME' ] ?: __( 'N/A', 'gastro' );?></h2>
   
    <text-line size="1.8" style="font-family:courier;font-size:13px;"><?php echo strtoupper($order[ 'AUTHOR_NAME' ]);?> &nbsp<?php echo $type;?> </text-line><br>
  
    
    <!--  <align mode="center"> -->
        <text-line style="height: 5px;"><?php echo nexting([], '-' );?></text-line>
        <!-- <bold> -->

           <h5 style="font-family:courier; height: 10px;margin-top:3px;margin-bottom:0px;"> 
          <?php if(@$table[0]) {  ?>   <? }else { ?> &nbsp <?php } ?> <?php echo @$order['CODE'];?><?php if(@$table[0]) { ?>/<?php echo @$table[0] ? $table[0][ 'NAME' ]: __( 'N/A', 'gastro' ); ?> <?php } ?> </h5>

       <text-line><?php echo nexting( [], '-' );?></text-line>

           <!--  <text-line size="1:1"><?php echo store_option( 'site_name' );?></text-line><br>
              <text-line size="1:6"><?php echo store_option('nexo_shop_address_1').' '.store_option('nexo_shop_city');?></text-line><br>
           <?php if(store_option('nexo_shop_phone')!=''){ ?> 
                     <text-line size="1:6"><?php echo 'TEL-'.store_option('nexo_shop_phone');?></text-line><br>
           <?php } ?>
           <br> -->

        <!-- </bold> -->
   <!--  </align> -->
  
      <align mode="left"> 
    <?php $array = array('0'); ?>

        <text-line><?php // echo nexting( [], '-' );?></text-line>
        <?php 
        $r=0;
             foreach( $items as $item ):
           
            ?>

             <?php //print_r($printed_items);?>
            <?php if ( ! in_array( $item[ 'RESTAURANT_FOOD_PRINTED' ],$array )):
            
             $r++;
            ?>


                <?php //print_r($item);?>
                <?php $modifiers    =   json_decode( @$item[ 'RESTAURANT_FOOD_MODIFIERS' ], true );?>
               <h5> <text-line style="font-family:courier;margin-top:2px;"><?php echo nexting([
                    $r.') '.$item[ 'NAME' ],
                    'x' . $item[ 'QUANTITE' ]
                ]);
                ?></text-line></h5>
                <?php if ( $modifiers ):?>
                    <?php foreach( $modifiers as $modifier ):?>
                        <?php if($modifier['default']==1):?>
                    <text-line style="font-family:courier; font-size:12px;"> <?php echo $modifier[ 'group_name' ] . ' : ' . $modifier[ 'name' ];?></text-line><br>
                       <?php endif;?>
                    <?php endforeach;?>
                <?php endif;?>
                <?php if ( ! empty( @$item[ 'RESTAURANT_FOOD_NOTE' ] ) ):?>
                <text-line size="1:0"><?php echo sprintf( __( 'Note : %s', 'gastro' ), @$item[ 'RESTAURANT_FOOD_NOTE' ] );?></text-line>
                <?php endif;?>
                
                <?php $printed_items[]  =   $item[ 'ID' ];?>
            <?php endif;?>
        <?php endforeach;?>
         <text-line><?php echo nexting( [], '-' );?></text-line><br>
         <text-line size="1:6" style="font-family:courier;font-size:13px;" ><?php echo strtoupper(date("D   M    j   Y H:i",strtotime($order[ 'DATE_CREATION' ]))); ?></text-line><br>
        <?php
        $this->cache->save( 'nps_order_' . $order[ 'ID' ] . '_kitchen_' . @$kitchen[ 'ID' ], $printed_items, module_config( 'gastro', 'gastro.gastro_printed_status_timeout' ) );
        ?>
   </align>
    <line-feed></line-feed>
   <!--  <text >
        <text-line>
        <?php echo nexting([
            __( 'Table :', 'gastro' ),
            @$table[0] ? $table[0][ 'NAME' ] : __( 'N/A', 'gastro' )
        ]);
        ?></text-line><br>
        <line-feed></line-feed>
        
        <text-line>
        <?php echo nexting([
            __( 'By :', 'gastro' ),
            @$order[ 'AUTHOR_NAME' ]
        ]);
        ?></text-line><br>
        <line-feed></line-feed>

        <text-line>
        <?php echo nexting([
            __( 'Date :', 'gastro' ),
            @$order[ 'DATE_CREATION' ]
        ]);
        ?></text-line><br>
        <line-feed></line-feed>

        <text-line>
        <?php echo nexting([
            __( 'Order :', 'gastro' ),
            @$order[ 'CODE' ]
        ]);
        ?></text-line><br>
        <line-feed></line-feed>
        
        <text-line>
        <?php echo nexting([
            __( 'Kitchen :', 'gastro' ),
            @$kitchen[ 'NAME' ] ?: __( 'N/A', 'gastro' )
        ]);
        ?></text-line><br>
        <line-feed></line-feed>

        <text-line>
        <?php echo nexting([
            __( 'Customer :', 'gastro' ),
            @$customer[ 'NOM' ] ?: __( 'N/A', 'gastro' )
        ]);
        ?></text-line><br>
        <line-feed></line-feed>

        <?php
        if( $order ) {
            switch( $order[ 'RESTAURANT_ORDER_TYPE' ] ) {
                case 'dinein' :  $type  =   __( 'Dine in', 'gastro' ); break;
                case 'takeaway' :  $type  =   __( 'Take Away', 'gastro' ); break;
                case 'delivery' :  $type  =   __( 'Delivery', 'gastro' ); break;
                default: $type  =   __( 'Unknow Order Type', 'gastro' ); break;
            }
        }
        ?>
        <text-line>
        <?php echo nexting([
            __( 'Type :', 'gastro' ),
            @$type
        ]);
        ?></text-line><br>
        <line-feed></line-feed><br>

    </text> -->

    <!--  -->
<!--   <align mode="left"> 
    <?php $array = array('ready'); ?>
     <text-line>
        <?php echo 'Items';
        ?></text-line><br>
        <text-line><?php echo nexting( [], '-' );?></text-line><br>
        <?php foreach( $items as $item ):?>

             <?php //print_r($printed_items);?>
           
                <?php //print_r($item);?>
                <?php $modifiers    =   json_decode( @$item[ 'RESTAURANT_FOOD_MODIFIERS' ], true );?>
               <h2> <text-line><?php echo nexting([
                    $item[ 'NAME' ],
                    'x' . $item[ 'QUANTITE' ]
                ]);
                ?></text-line></h2><br>
                <?php if ( $modifiers ):?>
                    <?php foreach( $modifiers as $modifier ):?>
                        <?php if($modifier['default']==1):?>
                    <text-line>-> <?php echo $modifier[ 'group_name' ] . ' : ' . $modifier[ 'name' ];?></text-line><br>
                       <?php endif;?>
                    <?php endforeach;?>
                <?php endif;?>
                <?php if ( ! empty( @$item[ 'RESTAURANT_FOOD_NOTE' ] ) ):?>
                <text-line size="1:0"><?php echo sprintf( __( 'Note : %s', 'gastro' ), @$item[ 'RESTAURANT_FOOD_NOTE' ] );?></text-line><br>
                <?php endif;?>
                <text-line><?php echo nexting( [], '-' );?></text-line><br>
                <?php $printed_items[]  =   $item[ 'ID' ];?>
            
        <?php endforeach;?>
        <?php
        $this->cache->save( 'nps_order_' . $order[ 'ID' ] . '_kitchen_' . @$kitchen[ 'ID' ], $printed_items, module_config( 'gastro', 'gastro.gastro_printed_status_timeout' ) );
        ?>
   </align>
    <line-feed></line-feed><br> -->
    <paper-cut></paper-cut>
</document>