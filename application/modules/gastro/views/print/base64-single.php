<?php
$this->load->model( 'Nexo_Misc' );
$this->cache        =   new CI_Cache(array( 'adapter' => 'file', 'backup' => 'file', 'key_prefix'    =>    'gastro_nps_print_status_' . store_prefix() ));
$printed_items      =   ! $this->cache->get( 'nps_order_' . $order[ 'ID' ] . '_kitchen_' . @$kitchen[ 'ID' ] ) || @$_GET[ 'refresh' ] === 'true' ? []   :   $this->cache->get( 'nps_order_' . $order[ 'ID' ] . '_kitchen_' . @$kitchen[ 'ID' ] );
// $printed_items          =   [];
?>
<div id="gastro-base64">
    <div class="row">
        <div class="col-sm-6">
            <?php echo sprintf( __( 'Table : %s' ), @$table[0] ? $table[0][ 'NAME' ] : __( 'N/A', 'gastro' ) );?>
        </div>
        <div class="col-sm-6">
            <?php echo sprintf( __( 'Author : %s' ), @$order[ 'AUTHOR_NAME' ] );?>
        </div>
        <div class="col-sm-6">
            <?php echo sprintf( __( 'Order : %s' ), @$order[ 'CODE' ] );?>
        </div>
        <div class="col-sm-6">
            <?php echo sprintf( __( 'Date : %s' ), @$order[ 'DATE_CREATION' ] );?>
        </div>
        <div class="col-sm-6">
            <?php echo sprintf( __( 'Kitchen : %s' ), @$kitchen[ 'NAME' ] ?: __( 'N/A', 'gastro' ) );?>
        </div>
        <?php
        if( $order ) {
            switch( $order[ 'RESTAURANT_ORDER_TYPE' ] ) {
                case 'dinein' :  $type  =   __( 'Dine in', 'gastro' ); break;
                case 'takeaway' :  $type  =   __( 'Take Away', 'gastro' ); break;
                case 'delivery' :  $type  =   __( 'Delivery', 'gastro' ); break;
                default: $type  =   __( 'Unknow Order Type', 'gastro' ); break;
            }
        }
        ?>
        <div class="col-sm-6">
            <?php echo sprintf( __( 'Type : %s' ), $type );?>
        </div>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th><?php echo __( 'Name', 'gastro' );?></th>
                <th><?php echo __( 'Quantity', 'gastro' );?></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach( $items as $item ):?>
            <?php if ( ! in_array( $item[ 'ID' ], $printed_items ) || @$_GET[ 'caching' ] === 'false' ):?>
            <?php $modifiers    =   json_decode( @$item[ 'RESTAURANT_FOOD_MODIFIERS' ], true );?>
            <tr>
                <td scope="row">
                    <span>
                        <?php echo $item[ 'NAME' ];?>
                    </span>
                    <?php if ( $modifiers ):?>
                        <br>
                        <?php foreach( $modifiers as $modifier ):?>
                        <span>> <?php echo $modifier[ 'group_name' ] . ' : ' . $modifier[ 'name' ];?></span><br>
                        <?php endforeach;?>
                    <?php endif;?>
                    <?php if ( ! empty( @$item[ 'RESTAURANT_FOOD_NOTE' ] ) ):?>
                    <br>
                    <span><?php echo @$item[ 'RESTAURANT_FOOD_NOTE' ];?></span>
                    <?php endif;?>
                </td>
                <td><?php echo 'x' . $item[ 'QUANTITE' ];?></td>
            </tr>
            <?php $printed_items[]  =   $item[ 'ITEM_ID' ];?>
            <?php endif;?>
        <?php endforeach;?>
        <?php
        if ( @$_GET[ 'caching' ] != 'false' ) {
            $this->cache->save( 'nps_order_' . $order[ 'ID' ] . '_kitchen_' . @$kitchen[ 'ID' ], $printed_items, module_config( 'gastro', 'gastro.gastro_printed_status_timeout' ) );
        }
        ?>
        </tbody>
    </table>
    <style>
    #gastro-base64 * {
        font-size: 2.5vw;
    }
    </style>
</div>


<style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}
</style>