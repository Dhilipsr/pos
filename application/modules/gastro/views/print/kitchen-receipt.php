<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<h2><?php echo store_option( 'site_name' );?></h2>
<h3 class="text-center"><?php echo store_option('nexo_shop_address_1').','.store_option('nexo_shop_city').','.store_option('nexo_shop_phone').','.store_option('nexo_shop_pobox');?></h3>
<p>
<?php echo sprintf( __( '<strong>Kitchen </strong>: %s', 'gastro' ), @$kitchen[ 'NAME' ] ? @$kitchen[ 'NAME' ] : __( 'Main Kitchen', 'nexo-restauran' ) );?><br>
    <?php if( isset( $table ) ):?>
        <?php echo sprintf( __( '<strong>Table </strong>: %s', 'gastro' ), @$table[0][ 'NAME' ] ? @$table[0][ 'NAME' ] : '' );?><br>
    <?php endif;?>
    <?php
    if( $order ):
        switch( $order[ 'RESTAURANT_ORDER_TYPE' ] ) {
            case 'dinein' :  $type  =   __( 'Dine in', 'gastro' ); break;
            case 'takeaway' :  $type  =   __( 'Take Away', 'gastro' ); break;
            case 'delivery' :  $type  =   __( 'Delivery', 'gastro' ); break;
            default: $type  =   __( 'Unknow Order Type', 'gastro' ); break;
        }
        ?>
    <?php echo sprintf( __( '<strong>Order </strong>: %s', 'gastro' ), $type );?><br>
    <?php endif;?>
    <?php echo sprintf( __( '<strong>Date</strong>: %s', 'gastro' ), @$order[ 'DATE_CREATION' ] );?><br>
    <?php echo sprintf( __( '<strong>Code</strong>: %s', 'gastro' ), @$order[ 'CODE' ] );?><br>
    <?php echo sprintf( __( '<strong>Placed By</strong>: %s', 'gastro' ), @$order[ 'AUTHOR_NAME' ] );?>
    <?php echo sprintf( __( '<strong>Customer</strong>: %s', 'gastro' ), @$order[ 'AUTHOR_NAME' ] );?>
</p>

<table class="table table-hover">
    <thead>
        <tr>
            <th><?php echo __( 'Product', 'gastro' );?></th>
            <th>#</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        // is meals is enabled
        $meals      =   [];
        foreach( $items as $item ):
            if( store_option( 'disable_meal_feature' ) == 'yes' ) {
        ?>
        <tr>
            <td >
                <?php echo empty( $item[ 'DESIGN' ] ) ? $item[ 'NAME' ] : $item[ 'DESIGN' ];?><br>
                <?php 
                if( @$item[ 'FOOD_NOTE'] != null ) {
                    echo $item[ 'FOOD_NOTE' ] . '<br>';
                }
                
                if( $modifiers  =   json_decode( $item[ 'MODIFIERS' ], true ) ) {
                    foreach( $modifiers as $modifier ) {
                        if( $modifier[ 'default' ] == '1' ) {
                            ?>
                            <em> + <?php echo $modifier[ 'name' ];?></em><br>
                            <?php
                        }                                
                    }
                }

                ?>                
            </td>
            <td style="text-align: center"> <?php echo $item[ 'QTE_ADDED' ];?></td>
        </tr>
        <?php 
            } else {
                if( @$meals[ $item[ 'MEAL' ] ] == null ) {
                    $meals[ $item[ 'MEAL' ] ]       =   [];
                }

                // push
                $meals[ $item[ 'MEAL' ] ][]         =   $item;
            }
        endforeach;
        ?>
    </tbody>
</table>
<?php
if( $meals ) {
    foreach( $meals as $key => $meal ) {
        ?>
    <table class="table table-bordered table-striped" style="margin-bottom:20px">
        <thead>
            <tr>
                <td colspan="1"><?php echo sprintf( __( 'Meal: %s', 'gastro' ), $key );?></td>
            </tr>
            
        </thead>
        <?php foreach( $meal as $item ):?>
        <tr>
            <td>
                <?php echo empty( $item[ 'DESIGN' ] ) ? $item[ 'NAME' ] : $item[ 'DESIGN' ];?><br>
                <?php 
                if( @$item[ 'FOOD_NOTE'] != null ) {
                    echo $item[ 'FOOD_NOTE' ] . '<br>';
                }
                
                if( $modifiers  =   json_decode( $item[ 'MODIFIERS' ], true ) ) {
                    foreach( $modifiers as $modifier ) {
                        if( $modifier[ 'default' ] == '1' ) {
                            ?>
                            <em> + <?php echo $modifier[ 'name' ];?></em><br>
                            <?php
                        }                                
                    }
                }
                ?> 
                ( x <?php echo $item[ 'QTE_ADDED' ];?> )               
            </td>
        </tr> 
        <?php endforeach;?>
    </table> 
        <?php
    }
}
?>
<?php include_once( dirname( __FILE__ ) . '/bootstrap4.min.php' );?>
<style>
* {
    text-transform: uppercase;
}
</style>
</body>
</html>


<style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}
</style>