<?php $product[ 'RESTAURANT_FOOD_MODIFIERS' ]    =   json_decode( @$product[ 'RESTAURANT_FOOD_MODIFIERS' ], true );?>
<?php if ( ! empty( @$product[ 'RESTAURANT_FOOD_MODIFIERS' ] ) ):?>
    <?php foreach( @$product[ 'RESTAURANT_FOOD_MODIFIERS' ] as $modifier ):?>
    	<?php if($modifier['default']==1) {?>
        <text-line>
    -> <?php echo $modifier[ 'group_name' ];?> : <?php echo $modifier[ 'name' ];?> (<?php echo trim( get_instance()->Nexo_Misc->cmoney_format( $modifier[ 'price' ] ) );?>)
    </text-line><br>
<?php } ?>
    <?php endforeach;?>
<?php endif;?>