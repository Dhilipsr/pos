<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$this->Gui->col_width(1, 4);

$this->Gui->add_meta( array(
    'col_id'    =>  1,
    'namespace' =>  'gastro',
    'type'      =>  'unwrapped'
) );

$this->Gui->add_item( array(
'type'        =>    'dom',
'content'    =>    $crud_content->output
), 'gastro', 1 );

$this->Gui->output();


?>

<style type="text/css">
	#MAX_SEATS_field_box{
		display: none !important;
	}

</style>

<script type="text/javascript">
    $(function() {
        $('#field-MAX_SEATS').val("50");
    });
</script> 
