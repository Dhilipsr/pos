<?php global $Options, $current_register;?>
<script>
tendooApp.directive( 'readyOrders', function(){
	return {
		templateUrl         :    '<?php echo site_url([ 'dashboard', store_slug(), 'gastro', 'templates', 'ready_orders' ] );?>',
		restrict            :   'E'
	}
});

tendooApp.controller( 'readerOrdersCTRL', [ 
'$scope', '$timeout', '$compile', '$rootScope', '$interval', '$http', '$filter', 
function( $scope, $timeout, $compile, $rootScope, $interval, $http, $filter ){
	$scope.$watch( 'newOrders', function( previous, current ){
		if( $scope.newOrders > 0 && previous < current ){
			NexoAPI.Toast()( '<?php echo _s( 'A new order is ready', 'gastro' );?>' );
		}
	});

	// $( '.new-orders-button' ).append( '<span class="badge badge-warning">42</span>' );
	$scope.newOrders         	=    0;  

	$scope.selectedOrder 		=	null;

	$scope.printServerURL        = '<?php echo store_option( 'nexo_print_server_url' );?>';


	//alert(selectedOrder);

	
	/**
	* Open Waiter Screen
	* @return void
	**/
	
	$scope.openReadyOrders       =    function(){

		NexoAPI.Bootbox().alert({
			message 		:	'<div class="ready-orders"><ready-orders></ready-orders></div>',
			title          :	'<?php echo _s( 'Ready orders', 'gastro' );?>',
			buttons 		:	{
				ok: {
					label: '<span ><?php echo _s( 'Close', 'gastro' );?></span></span>',
					className: 'btn-default'
				},
			}, 
			callback 		:	function( action ) {
				// $rootScope.$broadcast( 'open-select-order-type' );
			},
			className 	:	'ready-orders-box col-md-8 col-lg-12'
		});
		
		$scope.windowHeight           =	window.innerHeight;
		$scope.wrapperHeight          =	$scope.windowHeight - ( ( 100 * 2 ) + 30 );


		// 

		let current_id = '<?php echo $this->events->apply_filters('group_details', '')->id;?>';

		// if(current_id!=12){
		// 	$('.selectedOrder.TYPE').hide();
		// }

		let printAttr;
		switch( '<?php echo store_option( 'nexo_print_gateway' );?>' ) {
			case 'normal_print': 
				printAttr	=	'ng-click="printReceipt()"'; 
			break;
			case 'nexo_print_server': 
				printAttr	=	'ng-click="npsPrint()"'; 
			break;
			case 'register_nps': 
				printAttr	=	'ng-click="registerPrint()"'; 
			break;
		}
		
		
		$timeout( function(){
			angular.element( '.ready-orders-box .modal-dialog' );
			angular.element( '.ready-orders-box .modal-body' ).css( 'padding-top', '0px' );
			angular.element( '.ready-orders-box .modal-body' ).css( 'padding-bottom', '0px' );
			angular.element( '.ready-orders-box .modal-body' ).css( 'padding-left', '0px' );
			angular.element( '.ready-orders-box .modal-body' ).css( 'padding-right', '0px' );
			angular.element( '.ready-orders-box .modal-body' ).css( 'height', $scope.wrapperHeight );
			angular.element( '.ready-orders-box .modal-body' ).css( 'overflow-x', 'hidden' );
		}, 150 );


		// if($scope.orders.length>0){

		// } else {

		// 	$('.paid').addClass('uncolor');
		// 	$('.unpaid').addClass('color');
		// }
		//console.log($scope.orders);
		
		// $scope.orders_unpaid_color  =   'uncolor';
  //       $scope.orders_paid_color    =   'uncolor';

		$( '.ready-orders-box' ).last().find( '.modal-body' )
		.prepend('<div ng-click="paid()" class="col-md-6 col-lg-6 ng-scope paid" style="text-align:center;border:none;padding: 20px;margin-bottom: 1%;">Paid</div><div ng-click="unpaid()" class="col-md-6 col-lg-6 ng-scope unpaid" style="text-align:center;border:none;padding: 20px;margin-bottom: 1%;">Unpaid</div>');

		$( '.ready-orders-box' ).last().find( '.modal-header' )
		.append('<div class="col-md-6 search-order-field" style="padding:15px;float:right;"><div class="input-group"><span class="input-group-addon" id="basic-addon1"><?php echo __( 'Search Order', 'gastro' );?></span><input ng-model="searchreadyOrders" type="text" class="form-control" placeholder="<?php echo __( 'Order Code', 'gastro' );?>" aria-describedby="basic-addon1" style="height: 62px;"></div></div>');
		/*	.prepend( '<button class="sampleclassed col-md-6 col-lg-6" style="text-align:center;border:none;padding: 20px;margin-bottom: 1%;background-color:#87d69b;" ng-click="paidUnpaid()">Paid</button> <button class="sampleclassed col-md-6 col-lg-6" style="text-align:center;padding: 20px;border:none;margin-bottom: 1%;background-color:#c9cdca;" ng-click="paidUnpaid()">Non Paid</button>' );*/

		if(current_id!=12){
			$( '.ready-orders-box' ).last().find( '.modal-footer' )
			.prepend( '<a ng-show="selectedOrder != null && ( selectedOrder.TYPE != \'nexo_order_comptant\')" && selectedOrder.REAL_TYPE == \'dinein\'" ng-click="runPayment()" class="btn btn-primary selectedOrder.TYPE" style="font-size: 110%;color:white;background-color:#c40606;border-color:#c40606;"><?php echo _s( 'Pay that order', 'nexo-restarant' );?></a>' );
	    }
		$( '.ready-orders-box' ).last().find( '.modal-footer' )
		.prepend( '<a ng-show="selectedOrder != null && ( selectedOrder.REAL_TYPE == \'delivery\' || selectedOrder.REAL_TYPE == \'takeaway\' || selectedOrder.REAL_TYPE == \'dinein\' )" ' + printAttr + ' class="btn btn-primary" style="font-size: 110%;color:white;background-color:#3ba728;border-color:#3ba728;"><?php echo _s( 'Print Receipt', 'nexo-restarant' );?></a>' );
		$( '[data-bb-handler="ok"]' ).attr( 'style', 'width:122px' );

			
		$( '.ready-orders-box' ).last().find( '.modal-footer' )
			.prepend( '<a ng-show="selectedOrder != null && ( selectedOrder.REAL_TYPE == \'delivery\' || selectedOrder.REAL_TYPE == \'takeaway\' || selectedOrder.REAL_TYPE == \'dinein\' )"   ng-click="npsPrint_kitchen()" class="btn btn-primary selectedOrder.TYPE" style="font-size: 110%;color:white;float:left;"><?php echo _s( 'Reprint kitchen', 'nexo-restarant' );?></a>' );
		// $( '.ready-orders-box' ).last().find( '.modal-footer' )
		// .prepend( '<a ng-show="selectedOrder.STATUS == \'ready\'" ng-click="setAsServed()" class="btn btn-primary"><?php echo _s( 'Serve', 'nexo-restarant' );?></a>' );
		
		$( '.ready-orders' ).html( 
			$compile( $( '.ready-orders').html() )( $scope ) 
		);
		$( '.unpaid-ready-orders' ).html( 
			$compile( $( '.ready-orders').html() )( $scope ) 
		);
		$( '.ready-orders-box' )
		.find( '.modal-header' )
		.html( $compile( $( '.ready-orders-box' ).find( '.modal-header' ).html() )( $scope ) );
		$( '.ready-orders-box' )
		.find( '.modal-body' )
		.html( $compile( $( '.ready-orders-box' ).find( '.modal-body' ).html() )( $scope ) );
		$( '.ready-orders-box' )
		.find( '.modal-footer' )
		.html( $compile( $( '.ready-orders-box' ).find( '.modal-footer' ).html() )( $scope ) );
		$scope.fetchOrders();
		// console.log('123');
		$('.modal-dialog').css('width','100%');
	}

	$scope.printReceipt 		=	function(){
		$( 'body' ).append( '<iframe id="receipt-wrapper" style="visibility:hidden;height:0px;width:0px;position:absolute;top:0;" src="<?php echo dashboard_url([ 'orders', 'receipt' ]);?>/' + $scope.selectedOrder.ID + '?refresh=true&autoprint=true"></iframe>' );
	}

	/**
		* Nexo Print Server
		* @return void
		*/
	$scope.npsPrint         =   function() {
		// $.ajax( '<?php echo dashboard_url([ 'local-print'  ]);?>' + '/' + $scope.selectedOrder.ID, {
		// 	success 	:	function( printResult ) {
		// 		$.ajax( '<?php echo store_option( 'nexo_print_server_url' );?>/api/print', {
		// 			type  	:	'POST',
		// 			data 	:	{
		// 				'content' 	:	printResult,
		// 				'printer'	:	'<?php echo store_option( 'nexo_pos_printer' );?>'
		// 			},
		// 			dataType 	:	'json',
		// 			success 	:	function( result ) {
		// 				console.log( result );
		// 			}
		// 		});
		// 	}
		// });

		LocalPrintURL     =   '<?php echo site_url([ 'api', 'gastro', 'localprint']);?>/';
                var posPrinter='<?php echo store_option( 'nexo_pos_printer' );?>';

               if(posPrinter!="") {
                    if(posPrinter!="Choose an option") {

                        if($scope.printServerURL==''){
                            var host_url = 'http://'+'<?php echo $_SERVER['HTTP_HOST'];?>'+':1000';

                        } else {

                            var host_url = $scope.printServerURL;
                        }
                        var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
                        var theUrl = host_url+"/sendsampleregister";
                        xmlhttp.onreadystatechange = function() {
                            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                                console.log(xmlhttp.responseText);
                            }
                        }

                        xmlhttp.open("POST", theUrl);
                        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                        xmlhttp.send(JSON.stringify({'URL'   :   LocalPrintURL + '' + $scope.selectedOrder.ID+'?refresh=true&reprint=1','PRINTER'   :   posPrinter}));

                    }

                }
	}

	$scope.paid             =    function(){
		angular.element( '.paid' ).css( 'background', '#87d69b' );
		angular.element( '.unpaid' ).css( 'background', '#c9cdca' );
		angular.element( '.ready-orders' ).css( 'display', 'block' );
		angular.element( '.unpaid-ready-orders' ).css( 'display', 'none' );

	
		$scope.orders = [];
		$scope.selectedOrder 	=	null;
		$scope.orders_paid=[];


        // console.log('unpaid_orders'+JSON.stringify($scope.total_orders));
	
		 // $scope.orders = $scope.total_orders;

		   $scope.orders_paid = $scope.total_orders;

			_.each( $scope.orders_paid, ( order, index ) => {
				// console.log(order.CODE);
				// console.log(order['CODE']);

				if(order.PAYMENT_TYPE){
					if(order.PAYMENT_TYPE!="unpaid"){
						$scope.orders.push($scope.orders_paid[index]);
						// $scope.orders.splice(index,1);
						

					}

			    }
			
			});

	

	}

	$scope.unpaid             =    function(){
		angular.element( '.unpaid' ).css( 'background', '#87d69b' );
		angular.element( '.paid' ).css( 'background', '#c9cdca' );
		// angular.element( '.ready-orders' ).css( 'display', 'none' );
		angular.element( '.unpaid-ready-orders' ).css( 'display', 'block' );


		$scope.orders = [];
		$scope.selectedOrder 	=	null;
		$scope.orders_unpaid=[];

		 // console.log('unpaid_orders'+JSON.stringify($scope.total_orders));
		
		 $scope.orders_unpaid = $scope.total_orders;

		//  $scope.orders = $scope.orders_unpaid;

			_.each( $scope.orders_unpaid, ( order, index ) => {
				// console.log(order.CODE);
				// console.log(order['CODE']);

			   if(order.PAYMENT_TYPE){
					if(order.PAYMENT_TYPE=="unpaid"){
						$scope.orders.push($scope.orders_unpaid[index]);
						
						

					}

			    }
			
			});

		//	$scope.orders = $scope.orders_unpaid;
			

			// $scope.unpaid();
		

				
			

	}



	$scope.npsPrint_kitchen         =   function() {
	
						mutiplePrintURL     =   '<?php echo site_url([ 'api', 'gastro', 'kitchens', 'nps-splitted-print_ready' ]);?>/';
					    kitchens_get     =   '<?php echo site_url([ 'api', 'gastro', 'kitchens', 'getkitchen' ]);?>';
                        kitchens_equal = '<?php echo site_url([ 'api', 'gastro', 'kitchens', 'kitchens_products_ready' ]);?>';

		                $http.get( kitchens_get ,{
                            headers         :   {
                                '<?php echo $this->config->item('rest_key_name');?>'    :   '<?php echo @$Options[ 'rest_key' ];?>'
                            }
                        }).then( ( returned ) => {
                            
                           /// console.log(returned.data);

                           // array_data= [];

                            returned.data.map( data => {
                                  
                                const url   =   mutiplePrintURL + $scope.selectedOrder.ID +'/'+data.ID +'<?php echo store_get_param( '?' );?>&app_code=<?php echo store_option( 'nexopos_app_code' );?>';
                                    
                                var delivery_kitchen = '<?php echo get_option( store_prefix() . 'delivery_kitchen' );?>';

                                var takeaway_kitchen ='<?php echo get_option( store_prefix() . 'takeaway_kitchen' );?>';

                                var printer_takeway = '<?php echo store_option( 'printer_takeway' );?>';

                                var kitchens_printer = '';

                              

                                    $http.get( kitchens_equal+'/'+data.ID+'/'+$scope.selectedOrder.ID ,{
                                        headers         :   {
                                            '<?php echo $this->config->item('rest_key_name');?>'    :   '<?php echo @$Options[ 'rest_key' ];?>'
                                        }
                                    }).then( ( returned ) => {


                                           kitchens_printer ='';
                                           if(data.Printer_name!=''){
                                                     
                                             kitchens_printer = data.Printer_name;

                                           } else if(printer_takeway!='') {
                                             kitchens_printer = printer_takeway;
                                           } else {

                                             kitchens_printer ='';
                                           }



                                        if(returned.data==1){

                                            const url   =   mutiplePrintURL + $scope.selectedOrder.ID +'/'+data.ID +'<?php echo store_get_param( '?' );?>&app_code=<?php echo store_option( 'nexopos_app_code' );?>';
                                            


                                             if(kitchens_printer!='Choose an option'&&kitchens_printer!='') {

                                             	if($scope.printServerURL==''){

						                            var host_url = 'http://'+'<?php echo $_SERVER['HTTP_HOST'];?>'+':1000';

						                        } else {

						                            var host_url = $scope.printServerURL;
						                        }


                                              //  var host_url = '<?php echo $_SERVER['HTTP_HOST'];?>';


                                                var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
                                                var theUrl = host_url+"/sendsample";
                                                xmlhttp.onreadystatechange = function() {
                                                    if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                                                        console(xmlhttp.responseText);
                                                    }
                                                }

                                                xmlhttp.open("POST", theUrl);
                                                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                                                xmlhttp.send(JSON.stringify({'URL'   :   url + '' + returned.order_id,'PRINTER'   :   kitchens_printer}));  

                                            }



                                        } 
                                       
                                    });

                                     console.log( 'has finished kitchen receipt' );

                             
                                     
                            });
                          

                        });
	}

	/**
		* Register Print NPS
		* @return void
		*/
	$scope.registerPrint    =   function() {
		<?php if ( empty( $current_register[ 'ASSIGNED_PRINTER' ] ) || ! filter_var( $current_register[ 'NPS_URL' ], FILTER_VALIDATE_URL ) ):?>
			NexoAPI.Notify().warning(
				`<?php echo __( 'Unable to print', 'gastro' );?>`,
				`<?php echo __( 'No printer is assigned to the cash register or the server URL is incorrect.', 'gastro' );?>`
			);
		<?php else:?>
		// $.ajax( '<?php echo dashboard_url([ 'local-print' ]);?>' + '/' + $scope.selectedOrder.ID, {
		// 	success 	:	function( printResult ) {
		// 		$.ajax( '<?php echo $current_register[ 'NPS_URL' ];?>/api/print', {
		// 			type  	:	'POST',
		// 			data 	:	{
		// 				'content' 	:	printResult,
		// 				'printer'	:	'<?php echo $current_register[ 'ASSIGNED_PRINTER' ];?>'
		// 			},
		// 			dataType 	:	'json',
		// 			success 	:	function( result ) {
		// 				NexoAPI.Toast()( `<?php echo __( 'Print job submitted', 'gastro' );?>` );
		// 			},
		// 			error		:	() => {
		// 				NexoAPI.Notify().warning(
		// 					`<?php echo __( 'Unable to print', 'gastro' );?>`,
		// 					`<?php echo __( 'NexoPOS has not been able to connect to the Print Server or this latest has returned an unexpected error.', 'gastro' );?>`
		// 				);
		// 			}
		// 		});
		// 	}
		// });
		 LocalPrintURL     =   '<?php echo site_url([ 'api', 'gastro', 'localprint']);?>/'; 
		 if(registerPrinter!="") {
                                    if(registerPrinter!="Choose an option") {

                                        if(printServerURL==''){
                                            var host_url = 'http://'+'<?php echo $_SERVER['HTTP_HOST'];?>'+':1000';

                                        } else {

                                            var host_url = printServerURL;
                                        }
                                        var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
                                        var theUrl = host_url+"/sendsampleregister";
                                        xmlhttp.onreadystatechange = function() {
                                            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                                                console.log(xmlhttp.responseText);
                                            }
                                        }

                                        xmlhttp.open("POST", theUrl);
                                        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                                        xmlhttp.send(JSON.stringify({'URL'   :   LocalPrintURL + '' + returned.order_id+'?refresh=true','PRINTER'   :   registerPrinter}));

                                    }

                                }
		<?php endif;?>
	}

	/**
	* Payement
	* @return void
	**/
	
	$scope.runPayment             =    function(){


		//$('.ready-orders-box').modal('hide');
		
		v2Checkout.From 				=	'readerOrdersCTRL.runPayment';
		v2Checkout.emptyCartItemTable();
		// since by default the query don't return the meta. We can the force meta when the order is being open
		// on the ready order screen.
		// the modifier will be available on the cart
		console.log($scope.selectedOrder.items);
		// return false;
		$scope.selectedOrder.items.forEach( ( item ) => {


			item.metas 								=	new Object;
			if ( item.RESTAURANT_FOOD_MODIFIERS !== undefined ) {
				item.restaurant_food_modifiers 		=	JSON.parse( item.RESTAURANT_FOOD_MODIFIERS );
			}
			item.restaurant_food_issue		=	item.RESTAURANT_FOOD_ISSUE;
			item.restaurant_food_status 	=	item.RESTAURANT_FOOD_STATUS;
			item.restaurant_note			=	item.RESTAURANT_FOOD_NOTE;

			item.TAX		=	item.TAX;
			item.TOTAL_TAX 	=	item.TOTAL_TAX;
			//item.restaurant_note			=	item.RESTAURANT_FOOD_NOTE;
		});
			


		v2Checkout.CartItems 			=	$scope.selectedOrder.items;


		if( $scope.selectedOrder.REMISE_TYPE != '' ) {
			v2Checkout.CartRemiseType				=	$scope.selectedOrder.REMISE_TYPE;
			v2Checkout.CartRemise					=	NexoAPI.ParseFloat( $scope.selectedOrder.REMISE );
			v2Checkout.CartRemisePercent			=	NexoAPI.ParseFloat( $scope.selectedOrder.REMISE_PERCENT );
			v2Checkout.CartRemiseEnabled			=	true;
		}



		if( parseFloat( $scope.selectedOrder.GROUP_DISCOUNT ) > 0 ) {
			v2Checkout.CartGroupDiscount 			=	parseFloat( $scope.selectedOrder.GROUP_DISCOUNT ); // final amount
			v2Checkout.CartGroupDiscountAmount 		=	parseFloat( $scope.selectedOrder.GROUP_DISCOUNT ); // Amount set on each group
			v2Checkout.CartGroupDiscountType 		=	'amount'; // Discount type
			v2Checkout.CartGroupDiscountEnabled 	=	true;
		}



		v2Checkout.CartCustomerID 			=	$scope.selectedOrder.REF_CLIENT;
		// @since 2.7.3
		v2Checkout.CartNote 				=	$scope.selectedOrder.DESCRIPTION;
		v2Checkout.CartTitle 				=	$scope.selectedOrder.TITRE;

		// @since 3.1.2
		v2Checkout.CartShipping 			=	parseFloat( $scope.selectedOrder.SHIPPING_AMOUNT );
		$scope.price 						=	v2Checkout.CartShipping; // for shipping directive

		// adding meta
		v2Checkout.CartMetas 				=	{
			...v2Checkout.CartMetas,
			...$scope.selectedOrder.metas
		}



		$( '.cart-shipping-amount' ).html( $filter( 'moneyFormat' )( $scope.price ) );

		// Restore Custom Ristourne
		v2Checkout.restoreCustomRistourne();

		// Refresh Cart
		// Reset Cart state

		v2Checkout.buildCartItemTable();



		v2Checkout.refreshCart();

		v2Checkout.refreshCartValues();
		v2Checkout.ProcessURL				=	"<?php echo site_url(array( 'rest', 'nexo', 'order' ));?>" + '/<?php echo User::id();?>/' + $scope.selectedOrder.ID + "?store_id=<?php echo get_store_id();?>&from-pos-waiter-screen=true";
		v2Checkout.ProcessType				=	'PUT';


		// convert type to terminated
		if( _.indexOf( [ 'dinein', 'takeaway', 'delivery'], $scope.selectedOrder.REAL_TYPE ) != -1 ) {
			v2Checkout.CartType 				=	'nexo_order_' + $scope.selectedOrder.REAL_TYPE + '_paid';
		} else {
			// $scope.openReadyOrders();
			v2Checkout.resetCart();
			v2Checkout.ProcessURL 		=	"<?php echo site_url(array( 'rest', 'nexo', 'order', User::id() ));?>?store_id=<?php echo get_store_id();?>";
			v2Checkout.ProcessType 		=	'POST';
			return NexoAPI.Toast()( '<?php echo _s( 'The order type is not supported.', 'gastro' );?>' );
		}

		// Restore Shipping
		// @since 3.1
		_.each( $scope.selectedOrder.shipping, ( value, key ) => {
			$scope[ key ] 	=	value;
		});
		
		$rootScope.$broadcast( 'filter-selected-order-type', {
			namespace 	:	$scope.selectedOrder.RESTAURANT_ORDER_TYPE || $scope.selectedOrder.REAL_TYPE
		});
		$rootScope.$emit( 'payBox.openPayBox' );

		//$window.location.reload();

		//$state.reload();
	}

	
	/**
	 * Unselect all orders
	**/

	$scope.unselectAllOrders 	=	function(){
		_.each( $scope.orders, function( _order ) {
			_order.active       =    false;
		});
	}
	
	/***
	* Select Order
	* @return void
	**/
	
	$scope.selectOrder            =    function( order ) {
		// unselect all orders
		$scope.unselectAllOrders();
		
		

		order.active             =    true;
		$scope.selectedOrder 	=	order;

	}

	/**
	 * Resume Orders
	 * @return string
	**/

	$scope.resumeItems 		=	function( items ) {
		let itemNames 		=	[];
		_.each( items, function( item ){
			itemNames.push( item.DESIGN );
		});
		return itemNames.toString();
	}

	$scope.fetchOrders 		=	function() {
		// $scope.orders  = ;
		$http.get( '<?php echo site_url([ 'api', 'gastro', 'tables', 'orders', store_get_param( '?' ) ]);?>', {
                headers			:	{
                    '<?php echo $this->config->item('rest_key_name');?>'	:	'<?php echo get_option( 'rest_key' );?>'
                }
            })
		.then( function( returned ){



			// filter only ready items
			let indexToRemove 		=	[];
			let dineInOrders 		=	[];
			returned.data.forEach( ( order, index ) => {
				// if order is not ready or collected
				if( _.indexOf([ 'ready', 'collected' ], order.STATUS ) == -1 ) {
					indexToRemove.push( index );
				}

				if( order.REAL_TYPE == 'dinein' ) {
					dineInOrders.push( order );
				}
			});



			// Just emit what the server returns
			$rootScope.$emit( 'getOrders.withMetas', angular.copy( dineInOrders ) );



			// Clear all unused index
			indexToRemove.forEach( index => {
				returned.data.splice( index, 1 );
			});

			

			$scope.rawOrders		=	[];
			$scope.rawOrdersCodes 	=	[];
			
			_.each( returned.data, function( order ) {
				if( order.TYPE != 'nexo_order_comptants' ) {
					$scope.rawOrders.unshift( order );
					$scope.rawOrdersCodes.unshift( order.CODE );
				}
			});



			let indexToKill 		=	[];
			let indexCodeToKill 	=	[];
			// remove order which aren't not more listed
			_.each( $scope.orders, ( order, index ) => {
				if( _.indexOf( $scope.rawOrdersCodes, order.CODE ) == -1 ) {
					indexToKill.push( index );
					indexCodeToKill.push( _.indexOf( $scope.ordersCodes, order.CODE ) );
				}
			});

			// console.log($scope.orders);

				//$scope.total_order = $scope.orders;

			
			

			_.each( $scope.rawOrdersCodes, function( rawCode, index ){
				if( _.indexOf( $scope.ordersCodes, rawCode ) == -1 ) {
					$scope.orders.unshift( $scope.rawOrders[ index ] );
					$scope.ordersCodes.unshift( rawCode );
					$scope.newOrders++;
				}
			});



			$scope.total_orders = returned.data;

		

			

			//console.log($scope.total_orders);

			if( indexToKill.length > 0 ){
				// kill indexes
				indexToKill.forEach( index => {
					$scope.orders.splice( index, 1 );
					if( $scope.newOrders > 0 ) {
						$scope.newOrders--;
					}
				});
			}

			if( indexCodeToKill.length > 0 ) {
				indexCodeToKill.forEach( index => {
					$scope.ordersCodes.splice( index, 1 );
					if( $scope.newOrders > 0 ) {
						$scope.newOrders--;
					}
				});
			}


			// var currentIndex            =   0;
			// console.log('readyOrders');


		   $scope.orders = angular.copy($scope.total_orders);


		   



			// _.each( $scope.orders, ( order, index ) => {
			// 	// console.log(order.CODE);
			// 	// console.log(order['CODE']);

			// 	if(order.PAYMENT_TYPE){
			// 		if(order.PAYMENT_TYPE!="unpaid"){

			// 			console.log('log_paid');
			// 			//$scope.orders_paid.push($scope.orders[index]);
			// 			// $scope.orders.splice(index,1);
			// 			$scope.orders.push($scope.orders[index]);
			// 		}


			//     }
			
			// });

			_.each( $scope.orders, ( order, index ) => {
				// console.log(order.CODE);
				// console.log(order['CODE']);

				if(order.PAYMENT_TYPE){
					if(order.PAYMENT_TYPE!="unpaid"){

					    // console.log('paid1');
						
						$('.paid').addClass('color');

				        $('.unpaid').addClass('uncolor');
						

					} else {

					    // console.log('unpaid1');

						$('.paid').addClass('uncolor');
						$('.unpaid').addClass('color');

					}

			    }
			
			});

		   


			//  $scope.orders_unpaid = $scope.orders;

			// console.log('unpaid_orders'+JSON.stringify($scope.orders));

			

	

		});
	}

	/**
	 * Time SPan
	 * @param object order
	 * @return string
	**/

	$scope.timeSpan 		=	function( order ){
		return moment( order.DATE_CREATION ).from( tendoo.date );
	}

	$scope.rawOrders 			=	[];
	$scope.rawOrdersCodes 		=	[];
	$scope.orders 				=	[];
	$scope.orders_unpaid        =   [];
    $scope.orders_paid          =   [];
	$scope.ordersCodes  		=	[];
	$scope.total_orders          =   [];
	$scope.orders_unpaid_color  =   '';
    $scope.orders_paid_color    =   '';
	// $scope.rawOrders  =
	
	<?php if( store_option( 'gastro_disable_orders_fetch', 'no' ) === 'no' ):?>
	$interval( function(){
		$scope.fetchOrders();
	}, <?php echo store_option( 'gastro_order_refresh_interval', 6000 );?> );
	<?php endif;?>

	NexoAPI.events.addAction( 'close_paybox', function(){
		// if current order is a modification
		// we can cancel that.
		if( v2Checkout.ProcessType == 'PUT' && v2Checkout.From == 'readerOrdersCTRL.runPayment' ) {
			// $scope.openReadyOrders();
			v2Checkout.resetCart();
			v2Checkout.ProcessURL 		=	"<?php echo site_url(array( 'rest', 'nexo', 'order', User::id() ));?>?store_id=<?php echo get_store_id();?>";
			v2Checkout.ProcessType 		=	'POST';
		}
		$scope.selectedOrder 	=	null;
		$scope.unselectAllOrders();
	});

	/**
	 * Remove only served order from the list
	 * What even happen, when an order is proceeced, the selectedOrder is reset.
	**/

	NexoAPI.events.addFilter( 'test_order_type', function( data ){
		$scope.selectedOrder 	=	null;
		$scope.fetchOrders();
		return data;
	});

	/**
	 * Watch Events
	**/

	$scope.$on( 'open-ready-orders', function(){
		$scope.openReadyOrders();
	});
}]);
</script>
<script>
tendooApp.directive( 'billOrders', function(){
	return {
		templateUrl         :    '<?php echo site_url([ 'dashboard', store_slug(), 'gastro', 'templates', 'bill_orders' ] );?>',
		restrict            :   'E'
	}
});

tendooApp.controller( 'billordersCTRL', [ 
'$scope', '$timeout', '$compile', '$rootScope', '$interval', '$http', '$filter', 
function( $scope, $timeout, $compile, $rootScope, $interval, $http, $filter ){
	$scope.$watch( 'newOrders', function( previous, current ){
		if( $scope.newOrders > 0 && previous < current ){
			NexoAPI.Toast()( '<?php echo _s( 'A new order is ready', 'gastro' );?>' );
		}
	});

	// $( '.new-orders-button' ).append( '<span class="badge badge-warning">42</span>' );
	$scope.newOrders         	=    0;  

	$scope.selectedOrder 		=	null;

	$scope.printServerURL        = '<?php echo store_option( 'nexo_print_server_url' );?>';


	//alert(selectedOrder);

	
	/**
	* Open Waiter Screen
	* @return void
	**/
	
	$scope.billOrders       =    function(){

		NexoAPI.Bootbox().alert({
			message 		:	'<div class="bill-orders"><bill-orders></bill-orders></div>',
			title          :	'<?php echo _s( 'Bills', 'gastro' );?>',
			buttons 		:	{
				ok: {
					label: '<span ><?php echo _s( 'Close', 'gastro' );?></span></span>',
					className: 'btn-default'
				},
			}, 
			callback 		:	function( action ) {
				// $rootScope.$broadcast( 'open-select-order-type' );
			},
			className 	:	'bill-orders-box col-md-8 col-lg-12'
		});
		
		$scope.windowHeight           =	window.innerHeight;
		$scope.wrapperHeight          =	$scope.windowHeight - ( ( 100 * 2 ) + 30 );


		// 

		let current_id = '<?php echo $this->events->apply_filters('group_details', '')->id;?>';

		// if(current_id!=12){
		// 	$('.selectedOrder.TYPE').hide();
		// }

		let printAttr;
		switch( '<?php echo store_option( 'nexo_print_gateway' );?>' ) {
			case 'normal_print': 
				printAttr	=	'ng-click="printReceipt()"'; 
			break;
			case 'nexo_print_server': 
				printAttr	=	'ng-click="npsPrint()"'; 
			break;
			case 'register_nps': 
				printAttr	=	'ng-click="registerPrint()"'; 
			break;
		}
		
		
		$timeout( function(){
			angular.element( '.bill-orders-box .modal-dialog' );
			angular.element( '.bill-orders-box .modal-body' ).css( 'padding-top', '0px' );
			angular.element( '.bill-orders-box .modal-body' ).css( 'padding-bottom', '0px' );
			angular.element( '.bill-orders-box .modal-body' ).css( 'padding-left', '0px' );
			angular.element( '.bill-orders-box .modal-body' ).css( 'padding-right', '0px' );
			angular.element( '.bill-orders-box .modal-body' ).css( 'height', $scope.wrapperHeight );
			angular.element( '.bill-orders-box .modal-body' ).css( 'overflow-x', 'hidden' );
		}, 150 );


		// if($scope.orders.length>0){

		// } else {

		// 	$('.paid').addClass('uncolor');
		// 	$('.unpaid').addClass('color');
		// }
		//console.log($scope.orders);
		
		// $scope.orders_unpaid_color  =   'uncolor';
  //       $scope.orders_paid_color    =   'uncolor';

		$( '.bill-orders-box' ).last().find( '.modal-body' )
		.prepend('<div ng-click="paid()" class="col-md-6 col-lg-6 ng-scope paid" style="text-align:center;border:none;padding: 20px;margin-bottom: 1%;">Paid</div><div ng-click="unpaid()" class="col-md-6 col-lg-6 ng-scope unpaid" style="text-align:center;border:none;padding: 20px;margin-bottom: 1%;">Unpaid</div>');

		$( '.bill-orders-box' ).last().find( '.modal-header' )
		.append('<div class="col-md-6 search-order-field" style="padding:15px;float:right;"><div class="input-group"><span class="input-group-addon" id="basic-addon1"><?php echo __( 'Search Order', 'gastro' );?></span><input ng-model="searchbillOrders" type="text" class="form-control" placeholder="<?php echo __( 'Order Code', 'gastro' );?>" aria-describedby="basic-addon1" style="height: 62px;"></div></div>');
		/*	.prepend( '<button class="sampleclassed col-md-6 col-lg-6" style="text-align:center;border:none;padding: 20px;margin-bottom: 1%;background-color:#87d69b;" ng-click="paidUnpaid()">Paid</button> <button class="sampleclassed col-md-6 col-lg-6" style="text-align:center;padding: 20px;border:none;margin-bottom: 1%;background-color:#c9cdca;" ng-click="paidUnpaid()">Non Paid</button>' );*/

		if(current_id!=12){
			$( '.bill-orders-box' ).last().find( '.modal-footer' )
			.prepend( '<a ng-show="selectedOrder != null && ( selectedOrder.TYPE != \'nexo_order_comptant\')" && selectedOrder.REAL_TYPE == \'dinein\'" ng-click="runPayment()" class="btn btn-primary selectedOrder.TYPE" style="font-size: 110%;color:white;background-color:#c40606;border-color:#c40606;"><?php echo _s( 'Pay that order', 'nexo-restarant' );?></a>' );
	    }
		$( '.bill-orders-box' ).last().find( '.modal-footer' )
		.prepend( '<a ng-show="selectedOrder != null && ( selectedOrder.REAL_TYPE == \'delivery\' || selectedOrder.REAL_TYPE == \'takeaway\' || selectedOrder.REAL_TYPE == \'dinein\' || selectedOrder.REAL_TYPE == \'quickbill\')" ' + printAttr + ' class="btn btn-primary" style="font-size: 110%;color:white;background-color:#3ba728;border-color:#3ba728;"><?php echo _s( 'Print Receipt', 'nexo-restarant' );?></a>' );
		$( '[data-bb-handler="ok"]' ).attr( 'style', 'width:122px' );

			
		$( '.bill-orders-box' ).last().find( '.modal-footer' )
			.prepend( '<a ng-show="selectedOrder != null && ( selectedOrder.REAL_TYPE == \'delivery\' || selectedOrder.REAL_TYPE == \'takeaway\' || selectedOrder.REAL_TYPE == \'dinein\' )"   ng-click="npsPrint_kitchen()" class="btn btn-primary selectedOrder.TYPE" style="font-size: 110%;color:white;float:left;"><?php echo _s( 'Reprint kitchen', 'nexo-restarant' );?></a>' );
		// $( '.ready-orders-box' ).last().find( '.modal-footer' )
		// .prepend( '<a ng-show="selectedOrder.STATUS == \'ready\'" ng-click="setAsServed()" class="btn btn-primary"><?php echo _s( 'Serve', 'nexo-restarant' );?></a>' );
		
		$( '.bill-orders' ).html( 
			$compile( $( '.bill-orders').html() )( $scope ) 
		);
		$( '.unpaid-bill-orders' ).html( 
			$compile( $( '.bill-orders').html() )( $scope ) 
		);
		$( '.bill-orders-box' )
		.find( '.modal-header' )
		.html( $compile( $( '.bill-orders-box' ).find( '.modal-header' ).html() )( $scope ) );
		$( '.bill-orders-box' )
		.find( '.modal-body' )
		.html( $compile( $( '.bill-orders-box' ).find( '.modal-body' ).html() )( $scope ) );
		$( '.bill-orders-box' )
		.find( '.modal-footer' )
		.html( $compile( $( '.bill-orders-box' ).find( '.modal-footer' ).html() )( $scope ) );
		$scope.fetchOrders();
		// console.log('123');
		$('.modal-dialog').css('width','100%');
	}

	$scope.printReceipt 		=	function(){
		$( 'body' ).append( '<iframe id="receipt-wrapper" style="visibility:hidden;height:0px;width:0px;position:absolute;top:0;" src="<?php echo dashboard_url([ 'orders', 'receipt' ]);?>/' + $scope.selectedOrder.ID + '?refresh=true&autoprint=true"></iframe>' );
	}

	/**
		* Nexo Print Server
		* @return void
		*/
	$scope.npsPrint         =   function() {


		console.log("npsprint");
		// $.ajax( '<?php echo dashboard_url([ 'local-print'  ]);?>' + '/' + $scope.selectedOrder.ID, {
		// 	success 	:	function( printResult ) {
		// 		$.ajax( '<?php echo store_option( 'nexo_print_server_url' );?>/api/print', {
		// 			type  	:	'POST',
		// 			data 	:	{
		// 				'content' 	:	printResult,
		// 				'printer'	:	'<?php echo store_option( 'nexo_pos_printer' );?>'
		// 			},
		// 			dataType 	:	'json',
		// 			success 	:	function( result ) {
		// 				console.log( result );
		// 			}
		// 		});
		// 	}
		// });

		LocalPrintURL     =   '<?php echo site_url([ 'api', 'gastro', 'localprint']);?>/';
                var posPrinter='<?php echo store_option( 'nexo_pos_printer' );?>';
var Reprint = 'Reprint';
               if(posPrinter!="") {
                    if(posPrinter!="Choose an option") {

                        if($scope.printServerURL==''){
                            var host_url = 'http://'+'<?php echo $_SERVER['HTTP_HOST'];?>'+':1000';

                        } else {

                            var host_url = $scope.printServerURL;
                        }
                        var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
                        var theUrl = host_url+"/sendsampleregister";
                        xmlhttp.onreadystatechange = function() {
                            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                                console.log(xmlhttp.responseText);
                            }
                        }

                        xmlhttp.open("POST", theUrl);
                        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                        xmlhttp.send(JSON.stringify({'URL'   :   LocalPrintURL + '' + $scope.selectedOrder.ID+'?refresh=true&reprint=1','PRINTER'   :   posPrinter }));
                        console.log(URL);
                        console.log("sdfdsfdsdfs");

                    }

                }
	}

	$scope.paid             =    function(){

		// console.log("paid");
		// exit();
		angular.element( '.paid' ).css( 'background', '#87d69b' );
		angular.element( '.unpaid' ).css( 'background', '#c9cdca' );
		angular.element( '.bill-orders' ).css( 'display', 'block' );
		angular.element( '.unpaid-bill-orders' ).css( 'display', 'none' );

	
		$scope.orders = [];
		$scope.selectedOrder 	=	null;
		$scope.orders_paid=[];


        // console.log('unpaid_orders'+JSON.stringify($scope.total_orders));
	
		 // $scope.orders = $scope.total_orders;

		   $scope.orders_paid = $scope.total_orders;

			_.each( $scope.orders_paid, ( order, index ) => {
				// console.log(order.CODE);
				// console.log(order['CODE']);

				if(order.PAYMENT_TYPE){
					if(order.PAYMENT_TYPE!="unpaid"){
						$scope.orders.push($scope.orders_paid[index]);
						// $scope.orders.splice(index,1);
						

					}

			    }
			
			});

	

	}

	$scope.unpaid             =    function(){

		console.log("unpaid");
		angular.element( '.unpaid' ).css( 'background', '#87d69b' );
		angular.element( '.paid' ).css( 'background', '#c9cdca' );
		// angular.element( '.ready-orders' ).css( 'display', 'none' );
		angular.element( '.unpaid-bill-orders' ).css( 'display', 'block' );


		$scope.orders = [];
		$scope.selectedOrder 	=	null;
		$scope.orders_unpaid=[];

		 // console.log('unpaid_orders'+JSON.stringify($scope.total_orders));
		
		 $scope.orders_unpaid = $scope.total_orders;
//console.log($scope.total_orders);
		//  $scope.orders = $scope.orders_unpaid;

			_.each( $scope.orders_unpaid, ( order, index ) => {
				// console.log(order.CODE);
				// console.log(order['CODE']);

			   if(order.PAYMENT_TYPE){
					if(order.PAYMENT_TYPE=="unpaid"){
						$scope.orders.push($scope.orders_unpaid[index]);
						
						

					}

			    }
			
			});

		//	$scope.orders = $scope.orders_unpaid;
			

			// $scope.unpaid();
		

				
			

	}



	$scope.npsPrint_kitchen         =   function() {
	
						mutiplePrintURL     =   '<?php echo site_url([ 'api', 'gastro', 'kitchens', 'nps-splitted-print_ready' ]);?>/';
					    kitchens_get     =   '<?php echo site_url([ 'api', 'gastro', 'kitchens', 'getkitchen' ]);?>';
                        kitchens_equal = '<?php echo site_url([ 'api', 'gastro', 'kitchens', 'kitchens_products_ready' ]);?>';

		                $http.get( kitchens_get ,{
                            headers         :   {
                                '<?php echo $this->config->item('rest_key_name');?>'    :   '<?php echo @$Options[ 'rest_key' ];?>'
                            }
                        }).then( ( returned ) => {
                            
                           /// console.log(returned.data);

                           // array_data= [];

                            returned.data.map( data => {
                                  
                                const url   =   mutiplePrintURL + $scope.selectedOrder.ID +'/'+data.ID +'<?php echo store_get_param( '?' );?>&app_code=<?php echo store_option( 'nexopos_app_code' );?>';
                                    
                                var delivery_kitchen = '<?php echo get_option( store_prefix() . 'delivery_kitchen' );?>';

                                var takeaway_kitchen ='<?php echo get_option( store_prefix() . 'takeaway_kitchen' );?>';

                                var printer_takeway = '<?php echo store_option( 'printer_takeway' );?>';

                                var kitchens_printer = '';

                              

                                    $http.get( kitchens_equal+'/'+data.ID+'/'+$scope.selectedOrder.ID ,{
                                        headers         :   {
                                            '<?php echo $this->config->item('rest_key_name');?>'    :   '<?php echo @$Options[ 'rest_key' ];?>'
                                        }
                                    }).then( ( returned ) => {


                                           kitchens_printer ='';
                                           if(data.Printer_name!=''){
                                                     
                                             kitchens_printer = data.Printer_name;

                                           } else if(printer_takeway!='') {
                                             kitchens_printer = printer_takeway;
                                           } else {

                                             kitchens_printer ='';
                                           }



                                        if(returned.data==1){

                                            const url   =   mutiplePrintURL + $scope.selectedOrder.ID +'/'+data.ID +'<?php echo store_get_param( '?' );?>&app_code=<?php echo store_option( 'nexopos_app_code' );?>';
                                            


                                             if(kitchens_printer!='Choose an option'&&kitchens_printer!='') {

                                             	if($scope.printServerURL==''){

						                            var host_url = 'http://'+'<?php echo $_SERVER['HTTP_HOST'];?>'+':1000';

						                        } else {

						                            var host_url = $scope.printServerURL;
						                        }


                                              //  var host_url = '<?php echo $_SERVER['HTTP_HOST'];?>';


                                                var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
                                                var theUrl = host_url+"/sendsample";
                                                xmlhttp.onreadystatechange = function() {
                                                    if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                                                        console(xmlhttp.responseText);
                                                    }
                                                }

                                                xmlhttp.open("POST", theUrl);
                                                xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                                                xmlhttp.send(JSON.stringify({'URL'   :   url + '' + returned.order_id,'PRINTER'   :   kitchens_printer}));  

                                            }



                                        } 
                                       
                                    });

                                     console.log( 'has finished kitchen receipt' );

                             
                                     
                            });
                          

                        });
	}

	/**
		* Register Print NPS
		* @return void
		*/
	$scope.registerPrint    =   function() {

		console.log("registerprint");
		<?php if ( empty( $current_register[ 'ASSIGNED_PRINTER' ] ) || ! filter_var( $current_register[ 'NPS_URL' ], FILTER_VALIDATE_URL ) ):?>
			NexoAPI.Notify().warning(
				`<?php echo __( 'Unable to print', 'gastro' );?>`,
				`<?php echo __( 'No printer is assigned to the cash register or the server URL is incorrect.', 'gastro' );?>`
			);
		<?php else:?>
		// $.ajax( '<?php echo dashboard_url([ 'local-print' ]);?>' + '/' + $scope.selectedOrder.ID, {
		// 	success 	:	function( printResult ) {
		// 		$.ajax( '<?php echo $current_register[ 'NPS_URL' ];?>/api/print', {
		// 			type  	:	'POST',
		// 			data 	:	{
		// 				'content' 	:	printResult,
		// 				'printer'	:	'<?php echo $current_register[ 'ASSIGNED_PRINTER' ];?>'
		// 			},
		// 			dataType 	:	'json',
		// 			success 	:	function( result ) {
		// 				NexoAPI.Toast()( `<?php echo __( 'Print job submitted', 'gastro' );?>` );
		// 			},
		// 			error		:	() => {
		// 				NexoAPI.Notify().warning(
		// 					`<?php echo __( 'Unable to print', 'gastro' );?>`,
		// 					`<?php echo __( 'NexoPOS has not been able to connect to the Print Server or this latest has returned an unexpected error.', 'gastro' );?>`
		// 				);
		// 			}
		// 		});
		// 	}
		// });
		 LocalPrintURL     =   '<?php echo site_url([ 'api', 'gastro', 'localprint']);?>/'; 
		 if(registerPrinter!="") {
                                    if(registerPrinter!="Choose an option") {

                                        if(printServerURL==''){
                                            var host_url = 'http://'+'<?php echo $_SERVER['HTTP_HOST'];?>'+':1000';

                                        } else {

                                            var host_url = printServerURL;
                                        }
                                        var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
                                        var theUrl = host_url+"/sendsampleregister";
                                        xmlhttp.onreadystatechange = function() {
                                            if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                                                console.log(xmlhttp.responseText);
                                            }
                                        }

                                        xmlhttp.open("POST", theUrl);
                                        xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                                        xmlhttp.send(JSON.stringify({'URL'   :   LocalPrintURL + '' + returned.order_id+'?refresh=true','PRINTER'   :   registerPrinter}));

                                    }

                                }
		<?php endif;?>
	}

	/**
	* Payement
	* @return void
	**/
	
	$scope.runPayment             =    function(){

console.log("runpay");
		//$('.ready-orders-box').modal('hide');
		
		v2Checkout.From 				=	'billordersCTRL.runPayment';
		v2Checkout.emptyCartItemTable();
		// since by default the query don't return the meta. We can the force meta when the order is being open
		// on the ready order screen.
		// the modifier will be available on the cart
		console.log($scope.selectedOrder.items);
		// return false;
		$scope.selectedOrder.items.forEach( ( item ) => {


			item.metas 								=	new Object;
			if ( item.RESTAURANT_FOOD_MODIFIERS !== undefined ) {
				item.restaurant_food_modifiers 		=	JSON.parse( item.RESTAURANT_FOOD_MODIFIERS );
			}
			item.restaurant_food_issue		=	item.RESTAURANT_FOOD_ISSUE;
			item.restaurant_food_status 	=	item.RESTAURANT_FOOD_STATUS;
			item.restaurant_note			=	item.RESTAURANT_FOOD_NOTE;

			item.TAX		=	item.TAX;
			item.TOTAL_TAX 	=	item.TOTAL_TAX;
			//item.restaurant_note			=	item.RESTAURANT_FOOD_NOTE;
		});
			


		v2Checkout.CartItems 			=	$scope.selectedOrder.items;


		if( $scope.selectedOrder.REMISE_TYPE != '' ) {
			v2Checkout.CartRemiseType				=	$scope.selectedOrder.REMISE_TYPE;
			v2Checkout.CartRemise					=	NexoAPI.ParseFloat( $scope.selectedOrder.REMISE );
			v2Checkout.CartRemisePercent			=	NexoAPI.ParseFloat( $scope.selectedOrder.REMISE_PERCENT );
			v2Checkout.CartRemiseEnabled			=	true;
		}



		if( parseFloat( $scope.selectedOrder.GROUP_DISCOUNT ) > 0 ) {
			v2Checkout.CartGroupDiscount 			=	parseFloat( $scope.selectedOrder.GROUP_DISCOUNT ); // final amount
			v2Checkout.CartGroupDiscountAmount 		=	parseFloat( $scope.selectedOrder.GROUP_DISCOUNT ); // Amount set on each group
			v2Checkout.CartGroupDiscountType 		=	'amount'; // Discount type
			v2Checkout.CartGroupDiscountEnabled 	=	true;
		}



		v2Checkout.CartCustomerID 			=	$scope.selectedOrder.REF_CLIENT;
		// @since 2.7.3
		v2Checkout.CartNote 				=	$scope.selectedOrder.DESCRIPTION;
		v2Checkout.CartTitle 				=	$scope.selectedOrder.TITRE;

		// @since 3.1.2
		v2Checkout.CartShipping 			=	parseFloat( $scope.selectedOrder.SHIPPING_AMOUNT );
		$scope.price 						=	v2Checkout.CartShipping; // for shipping directive

		// adding meta
		v2Checkout.CartMetas 				=	{
			...v2Checkout.CartMetas,
			...$scope.selectedOrder.metas
		}



		$( '.cart-shipping-amount' ).html( $filter( 'moneyFormat' )( $scope.price ) );

		// Restore Custom Ristourne
		v2Checkout.restoreCustomRistourne();

		// Refresh Cart
		// Reset Cart state

		v2Checkout.buildCartItemTable();



		v2Checkout.refreshCart();

		v2Checkout.refreshCartValues();
		v2Checkout.ProcessURL				=	"<?php echo site_url(array( 'rest', 'nexo', 'order' ));?>" + '/<?php echo User::id();?>/' + $scope.selectedOrder.ID + "?store_id=<?php echo get_store_id();?>&from-pos-waiter-screen=true";
		v2Checkout.ProcessType				=	'PUT';


		// convert type to terminated
		if( _.indexOf( [ 'dinein', 'takeaway', 'delivery'], $scope.selectedOrder.REAL_TYPE ) != -1 ) {
			v2Checkout.CartType 				=	'nexo_order_' + $scope.selectedOrder.REAL_TYPE + '_paid';
		} else {
			// $scope.openReadyOrders();
			v2Checkout.resetCart();
			v2Checkout.ProcessURL 		=	"<?php echo site_url(array( 'rest', 'nexo', 'order', User::id() ));?>?store_id=<?php echo get_store_id();?>";
			v2Checkout.ProcessType 		=	'POST';
			return NexoAPI.Toast()( '<?php echo _s( 'The order type is not supported.', 'gastro' );?>' );
		}

		// Restore Shipping
		// @since 3.1
		_.each( $scope.selectedOrder.shipping, ( value, key ) => {
			$scope[ key ] 	=	value;
		});
		
		$rootScope.$broadcast( 'filter-selected-order-type', {
			namespace 	:	$scope.selectedOrder.RESTAURANT_ORDER_TYPE || $scope.selectedOrder.REAL_TYPE
		});
		$rootScope.$emit( 'payBox.openPayBox' );

		//$window.location.reload();

		//$state.reload();
	}

	
	/**
	 * Unselect all orders
	**/

	$scope.unselectAllOrders 	=	function(){
		_.each( $scope.orders, function( _order ) {
			_order.active       =    false;
		});
	}
	
	/***
	* Select Order
	* @return void
	**/
	
	$scope.selectOrder            =    function( order ) {
		// unselect all orders
		$scope.unselectAllOrders();
		
		

		order.active             =    true;
		$scope.selectedOrder 	=	order;

	}

	/**
	 * Resume Orders
	 * @return string
	**/

	$scope.resumeItems 		=	function( items ) {
		let itemNames 		=	[];
		_.each( items, function( item ){
			itemNames.push( item.DESIGN );
		});
		return itemNames.toString();
	}

	$scope.fetchOrders 		=	function() {
		// $scope.orders  = ;
		$http.get( '<?php echo site_url([ 'api', 'gastro', 'tables', 'billorders', store_get_param( '?' ) ]);?>', {
                headers			:	{
                    '<?php echo $this->config->item('rest_key_name');?>'	:	'<?php echo get_option( 'rest_key' );?>'
                }
            })
		.then( function( returned ){



			// filter only ready items
			let indexToRemove 		=	[];
			let dineInOrders 		=	[];
			returned.data.forEach( ( order, index ) => {
				// if order is not ready or collected
				// if( _.indexOf([ 'ready', 'collected' ], order.STATUS ) == -1 ) {
				// 	indexToRemove.push( index );
				// }

				if( order.REAL_TYPE == 'dinein' ) {
					dineInOrders.push( order );
				}
			});



			// Just emit what the server returns
			$rootScope.$emit( 'getOrders.withMetas', angular.copy( dineInOrders ) );



			// Clear all unused index
			indexToRemove.forEach( index => {
				returned.data.splice( index, 1 );
			});

			

			$scope.rawOrders		=	[];
			$scope.rawOrdersCodes 	=	[];
			
			_.each( returned.data, function( order ) {
				if( order.TYPE != 'nexo_order_comptants' ) {
					$scope.rawOrders.unshift( order );
					$scope.rawOrdersCodes.unshift( order.CODE );
				}
			});



			let indexToKill 		=	[];
			let indexCodeToKill 	=	[];
			// remove order which aren't not more listed
			_.each( $scope.orders, ( order, index ) => {
				if( _.indexOf( $scope.rawOrdersCodes, order.CODE ) == -1 ) {
					indexToKill.push( index );
					indexCodeToKill.push( _.indexOf( $scope.ordersCodes, order.CODE ) );
				}
			});

		//	console.log($scope.orders);

				//$scope.total_order = $scope.orders;

			
			

			_.each( $scope.rawOrdersCodes, function( rawCode, index ){
				if( _.indexOf( $scope.ordersCodes, rawCode ) == -1 ) {
					$scope.orders.unshift( $scope.rawOrders[ index ] );
					$scope.ordersCodes.unshift( rawCode );
					$scope.newOrders++;
				}
			});



			$scope.total_orders = returned.data;

		

			

			//console.log($scope.total_orders);

			if( indexToKill.length > 0 ){
				// kill indexes
				indexToKill.forEach( index => {
					$scope.orders.splice( index, 1 );
					if( $scope.newOrders > 0 ) {
						$scope.newOrders--;
					}
				});
			}

			if( indexCodeToKill.length > 0 ) {
				indexCodeToKill.forEach( index => {
					$scope.ordersCodes.splice( index, 1 );
					if( $scope.newOrders > 0 ) {
						$scope.newOrders--;
					}
				});
			}


			// var currentIndex            =   0;
			// console.log('readyOrders');


		   $scope.orders = angular.copy($scope.total_orders);


		   



			// _.each( $scope.orders, ( order, index ) => {
			// 	// console.log(order.CODE);
			// 	// console.log(order['CODE']);

			// 	if(order.PAYMENT_TYPE){
			// 		if(order.PAYMENT_TYPE!="unpaid"){

			// 			console.log('log_paid');
			// 			//$scope.orders_paid.push($scope.orders[index]);
			// 			// $scope.orders.splice(index,1);
			// 			$scope.orders.push($scope.orders[index]);
			// 		}


			//     }
			
			// });

			_.each( $scope.orders, ( order, index ) => {
				// console.log(order.CODE);
				// console.log(order['CODE']);

				if(order.PAYMENT_TYPE){
					if(order.PAYMENT_TYPE!="unpaid"){

					    // console.log('paid1');
						
						$('.paid').addClass('color');

				        $('.unpaid').addClass('uncolor');
						

					} else {

					    // console.log('unpaid1');

						$('.paid').addClass('uncolor');
						$('.unpaid').addClass('color');

					}

			    }
			
			});

		   


			//  $scope.orders_unpaid = $scope.orders;

			// console.log('unpaid_orders'+JSON.stringify($scope.orders));

			

	

		});
	}

	/**
	 * Time SPan
	 * @param object order
	 * @return string
	**/

	$scope.timeSpan 		=	function( order ){
		return moment( order.DATE_CREATION ).from( tendoo.date );
	}

	$scope.rawOrders 			=	[];
	$scope.rawOrdersCodes 		=	[];
	$scope.orders 				=	[];
	$scope.orders_unpaid        =   [];
    $scope.orders_paid          =   [];
	$scope.ordersCodes  		=	[];
	$scope.total_orders          =   [];
	$scope.orders_unpaid_color  =   '';
    $scope.orders_paid_color    =   '';
	// $scope.rawOrders  =
	
	<?php if( store_option( 'gastro_disable_orders_fetch', 'no' ) === 'no' ):?>
	$interval( function(){
		$scope.fetchOrders();
	}, <?php echo store_option( 'gastro_order_refresh_interval', 6000 );?> );
	<?php endif;?>

	NexoAPI.events.addAction( 'close_paybox', function(){
		// if current order is a modification
		// we can cancel that.
		if( v2Checkout.ProcessType == 'PUT' && v2Checkout.From == 'billordersCTRL.runPayment' ) {
			// $scope.openReadyOrders();
			v2Checkout.resetCart();
			v2Checkout.ProcessURL 		=	"<?php echo site_url(array( 'rest', 'nexo', 'order', User::id() ));?>?store_id=<?php echo get_store_id();?>";
			v2Checkout.ProcessType 		=	'POST';
		}
		$scope.selectedOrder 	=	null;
		$scope.unselectAllOrders();
	});

	/**
	 * Remove only served order from the list
	 * What even happen, when an order is proceeced, the selectedOrder is reset.
	**/

	NexoAPI.events.addFilter( 'test_order_type', function( data ){
		$scope.selectedOrder 	=	null;
		$scope.fetchOrders();
		return data;
	});

	/**
	 * Watch Events
	**/

	$scope.$on( 'open-bill-orders', function(){
		$scope.billOrders();
	});
}]);
</script>


<style type="text/css">
	
	.color {
		background-color:#87d69b;
	}
	.uncolor {
		background-color:#c9cdca;
	}
</style>



<script type="text/javascript">
	
	 $(document).ready(function () {
      $('.bootbox-close-button ').on('click',function (event) {


      	  console.log('cancel');
      	  var scope = angular.element(document.getElementById('readyorders')).scope();
          
          // event.preventDefault();
          // var id = $(this).closest('.row').attr('id'); // row I
      });
  });
</script>