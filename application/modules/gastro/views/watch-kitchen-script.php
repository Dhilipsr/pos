<?php global $Options; ?>
<script type="text/javascript">
    var watchRestaurantCTRL             =   function( $scope, $http, $timeout, $compile ) {
        $scope.orders                   =   [];
        $scope.products                 =   [];
        
        $scope.customers                =   <?php echo json_encode( $customers );?>;
        $scope.structuredCustomers      =   [];
        $scope.customers.forEach( customer => {
            $scope.structuredCustomers[ customer.ID ]     =   customer;
        });

        $scope.users                =   <?php echo json_encode( $users );?>;
        $scope.structuredUsers      =   [];
        $scope.users.forEach( user => {
            $scope.structuredUsers[ user.user_id ]     =   user;
        });
        
        $scope.tables                   =   <?php echo json_encode( $tables );?>;
        $scope.structuredTables         =   [];
        $scope.tables.forEach( table => {
            $scope.structuredTables[ table.ID ]     =   table;
            console.log($scope.structuredTables[ table.ID ]);
        });



        $scope.sort                   =   'yes';

        $scope.areas                   =   <?php echo json_encode( $areas );?>;
        $scope.structuredAreas         =   [];
        $scope.areas.forEach( area => {
            $scope.structuredAreas[ area.ID ]     =   area;
        });

        $scope.freshOrderMin            =   <?php echo  store_option( 'fresh_order_min', 10 );?>;
        $scope.lateOrderMin             =   <?php  echo store_option( 'late_order_min', 20 );?>;
        $scope.tooLateOrderMin          =   <?php  echo store_option( 'too_late_order_min', 30 );?>;
        $scope.siteLanguage             =   '<?php echo get_instance()->config->item( 'site_language' );?>';

        $scope.freshOrderColor        =   '<?php  echo store_option( 'fresh_order_color', 'bg-success box-success' );?>';
        $scope.lateOrderColor        =   '<?php  echo store_option( 'late_order_color', 'bg-warning box-warning' );?>';
        $scope.tooLateOrderColor        =   '<?php  echo store_option( 'too_late_order_color', 'bg-danger box-danger' );?>';
        
        $scope.timeInterval         =   <?php echo @$Options[ 'refreshing_seconds' ] == null ? 3000 : intval( @$Options[ 'refreshing_seconds' ] ) * 1000;?>;

        $scope.Orderorder = 'asc';

        $scope.isAreaRoomsDisabled  =   <?php echo store_option( 'disable_kitchens', 'yes' ) == 'yes' ? 'true': 'false';?>;

        if( ! $scope.isAreaRoomsDisabled ) {
            $scope.kitchen              =   <?php echo json_encode( $kitchen );?>;
            $scope.kitchen              =   $scope.kitchen[0];
            $scope.room_id              =   0;
            $scope.kitchen_id           =   $scope.kitchen.ID;
        } else {
            $scope.kitchen_id           =   0;
            $scope.room_id              =   0;
        }
        
        $scope.order_types              =   {
            'ready'       :    '<?php echo __( 'Ready', 'gastro' );?>',
            'ongoing'       :    '<?php echo __( 'On Going', 'gastro' );?>',
            'pending'       :    '<?php echo __( 'Pending', 'gastro' );?>',
            'canceled'       :    '<?php echo __( 'Canceled', 'gastro' );?>',
            'rejected'       :    '<?php echo __( 'Rejected', 'gastro' );?>',
            'partially'     :   '<?php echo __( 'Partially', 'gastro' );?>'
        }

        /**
         *  Testing order waiting time to apply a color
         * @param object order
         * @return string class
        **/

        $scope.testOrderWaitingTime         =   function( order ) {


            if(order=='canceled'){

                return "red";

            }else if(order=='not_ready'){

                 return "green";

            } else if(order=='in_preparation'){

                return "orange";

            }   
            //return order;

            // order.items.forEach( ( item ) => {

              
                    // if( order.items[0].RESTAURANT_FOOD_STATUS=='not_ready'){
                    //     //  console.log("not_ready=>"+item);
                    //      return $scope.freshOrderColor;
                    //      return false;

                    // }
                    // if( order.items[0].RESTAURANT_FOOD_STATUS=='in_preparation'){

                    //    // console.log("in_preparation=>"+item);

                    //     return $scope.lateOrderColor;
                    //     return false;

                    // } 
                    // if( order.items[0].RESTAURANT_FOOD_STATUS=='canceled'){

                    //    // console.log("canceled=>"+item);
                    //     return $scope.tooLateOrderColor;
                    //     return false;

                    // }

               // })


             //mutiplePrintURL     =   '<?php echo site_url([  'api', 'gastro', 'kitchens', 'orderstatus' ]);?>/';


            //  $http.get( '<?php echo site_url([ 'api', 'gastro', 'kitchens', 'orderstatus' ]);?>?' + order.CODE + '&<?php echo store_get_param( null );?>', {
            //  headers         :   {
            //          '<?php echo $this->config->item('rest_key_name');?>'    :   '<?php echo @$Options[ 'rest_key' ];?>'
            //      }
            //  }).then(function( data ) {
            //     return data.REF_COMMAND_CODE

            // });


          /*  if( $scope.freshOrderMin  < $scope.lateOrderMin  < $scope.tooLateOrderMin ) {
                let currentTime         =   moment( tendoo.date.format() ).diff( 
                    moment( order.DATE_MOD ).format(), 'minutes' 
                );

                if( currentTime > 0 && currentTime <= $scope.freshOrderMin ) {
                    return $scope.freshOrderColor + ' diff-' + currentTime;
                } else if( currentTime > $scope.freshOrderMin && currentTime <= $scope.lateOrderMin ) {
                    return $scope.lateOrderColor + ' diff-' + currentTime;
                } else if( currentTime > $scope.lateOrderMin ) { // check this if you would like to add more times
                    return $scope.tooLateOrderColor + ' diff-' + currentTime;
                } else {

                     return $scope.freshOrderColor + ' diff-' + currentTime;

                }
            } else {
                console.log( 'Invalid Alert Pattern' );
            }*/
        }

        /**
         * First character in capital letter
         * @param {string} word
         * @return {string} converted word;
        */
        $scope.ucwords          =   function( str ) {
            str = str.toLowerCase();
            return str.replace(/(^([a-zA-Z\p{M}]))|([ -][a-zA-Z\p{M}])/g,
                function(s){
                    return s.toUpperCase();
                });
        }

        /**
         *  Change Food State
         *  @param object order
         *  @param string food state
         *  @return void
        **/
        $scope.changeFoodState      =   ( order, state )  =>  {
            var postObject          =   {
                '<?php echo $this->security->get_csrf_token_name();?>'    :   '<?php echo $this->security->get_csrf_hash();
                ?>',
                selected_foods          :   [],
                all_foods               :   [],
                complete_cooking        :   true,
                order_id                :   order.ORDER_ID,
                order_code              :   order.CODE,
                state                   :   state,
                order_real_type         :   order.REAL_TYPE
            };

            order.meals.forEach( ( meal ) => {
                meal.forEach( ( item ) => {
                    if( item.active ) {
                        postObject.selected_foods.push( item.COMMAND_PRODUCT_ID );
                    }
                    postObject.all_foods.push( item.COMMAND_PRODUCT_ID );
                })
            })

            $http.post('<?php echo site_url([ 'api', 'gastro', 'kitchens', 'food-status', store_get_param('?') ] );?>', postObject, {
                headers			:	{
                    '<?php echo $this->config->item('rest_key_name');?>'	:	'<?php echo @$Options[ 'rest_key' ];?>'
                }
            }).then(function( data ) {
                $scope.fetchOrders(0);
                $scope.unselectAllItems( order );
            }).catch( err => {
                swal({
                    title : `<?php echo __( 'An error has occured', 'gastro' );?>`,
                    html: `${err.data.message}<br><br>
                    <ul class="list-group">
                    ${(() => {
                        const response  =   [];
                        if ( err.data.data.length > 0 ) {
                            console.log( err.data );
                            err.data.data.forEach( error => {
                                error.data.result.forEach( _error => {
                                    console.log( _error );
                                    response.push( `<li class="list-group-item text-left">${_error.message}</li>` );
                                })
                            })
                        }
                        console.log( err.data );
                        return response;
                    })().join( '' )}
                    </ul>
                    `
                })
            });
        }

        $scope.getOrders            =   function( timeInterval = 0 ) {
            $timeout( function(){
                $scope.fetchOrders( () => {
                    $scope.getOrders();
                    $scope.Orderorder = 'asc';

                    if( $scope.sort == 'yes'){
                        $scope.sort = 'no';
                    } else {
                         $scope.sort = 'yes';
                    }
                });
            }, timeInterval == 0 ? $scope.timeInterval : timeInterval );
        }

        $scope.showDetails          =   function( order ) {
            console.log( $scope.structuredTables, order );
            swal({
                title: '<?php echo _s( 'Order Details', 'gastro' );?>',
                html: `
                <div class="table-responsive" id="order-details-wrapper" style="text-align:left">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <td><?php echo __( 'Order Type : {{ orderType }}', 'gastro' );?></td>
                                <td><?php echo __( 'Customer :  {{ order.CUSTOMER_NAME }}', 'gastro' );?></td>
                            </tr>
                            <tr>
                                <td><?php echo __( 'Table : ${$scope.structuredTables[ order.RESTAURANT_TABLE_ID ][ "NAME" ]}', 'gastro' );?></td>
                                <td><?php echo __( 'Status : {{ orderStatus }}', 'gastro' );?></td>
                            </tr>
                            <tr>
                                <td><?php echo __( 'Placed On : {{ placedOn }}', 'gastro' );?></td>
                                <td><?php echo __( 'Expected On : {{ expectedOn }}', 'gastro' );?></td>
                            </tr>
                        </thead>
                    </table>
                </div>
                `
            });

            const vue   =   new Vue({
                el: '#order-details-wrapper',
                data: { order },
                mounted() {
                },
                methods: {

                },
                computed: {
                    orderType() {
                        console.log( this.order.RESTAURANT_ORDER_TYPE );
                        switch( this.order.RESTAURANT_ORDER_TYPE ) {
                            case 'dinein': return '<?php echo __( 'Dine in', 'gastro' );?>'; break;
                            case 'takeaway': return '<?php echo __( 'Take Away', 'gastro' );?>'; break;
                            case 'delivery': return '<?php echo __( 'Delivery', 'gastro' );?>'; break;
                            default: return '<?php echo __( 'Unknown Type', 'gastro' );?>'; break;
                        }
                    },

                    orderStatus() {
                        switch( this.order.TYPE ) {
                            case 'nexo_order_comptant': '<?php echo __( 'Paid', 'gastro' );?>'; break;
                            case 'nexo_order_advance': '<?php echo __( 'Partially', 'gastro' );?>'; break;
                            case 'nexo_order_devis': '<?php echo __( 'Unpaid', 'gastro' );?>'; break;
                        }
                    },

                    expectedOn() {
                        return moment( this.order.BOOKED_FOR ).format( 'll LT' );
                    },

                    placedOn() {
                        return moment( this.order.DATE_MOD ).format( 'll LT' );
                    }
                }
            })
        }

        /**
         * Nexo Print Server
         * @return void
         */
        $scope.npsPrint         =   function( order, printtVersionName = 'nexo_store' ) {

            const printer       =   {
                nexo_store: '<?php echo store_option( 'printer_nexostore' );?>',
                default_printer: '<?php echo store_option( 'printer_takeway' );?>',
                kitchen_printer: '<?php echo empty( @$kitchen ) ? store_option( 'printer_takeway' ) : store_option( 'printer_kitchen_' . @$kitchen[0][ 'ID' ]);?>'
            }

            $.ajax( '<?php echo dashboard_url([ 'local-print'  ]);?>' + '/' + order.ORDER_ID + '?refresh=true', {
                success 	:	function( printResult ) {
                    $.ajax( '<?php echo store_option( 'nexo_print_server_url' );?>/api/print', {
                        type  	:	'POST',
                        data 	:	{
                            'content' 	:	printResult,
                            'printer'	:	printer[ printtVersionName ]
                        },
                        dataType 	:	'json',
                        success 	:	function( result ) {
                            console.log( result );
                        }, 
                        error( error ) {
                            NexoAPI.Toast()( error.responseJSON.message );
                        }
                    });
                }
            });
        }

        /**
         * NPS Kitchen
         * @return void
         */
        $scope.npsPrintKitchen  =   function( order, printVersionName = 'nexo_store' ) {
            const printer       =   {
                nexo_store: '<?php echo store_option( 'printer_nexostore' );?>',
                default_printer: '<?php echo store_option( 'printer_takeway' );?>',
                kitchen_printer: '<?php echo empty( @$kitchen ) ? store_option( 'printer_takeway' ) : store_option( 'printer_kitchen_' . @$kitchen[0][ 'ID' ]);?>'
            }

            <?php
            if ( store_option( 'gastro_nexostore_split_print', 'no' ) === 'no' ) {
                $printPath       =   'nps-single-print';
            } else {
                $printPath       =   'nps-splitted-print';
            }
            ?>

            $.ajax( '<?php echo site_url([ 'api', 'gastro', 'kitchens', $printPath ]);?>' + '/' + order.ID + '?refresh=true', {
                success 	:	function( printResult ) {
                    $.ajax( '<?php echo store_option( 'nexo_print_server_url' );?>/api/print', {
                        type  	:	'POST',
                        data 	:	{
                            'content' 	:	printResult,
                            'printer'	:	printer[ printVersionName ]
                        },
                        dataType 	:	'json',
                        success 	:	function( result ) {
                            console.log( result );
                        }, 
                        error( error ) {
                            NexoAPI.Toast()( error.responseJSON.message );
                        }
                    });
                }
            });
        }

        /**
         * Handle Booked Orders
         * @return json
         */
        $scope.hasConstructedBooking    =   false; // should be called once when the script run
        $scope.bookedOrders             =   [];
        $scope.pendingReview            =   [];
        $scope.handleBooked             =   function( booked ) {
            const orderCodes            =   $scope.bookedOrders.map( order => order.CODE );
            booked.forEach( order => {
                if ( orderCodes.indexOf( order.CODE ) === -1 ) {
                    // a new booking order has been placed
                    // do this only when the booking has yet been constructed
                    if ( $scope.hasConstructedBooking ) {
                        $scope.synthesizer( '<?php echo _s( 'A new online order has been placed.', 'gastro' );?>' );
                    }

                    /**
                     * Because, after the book has constructed, all order
                     * are added at the end of the array. But it should 
                     * be at the beginning
                     */
                    if ( $scope.hasConstructedBooking )  {
                        $scope.bookedOrders.push( order );
                    } else {
                        $scope.bookedOrders.unshift( order );
                    }
                }

                /**
                 * Let's check if the order is close to be cooked
                 */
                if( order.RESTAURANT_BOOKED_FOR !== '' && $scope.pendingReview.indexOf( order.CODE ) === -1 ) {
                    const orderMoment   =   moment( order.RESTAURANT_BOOKED_FOR );
                    const serverMoment  =   moment( tendoo.now() );
                    const timeToWait    =   <?php echo store_option( 'gastro_cook_time_for_booked', 0 );?>;

                    if( orderMoment.clone().subtract( timeToWait, 'minutes' ).isSameOrBefore( serverMoment ) ) {
                        $scope.pendingReview.push( order.CODE );
                        /**
                        * if the cook time has started or
                        * should have started
                        */
                        $scope.queueOrderToKitchen( order );
                    }
                } else {
                    $scope.queueOrderToKitchen( order );
                }
            });

            $scope.hasConstructedBooking    =   true;
        }

        /**
         * Queue Order
         * @return void
         */
        $scope.queueOrderToKitchen    =   function( order ) {
            $scope.sendBookToKitchen( order ).then( result => {
                /**
                * remove the order 
                * from the booked orders
                */
                let indexToRemove;
                $scope.bookedOrders.forEach( ( _order, index ) => {
                    if( _order.CODE === order.CODE ) {
                        indexToRemove   =   index;
                    }
                });

                $scope.bookedOrders.splice( indexToRemove, 1 );
                $scope.pendingReview    =   $scope.pendingReview.map( code => code !== order.CODE );
            });
        }

        /**
         * Send Booked Order to
         * the kitchen
         * @param object order
         * @return void
         */
        $scope.sendBookToKitchen    =   function( order ) {
            return HttpRequest.post( 'api/gastro/kitchens/queue-booked', { order });
        }

        /**
         * Calculate Time
         * @param order
         * @return string
         */
        $scope.calculateTime        =   function( order ) {
            if ( 
                order.RESTAURANT_BOOKED_FOR !== null && 
                order.RESTAURANT_BOOKED_FOR !== undefined &&
                order.RESTAURANT_BOOKED_FOR !== ''
            ) {
                const tendooSecs    =   tendoo.date.format( 'X' );
                const orderSecs     =   moment( order.RESTAURANT_BOOKED_FOR ).format( 'X' );
                
                return moment.duration( orderSecs - tendooSecs, 's' ).humanize();
            }
            return '--:--:--';
        }
        
        /**
         *  fetch orders
         *  @param
         *  @return
        **/

        $scope.rawOrdersCodes   =   [];
        $scope.ordersCodes      =   [];

        $scope.fetchOrders      =   function( callback = null ) {

            $http.get( '<?php echo site_url([ 'api', 'gastro', 'kitchens', 'orders' ]);?>?from-kitchen=true&takeaway_kitchen=<?php echo store_option( 'takeaway_kitchen' );?>&delivery_kitchen=<?php echo store_option( 'delivery_kitchen' );?>&current_kitchen=' + $scope.kitchen_id + '&<?php echo store_get_param( null );?>', {
    			headers			:	{
    				'<?php echo $this->config->item('rest_key_name');?>'	:	'<?php echo @$Options[ 'rest_key' ];?>'
    			}
    		})
            .then( function( returned ){
                // if nothing is returned, then crash here
                if( returned.data.length == 0 ) {
                    $scope.columns          =   [];
                    typeof callback == 'function' ? callback() : null;
                    return;
                }


               
                /**
                 * Let's remove booked order
                 * to add it in a separate array
                 */
                // const booked     =   returned.data.filter( order => order.RESTAURANT_ORDER_STATUS === 'booking' );
                // $scope.handleBooked( booked );
                // <?php// endif;?>
                
                /**
                 * Remove the booked from the regular orders
                 */
             //   returned.data           =   returned.data.filter( order => order.RESTAURANT_ORDER_STATUS !== 'booking' );

                /**
                 * make sure 
                 */
                returned.data.forEach( order => {
                    order.items         =   order.items.filter( item => item.RESTAURANT_FOOD_STATUS !== 'ready' );
                });

                // filter orders. only allow supported orders
                let filteredOrders              =   [];
                $scope.rawOrdersCodes           =   []; // reset RawCodes
                returned.data.forEach( ( order, index ) => {
                    if( ! $scope.hideOrder( order ) ) {
                        filteredOrders.unshift( order );
                        $scope.rawOrdersCodes.push( order.CODE );
                    }
                });

                if( $scope.orders.length == 0 ) {
                    $scope.orders     =     filteredOrders;
                    $scope.orders.forEach( ( order, order_index ) => {
                        if( typeof order.meals == 'undefined' ) {
                            order.meals     =   [];
                        }

                        order.items.forEach( item => {
                            // if meal feature is disabled, we'll group all mean on the same array
                            if( typeof order.meals[0] == 'undefined' ) {
                                order.meals[0]   =   [];
                            }

                            item.RESTAURANT_FOOD_MODIFIERS      =   item.RESTAURANT_FOOD_MODIFIERS == 'null' || item.RESTAURANT_FOOD_MODIFIERS.length === 0 ? {} : angular.fromJson( item.RESTAURANT_FOOD_MODIFIERS );   
                            order.meals[0].push( item );
                        });
                        // save order code
                        $scope.ordersCodes.push( order.CODE );               
                    });
                } else {
                    // first remove all orders which doesnt exists
                    let newFilteredOrders           =   [];
                    let newFilteredOrdersCodes      =   [];
                    
                    // $scope.ordersCodes              =   [];
                    $scope.orders.forEach( ( order ) => {
                        // if an existing order exists in the raw
                        if( _.indexOf( $scope.rawOrdersCodes, order.CODE ) != -1 ) {
                            // Update order real type
                            
                            // Let update current orders items status
                            filteredOrders.forEach( ( __updatedOrder ) => {
                                if( order.CODE == __updatedOrder.CODE ) {
                                    if( typeof __updatedOrder.meals == 'undefined' ) {
                                        __updatedOrder.meals     =   [];
                                    }

                                    // console.log( __updatedOrder.REAL_TYPE );

                                    order.RESTAURANT_ORDER_STATUS       =   __updatedOrder.RESTAURANT_ORDER_STATUS;
                                    order.DATE_MOD                      =   __updatedOrder.DATE_MOD;

                                    __updatedOrder.items.forEach( ( item, item_index ) => {
                                        // if meal feature is disabled, we'll group all mean on the same array
                                        if( typeof __updatedOrder.meals[0] == 'undefined' ) {
                                            __updatedOrder.meals[0]   =   [];
                                        }

                                        let itemsStatuses           =   [];

                                        order.meals[0].forEach( ( currentMeal, currentMealIndex ) => {
                                            if( item_index == currentMealIndex ) {
                                                itemsStatuses[ item_index ]     =   typeof currentMeal.active == 'undefined' ? false : currentMeal.active;
                                            }
                                        });

                                        item.active         =   itemsStatuses[ item_index ];      
                                        item.RESTAURANT_FOOD_MODIFIERS      =   item.RESTAURANT_FOOD_MODIFIERS == 'null' || item.RESTAURANT_FOOD_MODIFIERS.length === 0 ? {} : angular.fromJson( item.RESTAURANT_FOOD_MODIFIERS );   
                                        __updatedOrder.meals[0].push( item );
                                    });
                                    
                                    order.meals     =   __updatedOrder.meals;
                                }
                            });   
                         
                            // we can make an animation here
                            newFilteredOrders.push( order );
                            newFilteredOrdersCodes.push( order.CODE );
                            // $scope.ordersCodes.push( order.CODE );
                        }
                    });

                    // Filtered orders and codes
                    $scope.orders               =   newFilteredOrders;
                    $scope.ordersCodes          =   newFilteredOrdersCodes;
                    
                    // just build new
                    let newOrders               =   [];
                    let newOrdersCodes          =   [];
                    
                    filteredOrders.forEach( ( order ) => {
                        if( _.indexOf( $scope.ordersCodes, order.CODE ) == -1 ) {
                         // the order we're building should not already exists ont he ordersCodes;
                            if( typeof order.meals == 'undefined' ) {
                                order.meals     =   [];
                            }

                            order.items.forEach( ( item ) => {
                                // if meal feature is disabled, we'll group all mean on the same array
                                if( typeof order.meals[0] == 'undefined' ) {
                                    order.meals[0]   =   [];
                                }

                                item.RESTAURANT_FOOD_MODIFIERS      =   item.RESTAURANT_FOOD_MODIFIERS == 'null' || item.RESTAURANT_FOOD_MODIFIERS.length === 0 ? {} : angular.fromJson( item.RESTAURANT_FOOD_MODIFIERS );     
                                order.meals[0].push( item );
                            });  
                            // save order code
                            $scope.ordersCodes.push( order.CODE );                   
                        
                            newOrders.push( order );
                            newOrdersCodes.push( order.CODE );
                        }
                    });
                    
                    // Only announce 1 order
                    if( newOrders.length > 0 ) {
                        if( newOrders.length == 1 ) {
                            const length    =   returned.data.length;
                            if( returned.data[ length - 1 ].RESTAURANT_ORDER_TYPE === 'takeaway' ) {
                                $scope.synthesizer( '<?php echo _s( 'A new take away order has been placed.', 'gastro' );?>' );
                            } else if( returned.data[ length - 1 ].RESTAURANT_ORDER_TYPE === 'delivery' ) {
                                $scope.synthesizer( '<?php echo _s( 'A new delivery order has been placed.', 'gastro' );?>' );
                            } else if( returned.data[ length - 1 ].RESTAURANT_ORDER_TYPE === 'dinein' ) {
                                $scope.synthesizer( '<?php echo _s( 'A new dine in order has been placed, at the table %s.', 'gastro' );?>'.replace( '%s', $scope.structuredTables[ returned.data[ length - 1 ].RESTAURANT_TABLE_ID ].NAME ) );
                            } else if( returned.data[ length - 1 ].RESTAURANT_ORDER_TYPE === 'booking' ) {
                                $scope.synthesizer( '<?php echo _s( 'A new booking order has been placed, at the table %s.', 'gastro' );?>'.replace( '%s', $scope.structuredTables[ returned.data[ length - 1 ].RESTAURANT_TABLE_ID ].NAME ) );
                            }   

                            NexoAPI.events.doAction( 'gastro_kitchen_new_order', newOrders[0] );                     
                        } 

                        newOrders.forEach( ( order ) => {
                            $scope.orders.unshift( order );
                        });

                        $scope.ordersCodes.concat( newOrdersCodes );
                    }               
                }

                /**
                * Let's make sure only orders with pending & ongoing items are displayed
                */
                $scope.orders.map( ( order, order_index ) => {
                    let readyMeals     =   order.meals[0].filter( ( item, item_index ) => {


                        return item.FOOD_STATUS == 'ready'
                    });

                    if ( readyMeals.length == order.meals[0].length ) {
                        let orderCodeIndex  =   $scope.ordersCodes.indexOf( order.CODE );
                        $scope.ordersCodes.splice( orderCodeIndex, 1 );
                        return order_index;
                    }  

                }).forEach( order_index => {
                    if ( order_index != undefined ) {
                        $scope.orders.splice( order_index, 1 );
                    }
                })

                // Order everything so that it can be shown as masonry
                var availableColumns        =   4;
                var currentIndex            =   0;
                var columns                 =   [];
                
                $scope.noValidOrder         =   false; 
                $scope.orders.forEach( ( order ) => {
                    if( order.STATUS != 'ready' ) {
                        if( typeof columns[ currentIndex ] == 'undefined' ) {
                            columns[ currentIndex ]  =  [];
                        }

                        // we'll skip order ready
                        
                        columns[ currentIndex ].push( order );

                        currentIndex++;
                        
                        if( currentIndex == availableColumns ) {
                            currentIndex    =   0;
                        }  
                    }
                });


               

                var order_temp =[];

                 order_temp = columns;
                if($scope.sort=='yes'){
                     order_temp =  $scope.shuffleArray(columns);

                }

               // console.log(order_temp);

                $scope.columns      =   order_temp; 
            
                order_temp =  $scope.shuffleArray(columns);

                $scope.columns      =   order_temp; 
                typeof callback == 'function' ? callback() : null;

            },function(){
                typeof callback == 'function' ? callback() : null;
            });
        }

        /**
         *  get existing order
         *  @param string order code
         *  @return object
        **/

        $scope.getExistingOrder         =   ( order_code ) => {
            for( let order of $scope.orders ) {
                if( order.CODE == order_code ) {
                    return order;
                }
            }
            return {};
        }

        $scope.shuffleArray = (array)=> {
                          var m = array.length, t, i;

                          // While there remain elements to shuffle
                          while (m) {
                            // Pick a remaining element…
                            i = Math.floor(Math.random() * m--);

                            // And swap it with the current element.
                            t = array[m];
                            array[m] = array[i];
                            array[i] = t;
                          }

                          return array;
        }
        /**
         *  Restrict display for each buttons
         *  @param string item status
         *  @return bool
        **/

        $scope.ifAllSelectedItemsAre        =   ( status, order ) => {
            let isNotActive   =   [], isActiveAndValid     =   [], isActiveAndNotValid     =   [];
            let totalFoodNbr    =   0;

            order.meals.forEach( ( meal ) => {
                meal.forEach( ( food ) => {
                    if( food.active ) {
                        if( food.RESTAURANT_FOOD_STATUS != status ) {
                            isActiveAndNotValid.push( false );
                        } else {
                            isActiveAndValid.push( true );
                        }
                    } else {
                        isNotActive.push( false );
                    }
                    totalFoodNbr++;
                });
            });

            // as much unchecked that available item
            return ( isActiveAndNotValid.length > 0 ) ? false : ( isActiveAndValid.length > 0 ) ? true : false;          
        }

        /**
         *  Get Existing item
         *  @param object order
         *  @param string barcode
         *  @return object
        **/

        $scope.getExistingItem          =   ( order_index, meal_index, item_barcode ) => {
            if( typeof $scope.orders[ order_index ] != 'undefined') {
                if( typeof $scope.orders[ order_index ].meals[ meal_index ] != 'undefined' ) {
                    for( let item of $scope.orders[ order_index ].meals[ meal_index ] ) {
                        if( item.CODEBAR == item_barcode ) {
                            return item;
                        }
                    }
                }   
            }
            return {}
        }

        /** 
         * Parse JSon
         * @param json
         * @return object
        **/

        $scope.parseJSON        =   function( json ) {
            return angular.fromJson( json )
        }

        /**
         *  Get Order Status
         *  @param object order
         *  @return void
        **/

        $scope.getOrderStatus   =   function( order ) {
            return $scope.order_types[ order.RESTAURANT_ORDER_STATUS ] === undefined ? '<?php echo _s( 'Unknow Order', 'gastro' );?>' : $scope.order_types[ order.RESTAURANT_ORDER_STATUS ];
        }

        $scope.getFoodStatus    =   function( order,status ) {

            // console.log(order.CODE);

            switch( status ) {



                case 'not_ready':  $('#'+order.CODE).addClass($scope.freshOrderColor); return '<?php echo __( 'Preparation', 'gastro' );?>'; break;
                case 'in_preparation':  $('#'+order.CODE).addClass($scope.lateOrderColor); return '<?php echo __( 'Preparation', 'gastro' );?>';  break;
                case 'ready': $('#'+order.CODE).addClass($scope.freshOrderColor);  return '<?php echo __( 'Ready', 'gastro' );?>'; break;

                case 'canceled': $('#'+order.CODE).addClass($scope.tooLateOrderColor); break;

                case 'collected': return '<?php echo __( 'Collected', 'gastro' );?>'; break;
                case 'served': return '<?php echo __( 'Served', 'gastro' );?>'; break;
                default: return status; break;
            }
        }

        /**
         *  Select Items
         *  @param object item
         *  @return void
        **/

        $scope.selectItem       =   function( food ){
            if( typeof food.active === 'undefined' ) {
                food.active        =   true;
            } else {
                food.active        =   !food.active;
            }
        }

        /**
         *  Select ALl Items
         *  @param object
         *  @return void
        **/

        $scope.selectAllItems       =   function( order ) {
            order.meals.forEach( ( meal ) => {
                meal.forEach( ( item ) => {
                    item.active     =   true;
                    NexoAPI.events.doAction( 'gastro_kitchen_select_item', item );
                })
            })
        }

        /**
         *  Unselect All items
         *  @param object order
         *  @return void
        **/

        $scope.unselectAllItems     =   function( order_index ){
            if( typeof order_index != 'object' ) {
                for( let meal_index in $scope.orders[ order_index ].meals ) {
                    for( let item of $scope.orders[ order_index ].meals[ meal_index ] ) {
                        item.active     =   false;
                    }
                }
            } else {
                for( let meal_index in order_index.meals ) {
                    for( let item of order_index.meals[ meal_index ] ) {
                        item.active     =   false;
                    }
                };
            }

            NexoAPI.events.doAction( 'gastro_kitchen_unselect_item' );
        }

        /**
         *  Cook
         *  @param  order
         *  @return void
        **/

        $scope.cook                 =   ( order )   =>  {
            var postObject          =   {
                '<?php echo $this->security->get_csrf_token_name();?>'    :   '<?php echo $this->security->get_csrf_hash();
                ?>',
                during_cooking          :   [],
                not_cooked              :   [],
                complete_cooking        :   true,
                order_id                :   order.ORDER_ID,
                order_code              :   order.CODE,
                order_real_type         :   order.REAL_TYPE
            };

            for( let item of order.items ) {
                if( ! item.active ) {
                    postObject.complete_cooking     =   false;
                    postObject.not_cooked.push( item.COMMAND_PRODUCT_ID );
                } else {
                    postObject.during_cooking.push( item.COMMAND_PRODUCT_ID );
                }
            }

            $http.post('<?php echo site_url([ 'api', 'gastro', 'kitchens', 'cook', store_get_param('?') ] );?>', postObject, {
    			headers			:	{
    				'<?php echo $this->config->item('rest_key_name');?>'	:	'<?php echo @$Options[ 'rest_key' ];?>'
    			}
    		}).then(function( data ) {
                $scope.fetchOrders();
                $scope.unselectAllItems( order );
            }, ( error ) => {
                console.log( error );
            })
        }

        /**
         *  Speech Synthesizer
         * @param void
         * @return void
        **/

        $scope.synthesizer          =   function( word ) {
            <?php if( store_option( 'enable_kitchen_synthesizer' ) == 'yes' ):?>
            var msg = new SpeechSynthesisUtterance();
            var voices = window.speechSynthesis.getVoices();
            msg.voice = voices[1]; // Note: some voices don't support altering params
            msg.voiceURI = 'native';
            msg.volume = 1; // 0 to 1
            msg.rate = 1; // 0.1 to 10
            msg.pitch = 2; //0 to 2
            msg.text = word;
            msg.lang = $scope.siteLanguage.replace( '_', '-' );

            msg.onend = function(e) {
                // console.log('Finished in ' + event.elapsedTime + ' seconds.');
            };

            speechSynthesis.speak(msg);
            <?php endif;?>
        }

        /**
         *  Toggle FullScreen
         *  @param void
         *  @return void
        **/


        $scope.toggleFullScreen     =   ()  =>  {
            if (!document.fullscreenElement &&    // alternative standard method
              !document.mozFullScreenElement && !document.webkitFullscreenElement && !document.msFullscreenElement ) {  // current working methods
            if (document.documentElement.requestFullscreen) {
              document.documentElement.requestFullscreen();
            } else if (document.documentElement.msRequestFullscreen) {
              document.documentElement.msRequestFullscreen();
            } else if (document.documentElement.mozRequestFullScreen) {
              document.documentElement.mozRequestFullScreen();
            } else if (document.documentElement.webkitRequestFullscreen) {
              document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
            }
          } else {
            if (document.exitFullscreen) {
              document.exitFullscreen();
            } else if (document.msExitFullscreen) {
              document.msExitFullscreen();
            } else if (document.mozCancelFullScreen) {
              document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
              document.webkitExitFullscreen();
            }
          }
        }

        $scope.fetchOrders( () => {
            $scope.getOrders();
        });


        /**
         *  Hide order
         * @param object
         * @return boolean
        **/

        $scope.hideOrder        =   function( order ){
            let __return          =   false;
            
            if( [ 'ready', 'served' ].includes( order.STATUS ) || order.items.length === 0 ) {
                __return        =   true;
            }

            return __return;
        }

        // $( '.content-header h1' ).append( $( '.kitchen-buttons' )[0].innerHTML );
        // angular.element( '.kitchen-buttons' ).html( $compile( $( '.kitchen-buttons' ).html() )($scope) );
    }

    tendooApp.controller( 'watchRestaurantCTRL', [ '$scope', '$http', '$timeout', '$compile', watchRestaurantCTRL ]);
</script>