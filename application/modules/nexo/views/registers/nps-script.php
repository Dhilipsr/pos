
<?php  
$data = $this->events->apply_filters('font_size', $this->config->item('default_user_names'))['button_size'];

if($data=='small'){


    ?>

    <style type="text/css">
        
        .restdrop{
    display: flex !important;
    flex-direction: column !important;
    height: 47px !important;

 }

    </style>

<?php 


} elseif ($data=='medium') {
    # code...

?>
    
    <style type="text/css">


   .restdrop{
    display: flex !important;
    flex-direction: column !important;
    height: 61px !important;

 }


     .btn {

           height: 35px !important;
           width: 119px !important;
       
    }

    </style>

    <?php
} else {  ?>


    <style type="text/css">


      .restdrop {  
        display: flex !important;
        flex-direction: column !important;
        height: 85px !important;
        /* align-items: self-end; */
        justify-content: space-between !important;
        margin-bottom: 23px !important;
    }

    #search-item-form button{
        width: 66px !important;
    }
    #search-item-form input{

        height:55px !important;
    }

     .btn {

           height: 40px !important;
           max-width: 135px !important;
       
    }
    .btn dropdown-toggle btn-default{


         height: unset !important;
         width: unset !important;
    }

    </style>


<?php
}

?>

<style type="text/css">
    
   

   
</style>
<script>
function loadPrinters( select ) {
    if ( select.val() ) {
        
    
    <?php $URL = site_url([ 'api', 'gastro', 'kitchens', 'getprinters' ]);?>
    
        $.ajax( {
            
            url:'<?php echo $URL;?>',
            headers: {"X-API-KEY": "9zOW3T3ZmBCYQWVDbrVWcFa4mApMrOZGa91mzUxr"},
         	method:'GET',
            success    :   function( result ) {
                $( '[name="ASSIGNED_PRINTER"]' ).html( '<option><?php echo __( 'Choisir une option', 'nexo' );?></option>' );
                result.forEach( printer => {
                    let selected    =  ( printer.name ==  '<?php echo @$register[ 'ASSIGNED_PRINTER' ];?>' ) ? 'selected="selected"' : null;
                    $( '[name="ASSIGNED_PRINTER"]' ).append( `<option ${selected} value="${printer.name}">${printer.name}</option>` );
                });
                NexoAPI.Toast()( `<?php echo __( 'La connection avec le serveur établie', 'nexo' );?>` );
                $( '[name="ASSIGNED_PRINTER"]' ).trigger( "chosen:updated" );
            },
            error   :   function() {
                NexoAPI.Notify().warning(
                    `<?php echo __( 'Une erreur est survenu', 'nexo' );?>`,
                    `<?php echo __( 'Impossible d\'accéder au serveur de l\'imprimante. Assurez-vous que l\'URL mentionnée est correcte. En cas de soucis, contactez l\'assistance.', 'nexo' );?>`,
                );
            }
        })
    }
}
$( document ).ready( function() {
    $( '[name="NPS_URL"]' ).blur( function() {
        loadPrinters( $( this ) );
    });

    loadPrinters( $( '[name="NPS_URL"]' ) );
});
</script>

<style type="text/css">
    
   .modal-lg_new{

        width: 218px !important;
        height: 56px !important;
     }

     
</style>