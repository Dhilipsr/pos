

<?php  
$data = $this->events->apply_filters('font_size', $this->config->item('default_user_names'))['button_size'];

if($data=='small'){


    ?>
<style type="text/css">

    .btn { 
            /*padding: 1px 5px;*/
    padding: 17px 10px;

    font-size: 14px;
    line-height: 1.5;
    border-radius: 3px;

     }


     #form-button-save { 
         padding: 1px 5px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 3px;

     }

          input-group .form-control:last-child, .input-group-addon:last-child, .input-group-btn:first-child>.btn-group:not(:first-child)>.btn, .input-group-btn:first-child>.btn:not(:first-child), .input-group-btn:last-child>.btn, .input-group-btn:last-child>.btn-group>.btn, .input-group-btn:last-child>.dropdown-toggle {
    height: 58px;
    }
    .input-group-btn:first-child>.btn, .input-group-btn:first-child>.btn-group {
    height: 58px;
    width: 90px;
    }

    .input-group .form-control:last-child, .input-group-addon:last-child, .input-group-btn:first-child>.btn-group:not(:first-child)>.btn, .input-group-btn:first-child>.btn:not(:first-child), .input-group-btn:last-child>.btn, .input-group-btn:last-child>.btn-group>.btn, .input-group-btn:last-child>.dropdown-toggle {
    height: 57px;
}
</style>


<?php 


} elseif ($data=='medium') {
    # code...

?>
    
    <style type="text/css">

     .btn {
      /*  padding: 5px 7px;*/
          padding: 17px 10px;
    font-size: 14px;
    line-height: 1.5;
    border-radius: 3px;
    }
     #form-button-save {
        padding: 5px 10px;
    font-size: 12px;
    line-height: 1.5;
    border-radius: 3px;
    }
            .input-group-btn:first-child>.btn, .input-group-btn:first-child>.btn-group {
    width: 95px;
    height: 58px;
    }
    .input-group .form-control:last-child, .input-group-addon:last-child, .input-group-btn:first-child>.btn-group:not(:first-child)>.btn, .input-group-btn:first-child>.btn:not(:first-child), .input-group-btn:last-child>.btn, .input-group-btn:last-child>.btn-group>.btn, .input-group-btn:last-child>.dropdown-toggle {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
    height: 58px;
    }
    .input-group-btn:first-child>.btn, .input-group-btn:first-child>.btn-group {
    margin-right: -1px;
    width: 95px;
  }
  .btn-group>.btn:first-child {
  
    height: 58px;
}

    </style>

    <?php
} else {  ?>


    <style type="text/css">

     .btn {

        padding: 17px 10px;
        font-size: 14px;
        line-height: 1.3333333;
        border-radius: 6px;
    }

    
    #form-button-save {

        padding: 10px 16px;
        font-size: 18px;
        line-height: 1.3333333;
        border-radius: 6px;
    }

    input{
        height: 54px;
    }
        .input-group-btn:first-child>.btn, .input-group-btn:first-child>.btn-group {
    width: 95px;
    }
    .input-group .form-control:last-child, .input-group-addon:last-child, .input-group-btn:first-child>.btn-group:not(:first-child)>.btn, .input-group-btn:first-child>.btn:not(:first-child), .input-group-btn:last-child>.btn, .input-group-btn:last-child>.btn-group>.btn, .input-group-btn:last-child>.dropdown-toggle {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
    height: 54px;
    }
    .input-group-btn:first-child>.btn, .input-group-btn:first-child>.btn-group {
    margin-right: -1px;
    width: 95px;
  }

    </style>


<?php
}

?>
<?php $fontsize = $this->events->apply_filters('font_size', $this->config->item('default_user_names'))['font_size'];

if($fontsize=='large'){
?>

<style type="text/css">
    .logocenter.profiletopright.col-lg-6 {
        position: absolute;
        margin-left: 88% !important;
        font-family: -webkit-body;
    }
</style>

<?php } elseif ($fontsize=='medium') { ?>
   


<?php } else{ ?>

<?php } ?>

<style type="text/css">
    
    #totalbody{
        font-size: <?php echo $this->events->apply_filters('font_size', $this->config->item('default_user_names'))['font_size'];
            ?>;

    }

   
</style>
<div class="nexo-overlay" style="width: 100%; height: 100%; background: rgba(255, 255, 255, 1); z-index: 5000; position: fixed; top: 0px; left: 0px;"><i class="fa fa-refresh fa-spin nexo-refresh-icon" style="color: rgb(0, 0, 0); font-size: 50px; position: absolute; top: 50%; left: 50%; margin-top: -25px; margin-left: -25px; width: 44px; height: 50px;"></i></div>