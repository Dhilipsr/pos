
<style type="text/css">
    
    #registerprint {
        /*display: block;*/
        width: 115px;
        height: 25px;
        background: #4E9CAF;
        padding: 10px;
        text-align: center;
        border-radius: 5px;
        color: white;
        font-weight: bold;
        line-height: 25px;
     }

</style>
<script>
    const RegistersData     =   {
        textDomain: {
            close: `<?php echo __( 'Fermer', 'nexo' );?>`,
            operation: `<?php echo __( 'Opération', 'nexo' );?>`,
            amount: `<?php echo __( 'Montant', 'nexo' );?>`,
            reason: `<?php echo __( 'Raison', 'nexo' );?>`,
            date: `<?php echo __( 'Date', 'nexo' );?>`,
            author: `<?php echo __( 'Auteur', 'nexo' );?>`,
            opening: '<?php echo _s( 'Ouvrir', 'nexo' );?>', 
            disbursement: '<?php echo _s( 'Décaissement', 'nexo' );?>', 
            cashing: '<?php echo _s( 'Encaissement', 'nexo' );?>', 
            closing: '<?php echo _s( 'Fermer', 'nexo' );?>', 
            idle_starts: '<?php echo _s( 'Début d\'inactivité ', 'nexo' );?>', 
            idle_ends: '<?php echo _s( 'Fin d\'inactivité ', 'nexo' );?>', 
            unknownOperationType: '<?php echo _s( 'Opération Inconnue', 'nexo' );?>', 
            unexpectedErrorOccured: '<?php echo _s( "Une erreur inattendue s'est produite", 'nexo' );?>', 
            nextPage: '<?php echo _s( 'Page Suivante', 'nexo' );?>', 
            prevPage: '<?php echo _s( 'Page Précédente', 'nexo' );?>', 
            confirmYourOperation: '<?php echo _s( 'Souhaitez-vous continuer ?', 'nexo' );?>', 
            registerHistoryWillBeErased: '<?php echo _s( 'L\'historique de la caisse enregistreuse sera supprimé ?', 'nexo' );?>', 
            noEntry: '<?php echo _s( 'Aucune entrée à afficher...', 'nexo' );?>', 
        },
        element: '#register-vue-wrapper',
        url: {
            deleteHistory: `<?php echo site_url([ 'api', 'nexopos', 'registers', 'clearHistory', '{id}' . store_get_param('?')]);?>`
        }
    }
</script>
<script src="<?php echo module_url( 'nexo' ) . '/js/registers-list.vue.js';?>"></script>
<script src="<?php echo module_url( 'nexo' ) . '/js/clear-register-history.vue.js';?>"></script>

<!-- dev URL -->
<script src="https://rawgit.com/unconditional/jquery-table2excel/master/src/jquery.table2excel.js"></script>
<script type="text/javascript">
    "use strict";

    $( document ).ajaxComplete(function(e) {
        $( '.open_register' ).not( '.bound' ).bind( 'click', function(){
            const $this     =   $( this );
            HttpRequest.get( '<?php echo site_url( array( 'api', 'nexopos', 'registers', store_get_param('?') ) );?>' ).then( result => {
                const registers     =   result.data;
                const openByUser    =   registers.filter( register => register.USED_BY === '<?php echo User::id();?>' && $( this ).data( 'item-id' ) !== parseInt( register.ID ) && register.STATUS === 'opened' );
                const targeted      =   registers.filter( register => $( this ).data( 'item-id' ) === parseInt( register.ID ) )[0];

                if ( openByUser.length > 0 ) {
                    return NexoAPI.Notify().warning(
                        '<?php echo _s( 'Attention', 'nexo' );?>',
                        `<?php echo __( 'Vous ne pouvez plus ouvrir de nouvelle caisse enregistreuse, car vous avez déjà ouvert une caisse. 
                        Fermez cette caisse et essayez à nouveau', 'nexo' );?>`
                    );
                }

                if( targeted.STATUS == 'opened' ) {
                    if( targeted.USED_BY != '<?php echo User::id();?>' && <?php echo ( User::in_group([ 'master', 'store.manager' ]) ) ? 'false == true' : 'true == true';?> ) {
                        // Display confirm box to logout current user and login
                        bootbox.alert( '<?php echo _s( 'Impossible d\'accéder à une caisse en cours d\'utilisation. Si le problème persiste, contactez l\'administrateur.', 'nexo' );?>' );
                    } else {
                        bootbox.alert( '<?php echo _s( 'Vous allez être redirigé vers la caisse...', 'nexo' );?>' );
                        // Document Location
                    }
                } else if( targeted.STATUS == 'locked' ) {
                    bootbox.alert( '<?php echo _s( 'Impossible d\'accéder à une caisse verrouillée. Si le problème persiste, contactez l\'administrateur.', 'nexo' );?>' );

                } else if( targeted.STATUS == 'closed' ) {
                    var dom		=	'<h3 class="modal-title"><?php echo _s( 'Ouverture de la caisse', 'nexo' );?></h3><hr style="margin:10px 0px;">';

                        dom		+=	'<p><?php echo tendoo_info( sprintf( _s( '%s, vous vous préparez à ouvrir une caisse. Veuillez spécifier le montant initiale de la caisse', 'nexo' ), User::pseudo() ) );?></p>' +
                                    `
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1"><?php echo _s( 'Solde d\'ouverture de la caisse', 'nexo' );?></span>
                                            <input type="text" class="form-control open_balance" placeholder="<?php echo _s( 'Montant', 'nexo' );?>" aria-describedby="basic-addon1">
                                        </div>  
                                    </div>                                
                                    <div class="form-group">
                                        <label for="textarea"><?php echo __( 'Remarques', 'nexo' );?></label>
                                        <textarea name="" id="textarea" class="form-control note" rows="3" required="required"></textarea>
                                    </div>
                                    `;

                    bootbox.confirm( dom, function( action ) {
                        if( action ) {
                            $.ajax( '<?php echo site_url( array( 'rest', 'nexo', 'open_register' ) );?>/' + $this.data( 'item-id' ) + '?<?php echo store_get_param( null );?>', {
                                dataType	:	'json',
                                type		:	'POST',
                                data		:	_.object( [ 'date', 'balance', 'used_by', 'note' ], [ '<?php echo date_now();?>', $( '.open_balance' ).val(), '<?php echo User::id();?>', $( '.note' ).val() ]),
                                success: function( data ){
                                    bootbox.alert( '<?php echo _s( 'La caisse a été ouverte. Veuillez patientez...', 'nexo' );?>' );
                                    document.location	=	'<?php echo dashboard_url([ 'use', 'register' ]);?>/' + $this.data( 'item-id');
                                }
                            });
                        }
                    });

                    // Set custom width
                    $( '.modal-title' ).closest( '.modal-dialog' ).css({
                        'width'		:	'50%'
                    })
                }
            }).catch( ( err ) => {
                bootbox.alert( '<?php echo _s( 'Une erreur s\'est produite durant l\'ouverture de la caisse.', 'nexo' );?>' );
            })

            return false;
        });
        $( '.open_register' ).addClass( 'bound' );

        $( '.close_register' ).not( '.bound' ).bind( 'click', function(){
            var $this	=	$( this );
            $.ajax( '<?php echo site_url( array( 'rest', 'nexo', 'register_status' ) );?>/' + $( this ).data( 'item-id' ) + '?<?php echo store_get_param( null );?>', {
                success		:	function( data ){
                    // Somebody is logged in
                    if( data[0].STATUS == 'opened' ) {

                        if( data[0].USED_BY != '<?php echo User::id();?>' && <?php echo ( User::in_group([ 'master', 'store.manager' ]) ) ? 'false == true' : 'true == true';?> ) {
                            bootbox.alert( '<?php echo _s( 'Vous ne pouvez pas fermer cette caisse. Si le problème persiste, contactez l\'administrateur.', 'nexo' );?>' );
                            return;
                        }

                        var dom		=	'<h3 class="modal-title"><?php echo _s( 'Fermeture de la caisse', 'nexo' );?></h3><hr style="margin:10px 0px;">';

                            dom		+=	'<p><?php echo tendoo_info( sprintf( _s( '%s, vous vous préparez à fermer une caisse. Veuillez spécifier le montant finale de la caisse', 'nexo' ), User::pseudo() ) );?></p>' +
                                        `<div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><?php echo _s( 'Solde de fermeture de la caisse', 'nexo' );?></span>
                                                <input type="text" class="form-control open_balance" placeholder="<?php echo _s( 'Montant', 'nexo' );?>" aria-describedby="basic-addon1">
                                            </div>  
                                        </div> 
                                        <div class="form-group">
                                            <label for="textarea"><?php echo __( 'Remarques', 'nexo' );?></label>
                                            <textarea name="" id="textarea" class="form-control note" rows="3" required="required"></textarea>
                                        </div>
                                        `

                        bootbox.confirm( dom, function( action ) {
                            if( action == true ) {
                                $.ajax( '<?php echo site_url( array( 'rest', 'nexo', 'close_register' ) );?>/' + $this.data( 'item-id' ) + '?<?php echo store_get_param( null );?>', {
                                    dataType	:	'json',
                                    type		:	'POST',
                                    data		:	_.object( [ 'date', 'balance', 'used_by', 'note' ], [ '<?php echo date_now();?>', $( '.open_balance' ).val(), '<?php echo User::id();?>', $( '.note' ).val() ]),
                                    success: function( data ){
                                        bootbox.alert( '<?php echo _s( 'La caisse a été fermée. Veuillez patientez...', 'nexo' );?>' );

                                      
                                            var xmlhttp = new XMLHttpRequest();
                                            var originalContents = document.body.innerHTML;
                                            xmlhttp.open('GET',"<?php echo site_url(array( 'dashboard', store_slug(), 'nexo', 'registerhistory' ));?>/" +$this.data( 'item-id' ), false);
                                            xmlhttp.send(null);
                                            if(xmlhttp.status == 200) {
                                                document.body.innerHTML = xmlhttp.responseText;
                                                window.print();
                                                document.body.innerHTML = originalContents;;
                                            }



                        

                                        // window.frames["CurrentReceipt"].focus();
                                        // window.frames["CurrentReceipt"].print();

                                        document.location	=	'<?php echo current_url();?>';
                                        //alert('hi');
                                    }
                                });
                            }
                        });

                        // Set custom width
                        $('.modal-footer').closest('.btn-default').css({
                            'width'		:	'100px'
                        })

                         $( '.modal-title' ).closest( '.modal-dialog' ).css({
                            'width'     :   '50%'
                        })

                    } else if( data[0].STATUS == 'locked' ) {

                        bootbox.alert( '<?php echo _s( 'Impossible de fermer une caisse verrouillée. Si le problème persiste, contactez l\'administrateur.', 'nexo' );?>' );

                    } else if( data[0].STATUS == 'closed' ) {

                        bootbox.alert( '<?php echo _s( 'Cette caisse est déjà fermée.', 'nexo' );?>' );

                    }

                },
                dataType	:	"json",
                error		:	function(){
                    bootbox.alert( '<?php echo _s( 'Une erreur s\'est produite durant l\'ouverture de la caisse.', 'nexo' );?>' );
                }
            })

            return false;
        });
        $( '.close_register' ).addClass( 'bound' );

        $( '.register_history' ).not( '.bound' ).bind( 'click', function(){
            const textDomain    =   RegistersData.textDomain;
            RegistersData.url   =   {
                get: '<?php echo site_url([ 'api', 'nexopos', 'registers', 'history' ]);?>/' + $( this ).data( 'item-id' ) + '?page={page}<?php echo store_get_param( '&' );?>',
                ...RegistersData.url
            }

            bootbox.alert( `
                <table class="table" id="registerhist">
            
                    <thead>
                        <tr>
                            <th>${textDomain.operation}</th>
                            <th>${textDomain.amount}</th>
                            <th>${textDomain.reason}</th>
                            <th>${textDomain.date}</th>
                            <th>${textDomain.author}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="entry of crudResult.entries">
                            <td>{{ entry.TYPE | registerOperation }}</td>
                            <td>{{ entry.BALANCE | moneyFormat }}</td>
                            <td>{{ entry.NOTE }}</td>
                            <td>{{ entry.DATE_CREATION }}</td>
                            <td>{{ entry.AUTHOR_NAME }}</td>
                        </tr>
                        <tr v-if="crudResult.entries.length === 0">
                            <td colspan="5">${textDomain.noEntry}</td>
                        </tr>
                    </tbody>
                </table>
            ` );

            $( '.modal-dialog' ).css({
                'height': '100%',
                'width': '100%',
                'display': 'flex',
                'flex-direction': 'column',
                'margin': '0',
                'justify-content': 'center',
                'align-items': 'center'
            });

            $( '.modal-content' ).css({
                width: '90%',
                height: '90%',
                'display': 'flex',
                'flex-direction': 'column',
            });

            $( '.modal-body' ).css( 'flex', 1 );
            $( '.modal-body' ).css( 'overflow-y', 'auto' );
            $( '.modal-footer' ).html(
                `<div style="display: flex; justify-content: space-between">
                    <div>
                        <nav aria-label="Page navigation example" v-if="crudResult">
                            <ul class="pagination" style="margin:0">
                                <li :class="crudResult.prev_page === -1 ? 'disabled' : ''"@click="loadRegisterHistory( crudResult.prev_page )" class="page-item"><a class="page-link" href="#">${textDomain.prevPage}</a></li>
                                <li :class="crudResult.next_page === -1 ? 'disabled' : ''"@click="loadRegisterHistory( crudResult.next_page )" class="page-item"><a class="page-link" href="#">${textDomain.nextPage}</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div>
                        <a class="btn btn-default" @click="loadRegisterHistory()"><i class="fa fa-refresh"></i></a>
                        <a class="btn btn-primary" @click="closePopup()">${textDomain.close}</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12">
   
                        
                            
                        
                        
                        ` 
            )

            $( '.modal-dialog' ).attr( 'id', RegistersData.element.substr(1) );

            const VueApp    =   new RegistersListVueApp( RegistersData );

            return false;
        });
        $( '.register_history' ).addClass( 'bound' );
    });
    
    function test() {
       // var htmls = "";
       //      var uri = 'data:application/vnd.ms-excel;base64,';
       //      var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'; 
       //      var base64 = function(s) {
       //          return window.btoa(unescape(encodeURIComponent(s)))
       //      };

       //      var format = function(s, c) {
       //          return s.replace(/{(\w+)}/g, function(m, p) {
       //              return c[p];
       //          })
       //      };

       //      htmls = "YOUR HTML AS TABLE"

       //      var ctx = {
       //          worksheet : 'Worksheet',
       //          table : htmls
       //      }


       //      var link = document.createElement("a");
       //      link.download = "export.xls";
       //      link.href = uri + base64(format(template, ctx));
       //      link.click();



    // var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
    // var textRange; var j=0;
    // tab = document.getElementById('registerhist'); // id of table

    // for(j = 0 ; j < tab.rows.length ; j++) 
    // {     
    //     tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
    //     //tab_text=tab_text+"</tr>";
    // }

    // tab_text=tab_text+"</table>";
    // tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    // tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    // tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    // var ua = window.navigator.userAgent;
    // var msie = ua.indexOf("MSIE "); 

    // if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    // {
    //     txtArea1.document.open("txt/html","replace");
    //     txtArea1.document.write(tab_text);
    //     txtArea1.document.close();
    //     txtArea1.focus(); 
    //     sa=txtArea1.document.execCommand("SaveAs",true,"Say Thanks to Sumit.xls");
    // }  
    // else                 //other browser not tested on IE 11
    //     sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

    // return (sa);

     
    // window.open('data:application/vnd.ms-excel,' +  encodeURIComponent($('#registerhist').html()));


            // var html = $('#registerhist' ).html();
            // var a = document.createElement('a');
            // a.id = 'tempLink';
            // a.href = 'data:application/vnd.ms-excel,' + html;
            // a.download = "test.xls";
            // document.body.appendChild(a);
            // a.click(); // Downloads the excel document
          //  document.getElementById('tempLink').remove();


// 

          $("#registerhist").table2excel({
                //Exclude CSS class specific to this plugin
                exclude: ".noExl",
                name: "Register history",
                filename: "Accounthistory"
            });


    }

    $('.btndds').click(function() {
    location.reload();
});


    $('#registershistory').click(function(){
        
        alert('hi');
       /* var print_url = $(this).attr('data-url');

        var form_input_html = '';
        $.each($(this).closest('.flexigrid').find('.filtering_form').serializeArray(), function(i, field) {
            form_input_html = form_input_html + '<input type="hidden" name="'+field.name+'" value="'+field.value+'">';
        });

        var form_on_demand = $("<form/>").attr("id","print_form").attr("method","post").attr("action",print_url).html(form_input_html);

        $(this).closest('.flexigrid').find('.hidden-operations').html(form_on_demand);

        var _this_button = $(this);

        $(this).closest('.flexigrid').find('#print_form').ajaxSubmit({
            beforeSend: function(){
                _this_button.find('.fbutton').addClass('loading');
                _this_button.find('.fbutton>div').css('opacity','0.4');
            },
            complete: function(){
                _this_button.find('.fbutton').removeClass('loading');
                _this_button.find('.fbutton>div').css('opacity','1');
            },
            success: function(html_data){
                $("<div/>").html(html_data).printElement();
            }
        });*/
    });


</script>

<style type="text/css">
    
      .modal-footer .btn{
        width: 100px !important;
     }
     .modal-footer .btn+.btn{
         width: 100px !important;
     }
</style>






