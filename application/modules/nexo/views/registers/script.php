<?php global $Options,$current_register;?>
<script type="text/javascript">
const printServerURL_new        = '<?php echo store_option( 'nexo_print_server_url' );?>';
const posPrinter            =   `<?php echo store_option( 'nexo_pos_printer' );?>`;
const registerPrinter       =   `<?php echo $current_register[ 'ASSIGNED_PRINTER' ];?>`;
    "use strict";
    $( document ).ready(function(e) {
        $( '.close_register' ).bind( 'click', function(){
            var $this	=	$( this );
            $.ajax( '<?php echo site_url( array( 'rest', 'nexo', 'register_status' ) );?>/' + $( this ).data( 'item-id' ) + '?<?php echo store_get_param( null );?>', {
                success		:	function( data ){
                    // Somebody is logged in
                    if( data[0].STATUS == 'opened' ) {

                        if( data[0].USED_BY != '<?php echo User::id();?>'  ) {
                            bootbox.alert( '<?php echo _s( 'You cannot close this cash register. If the problem persists, contact the administrator.', 'nexo' );?>' );
                            return;
                        }

                        var dom		=	'<h3 class="modal-title"><?php echo _s( 'Fermeture de la caisse', 'nexo' );?></h3><hr style="margin:10px 0px;">';

                            dom		+=	'<p><?php echo tendoo_info( sprintf( _s( '%s, vous vous préparez à fermer une caisse. Veuillez spécifier le montant finale de la caisse', 'nexo' ), User::pseudo() ) );?></p>' +
                                        `<div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon" id="basic-addon1"><?php echo _s( 'Solde de fermeture de la caisse', 'nexo' );?></span>
                                                <input type="text" class="form-control open_balance" placeholder="<?php echo _s( 'Montant', 'nexo' );?>" aria-describedby="basic-addon1">
                                            </div>  
                                        </div> 
                                        <div class="form-group">
                                            <label for="textarea"><?php echo __( 'Remarques', 'nexo' );?></label>
                                            <textarea name="" id="textarea" class="form-control note" rows="3" required="required"></textarea>
                                        </div>
                                        `;

                        bootbox.confirm( dom, function( action ) {
                            if( action == true ) {
                                $.ajax( '<?php echo site_url( array( 'rest', 'nexo', 'close_register' ) );?>/' + $this.data( 'item-id' ) + '?<?php echo store_get_param( null );?>', {
                                    dataType	:	'json',
                                    type		:	'POST',
                                    data		:	_.object( [ 'date', 'balance', 'used_by', 'note' ], [ '<?php echo date_now();?>', $( '.open_balance' ).val(), '<?php echo User::id();?>', $( '.note' ).val() ]),
                                    success: function( data ){
                                        bootbox.alert( '<?php echo _s( 'The cash register has been closed. Please wait...', 'nexo' );?>' );
                                        <?php if( User::in_group( 'shop.cashier' ) ):?>
                                            
                                            LocalPrintURL ="<?php echo site_url(array( 'api','gastro', 'registryhistory' ));?>/" +$this.data( 'item-id' );
                                            
                                            http://a2000demo.com/restaurant_pos_webapp_feature/api/gastro/registryhistory/6
                                            if(registerPrinter!="") {
                                                if(registerPrinter!="Choose an option") {
            
                                                    if(printServerURL==''){
                                                        var host_url = 'http://<?php echo $_SERVER['HTTP_HOST'];?>'+':1000';
            
                                                    } else {
            
                                                        var host_url = printServerURL_new;
                                                    }
                                                    var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
                                                    var theUrl = host_url+"/sendsampleregister";
                                                    xmlhttp.onreadystatechange = function() {
                                                        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                                                            console.log(xmlhttp.responseText);
                                                        }
                                                    }
            
                                                    xmlhttp.open("POST", theUrl);
                                                    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                                                    xmlhttp.send(JSON.stringify({'URL'   :   LocalPrintURL ,'PRINTER'   :   registerPrinter}));
            
                                                }
        
                                             }
                                            // var xmlhttp = new XMLHttpRequest();
                                            // var originalContents = document.body.innerHTML;
                                            // xmlhttp.open('GET',"<?php echo site_url(array( 'dashboard', store_slug(), 'nexo', 'registryhistory' ));?>/" +$this.data( 'item-id' ), false);
                                            // xmlhttp.send(null);
                                            // if(xmlhttp.status == 200) {
                                            //     document.body.innerHTML = xmlhttp.responseText;
                                            //     window.print();
                                            //     document.body.innerHTML = originalContents;;
                                            // }

                                           document.location	=	'<?php echo dashboard_url([ 'registers', 'for_cashiers?notice=register_has_been_closed' ]);?>';
                                        <?php else:?>


                                            // var xmlhttp = new XMLHttpRequest();
                                            // var originalContents = document.body.innerHTML;
                                            // xmlhttp.open('GET',"<?php echo site_url(array( 'dashboard', store_slug(), 'nexo', 'registerhistory' ));?>/" +$this.data( 'item-id' ), false);
                                            // xmlhttp.send(null);
                                            // if(xmlhttp.status == 200) {
                                            //     document.body.innerHTML = xmlhttp.responseText;
                                            //     window.print();
                                            //     document.body.innerHTML = originalContents;;
                                            // }
                                            
                                            LocalPrintURL ="<?php echo site_url(array( 'api','gastro', 'registryhistory' ));?>/" +$this.data( 'item-id' );
                                            
                                            if(registerPrinter!="") {
                                                if(registerPrinter!="Choose an option") {
            
                                                    if(printServerURL==''){
                                                        var host_url = 'http://<?php echo $_SERVER['HTTP_HOST'];?>'+':1000';
            
                                                    } else {
            
                                                        var host_url = printServerURL_new;
                                                    }
                                                    var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
                                                    var theUrl = host_url+"/sendsampleregister";
                                                    xmlhttp.onreadystatechange = function() {
                                                        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
                                                            console.log(xmlhttp.responseText);
                                                        }
                                                    }
            
                                                    xmlhttp.open("POST", theUrl);
                                                    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
                                                    xmlhttp.send(JSON.stringify({'URL'   :   LocalPrintURL ,'PRINTER'   :   registerPrinter}));
            
                                                }
        
                                             }


                                            
                        

                                             //       window.frames["CurrentReceipt"].focus();
                                             //       window.frames["CurrentReceipt"].print();
                                             //       var val = $this.data( 'item-id' );
                                            document.location	=	'<?php echo dashboard_url([ 'registers?notice=register_has_been_closed']);?>';
                                        <?php endif;?>
                                    }
                                });
                            }
                        });

                        // Set custom width
                        $( '.modal-title' ).closest( '.modal-dialog' ).css({
                            'width'		:	'80%'
                        })

                    } else if( data[0].STATUS == 'locked' ) {

                        bootbox.alert( '<?php echo _s( 'Unable to close a locked cash register. If the problem persists, contact the administrator.', 'nexo' );?>' );

                    } else if( data[0].STATUS == 'closed' ) {

                        bootbox.alert( '<?php echo _s( 'This fund is already closed.', 'nexo' );?>' );

                    }

                },
                dataType	:	"json",
                error		:	function(){
                    bootbox.alert( '<?php echo _s( 'Une erreur s\'est produite durant l\'ouverture de la caisse.', 'nexo' );?>' );
                }
            })

        return false;
    });
});
</script>

<style type="text/css">
    
     .content{
      
        background:  <?php echo $this->events->apply_filters('font_size', $this->config->item('default_user_names'))['cash_background_color'];
            ?> !important;
    }
</style>



<?php  
$data = $this->events->apply_filters('font_size', $this->config->item('default_user_names'))['button_size'];

if($data=='small'){


    ?>

    <style type="text/css">
        
        .restdrop {
            display: flex !important;
            flex-direction: column !important;
            height: 47px !important;

         }
         



    </style>

<?php 


} elseif ($data=='medium') {
    # code...

?>
    
    <style type="text/css">


   .restdrop{
    display: flex !important;
    flex-direction: column !important;
    height: 61px !important;

 }


    

    </style>

    <?php
} else {  ?>


    <style type="text/css">


      .restdrop {  
        display: flex !important;
        flex-direction: column !important;
        height: 85px !important;
        /* align-items: self-end; */
        justify-content: space-between !important;
        margin-bottom: 23px !important;
    }


    </style>


<?php
}

?>

<style type="text/css">
    
    #totalbody{
        font-size: <?php echo $this->events->apply_filters('font_size', $this->config->item('default_user_names'))['font_size'];
            ?>;

    }

    .help-block{
        color: red !important;
    }

  /*  @media (min-width: 1370) and (max-width: 1420px) {

        .eyeclose {

        margin-right: 4px;

        }


        
    }*/

     @media (min-width: 992px) and (max-width: 1865px) {


        .eyeclose {

           /*top: 201px !important;
*/         /*  width: 48% !important;*/

        }
    }

      @media (max-width: 991px)  {


        .eyeclose {

       
      /* width: 100% !important;*/

        }
    }


.slick-item:hover{
    box-shadow: none !important;
    
}
.shop-items:hover{
    box-shadow: none !important;
}
.content-header{
        display: none !important;
    }
   
.dropdown-menu{
   /*height: calc(100vh - 71px) !important;*/
    overflow-y:scroll;
   height:auto;
   max-height:200px;
}

</style>


<style type="text/css">
    [data-bb-handler~=cancel] {
    margin-right: 18px;
    width: 12%;
    font-size: 80%;
    }
    [data-bb-handler="confirm"] {
  
    width: 12%;
    font-size: 80%;
    }
    [data-bb-handler="ok"] {
  
    width: 12%;
    font-size: 110%;
    }
    .swal2-styled{
    width: 29%;
    height: 52px;
    font-size: 110%;

    }
   /* #paybutton {
        display: none;
    }*/

</style>


<script type="text/javascript">
    

    $(document).ready(function(){
        $('#payerbutton').hide();
        var id = <?php echo $this->events->apply_filters('group_details', '')->id;?>;

        if(id=="12"){

            //var data = $(this).find("[name='id']").val()
           // alert(data);
            //var value_input = $("input[name*='site_name']").val();
            //alert(value_input);
           // console.log(id)
           //if(value_input!=''){

                //alert(id);
                $('#takeaway').css('display','none !important');
                $('#delivery').css('display','none !important');
                $('.history-box-button').css('display','none !important');

                 $('.history-box-button').css('display','none !important');
                 $("#splitorder_payment").prop("disabled",true);
               

            //} else {

                            //}

        } 

    });


    
</script>



