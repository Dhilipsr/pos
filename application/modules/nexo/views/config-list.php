<?php

// echo "<pre>";
// print_r($crud_content->output);
// die();
$this->Gui->col_width(1, 4);
// var_dump( $crud_content );die;
$this->Gui->add_meta(array(
    'namespace'    =>    'categories',
    'type'        =>    'unwrapped',
    'col_id'    =>    1,
    'title'        =>    __('Gestion & Création des categories', 'nexo')
));

$this->Gui->add_item(array(
    'type'        =>    'dom',
    'content'    =>    $crud_content->output
), 'categories', 1);

$this->Gui->output();


?>

<style type="text/css">
	
	#save-and-go-back-button{
		display: none;
	}
	#cancel-button{
    display: none ;
  }
	p a{
	 display: none;
	}
	#field_button_size_chosen{
		width: 15% !important;
	}
	#field_font_size_chosen{
		width: 15% !important;
	}
	#field_cash_background_color_chosen{
		width: 15% !important;
	}
	#field-cash_background_color{
		    width: 160px !important;
	}
		/*#save-and-go-back-button{
		display: none;
	}*/
</style>

<script>
   $("document").ready(function() {
        $('#field-cash_background_color').prop('type', 'color');

  });
</script>
