<script>
$( document ).ready(function(){


    <?php $URL = site_url([ 'api', 'gastro', 'kitchens', 'getprinters' ]);?>
    $.ajax( 
    	{
    	url:'<?php echo $URL;?>', 
    	headers: {"X-API-KEY": "9zOW3T3ZmBCYQWVDbrVWcFa4mApMrOZGa91mzUxr"},
    	method:'GET',
        success    :   function( result ) {
        	//console.log(result);
            $( '[name="<?php echo store_prefix();?>nexo_pos_printer"]' ).html( '<option><?php echo __( 'Choisir une option', 'nexo' );?></option>' );
            result.forEach( printer => {
            	console.log(printer);
                let selected    =   printer.name ==  '<?php echo store_option( 'nexo_pos_printer' );?>' ? 'selected="selected"' : null;
                $( '[name="<?php echo store_prefix();?>nexo_pos_printer"]' ).append( `<option ${selected} value="${printer.name}">${printer.name}</option>` );
            });
        }
    })
})
</script>