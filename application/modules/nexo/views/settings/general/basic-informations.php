<?php

$this->Gui->add_item(array(
    'type'        =>    'text',
    'name'        =>    store_prefix() . 'site_name',
    'label'        =>    __('Nom de la boutique', 'nexo'),
    'desc'        =>    __('Vous pouvez utiliser le nom du site', 'nexo')
), $namespace, 1);

$this->Gui->add_item(array(
    'type'        =>    'text',
    'name'        =>    store_prefix() . 'nexo_shop_address_1',
    'label'        =>    __('Address 1', 'nexo')
), $namespace, 1);

?>


<?php

$this->Gui->add_item(array(
    'type'        =>    'text',
    'name'        =>    'note',
    'value'=>'(This address will be reflected in invoice)',
    ''
), $namespace, 1);

$this->Gui->add_item(array(
    'type'        =>    'text',
    'name'        =>    store_prefix() . 'nexo_shop_address_2',
    'label'        =>    __('Address 2', 'nexo')
), $namespace, 1);

$this->Gui->add_item(array(
    'type'        =>    'text',
    'name'        =>    store_prefix() . 'nexo_shop_city',
    'label'        =>    __('Ville', 'nexo')
), $namespace, 1);

$this->Gui->add_item(array(
    'type'        =>    'text',
    'name'        =>    store_prefix() . 'nexo_shop_phone',
    'label'        =>    __('Téléphone pour la boutique', 'nexo')
), $namespace, 1);

$this->Gui->add_item(array(
    'type'        =>    'text',
    'name'        =>   store_prefix() . 'nexo_shop_street',
    'label'        =>    __('Rue de la boutique', 'nexo')
), $namespace, 1);

$this->Gui->add_item(array(
    'type'        =>    'text',
    'name'        =>    store_prefix() . 'nexo_shop_pobox',
    'label'        =>    __('Boite postale', 'nexo')
), $namespace, 1);

$this->Gui->add_item(array(
    'type'        =>    'text',
    'name'        =>    store_prefix() . 'nexo_shop_email',
    'label'        =>    __('Email pour la boutique', 'nexo')
), $namespace, 1);

$this->Gui->add_item(array(
    'type'        =>    'text',
    'name'        =>    store_prefix() . 'nexo_shop_fax',
    'label'        =>    __('Fax pour la boutique', 'nexo')
), $namespace, 1);

$this->Gui->add_item(array(
    'type'        =>    'textarea',
    'name'        =>    store_prefix() . 'nexo_other_details',
    'label'        =>    __('Détails supplémentaires', 'nexo'),
    'description'    =>    __('Ce champ est susceptible d\'être utilisé au pied de page des rapports', 'nexo')
), $namespace, 1);



?>


<script>

    $(document).ready(function(){

        var id = <?php echo $this->events->apply_filters('group_details', '')->id;?>;

        if(id!="4"){

            //var data = $(this).find("[name='id']").val()
           // alert(data);
            //var value_input = $("input[name*='site_name']").val();
            //alert(value_input);
           // console.log(id)
           //if(value_input!=''){

                //alert(id);
                $('input[name="site_name"]').prop('disabled', true);

                 $('input[name="nexo_shop_address_1"]').prop('disabled', true);

            //} else {

                            //}

        } else {

            // var value_input = $("input[name*='site_name']").val();
            //    // alert(value_input);
            //     if(value_input!=''){
                        $('input[name="site_name"]').prop('disabled', false);
                         $('input[name="nexo_shop_address_1"]').prop('disabled', false);
           //     }

        }

    $('input[name="note"]').prop('disabled', true);
    });

</script>

<style type="text/css">
    
    input[name="note"]{
        position: relative;
        top: -10px;
        border: unset !important;
    }
</style>
