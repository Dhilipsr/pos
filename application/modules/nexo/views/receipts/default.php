<?php
/**
 * Starting Cache
 * Cache should be manually restarted
**/

error_reporting(0);
use Carbon\Carbon;

if (! $order_cache = $cache->get($order[ 'order' ][0][ 'ID' ]) || @$_GET[ 'refresh' ] == 'true') {
    ob_start();
}
if(isset($_GET[ 'reprint' ])){

    $reprint=1;

}else{
    $reprint=0;
}


if(isset($_GET[ 'cech1' ])){

    $take=123;

}
if(isset($_GET[ 'cech123' ])){

    $take=23;

}
?>
<?php if( @$_GET[ 'ignore_header' ] != 'true' ):?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo sprintf(__('Order ID : %s &mdash;Shop Receipt', 'nexo'), $order[ 'order' ][0][ 'CODE' ]);?></title>
<link rel="stylesheet" media="all" href="<?php echo css_url('nexo') . '/bootstrap.min.css';?>" />
<link rel="stylesheet" media="all" href="<?php echo module_url( 'nexo' ) . 'fonts/receipt-stylesheet.css';?>" />
</head>

<body id="printing" >
<?php endif;?>



<?php global $Options;?>
<?php if (@$order[ 'order' ][0][ 'CODE' ] != null):?>
<div class="container-fluid" style="width: 80%;">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row order-details">
                 <?php 

                  

                            $first_str = substr(@$order['order'][0]['CODE'],0,2);

                            $second_str = substr(@$order['order'][0]['CODE'],2,2);

                            $third_str = substr(@$order['order'][0]['CODE'],4,2);
                            $four_str = substr(@$order['order'][0]['CODE'],6);

                              ?>
                      <?php if($reprint==1){ ?>
                    </br><h2 class="text-center" style="margin-bottom: 0px;">Reprint</h2>
                    <?php }else{ } ?>

                     <?php
                              if($take==23){ 

                              ?>
                              <h2 class="text-center" style="margin-bottom: 0px;">Company Bill</h2>
                          <?php } ?>
                           <?php
                              if($take==123){ 

                              ?>
                              <h2 class="text-center" style="margin-bottom: 0px;">Customer Bill</h2>
                          <?php } ?>
                          
                <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                    <?php if(@$order[ 'order' ][0]['RESTAURANT_ORDER_TYPE']=='takeaway' ||@$order[ 'order' ][0]['RESTAURANT_ORDER_TYPE']=='delivery') { ?> <h2 class="text-center"><?php echo "Token No:".$four_str;?></h2> <?php } ?>

                  

                    <?php if( store_option( 'url_to_logo' ) != null ):?>
                        <div class="text-center">
                            <img src="<?php echo store_option( 'url_to_logo' );?>" 
                            style="display:inline-block;<?php echo store_option( 'logo_height' ) != null ? 'height:' . store_option( 'logo_height' ) . 'px' : '';?>
                            ;<?php echo store_option( 'logo_width' ) != null ? 'width:' . store_option( 'logo_width' ) . 'px' : '';?>"/>
                        </div>
                    <?php else:?>
                        <h2 class="text-center"><?php echo @$Options[ store_prefix() . 'site_name' ];?></h2>
                       <h3 class="text-center" style="margin-bottom: 0px;margin-top: 0px;"><?php echo store_option('nexo_shop_address_1').' '.store_option('nexo_shop_city').'  '.store_option('nexo_shop_pobox');?></h3>

  <h3 class="text-center" style="margin-bottom: 0px;margin-top: 0px;"><?php echo store_option('nexo_shop_address_2');?></h3>

                        <?php if(store_option('nexo_shop_phone')!=''){ ?> 
                        <h3 class="text-center" style="    margin-bottom: 0px;margin-top: 0px;"><?php echo "TEL:".store_option('nexo_shop_phone');?></h3> 
                        <?php } ?>
                
                        <?php endif;?>
                </div>
                <?php ob_start();?>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                    <?php echo xss_clean( @$Options[ store_prefix() . 'receipt_col_1' ] );?>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                    <?php echo xss_clean( @$Options[ store_prefix() . 'receipt_col_2' ] );?>
                </div>
            </div>
            <?php if( @$_GET[ 'is-pdf' ] ):?>
            <br>
            <br>
            <?php endif;?>
            <?php
            $string_to_parse    =   ob_get_clean();
            echo $this->parser->parse_string( $string_to_parse, $template , true );
            ?>
            <div >
               <div class="text-center" style="    margin-bottom: 0px;margin-top: 0px;">

                        

                            <?php 

                            $order_type='';

                            if(@$order[ 'order' ][0]['RESTAURANT_ORDER_TYPE']=='takeaway'){

                                $order_type = 'TA';

                            } elseif(@$order[ 'order' ][0]['RESTAURANT_ORDER_TYPE']=='delivery') {
                               $order_type = 'DELIVERY';
                            } elseif(@$order[ 'order' ][0]['RESTAURANT_ORDER_TYPE']=='dinein') {
                                $order_type='DI';

                             } else {

                                   $order_type='QI';
                             }

                            $first_str = substr(@$order['order'][0]['CODE'],0,2);

                            $second_str = substr(@$order['order'][0]['CODE'],2,2);

                            $third_str = substr(@$order['order'][0]['CODE'],4,2);
                            $four_str = substr(@$order['order'][0]['CODE'],6);

                              ?>

                        <hr  style="    margin-bottom: 0px;margin-top: 0px;">

                        <div style="display:flex;justify-content:space-between;">
                                      <div style="width:35%;text-align: left;font-size: 10px;">
                                     <strong>Bill No : <?php echo @$order['order'][0]['CODE'] ;?></strong>
                                    </div>
                                     <div style="width:35%;text-align: right;font-size: 10px;">
                                     <strong> <?php echo date('d-m-Y g:i:s A',strtotime(@$order[ 'order' ][0]['DATE_CREATION']));?></strong>
                                    </div>

                        </div>

                       <div style="display:flex;justify-content:space-between;">
                          <div style="width:50%;text-align: left;font-size: 10px;">
                             <strong >GST Reg : <?php echo $this->events->apply_filters('tax', $this->config->item('default_user_names'))['REGISTER_NUMBER'];?></strong>
                            </div>
                            <div style="width:35%;">
                              
                            </div>

                        </div>
                                

                      
                    
                    </div>
             

    <div class="">
     <p style="    margin-bottom: 0px;
    margin-top: 0px;"><span class="alignleft"><strong>CTR #:<?php echo ' '.$order_type;?> / <?php echo $this->events->apply_filters('getregister',@$order[ 'order' ][0]['REF_REGISTER'] )['NAME'];?></strong></span><span class="aligncenter"><?php echo ' '.$order_type;?></span><span  class="alignright"><?php echo $this->events->apply_filters('getcustomer',@$order[ 'order' ][0]['REF_CLIENT'] )['NOM'];?>/</span></p></div>
                    
                      
                </div>
                <hr style="margin-bottom: 0%; margin-top:0%;">
                <table class="table table-hover"  style=" margin-top: 0px;">
                    <thead>
                        <tr>
                            <th class="col-md-6"><?php _e('Produits', 'nexo');?></th>
                            <th class="col-md-2 text-right"><?php _e('Prix', 'nexo');?></th>
                            <th class="text-right">Qty</th>
                            <?php if( @$Options[ store_prefix() . 'unit_item_discount_enabled' ] == 'yes' ):?>
                            <th class="col-md-1 text-right"><?php _e('Remise', 'nexo');?></th>
                            <?php endif;?>
                            <th style="width: 180px;" class="col-md-1 text-right"><?php _e('Total', 'nexo');?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $total_global    =    0;
                        $total_unitaire    =    0;
                        $total_quantite    =    0;
                        $total_discount     =   0;
                        $products       =   $this->events->apply_filters( 'receipt_items', $order[ 'products' ] );

                        if( $products ) {
                            foreach ( $products as $_produit) {

                                // $total_global        +=    floatval($_produit[ 'PRIX_TOTAL' ]);
                                $total_unitaire         +=    floatval($_produit[ 'PRIX_BRUT' ]);
                                $total_quantite         +=    floatval($_produit[ 'QUANTITE' ]);
                                $total_global           +=    ( floatval($_produit[ 'PRIX_TOTAL' ]) );
                                ?>
                                <tr>
                                    <td>
                                        <?php if ( store_option( 'item_name' ) == 'use_both' ):?>
                                            <?php echo empty( $_produit[ 'DESIGN' ] ) ? $_produit[ 'NAME' ] : $_produit[ 'DESIGN' ];?><br>
                                            <?php echo $_produit[ 'ALTERNATIVE_NAME' ];?>
                                        <?php elseif ( store_option( 'item_name' ) == 'only_secondary' ):?>
                                            <?php echo $_produit[ 'ALTERNATIVE_NAME' ];?>
                                        <?php else: // use_primary?>
                                            <?php echo empty( $_produit[ 'DESIGN' ] ) ? $_produit[ 'NAME' ] : $_produit[ 'DESIGN' ];?>
                                        <?php endif;?>
                                        <?php $after_item_name  =    $this->events->apply_filters( 'receipt_after_item_name', [ 
                                            'output'        =>  '',
                                            'item'          =>  $_produit
                                        ]);
        
                                        echo $after_item_name[ 'output' ];                                     
                                        ?>
                                    </td>
                                    <td class="text-right">
                                    <?php echo $this->Nexo_Misc->cmoney_format( floatval($_produit[ 'PRIX_BRUT' ]) );?>
                                    </td>
                                    <td class="" style="text-align: right"> <?php echo $_produit[ 'QUANTITE' ];
                                    ?> </td>
                                    <?php if( @$Options[ store_prefix() . 'unit_item_discount_enabled' ] == 'yes' ):?>
                                    <td  class="" style="text-align: right">
                                    <?php
                                    $discount_amount            =   0;
                                    if( $_produit[ 'DISCOUNT_TYPE' ] == 'percentage' ) {
                                        $discount_amount        =   ( ( floatval( $_produit[ 'PRIX_BRUT' ] ) * intval( $_produit[ 'QUANTITE' ] ) ) * floatval( $_produit[ 'DISCOUNT_PERCENT' ] ) ) / 100;
                                        echo '- ' . $this->Nexo_Misc->cmoney_format( floatval( $discount_amount ) );
                                    } else if( $_produit[ 'DISCOUNT_TYPE' ] == 'flat' ) {
                                        $discount_amount        =   ( floatval( $_produit[ 'DISCOUNT_AMOUNT' ] ) * intval( $_produit[ 'QUANTITE' ] ) );
                                        echo '- ' . $this->Nexo_Misc->cmoney_format( floatval( $discount_amount ) );
                                    }
    
                                    $total_discount         +=  $discount_amount;
    
                                    ?> </td>
                                    <?php endif;?>
                                    <td class="text-right">
                                        <?php echo $this->Nexo_Misc->cmoney_format( floatval( $_produit[ 'PRIX_TOTAL' ] ) );?>
                                    </td>
                                </tr>
                            
                            <?php
                            }
    
                            ?>
                            
                            <tr>
                                
                                <td class=""><strong><?php _e('Sous Total', 'nexo');?></strong></td>
    
                                <td class="text-right">
                                <?php /*echo sprintf(
                                    __( '%s %s %s', 'nexo' ),
                                    $this->Nexo_Misc->display_currency( 'before' ),
                                    floatval( $total_unitaire ),
                                    $this->Nexo_Misc->display_currency( 'after' )
                                )*/;?>
                                </td>
                                <td class="" style="text-align: right"><?php echo $total_quantite;?></td>
                                <?php if( @$Options[ store_prefix() . 'unit_item_discount_enabled' ] == 'yes' ):?>
                                <td class="" style="text-align: right"><?php echo $this->Nexo_Misc->cmoney_format( floatval( $total_discount ) );?></td>
                                <?php endif;?>
                                <td class="text-right">
                                <?php echo $this->Nexo_Misc->cmoney_format(
                                    floatval($total_global)
                                );?>
                                </td>
                            </tr>
                            
                            <?php
                            if( ! empty( $order[ 'order' ][0][ 'SHIPPING_AMOUNT' ] ) ):
                            ?>
                            
                            <tr>
                                <td><?php echo __( 'Livraison', 'nexo' );?></td>
                                <td></td>
                                <td></td>
                                <?php if( @$Options[ store_prefix() . 'unit_item_discount_enabled' ] == 'yes' ):?>
                                <td></td>
                                <?php endif;?>
                                <td class="text-right">
                                <?php echo $this->Nexo_Misc->cmoney_format(
                                    floatval( $order[ 'order' ][0][ 'SHIPPING_AMOUNT' ] )
                                );?>
                                </td>
                            </tr>
    
                            <?php endif;?>
        
                            <?php if (floatval($_produit[ 'RISTOURNE' ])):?>
                            <tr>
                                <td class=""><?php _e('Remise automatique', 'nexo');?></td>
                                <td class="" style="text-align: right"> </td>
                                <td class="text-right">(-)</td>
                                <?php if( @$Options[ store_prefix() . 'unit_item_discount_enabled' ] == 'yes' ):?>
                                <td class="text-right"></td>
                                <?php endif;?>
                                <td class="text-right">
                                <?php echo $this->Nexo_Misc->cmoney_format(
                                    floatval($_produit[ 'RISTOURNE' ])
                                );?>
                                </td>
                            </tr>
                            <?php endif;?>
                            <?php if ( $_produit[ 'REMISE_TYPE' ] == 'flat' ):?>
                            <tr>
                                <td class=""><?php _e('Remise expresse', 'nexo');?></td>
                                <td class="" style="text-align: right"> </td>
                                <td class="text-right">(-)</td>
                                <?php if( @$Options[ store_prefix() . 'unit_item_discount_enabled' ] == 'yes' ):?>
                                <td class="text-right"></td>
                                <?php endif;?>
                                <td class="text-right">
                                <?php echo $this->Nexo_Misc->cmoney_format(
                                    floatval($_produit[ 'REMISE' ])
                                );?>
                                </td>
                            </tr>
                            <?php endif;?>
                            <?php if ( $_produit[ 'REMISE_TYPE' ] == 'percentage' ):?>
                            <tr>
                                <td class=""><?php echo sprintf( __('Remise (%s%%)', 'nexo'), $_produit[ 'REMISE_PERCENT' ] );?></td>
                                <td class="" style="text-align: right"> </td>
                                <td class="text-right">(-)</td>
                                <?php if( @$Options[ store_prefix() . 'unit_item_discount_enabled' ] == 'yes' ):?>
                                <td class="text-right"></td>
                                <?php endif;?>
                                <td class="text-right">
                                <?php echo $this->Nexo_Misc->cmoney_format(
                                    ( nexoCartGrossValue( $order[ 'products' ] ) * floatval( $_produit[ 'REMISE_PERCENT' ] ) ) / 100
                                );?>
                                </td>
                            </tr>
                            <?php endif;?>
                            <?php if ( $order[ 'order' ][0][ 'GROUP_DISCOUNT' ] != '0' ):?>
                            <tr>
                                <td class=""><?php _e('Remise de groupe', 'nexo');?></td>
                                <td class="" style="text-align: right"> </td>
                                <td class="text-right">(-)</td>
                                <?php if( @$Options[ store_prefix() . 'unit_item_discount_enabled' ] == 'yes' ):?>
                                <td class="text-right"></td>
                                <?php endif;?>
                                <td class="text-right">
                                <?php echo $this->Nexo_Misc->cmoney_format(
                                    floatval( $order[ 'order' ][0][ 'GROUP_DISCOUNT' ] )
                                );?>
                                </td>
                            </tr>
                            <?php endif;?>
                            
                            <?php
                            /**
                             * let's capture the taxes
                             * section to make it customizable
                             */
                            ob_start();
                            ?>

                            <?php if ( in_array( store_option( 'nexo_vat_type' ),  [ 'fixed', 'variable', 'item_vat' ], true )):?>
                            <tr>
                                <td class=""><?php _e('Net Hors Taxe', 'nexo');?></td>
                                <td class="text-right"></td>
                                <td class="" style="text-align: right">(=)</td>
                                <?php if( @$Options[ store_prefix() . 'unit_item_discount_enabled' ] == 'yes' ):?>
                                <td class="text-right"></td>
                                <?php endif;?>
                                <td class="text-right">
                                <?php 
                                echo $this->Nexo_Misc->cmoney_format( $order[ 'order' ][0][ 'NET_TOTAL' ] );
                                // echo $this->Nexo_Misc->cmoney_format(
                                // bcsub(
                                //     floatval($total_global) + floatval($order[ 'order'][0][ 'SHIPPING_AMOUNT' ]),
                                //     (
                                //         floatval(@$_produit[ 'RISTOURNE' ]) +
                                //         floatval(@$_produit[ 'RABAIS' ]) +
                                //         floatval(@$_produit[ 'REMISE' ]) +
                                //         nexoCartPercentageDiscount( $order[ 'products' ], $_produit ) +
                                //         floatval(@$_produit[ 'GROUP_DISCOUNT' ])
                                //     ), 2
                                // ) );
                                ?>
                                </td>
                            </tr>

                            <?php if( $tax || store_option( 'nexo_vat_type' ) == 'fixed' ):?>            
                            <tr>
                                <?php if ( $tax && store_option( 'nexo_vat_type' ) == 'variable' ):?>
                                    <td class=""><?php echo sprintf( __( '%s (%s%%)', 'nexo' ), $tax[0][ 'NAME' ], $tax[0][ 'RATE' ] );?></td>
                                <?php elseif ( store_option( 'nexo_vat_type' ) == 'fixed' ):?>
                                    <td class=""><?php _e('Services', 'nexo');?> (<?php echo @$Options[ store_prefix() . 'nexo_vat_percent' ];?>%)</td>
                                <?php endif;?>

                                <td class="text-right"></td>
                                <td class="" style="text-align: right">(+)</td>
                                <?php if( @$Options[ store_prefix() . 'unit_item_discount_enabled' ] == 'yes' ):?>
                                <td class="text-right"></td>
                                <?php endif;?>
                                <td class="text-right">
                                <?php echo $this->Nexo_Misc->cmoney_format(
                                    $_produit[ 'TVA' ]
                                );?>
                                </td>
                            </tr>
                            <?php elseif( ! empty( @$tax ) && store_option( 'nexo_vat_type' ) == 'variable' ):?>    
                                <tr>
                                    <td><?php echo sprintf( __( '%s (%s%%)', 'nexo' ), $tax[0][ 'NAME' ], $tax[0][ 'RATE' ] );?></td>
                                    <td></td>
                                    <td class="text-right">(+)</td>
                                    <?php if( @$Options[ store_prefix() . 'unit_item_discount_enabled' ] == 'yes' ):?>
                                    <td class="text-right"></td>
                                    <?php endif;?>
                                    <td class="text-right"><?php echo $this->Nexo_Misc->cmoney_format( $order[ 'TVA' ] );?></td>
                                </tr>
                            <?php elseif( store_option( 'nexo_vat_type' ) == 'item_vat' ):?>    
                                <?php $taxes = json_decode( @$order[ 'metas' ][ 'taxes' ], true );;?>
                                <?php if ( $taxes ) {
                                    foreach( $taxes as $tax ) {
                                        ?>
                                        <tr>
                                            <td><?php echo sprintf( __( '%s (%s%%)', 'nexo' ), $tax[ 'NAME' ], $tax[ 'RATE' ] );?></td>
                                            <td></td>
                                            <td class="text-right">(+)</td>
                                            <?php if( @$Options[ store_prefix() . 'unit_item_discount_enabled' ] == 'yes' ):?>
                                            <td class="text-right"></td>
                                            <?php endif;?>
                                            <td class="text-right"><?php echo $this->Nexo_Misc->cmoney_format( $tax[ 'TOTAL_TAX' ] );?></td>
                                        </tr>
                                        <?php
                                    }
                                }?>
                            <?php endif;?>
                            
                            <?php if( store_option( 'nexo_vat_type' ) === 'item_vat' ) { ?>
                            <tr>
                                <td class=""><strong><?php _e('TTC', 'nexo');?></strong></td>
                                <td class="text-right"></td>
                                <td class="" style="text-align: right">(=)</td>
                                <?php if( @$Options[ store_prefix() . 'unit_item_discount_enabled' ] == 'yes' ):?>
                                <td class="text-right"></td>
                                <?php endif;?>
                                <td class="text-right">
                                <?php echo $this->Nexo_Misc->cmoney_format( 
                                    bcsub(
                                        floatval($total_global) + floatval($order[ 'order'][0][ 'SHIPPING_AMOUNT' ]),
                                        (
                                            floatval(@$_produit[ 'RISTOURNE' ]) +
                                            floatval(@$_produit[ 'RABAIS' ]) +
                                            floatval(@$_produit[ 'REMISE' ]) +
                                            nexoCartPercentageDiscount( $order[ 'products' ], $_produit ) +
                                            floatval(@$_produit[ 'GROUP_DISCOUNT' ])
                                        ), 2
                                    )
                                );?>
                                </td>
                            </tr>
                            <?php } else { ?>
                            <tr>
                                <td class=""><strong><?php _e('TTC', 'nexo');?></strong></td>
                                <td class="text-right"></td>
                                <td class="" style="text-align: right">(=)</td>
                                <?php if( @$Options[ store_prefix() . 'unit_item_discount_enabled' ] == 'yes' ):?>
                                <td class="text-right"></td>
                                <?php endif;?>
                                <td class="text-right">
                                <?php echo $this->Nexo_Misc->cmoney_format( 
                                    bcsub(
                                        floatval($total_global) + floatval($_produit[ 'TVA' ]) + floatval($order[ 'order'][0][ 'SHIPPING_AMOUNT' ]),
                                        (
                                            floatval(@$_produit[ 'RISTOURNE' ]) +
                                            floatval(@$_produit[ 'RABAIS' ]) +
                                            floatval(@$_produit[ 'REMISE' ]) +
                                            nexoCartPercentageDiscount( $order[ 'products' ], $_produit ) +
                                            floatval(@$_produit[ 'GROUP_DISCOUNT' ])
                                        ), 2
                                    )
                                );?>
                                </td>
                            </tr>
                            <?php } ;?>
                            
                            
                             <?php else:?>


                                <?php foreach ($products as $data) {

                                    $data_new += $data['TOTAL_TAX'];
                                   
                                }
                                ?>
                                    <tr>
                                <td class=""><strong><?php _e('Tax', 'nexo');?>(Including <?php echo $this->events->apply_filters('tax', $this->config->item('default_user_names'))['RATE'];?> %)</strong></td>
                              <!--   <td class="text-right" style="white-space: nowrap;">Price Inclusive Of GST  </td>
                                <td class="" style="text-align: right;white-space: nowrap;"><?php echo $this->events->apply_filters('tax', $this->config->item('default_user_names'))['RATE'];?> %</td> -->
                                <td></td>
                                <td></td>
                              
                                <td class="text-right"><strong>
                                <?php echo $this->Nexo_Misc->cmoney_format( 
                                     floatval( $data_new ));
                                     ?>
                                </td></strong>
                            </tr>


                         


                           

                            <tr style="font-size: 15px;">
                                <td class=""><span style="font-size: 13px;"><?php _e('Gross total', 'nexo');?></span></td>
                                <td class="text-right"></td>
                                <td class="" style="text-align: right;font-size: 13px;">(=)</td>
                                <?php if( @$Options[ store_prefix() . 'unit_item_discount_enabled' ] == 'yes' ):?>
                                <td class="text-right"></td>
                                <?php endif;?>
                                <td class="text-right"><span style="font-size: 13px;white-space: nowrap;">
                                <?php
                                echo $this->Nexo_Misc->cmoney_format( bcsub(
                                    floatval($total_global) + floatval($_produit[ 'TVA' ]) + floatval($order[ 'order'][0][ 'SHIPPING_AMOUNT' ]),
                                    (
                                        floatval(@$_produit[ 'RISTOURNE' ]) +
                                        floatval(@$_produit[ 'RABAIS' ]) +
                                        floatval(@$_produit[ 'REMISE' ]) +
                                        ( ( floatval( @$_produit[ 'REMISE_PERCENT' ] ) * $total_global ) / 100 ) +
                                        floatval(@$_produit[ 'GROUP_DISCOUNT' ])
                                    ), 2
                                ) )?>
                                </td></span>
                            </tr>
                            <?php endif;?>
                            <?php 
                            /**
                             * get the view output 
                             * to be rendered
                             */
                            $view   =   ob_get_clean();
                            ?>
                            <?php echo $this->events->apply_filters_ref_array( 
                                'render_taxes_default_receipt', 
                                [
                                    $view,
                                    compact( 'total_discount', '_produit', 'order', 'total_global', 'total_quantite' )
                                ]
                            );?>
    
                            <?php
                            $order_payments         =   $this->Nexo_Misc->order_payments( $order[ 'order' ][0][ 'CODE' ] );
                            $payment_types          =   $this->events->apply_filters( 'nexo_payments_types', $this->config->item( 'nexo_payments_types' ) );
    
                            foreach( $order_payments as $payment ) {
                                ?>
                                <tr>
                                    <td class="">
                                        <?php echo @$payment_types[ $payment[ 'PAYMENT_TYPE' ] ] == null ? __( 'Type de paiement inconnu', 'nexo' ) : @$payment_types[ $payment[ 'PAYMENT_TYPE' ] ]; ?></td>
                                    <td class="" style="text-align: right"> </td>
                                    <td class="text-right"></td>
                                    <?php if( @$Options[ store_prefix() . 'unit_item_discount_enabled' ] == 'yes' ):?>
                                    <td class="text-right"></td>
                                    <?php endif;?>
                                    <td class="text-right">
                                    <?php if ( $payment[ 'OPERATION' ] === 'incoming' ):?>
                                    <?php echo $this->Nexo_Misc->cmoney_format( floatval( $payment[ 'MONTANT' ] ) );?>
                                    <?php else:?>
                                    <?php echo $this->Nexo_Misc->cmoney_format( - floatval( $payment[ 'MONTANT' ] ) );?>
                                    <?php endif;?>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>

                            <?php if ( in_array( $order[ 'order' ][0][ 'TYPE' ], [ 'nexo_order_partially_refunded', 'nexo_order_refunded' ] ) ):?>
                            <?php
                            $this->load->module_model( 'nexo', 'Nexo_Orders_Model', 'orderModel' );
                            $refunds    =   get_instance()->orderModel->order_refunds( $order[ 'order' ][0][ 'ID' ] );
                            $totalRefunds   =   array_sum( array_map( function( $refund ) {
                                return floatval( $refund[ 'TOTAL' ] );
                            }, $refunds ) );
                            ?>
                            <tr>
                                <td>
                                    <strong><?php echo __( 'Remboursement', 'nexo' );?></strong>
                                </td>
                                <td class="" style="text-align: right"> </td>
                                <td class="text-right"></td>
                                <?php if( @$Options[ store_prefix() . 'unit_item_discount_enabled' ] == 'yes' ):?>
                                <td class="text-right"></td>
                                <?php endif;?>
                                <td class="text-right"><strong><?php echo $this->Nexo_Misc->cmoney_format( - floatval( $totalRefunds ) );?></strong></td>
                            </tr>
                            <?php else:?>
                            <?php $totalRefunds     =   0;?>
                            <?php endif;?>
    
                            <tr style="font-size:10px;">
                                <td class="" style="white-space: nowrap;font-size: 13px;"><strong><?php _e('Grand Total', 'nexo');?></strong></td>
                                <td class="" style="text-align: right"> </td>
                                <td class="text-right"></td>
                                <?php if( @$Options[ store_prefix() . 'unit_item_discount_enabled' ] == 'yes' ):?>
                                <td class="text-right"></td>
                                <?php endif;?>
                                <td class="text-right">
                <strong style="font-size: 13px;white-space: nowrap;">
                                <?php echo $this->Nexo_Misc->cmoney_format( 
                                    ( floatval( $order[ 'order' ][0][ 'SOMME_PERCU' ] ) - $totalRefunds )
                                );?>
                </strong>
                                </td>
                            </tr>

                            <?php
                            $terme        =    floatval( $_produit[ 'SOMME_PERCU' ] ) >= floatval( $order[ 'order' ][0][ 'TOTAL' ] ) ? __('Changes :', 'nexo') : __('&Agrave; percevoir :', 'nexo');
                            ?>
                            <tr>
                                <td class="text-right" colspan="<?php echo @$Options[ store_prefix() . 'unit_item_discount_enabled' ] == 'yes' ? 4 : 3;?>"><h4><strong ><?php echo $terme;?></strong></h4></td>
                                <td class="text-right text-danger"><h4><strong>
                                    <?php
                                    echo $this->Nexo_Misc->cmoney_format( 
                                        abs( 
                                            (
                                                ( 
                                                    floatval( $order[ 'order' ][0][ 'TOTAL' ]) -
                                                    floatval( $order[ 'order' ][0][ 'SOMME_PERCU' ])
                                                ) 
                                                + floatval( $totalRefunds )
                                            )
                                        )
                                    );
                                    ;?>
                                </strong>
                                </h4></td>
                            </tr>
                            <?php
                        } else {
                            ?>
                            <tr>
                                <td colspan="5">
                                    <?php echo __( 'Aucun produit à afficher', 'nexo' );?>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>


                <?php include_once( dirname( __FILE__ ) . '/barcode.php' );?>
                  <?php if(@$order[ 'order' ][0]['RESTAURANT_ORDER_TYPE']=='takeaway' ||@$order[ 'order' ][0]['RESTAURANT_ORDER_TYPE']=='delivery') { ?> <h4 class="text-center"><?php echo "Present the receipt for  collection.Collection may not be in order. Due to different Processing times. Minimum waiting time  is 15 minutes";?></h4> <?php } ?>

                <p class="text-center"><?php echo xss_clean( $order[ 'order' ][0][ 'DESCRIPTION' ] );?></p>
                <p class="text-center"><?php echo xss_clean( $this->parser->parse_string( @$Options[ store_prefix() . 'nexo_bills_notices' ], $template , true ) );?></p>
                <?php if( @$_GET[ 'is-pdf' ] == null ):?>
                <div class="container-fluid hideOnPrint">
                    <div class="row hideOnPrint">
                        <div class="col-lg-12">
                        <a href="<?php echo dashboard_url([ 'orders' ]);?>" class="btn btn-success btn-lg btn-block"><?php _e('Revenir à la liste des commandes', 'nexo');?></a>
                        </div>
                    </div>
                </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>
<?php else:?>
<div class="container-fluid"><?php echo tendoo_error(__('Une erreur s\'est produite durant l\'affichage de ce reçu. La commande concernée semble ne pas être valide ou ne dispose d\'aucun produit.', 'nexo'));?></div>
<div class="container-fluid hideOnPrint">
    <div class="row hideOnPrint">
        <div class="col-lg-12">
        <a href="<?php echo dashboard_url([ 'orders' ]);?>" class="btn btn-success btn-lg btn-block"><?php _e('Revenir à la liste des commandes', 'nexo');?></a>
        </div>
    </div>
</div>
<?php endif;?>
<style>
@media print {
    /** {
         padding: 0px;
         margin: 0px;
        text-transform: uppercase;
    }*/
    .hideOnPrint {
        display:none !important;
    }
    td, th {font-size: 3.6vw;}
    .order-details, p {
        font-size: 2.9vw;
    }
    .order-details h2 {
        font-size: 5.5vw;
    }
    h3 {
        font-size: 3.2vw;
    }
    h4 {
        font-size: 3.2vw;
    }
   /* .table>tbody>tr>td{
        padding: 0px ;
    }*/


}

#page {
   border-collapse: collapse;
}

/* And this to your table's `td` elements. */
#page td {
   padding: 0; 
   margin: 0;
}
</style>
<?php include( dirname( __FILE__ ) . '/receipt-footer.php' );?>
<?php if( @$_GET[ 'ignore_header' ] != 'true' ):?>
</body>
</html>
<?php endif;?>

<?php
if (! $cache->get($order[ 'order' ][0][ 'ID' ]) || @$_GET[ 'refresh' ] == 'true') {
    $cache->save($order[ 'order' ][0][ 'ID' ], ob_get_contents(), 999999999); // long time
}
?>


<style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}


</style>

<style type="text/css">
     .table{
        margin-bottom: -16px;
    }
    .h4, .h5, .h6, h4, h5, h6 {
    margin-top: 2px;
    margin-bottom: 4px;
}
.table>tbody>tr>td{
    padding: 4px;
}


.alignleft {
  float: left;
  width:33.33333%;
  text-align:left;
}
.aligncenter {
  float: left;
  width:33.33333%;
  text-align:center;
}
.alignright {
 float: left;
 width:33.33333%;
 text-align:right;
}​

</style>