<?php 
error_reporting(0);
use Carbon\Carbon;
echo '<?xml version="1.0" encoding="UTF-8"?>';?>

<!doctype html>
<?php global $Options;?>
<html>
<head>
<meta charset="utf-8">
<title>Sales receipt</title>
<link rel="stylesheet" media="all" href="<?php echo css_url('nexo') . '/bootstrap.min.css';?>" />
<link rel="stylesheet" media="all" href="<?php echo module_url( 'nexo' ) . 'fonts/receipt-stylesheet.css';?>" />
</head>

<body id="printing">


    <div class="container-fluid" style="width: 100%;">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="row order-details">
                    <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12">
                        <?php if( store_option( 'url_to_logo' ) != null ):?>
                            <div class="text-center">
                                <img src="<?php echo store_option( 'url_to_logo' );?>" 
                                style="display:inline-block;<?php echo store_option( 'logo_height' ) != null ? 'height:' . store_option( 'logo_height' ) . 'px' : '';?>
                                ;<?php echo store_option( 'logo_width' ) != null ? 'width:' . store_option( 'logo_width' ) . 'px' : '';?>"/>
                            </div>
                        <?php else:?>
                           <h2 class="text-center"><?php echo 'LOGOUT MEMO';?></h2>
                             <h3 class="text-center" style="white-space: nowrap"><?php echo @$Options[ store_prefix() . 'site_name' ];?></h3>
                       <h5 class="text-center" style="margin-bottom: 0px;margin-top: 0px;"><?php echo store_option('nexo_shop_address_1');?></h5>
                       
                       <h5 class="text-center" style="margin-bottom: 0px;margin-top: 0px;"><?php echo store_option('nexo_shop_city').'  '.store_option('nexo_shop_pobox');?></h5>
                       <br>
                        <?php if(store_option('nexo_shop_phone')!=''){ ?> 
                        <h5 class="text-center" style="margin-bottom: 0px;margin-top: 0px;"><?php echo "TEL:".store_option('nexo_shop_phone');?></h5> 
                        <?php } ?>

                        <?php endif;?>
                    </div>
                    <?php ob_start();?>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <?php echo xss_clean( @$Options[ store_prefix() . 'receipt_col_1' ] );?>
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 text-right">
                        <?php echo xss_clean( @$Options[ store_prefix() . 'receipt_col_2' ] );?>
                    </div>
                </div>

                                <?php
                   $this->events->do_action( 'nps_before_items', compact( 'items','transactions','opening_amount','closing_amount','registers'));

                   // echo "<pre>";
                   // print_r($transactions);
                   // die();
                    $opening_Amount = 0;

                    foreach ($opening_amount as $datavalue) {
                          
                         // if($datavalue->TYPE=='opening') {

                            $opening_Amount  = $datavalue['BALANCE'];

                            $opening_time  = $datavalue['DATE_CREATION'];

                            $Terminal_id = $datavalue['REF_REGISTER'];

                            $Terminal_name = $datavalue['AUTHOR_NAME'];

                            
                         /// }

                    }

                      // echo $opening_Amount;
                      // die();
                       $closing_Amount =0;

                    foreach ($closing_amount as $datavalue) {
                          
                          //if($datavalue->TYPE=='closing') {

                            $closing_Amount  = $datavalue['BALANCE'];

                             $closing_time  = $datavalue['DATE_CREATION'];

                             $Remark  = $datavalue['NOTE'];

                            
                          //}


                      }

                    //  echo number_format((float)$foo, 2, '.', '');  // Outputs -> 105.00


                    $subTotal = 0;
                    $discountTotal = 0;

                    $startofday = new DateTime();
                    $startofday->setTime(0,0);
                    $startDate =$startofday->format('Y-m-d');

                    //$currentdate('Y-m-d H:i:s',$today);

                    // echo "<br>";
                    $endOfDay = new DateTime();
                    $endOfDay->setTime(23,59);
                    $endDate = $endOfDay->format('Y-m-d');

                    $order1 ='takeaway';
                    $order2= 'delivery';
                    $order3='dinein';

                    $payment1 ='cash';
                    $payment2= 'NETS';
                    $payment3='VISA';
                    $payment4='MASTER';
                    $payment5 ='FOOD_PANDA';
                    $payment6 ='DELIVEROO';
                    $payment7 ='GRAB_FOOD';

                    $takeaway = array();
                    $delivery = array();
                    $dinein = array();

                    //echo "<pre>";
                    // print_r($new_array);
                    // die();

                    $takeaway['BALANCE'] = 0;

                    $delivery['BALANCE'] = 0;

                    $dinein['BALANCE'] = 0;

                    $Cash['BALANCE'] = 0;

                    $NETS['BALANCE'] = 0;

                    $VISA['BALANCE'] = 0;

                    $MASTER['BALANCE'] = 0;
                    
                    $FOOD_PANDA['BALANCE'] = 0;
                    $DELIVEROO['BALANCE'] = 0;
                    $GRAB_FOOD['BALANCE'] = 0;

                    $total_amount  =0;

                    $i=0;
                    $j=0;
                    $k=0;
                    $l=0;
                    $m=0;
                    $n=0;
                    $o=0;
                    $p=0;
                    $q=0;
                    $r=0;
                    

                    

                    foreach ($transactions as $datavalue) {
                        
                     if($datavalue['RESTAURANT_ORDER_STATUS']!='hold' && $datavalue['PAYMENT_TYPE']!='unpaid') {
                         
                        if($datavalue['RESTAURANT_ORDER_TYPE']==$order1){

                            $i++;
                            $takeaway['BALANCE'] += $datavalue['TOTAL'];


                        }
                         if($datavalue['RESTAURANT_ORDER_TYPE']==$order2){
                             $j++;
                            $delivery['BALANCE'] += $datavalue['TOTAL'];
                        }
                         if($datavalue['RESTAURANT_ORDER_TYPE']==$order3){
                            $k++;  
                            $dinein['BALANCE'] += $datavalue['TOTAL'];

                        }

                        if($datavalue['PAYMENT_TYPE']==$payment1){

                            $l++;
                            $Cash['BALANCE'] += $datavalue['TOTAL'];


                        }
                         if($datavalue['PAYMENT_TYPE']==$payment2){
                             $m++;
                            $NETS['BALANCE'] += $datavalue['TOTAL'];
                        }
                         if($datavalue['PAYMENT_TYPE']==$payment3){
                            $n++;  
                            $VISA['BALANCE'] += $datavalue['TOTAL'];

                        } 
                        if($datavalue['PAYMENT_TYPE']==$payment4){
                            $o++;  
                            $MASTER['BALANCE'] += $datavalue['TOTAL'];

                        }
                         if($datavalue['PAYMENT_TYPE']==$payment5){
                            $p++;  
                            $FOOD_PANDA['BALANCE'] += $datavalue['TOTAL'];

                        }
                         if($datavalue['PAYMENT_TYPE']==$payment6){
                            $q++;  
                            $DELIVEROO['BALANCE'] += $datavalue['TOTAL'];

                        }
                         if($datavalue['PAYMENT_TYPE']==$payment7){
                            $r++;  
                            $GRAB_FOOD['BALANCE'] += $datavalue['TOTAL'];

                        }

                       // if($datavalue->TYPE=='cashing') {
                           $total_amount  += $datavalue['TOTAL'];
                      }

                    }

                    $takeaway['COUNT'] = $i==''?0:$i;
                    $takeaway['type'] = 'Takeaway Bills';
                    $delivery['COUNT'] = $j==''?0:$j;
                     $delivery['type'] = 'Delivery Bills';
                    $dinein['COUNT'] = $k==''?0:$k;
                     $dinein['type'] = 'Dinein Bills';

                    $Cash['COUNT'] = $l==''?0:$l;
                    $Cash['type'] = 'Cash';
                    $Cash['Ctype'] = 'CASH';
                    $NETS['COUNT'] = $m==''?0:$m;
                     $NETS['type'] = 'Nets';
                     $NETS['Ctype'] = 'NETS';
                    $VISA['COUNT'] = $n==''?0:$n;
                     $VISA['type'] = 'Visa';
                      $VISA['Ctype'] = 'VISA';
                    $MASTER['COUNT'] = $o==''?0:$o;
                     $MASTER['type'] = 'Master';
                      $MASTER['Ctype'] = 'MASTER';
                      
                       $FOOD_PANDA['COUNT'] = $p==''?0:$p;
                     $FOOD_PANDA['type'] = 'FOOD_PANDA';
                      $FOOD_PANDA['Ctype'] = 'FOOD_PANDA';
                      
                      $DELIVEROO['COUNT'] = $q==''?0:$q;
                     $DELIVEROO['type'] = 'DELIVEROO';
                      $DELIVEROO['Ctype'] = 'DELIVEROO';
                      
                       $GRAB_FOOD['COUNT'] = $r==''?0:$r;
                     $GRAB_FOOD['type'] = 'GRAB_FOOD';
                      $GRAB_FOOD['Ctype'] = 'GRAB_FOOD';


                    $ORDER_TYPE = array();
                    array_push($ORDER_TYPE, $takeaway);
                    array_push($ORDER_TYPE, $delivery);
                    array_push($ORDER_TYPE, $dinein);

                    $PAYMENT_TYPE = array();
                    array_push($PAYMENT_TYPE, $Cash);
                    array_push($PAYMENT_TYPE, $NETS);
                    array_push($PAYMENT_TYPE, $VISA);
                    array_push($PAYMENT_TYPE, $MASTER);
                     array_push($PAYMENT_TYPE, $FOOD_PANDA);
                      array_push($PAYMENT_TYPE, $DELIVEROO);
                       array_push($PAYMENT_TYPE, $GRAB_FOOD);

                     
                       // echo $array_count;
                      // die();
            

                   //  echo "<pre>";
                   // // print_r($PAYMENT_TYPE);
                   //  print_r($items);
                   //  die();

                    // $end_date = $event_row['end_date'];
                    // $date = new DateTime("now");
                ?>
                <hr style="border-top: dotted 1px;">
                <div class="row d-flex " >


               <!--  <div style="display:flex;justify-content:space-between;"> -->


                   <!--  <div style="width:35%;text-align: left;font-size: 10px;">
                    <strong>Terminal ID : Dhoni</strong>
                    </div>
                    <div style="width:35%;text-align: right;font-size: 10px;">
                    <strong>Cashier : Superadmin</strong>
                    </div> -->

                    <div class="col-md-4 col-xs-5"> 
                        <h5>Terminal ID </h5>
                    </div>
                    <div class="col-md-8 col-xs-7"> 
                        <h5> : <?php echo $Terminal_id;?></h5>
                    </div>
                     <div class="col-md-4 col-xs-5"> 
                        <h5> Cashier</h5>
                    </div>
                    <div class="col-md-8 col-xs-7"> 
                        <h5> : <?php echo $registers[0]['name'];?></h5>
                    </div>
                     <div class="col-md-4 col-xs-5"> 
                        <h5>Login Time</h5>
                    </div>
                    <div class="col-md-8 col-xs-7"> 
                        
                         <h5>: <?php  echo  date("Y-m-d h:i",strtotime($opening_time)); ?></h5>
                    </div>

                     <div class="col-md-4 col-xs-5"> 
                        <h5>Logout Time</h5>
                    </div>
                    <div class="col-md-8 col-xs-7"> 
                        <h5>: <?php  echo  date("Y-m-d h:i",strtotime($closing_time)); ?></h5>
                    </div>
                   

           

                   
        
                 </div>
                 <hr style="border-top: dotted 1px;">
                  <div class="row d-flex " >
               <!--  <div style="display:flex;justify-content:space-between;"> -->


                   <!--  <div style="width:35%;text-align: left;font-size: 10px;">
                    <strong>Terminal ID : Dhoni</strong>
                    </div>
                    <div style="width:35%;text-align: right;font-size: 10px;">
                    <strong>Cashier : Superadmin</strong>
                    </div> -->

                    <div class="col-md-5 col-xs-5"> 
                        <h5 style="    text-align: end;">Opening Cash </h5> 
                    </div>
                    <div class="col-md-7 col-xs-7"> 
                        <h5 style="text-align: end;"><span style="text-align: end;"> <?php echo number_format($opening_Amount, 2, '.', '');  // Outputs -> 105.00 $opening_Amount;?></span></h5>
                    </div>
            
                               
                 </div>
                 <hr style="border-top: dotted 1px;">

                 <div class="row d-flex " >
              

                    <p style="margin-left: 3% ">
                            
                        <?php
                          $j=0;
                        $array_count=count($PAYMENT_TYPE);

                      foreach ($PAYMENT_TYPE as $data) {
                        # code...
                        // echo "<pre>";
                        // print_r($data);
                        if($data['BALANCE']!=0){
                          $j++;
                        }

                      }
                      //die();
                        
                        $limit= $j-1;
                        $i=0;
                      foreach ($PAYMENT_TYPE as $data) {
                            if($data['BALANCE']!=0){
                                $i++;
                                echo $data['Ctype'];


                                if($i<=$limit&&$j!=1&&$j!=0){
                                    echo "/";
                                }
                        
                            }

                           
                       }
                       echo " SALES";

                        ?>


                    </p>

                   <?php  foreach ($PAYMENT_TYPE as $data) { 

                    if($data['BALANCE']!=0){
                    ?>
                     <div class="col-md-4 col-xs-4"> 
                        <h5 style="text-align: left;"><?php echo $data['type'].'('.$data['COUNT'].')';?></h5>
                    </div>
                    <div class="col-md-8 col-xs-8"> 
                        <h5 style="    text-align: end;"> <?php echo number_format($data['BALANCE'], 2, '.', '');  // Outputs -> 105.00 $opening_Amount;?></h5>
                    </div>
                    
                   <?php } } ?>
            
                               
                 </div>
                 <hr style="border-top: dotted 1px;">
                  <div class="row d-flex " >
               <!--  <div style="display:flex;justify-content:space-between;"> -->


                   <!--  <div style="width:35%;text-align: left;font-size: 10px;">
                    <strong>Terminal ID : Dhoni</strong>
                    </div>
                    <div style="width:35%;text-align: right;font-size: 10px;">
                    <strong>Cashier : Superadmin</strong>
                    </div> -->

                    <div class="col-md-5 col-xs-5"> 
                        <h5 style="    text-align: end;">Total Sales</h5>
                    </div>
                    <div class="col-md-7 col-xs-7"> 
                        <h5 style="    text-align: end;"><span  style="    text-align: end;"> <?php echo number_format($total_amount, 2, '.', '');  // Outputs -> 105.00 $opening_Amount;?></span></h5>
                    </div>
            
                               
                 </div>
                 <hr style="border-top: dotted 1px;">

                 
                 <div class="row d-flex " >
              

                    <p style="margin-left: 3% ">
                            
                        <?php
                        $i=0;
                        $array_count=count($PAYMENT_TYPE);
                        
                        echo " SALES TYPE";

                        ?>


                    </p>

                   <?php  foreach ($ORDER_TYPE as $data) {

                    if($data['BALANCE']!=0){
                    ?>
                     <div class="col-md-6 col-xs-6"> 
                        <h5 style="text-align: left;"><?php echo $data['type'].'('.$data['COUNT'].')';?></h5>
                    </div>
                    <div class="col-md-6 col-xs-6"> 
                        <h5 style="text-align: end;">  <?php echo number_format($data['BALANCE'], 2, '.', '');  // Outputs -> 105.00 $opening_Amount;?></h5>
                    </div>
                    
                   <?php } } ?>
            
                               
                 </div>

                                  <hr style="border-top: dotted 1px;">
                  <div class="row d-flex " >
               

                  <div class="col-md-5 col-xs-5"> 
                        <h5 style="    text-align: end;">Total Sales</h5>
                    </div>
                    <div class="col-md-7 col-xs-7"> 
                        <h5 style="    text-align: end;"><span style="    text-align: end;"> <?php echo number_format($total_amount, 2, '.', '');  // Outputs -> 105.00 $opening_Amount;?></span></h5>
                    </div>
            
                               
                 </div>

                 <hr style="border-top: dotted 1px;">
                  
                 <br>

                  <h5>Remark : <?php echo $Remark;?></h5>
                </div>
                <?php if( @$_GET[ 'is-pdf' ] ):?>
                <br>
                <br>
                <?php endif;?>

                <div class="row" style="display: none !important;">
                    <div class="text-center">
                        <h3><?php _e('Daily sales receipt', 'nexo');?></h3>
                    </div>

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="col-md-2" style="text-align: center;">Operation</th>
                                <th class="col-md-2" style="text-align: center;">Amount</th>
                                <th class="col-md-3" style="text-align: center;">Reason</th>
                                <th class="col-md-3" style="text-align: center;">Date</th>
                                <th class="col-md-3" style="text-align: center;" >Author</th>
                            </tr>

                        </thead>

                        <tbody>
                        <?php foreach ($items as $new): ?>

                        <?php  if((date('Y-m-d', strtotime($new->DATE_CREATION))>=$startDate)&&(date('Y-m-d', strtotime($new->DATE_CREATION))<=$endDate)) { ?>
                        <tr>
                            <td style="text-align: center;"><?php echo $new->TYPE;?></td>
                            <td style="text-align: center;"><?php echo $new->BALANCE;?></td>
                            <td style="text-align: center;"><?php echo $new->NOTE;?></td>
                            <td style="text-align: center;"><?php echo $new->DATE_CREATION;?></td>
                            <td style="text-align: center;"><?php echo $new->AUTHOR_NAME;?></td>
                        </tr>
                        <?php } ?>

                        <?php endforeach ?>


                        </tbody>


                    </table>
                    
                </div>
            </div>
        </div>
    </div>
   
</body>
</html>

<style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}
hr{
    margin:2% 0 ;
}
h4,h5{
    margin: 1%;
}
</style>

<style type="text/css">
    hr{
    margin:2% 0 ;
}
</style>