<style media="screen, print">
#print-base64 > * {
    text-transform: uppercase;
    direction: ltr;
    font-size: 1.8vw;
}
#print-base64 .hideOnPrint {
    display: none;
}


</style>

<style type="text/css" media="print">
@page {
    size: auto;   /* auto is the initial value */
    margin: 0;  /* this affects the margin in the printer settings */
}
</style>

<?php $this->events->do_action( 'nps_base64_footer' );?>
