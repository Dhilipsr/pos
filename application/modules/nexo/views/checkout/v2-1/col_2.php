<ul class="nav nav-tabs tab-grid hidden-lg hidden-md">
    <!--  -->
    <li ng-click="showPart( 'cart', $event );" class="{{ cartIsActive }}"><a
            href="#"><?php echo __( 'Panier', 'nexo' );?>
            <span class="label label-primary total-items-label">0</span></a></li>
    <li ng-click="showPart( 'grid', $event );" class="{{ gridIsActive }}"><a
            href="#"><?php echo __( 'Produits', 'nexo' );?></a></li>
</ul>
<div class="box mb-0 box-primary direct-chat direct-chat-primary" id="product-list-wrapper" style="visibility:hidden">
    <div class="box-header with-border search-field-wrapper">
        <form action="#" method="post" id="search-item-form">
            <div class="input-group">
                <div class="input-group-btn">
                    <button type="submit" class="btn btn-large btn-default" style="height: 55px;"><i class="fa fa-search"></i></button>
                    <button type="button" class="enable_barcode_search btn btn-large btn-default" style="height: 55px;"><i
                            class="fa fa-barcode"></i></button>
                </div>

                <input autocomplete="off" type="text" name="item_sku_barcode"
                    placeholder="<?php _e('Codebarre, UGS, nom du produit ou de la catégorie...', 'nexo');?>"
                    class="form-control" style="height: 55px;">
            </div>
        </form>
    </div>
    <div class="box-header with-border cattegory-slider" style="padding:0px;">
        <div class="container-fluid">
            <div class="row">
                <div :class="{ 'category-toggled' : status }" @click="status = ! status" class="col-lg-1 col-md-1 hidden-sm hidden-xs text-center toggle-categories"
                    style="padding:0;border-right:solid 1px #EEE;">
                        <i style="font-size:20px;line-height:40px;"
                        class="fa fa-eye"></i>
                </div>
                <div class="col-lg-1 col-md-1 hidden-sm hidden-xs text-center slick-button cat-prev hide"
                    style="padding:18px;border-right:solid 1px #EEE;"><i style="font-size:30px;line-height:67px;"
                        class="fa fa-arrow-left"></i></div>
                <div class="col-lg-12 col-md-9 col-sm-12 add_slick_inside" style="padding:0;">

                    <div class="slick slick-wrapper">
                        <!-- Waiting Categories -->
                    </div>

                </div>
              <!--   <div class="col-lg-1 col-md-1 hidden-sm hidden-xs text-center slick-button cat-next hide"
                    style="padding:18px;border-left:solid 1px #EEE;"><i style="font-size:30px;line-height:67px;"
                        class="fa fa-arrow-right"></i></div> -->
            </div>
        </div>
    </div>
    <div class="box-body">
        <div class="direct-chat-messages item-list-container"  <?php if ( store_option( 'favorite_item' ) != 'no' ) { ?> style="padding:0px;max-height: 56%;" <?php } else { ?> style="padding:0px;max-height: 100%;"  <?php } ?>>
           
            <div class="swiper-container2 ">
                  <!-- <div class="col-lg-1 col-md-1 hidden-sm hidden-xs text-center slick-button item-prev"
                    style="padding:6px;border-right:solid 1px #EEE;"><i style="font-size:30px;line-height:89px;"
                        class="fa fa-arrow-left"></i></div> -->
                <div class="row" id="filter-list"
                    style="padding-left:0px;padding-right:0px;margin-left:0px;margin-right:0px;padding-bottom:0px;">
                </div>
                <!-- <div class="col-lg-1 col-md-1 hidden-sm hidden-xs text-center slick-button item-next"
                    style="padding:6px;border-left:solid 1px #EEE;"><i style="font-size:30px;line-height:89px;bottom: 161px;"
                        class="fa fa-arrow-right"></i></div> -->
            </div>
            <div class="row" id="filter-categories" style="padding-left:0px;padding-right:0px;margin-left:0px;margin-right:0px;padding-bottom:0px;">
                
            </div>
        </div>
         <?php if ( store_option( 'favorite_item' ) != 'no' ) { ?>
         <div class="direct-chat-messages item-list-container eyeclose" style="padding: 0px;
    clear: both;
    position: relative;
    width: 100%;
    background-color: white;
    bottom: 0% !important;
    height: 0px !important;">
            <div class="favpos"><strong>FAVOURITE ITEMS</strong><br></div>
             <div class="swiper-container favbottom">
                <div class="row swiper-wrapper" id="filter-listfav"
                style="padding-left:0px;padding-right:0px;margin-left:0px;margin-right:0px;padding-bottom:0px;">
                   
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
    <div class="overlay" id="product-list-splash">
        <i class="fa fa-refresh fa-spin"></i>
    </div>
</div>
<style type="text/css">
    .slick-wrapper .draggable {
        direction: ltr !important;
    }
    .toggle-categories:hover {
        cursor: pointer;
    }
    .category-toggled {
        background: #EEE;
        box-shadow: inset 0px 2px 5px 2px #d8d7d7;
    }
    .slick-button:hover {
        background: #F2F2F2;
        cursor: pointer;
    }

    .slick-item:hover {
        box-shadow: 0 0 11px rgba(33,33,33,.2) !important;
    }

    .slick-item-active {
        background: #EEE
    }
</style>
<style type="text/css">
    .content-wrapper>.content {
        padding-bottom: 0px;
    }


    /*  bootstrap tab */

    div.bootstrap-tab {
        border-left: 1px #EEE solid;
        border-right: 1px #EEE solid;
        background: #FFF;
    }

    div.bootstrap-tab-container {
        z-index: 10;
        background-color: #ffffff;
        padding: 0 !important;
        background-clip: padding-box;
        opacity: 0.97;
        filter: alpha(opacity=97);
    }

    div.bootstrap-tab-menu {
        padding-right: 0;
        padding-left: 0;
        padding-bottom: 0;
    }

    div.bootstrap-tab-menu div.list-group {
        margin-bottom: 0;
    }

    div.bootstrap-tab-menu div.list-group>a {
        margin-bottom: -1px;
    }

    div.bootstrap-tab-menu div.list-group>a:nth-child(1) {
        margin-top: -1px;
    }

    div.bootstrap-tab-menu div.list-group>a .glyphicon,

    div.bootstrap-tab-menu div.list-group>a.active,
    div.bootstrap-tab-menu div.list-group>a.active .glyphicon,
    div.bootstrap-tab-menu div.list-group>a.active .fa {
        background-color: #EEE;
        /** #9792e4;**/
        background-image: #EEE;
        /** #9792e4; **/
        color: #333;
        border: solid 1px #DDD;
    }

    div.bootstrap-tab-menu div.list-group>a.active:after {
        content: '';
        position: absolute;
        left: 100%;
        top: 50%;
        margin-top: -13px;
        border-left: 0;
        border-bottom: 13px solid transparent;
        border-top: 13px solid transparent;
        border-left: 10px solid #EEE;
        /** #9792e4; **/
    }

    div.bootstrap-tab-content {
        /** background-color: #ffffff; **/
        /* border: 1px solid #eeeeee; */
        padding-left: 0px;
        padding-top: 10px;
    }

    div.bootstrap-tab div.bootstrap-tab-content:not(.active) {
        display: none;
    }

    .pay-box-container .list-group-item:last-child,
    .pay-box-container .list-group-item:first-child {
        border-radius: 0px !important;
        border-radius: 0px !important;
    }
    .floatting-shortcut {
        background: #b5c7ff;
        font-weight: 600;
        padding: 0px 6px;
        position: absolute;
        top: 10px;
        border: solid 1px #545986;
        border-radius: 10px;
        box-shadow: 0px 0px 5px -1px #9797be;
        left: 10px;
    }

    .expired-item {
        background: #ffc1c1;
    }

    .expired-item:hover {
        background: #ffc1c1;
        box-shadow: unset;
    }
</style>
<script type="text/javascript">
    function toggleFullScreen() {
        var doc = window.document;
        var docEl = doc.documentElement;

        var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl
            .webkitRequestFullScreen ||
            docEl.msRequestFullscreen;
        var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc
            .msExitFullscreen;

        if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc
            .msFullscreenElement) {
            requestFullScreen.call(docEl);
           $('#Fullscreen').text('Exit Fullscreen');
        } else {
             $('#Fullscreen').text('Fullscreen');
            cancelFullScreen.call(doc);
        }
    }

    function isFullScreen() {
        var doc = window.document;
        var docEl = doc.documentElement;

        var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl
            .webkitRequestFullScreen ||
            docEl.msRequestFullscreen;
        var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc
            .msExitFullscreen;

        if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc
            .msFullscreenElement) {
            return false;
        } else {
            return true;
        }
    }
</script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/5.4.5/css/swiper.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/5.4.5/js/swiper.js"></script>

  <style>

    .swiper-container {
        width: 100%;
        height: unset !important;
        padding: 5px;
        border-right: solid 1px #DEDEDE;
        border-bottom: solid 1px #DEDEDE;
        border: unset;
            margin-top: -2%;
    }
      .swiper-container2 {
        width: 100%;
        height: unset !important;
        padding: 5px;
        border-right: solid 1px #DEDEDE;
        border-bottom: solid 1px #DEDEDE;
        border: unset;
    }




    .swiper-slide {
      text-align: center;
      font-size: 18px;
      background: #fff;
      display: block;
    width: 25% !important;
    min-height: 141px;
    max-height: 141px;
    display: flex;
    display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      height: calc((100% - 30px) / 2);

      /* Center slide text vertically */
      /*display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;*/
    }
    .favpos {
        margin-left: 10px;
        position: absolute;
        top: 10%;
        margin-top: -1%;
        font-size: 25px;
    }
    .favbottom {
        position: absolute;
        top: 27% !important;
    }
    .item-grid-title{
        white-space:unset;
    }
    .slick_item{
        overflow: hidden;
    }
  </style>
   <script>
    //var swiper = new Swiper('.swiper-container');
     window.setTimeout(function(){
        var mySwiper = new Swiper ('.swiper-container', {
            loop: false,
            autoplay: 3000,
            autoplayDisableOnInteraction: false
        });
        //   var mySwiper = new Swiper ('.swiper-container2', {
        //     slidesToShow:3,
        //     slidesToScroll:3

        // });

        $('.slick_item').slick({
          dots: true,
          slidesPerRow: 4,
          rows: 2,
          responsive: [
          {
            breakpoint: 1024,   
            settings: {
              slidesPerRow: 4,
              rows: 2,
            }
          },
          {
            breakpoint: 640,
            settings: {
              slidesPerRow: 4,
              rows: 2,
            }
          }
        ]
      });

         // Bind Next button
        // $( '.item-next' ).bind( 'click', function(){
        //     $('.slick_item').slick( 'slickNext' );
        // });
        // // Bind Prev button
        // $( '.item-prev' ).bind( 'click', function(){
        //     $('.slick_item').slick( 'slickPrev' );
        // });

        // $('.item-next').css('top','-156px');

    }, 1000);

    




/*     $(document).ready(function(){
      $('.slick_item').slick({
          dots: true,
          slidesPerRow: 1,
          rows: 2,
          responsive: [
          {
            breakpoint: 1024,   
            settings: {
              slidesPerRow: 1,
              rows: 1,
            }
          },
          {
            breakpoint: 640,
            settings: {
              slidesPerRow: 1,
              rows: 1,
            }
          }
        ]
      });
    });*/

  </script>

  <style type="text/css">
   
::-webkit-scrollbar {
    width: 0px;  /* Remove scrollbar space */
    background: transparent;  /* Optional: just make scrollbar invisible */
}
/* Optional: show position indicator in red */
::-webkit-scrollbar-thumb {
    background: transparent;
}

.modinavstart{
    display: flex;
    flex-wrap:wrap;
    margin-left:unset !important;
    width: 120%;
    display: flex;
    flex-direction: column;
    flex-wrap: wrap;
    height: 11em;

}
.modinavstart li{
  
         flex-basis: calc(33% - 6px);
         padding-right: 0px !important;
         margin: 3px;


}

.modinavstart li span{

padding:6px !important;
}


  </style>
