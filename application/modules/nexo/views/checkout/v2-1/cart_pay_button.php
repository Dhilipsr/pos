<div class="btn-group payerbutton" ng-hide="hideme" role="group" ng-controller="payBox" id="payerbutton">
     <button type="button" class="btn btn-default btn-lg" ng-click="openPayBox()" style="margin-bottom:0px;background-color: #498349;color: white;"> <i class="fa fa-money"></i>
     <span id="here" class="hidden-xs payer_payment"><?php _e('Payer', 'nexo');?></span>
     </button>
     
</div>

<div class="btn-group paylater" ng-hide="hideme" role="group" id="paylater">
      <button type="button" class="btn btn-default btn-lg paylater" ng-click="paylater()" style="margin-bottom:0px;background-color: #498349;color: white;"> <i class="fa fa-money"></i>
     <span class="hidden-xs payer"><?php _e('Pay later', 'nexo');?></span>
     </button>

</div>



<script type="text/javascript">
	
	 $(document).ready(function () {
      $('#paylater').on('click',function (event) {

      	  var scope = angular.element(document.getElementById('takeaway')).scope();
          scope.paylater();
          // event.preventDefault();
          // var id = $(this).closest('.row').attr('id'); // row I
      });
  });
</script>