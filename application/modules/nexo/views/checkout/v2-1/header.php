
<style type="text/css">
    .logocenter {
        padding: 10px;
    }
    .logocenter.profiletopright.col-lg-6 {
        position: absolute;
        margin-left: 91%;
        font-family: -webkit-body;
    }
</style>
<div>
    <span class="logo-lg" style="float: right"><?php  echo $this->events->apply_filters('getsitename', $this->config->item('default_user_names'))['value']; ?></span>
    
</div>
<div style="display: none;">
    <div style="float: right;"> 
    <!-- <span class="logo-mini"><?php echo $this->events->apply_filters('dashboard_logo_small', $this->config->item('tendoo_logo_min'));?></span> -->
            <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><?php  echo $this->events->apply_filters('getsitename', $this->config->item('default_user_names'))['value']; ?></span> </div>
</div>
<div class="row checkout-header" ng-controller="checkoutHeaderCTRL" style="padding-bottom:10px">
    <div class="col-lg-6 left-button-columns">


        <?php
        /**
         * If cashier see order list
         */

        $menus  =   [];
        if ( User::can( 'nexo.view.orders' ) ) {
            $menus[]    =   [
                'class' =>  'default homedashboard',
                'text'  =>  __( 'Tableau de bord', 'nexo' ),
                'icon'  =>  'home',
                'attrs' =>  [
                   // 'ng-click'  =>  'goTo( \'' . dashboard_url([ 'orders' ] ) . '\' )'
                ]
            ];
        }
        
        /**
         * If cashier can use Hold order button
         */

          // [
            //     'class' =>  'default calculator-button',
            //     'text'  =>  __( 'Calculatrice', 'nexo' ),
            //     'icon'  =>  'calculator',
            //     'attrs' =>  [
            //         'ng-click'  =>  'openCalculator()',
            //     ]
            // ],
        if ( true ) {
            // $menus[]    =   [
            //     'class' =>  'default history-box-button',
            //     'text'  =>  __( 'Retrive', 'nexo' ),
            //     'icon'  =>  'history',
            //     'attrs' =>  [
            //         'ng-click'  =>  'openHistoryBox()'
            //     ]
            // ];
        }
        
        foreach( $this->events->apply_filters( 'checkout_header_menus_1', $menus ) as $menu ) {
            $attrs      =   '';
            if( is_array( @$menu[ 'attrs' ] ) ) {
                foreach( @$menu[ 'attrs' ] as $name => $value ) {
                    $attrs  .= $name . '="' . $value . '" ';
                }
            }

            include( dirname( __FILE__ ) . '/button.php' );
        };?>
    </div>
 <div class="col-lg-6 right-button-columns">
        <?php 
        $buttons    =   [
            [
                'class' =>  'default',
                // 'icon'  =>  'window-maximize',
                'text'  =>  __( 'Fullscreen', 'nexo' ),
                'attrs' =>  [
                    'ng-click'  =>  'openFullScreen()',
                    'id'=>"Fullscreen",
                ]
            ],
            // [
            //     'class' =>  'default calculator-button',
            //     'text'  =>  __( 'Calculatrice', 'nexo' ),
            //     'icon'  =>  'calculator',
            //     'attrs' =>  [
            //         'ng-click'  =>  'openCalculator()',
            //     ]
            // ],

                 [
                'class' =>  'default history-box-button Retrive',
                'text'  =>  __( 'Retrive', 'nexo' ),
                'icon'  =>  'history',
                'attrs' =>  [
                    'ng-click'  =>  'openHistoryBox()'
                ]
            ]

                    ];

        if ( store_option( 'nexo_use_cashrawer' ) === 'yes' ) {
            $buttons[]  =   [
                'class' =>  'default',
                'icon'  =>  'toggle-up',
                'attrs' =>  [
                    'id'  =>  'drawer-vue',
                    '@click'    =>  'openCashDrawer()'
                ]
            ];
        }

        foreach( $this->events->apply_filters( 'checkout_header_menus_2', $buttons ) as $menu ) {
            $attrs      =   '';
            if( is_array( @$menu[ 'attrs' ] ) ) {
                foreach( @$menu[ 'attrs' ] as $name => $value ) {
                    $attrs  .= $name . '="' . $value . '" ';
                }
            }
            
            include( dirname( __FILE__ ) . '/button.php' );
        };

        $button1=[
            [
                'class' =>  'warning',
                'text'  =>  __( 'Déconnexion', 'nexo' ),
                'icon'  =>  'sign-out',
                'attrs' =>  [
                    'ng-click'  =>  'signOut()',
                ]
            ]
        ];
        foreach( $this->events->apply_filters( 'checkout_header_menus_2', $button1 ) as $menu ) {
            $attrs      =   '';
            if($menu['class']=='warning'){
            if( is_array( @$menu[ 'attrs' ] ) ) {
                foreach( @$menu[ 'attrs' ] as $name => $value ) {
                    $attrs  .= $name . '="' . $value . '" ';
                }
            }
            
            include( dirname( __FILE__ ) . '/button.php' );
        }
        };

        ?>
    </div>
    
   <?php  if ( store_option( 'hold_orders' ) != 'yes' ) { ?>
       
       <style>
           .Retrive{
               display:none!important;
           }
       </style>
       
    <?php } ?>

    <!--<div class="col-md-12 hidden-lg" style="margin-bottom:10px">
        <?php foreach( $this->events->apply_filters( 'checkout_header_menus_1', [
            [
                'class' =>  'default',
                'text'  =>  __( 'Tableau de bord', 'nexo' ),
                'icon'  =>  'home',
                'attrs' =>  [
                    'ng-click'  =>  'goTo( \'' . dashboard_url([ 'commandes', 'lists' ] ) . '\' )'
                ]
            ], [
                'class' =>  'default calculator-button',
                'text'  =>  __( 'Calculatrice', 'nexo' ),
                'icon'  =>  'calculator',
                'attrs' =>  [
                    'ng-click'  =>  'openCalculator()',
                ]
            ], [
                'class' =>  'default history-box-button',
                'text'  =>  __( 'En Attente', 'nexo' ),
                'icon'  =>  'history',
                'attrs' =>  [
                    'ng-click'  =>  'openHistoryBox()'
                ]
            ]
        ]) as $menu ) {
            $attrs      =   '';
            if( is_array( @$menu[ 'attrs' ] ) ) {
                foreach( @$menu[ 'attrs' ] as $name => $value ) {
                    $attrs  .= $name . '="' . $value . '" ';
                }
            }
            ?>
            <button <?php echo $attrs;?> class="btn btn-sm btn-<?php echo @$menu[ 'class' ] == null ? 'default' : $menu[ 'class' ];?>">
                <i class="fa fa-<?php echo @$menu[ 'icon' ];?>"></i> <?php echo @$menu[ 'text' ];?>
            </button>
            <?php
        };?>
        <?php foreach( $this->events->apply_filters( 'checkout_header_menus_2', [
            [
                'class' =>  'default',
                'icon'  =>  'window-maximize',
                'attrs' =>  [
                    'ng-click'  =>  'openFullScreen()'
                ]
            ],
        ]) as $menu ) {
            $attrs      =   '';
            if( is_array( @$menu[ 'attrs' ] ) ) {
                foreach( @$menu[ 'attrs' ] as $name => $value ) {
                    $attrs  .= $name . '="' . $value . '" ';
                }
            }
            ?>
            <button <?php echo $attrs;?> class="btn btn-sm btn-<?php echo @$menu[ 'class' ] == null ? 'default' : $menu[ 'class' ];?>">
                <i class="fa fa-<?php echo @$menu[ 'icon' ];?>"></i> <?php echo @$menu[ 'text' ];?>
            </button>
            <?php
        };?>
    </div>-->
</div>
<style type="text/css">
body > div.wrapper > div > div.content {
    padding-top: 10px;
}

.checkout-header .btn {
    box-shadow: 1px 1px 1px 0px #909090;
    border: solid 1px #cacaca;
    margin-right: 8px;
    margin-top: 5px;
    padding: 13px 5px;
}

.checkout-header .btn-warning {
    background: #ff6262;
    color: #FFF;
    border: solid 1px #ca0000;
}
.shadowed-dropdown {
    box-shadow: 0px 2px 5px 0px #717171;
    border: solid 1px #9a9a9a;
}

</style>
<script type="text/javascript">
   // $window.open("somepath/", "_blank");

    $(document).ready(function(){
    // $(".homedashboard").attr('target','_blank');
    $('.homedashboard').click(function(){
    //var url=($(this).attr('ng-click'));
    window.open( "<?php echo dashboard_url([ 'orders' ]) ?>");
}); 
})


    
     $(document).ready(function () {
      $('#quickbill ').on('click',function (event) {


          
          var scope = angular.element(document.getElementById('takeaway')).scope();
          scope.switchOrderType( 'quickbill' );
          // event.preventDefault();
          // var id = $(this).closest('.row').attr('id'); // row I
      });
  });

</script>