
<?php if ( store_option( 'hold_orders' ) != 'no' ) { ?>
<div class="btn-group" role="group" ng-controller="saveBox" >
    <button type="button" class="hold_btn btn btn-default btn-lg" ng-click="openSaveBox()" style="margin-bottom:0px;background-color: #4580bd;color: white;"> <i class="fa fa-hand-stop-o"></i>
        <span class="hidden-xs"><?php _e('En attente', 'nexo');?></span>
    </button>
</div>
<?php } ?>