<?php
if ( is_multistore() && multistore_enabled() ) {
    global $StoreRoutes;
    $Routes     =   $StoreRoutes;
} else {
    global $Routes;
}

$Routes->get( 'pin-login/settings', 'PinLogin\Controllers\Controller@settings' );



$Routes->get( 'pin-login/get', 'PinLogin\Controllers\Controller@pin_login' );

//$Routes->post( 'pin-login/settings', 'PinLogin\Controllers\Controller@pin_code');

// $Routes->post( 'pin-login', 'PinLogin\Controllers\Controller@pin_code' );