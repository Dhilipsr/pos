<?php
namespace PinLogin\Inc;

use Tendoo_Module;

class Setup extends Tendoo_Module
{
    /**
     * While installing
     * add a pin column on users table
     * @return void
     */
    public function install()
    {
        $columns            =   $this->db->list_fields( 'aauth_users' );
        if ( ! in_array( 'pin' , $columns ) ) {
            $this->db->query('ALTER TABLE `'. $this->db->dbprefix . 'aauth_users` ADD `pin` varchar(200) NOT NULL AFTER `pass`;');
        }
    }

    /**
     * While uninstalling remove
     * the user pin column from users table
     * @return void
     */
    public function uninstall()
    {
        $columns            =   $this->db->list_fields( 'aauth_users' );
        if ( ! in_array( 'pin' , $columns ) ) {
            $this->db->query('ALTER TABLE `'. $this->db->dbprefix . 'aauth_users` DROP `pin`');
        }
    }
}