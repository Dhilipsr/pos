<?php
namespace PinLogin\Inc;

use Tendoo_Module;

class Filters extends Tendoo_Module
{
    /**
     * register pin login settings
     * page for NexoPOS 
     * @param array of url
     * @return array mutated
     */
    public function admin_menus( $menus )
    {
        if ( ! empty( $menus[ 'nexo_settings' ] ) && ! is_multistore() ) {
            $menus  =   array_insert_after( 'modules', $menus, 'pin-login', [
                [
                    'title'             =>      __( 'Pin Login', 'pin-login' ),
                    'href'              =>      site_url([ 'dashboard', 'pin-login', 'settings' ]),
                    'icon'              =>      'fa fa-shield',
                    'permission'        =>      'nexo.manage.settings'
                ]
            ]);
        }

        return $menus;
    }
}