<?php
namespace PinLogin\Inc;

use Tendoo_Module;
use User;
use PinLogin\Inc\Setup;

class Actions extends Tendoo_Module
{
    /**
     * register the footer view


     */

    public function common_footer()
    {
        if ( $this->uri->segment(1) === 'sign-in' ) {
            $this->load->module_view( 'pin-login', 'login' );
        }
    }

    public function load_frontend()
    {
        // echo "<pre>";
        // print_r($this->uri->uri_string());
        // die();
        if ( $this->uri->uri_string() === 'pin-login' ) {
            $post   =   json_decode( get_instance()->input->raw_input_stream, true );
            $user   =   $this->db->where( 'pin', md5( $post[ 'pin' ] ) )
                ->get( 'aauth_users' )
                ->result_array();


           

            if ( $user ) {
                get_instance()->auth->login_fast( $user[0][ 'id' ]);
                echo json_encode([
                    'status'    =>  'success',
                    'message'   =>  __( 'The user has been successfully logged' )
                ]);
                return;
            }
            echo json_encode([
                'status'    => 'failed',
                'message'   => __( 'Unable to login using the provided PIN Code.', 'pin-login' )
            ]);
            return;
        }
    }

    /**
     * install the module while it's being enabled
     * @param string module namespace
     * @return void
     */
    public function do_enable_module( $module ) 
    {
        if ( $module === 'pin-login' ) {
            set_option( 'pin-module-enabled', true );
            (new Setup)->install();
        }
    }

    public function load_profile_fields( $config )
    {
        extract( $config );
        /**
         * ->mode
         * ->groups
         * ->meta_namespace
         * ->col_id
         * ->gui
         * ->user_id
         */
        $gui->add_item([
            'type'      =>  'dom',
            'content'   =>  $this->load->module_view( 'pin-login', 'profile', null, true )
        ], 'user_profile', 1 );        
    }

    public function saving_profile()
    {
        if ( ! empty( $this->input->post( 'pin' ) ) && strlen( $this->input->post( 'pin' ) ) >= 4 ) {
            $this->db->where( 'id', User::id() )
                ->update( 'aauth_users', [
                    'pin'   => md5( User::id() . $this->input->post( 'pin' ) )
                ]);
        }
    }
}