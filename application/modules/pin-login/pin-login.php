<?php
namespace PinLogin;

use Tendoo_Module;
use PinLogin\Inc\Actions;
use PinLogin\Inc\Filters;
use User;

include_once( dirname( __FILE__ ) . '/inc/Actions.php' );
include_once( dirname( __FILE__ ) . '/inc/Filters.php' );
include_once( dirname( __FILE__ ) . '/inc/Setup.php' );

class Module extends Tendoo_Module
{
    public function __construct()
    {
        parent::__construct();
    
        $this->actions  =   new Actions;
        $this->filters  =   new Filters;

        $this->events->add_action( 'common_footer', [ $this->actions, 'common_footer' ]);
        $this->events->add_action( 'load_frontend', [ $this->actions, 'load_frontend' ]);
        $this->events->add_action( 'do_enable_module', [ $this->actions, 'do_enable_module' ]);
        $this->events->add_action( 'load_users_custom_fields', [ $this->actions, 'load_profile_fields' ], 9 );
        $this->events->add_action( 'user_profile_rules', [ $this->actions, 'saving_profile' ]);
        $this->events->add_filter( 'admin_menus', [ $this->filters, 'admin_menus' ], 25 );
    }

}
new Module;