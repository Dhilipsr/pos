<?php

$this->Gui->col_width(1, 2);

$this->Gui->add_meta(array(
    'type'			=>    'unwrapped',
    'col_id'		=>    1,
    'namespace'	    =>    'pin_login_settings',
    'gui_saver'     =>  true,
    'footer'        =>  [
        'submit'    =>  [
            'label' =>  __( 'Save Settings', 'pin-login' )
        ]
    ]
));

$this->Gui->add_item([
    'type'           =>    'select',
    'options'        =>     [
        'pin'           =>   __( 'Pin Login', 'pin-login' ),
        'regular'       =>   __( 'Regular Login', 'pin-login' )
    ],
    'name'           =>	    'prefered_login_option',
    'label'          =>     __( 'Default Login Option', 'pin-login' ),
    'description'    =>     __( 'This option let you define which option should be used on the authentication page.', 'pin-login' ),
    'placeholder'    =>     ''
], 'pin_login_settings', 1 );

$this->Gui->output();