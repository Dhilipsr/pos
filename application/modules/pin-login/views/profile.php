<div class="form-group" id="pincode">
    <label for="input-id" ><?php echo __( 'Pin Code', 'pin-login' );?></label>
    <div class="input-group">
        <div class="input-group-addon"><?php echo User::id();?></div>
        <input name="pin" type="number" class="form-control" placeholder="<?php echo __( 'Eg : 5874', 'pin-login' );?>">
    </div>
    <p class="text-description"><?php echo sprintf( __( 'Your PIN has a prefix which is <strong>%s</strong>. Your final PIN will be composed with your prefix and the PIN you input on the field.', 'pin-login' ), User::id() );?></p>
</div>
