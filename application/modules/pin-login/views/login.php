<link rel="stylesheet" href="<?php echo base_url() . '/public/bower_components/sweetalert2/dist/sweetalert2.min.css';?>">
<link rel="stylesheet" href="<?php echo module_url( 'pin-login' ) . '/css/login.css';?>">
<script src="<?php echo module_url( 'nexo' ) . '/bower_components/vue/dist/vue.min.js';?>"></script>
<script src="<?php echo module_url( 'nexo' ) . '/bower_components/axios/dist/axios.min.js';?>"></script>
<script src="<?php echo base_url() . '/public/bower_components/sweetalert2/dist/sweetalert2.min.js';?>"></script>
<script>
const PinLoginData  =   {
    textDomain: {
        regularLogin: `<?php echo __( 'Regular', 'pin-login' );?>`,
        pinLogin: `<?php echo __( 'Pin Login', 'pin-login' );?>`,
        submit: `<?php echo __( 'Submit', 'pin-login' );?>`,
        inputPin: `<?php echo __( 'Input Your PIN', 'pin-login' );?>`,
        inputYourPinToStart: `<?php echo __( 'Input your pin to start your session', 'pin-login' );?>`,
        wrongPin: `<?php echo __( 'Wrong Pin Provided', 'pin-login' );?>`,
        needMoreCharacters: `<?php echo __( 'You Pin code should have at least 4 numbers to be valid', 'pin-login' );?>`,
        rememberMe: `<?php echo __( 'Remember Me', 'pin-login' );?>`
    },
    url: {
        login:'<?php echo site_url( ['sign_in/pin_login','post'] );?>', //'<?php echo site_url( ['pin-login'] );?>',
       redirection: '<?php echo isset( $_GET[ 'redirect' ] ) ? site_url([ 'dashboard' ]) : $_GET[ 'redirect' ];?>'
    },
    screen  :   '<?php echo get_option( 'prefered_login_option', 'regular' );?>',
    csrf_secure: '<?php echo $this->security->get_csrf_hash();?>'
}
</script>
<script src="<?php echo module_url( 'pin-login' ) . '/js/base.js';?>"></script>