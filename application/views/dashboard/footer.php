



<?php
/**
* 	File Name 	: footer.php
*	Description :	hold dashboard footer section
*	Since		:	1.4
**/


$data = base_url(uri_string());

$foot_data = explode('/', $data);
	if(in_array('kitchen-screen', $foot_data)) {
?>
<?php } else { ?>

<footer class="main-footer" style="height: 54px;">
    <div class="pull-right hidden-xs">
        <?php echo $this->events->apply_filters( 'dashboard_footer_right', sprintf( __( 'You\'re using <b>%s</b> %s' ), get( 'app_name' ), get('str_core') ) );?>
    </div>
    <?php echo $this->events->apply_filters('dashboard_footer_text', '<small>' . sprintf(__('Thank you for using A2000 F&B POS &mdash; %s in %s seconds'), $this->benchmark->memory_usage(), '{elapsed_time}') . '</small>');?>
</footer>
<?php 		} 
?>
<?php $this->events->do_action( 'dashboard_footer' );?>


