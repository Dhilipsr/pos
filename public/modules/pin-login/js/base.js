
const PinLogin  =   new class {
    constructor() {

        this.footerTemplate     =   `
        <div id="pin-keyboard" v-if="screen === 'pin'">
            <p class="login-box-msg">{{ textDomain.inputYourPinToStart }}</p>
            <div class="alert alert-warning" v-if="error.length > 0">
                {{ error }}
            </div>
            <form method="post">
                <div class="form-group">
                    <input readonly style="text-align:center; font-size: 30px;" v-model="screenValue" type="password" :placeholder="textDomain.inputPin" class="form-control input-lg"> 
                </div>
                <div v-for="row of rows" class="row mb-20">
                    <div v-for="input of row" class="col-md-4 col-lg-4 col-sm-4 col-xs-4 d-flex">
                        <button :class="{ 'disabled': isSubmitting }" @click="handle( input )" type="button" class="btn btn-lg btn-default pin-keyboard-button">{{ input }}</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 d-flex">
                        <button :class="{ 'disabled': isSubmitting }" @click="submit()" type="button" class="btn btn-lg btn-primary pin-keyboard-button">{{ textDomain.submit }}</button>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 d-flex">
                        <button :class="{ 'disabled': isSubmitting }" @click="handle(0)" type="button" class="btn btn-lg btn-default pin-keyboard-button">0</button>
                    </div>
                    <div class="col-md-4 col-lg-4 col-sm-4 col-xs-4 d-flex">
                        <button :class="{ 'disabled': isSubmitting }" @click="backward(1)" type="button" class="btn btn-lg btn-default pin-keyboard-button">
                            <i class="fa fa-long-arrow-left"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div id="pin-login">
            <div class="btn-group-lg btn-group btn-group-justified" role="group" aria-label="Justified button group"> 
                <a :class="{ 'active': screen === 'regular' }" href="javascript:void(0)" @click="toggleLoginVue('regular')" class="btn btn-default" role="button">${PinLoginData.textDomain.regularLogin}</a> 
                <a :class="{ 'active': screen === 'pin' }" href="javascript:void(0)" @click="toggleLoginVue('pin')" class="btn btn-default" role="button">${PinLoginData.textDomain.pinLogin}</a> 
            </div>
        </div>
        `;
        this.pinTemplate        =   `
        
        `

        $( '.login-box' ).append( this.footerTemplate );
        $( '.login-box-body' ).attr( 'v-if', `screen === 'regular'` );

        this.constructVue();
    }


    constructVue() {
        this.vue    =   new Vue({
            el: '.login-box',
            data: { 
                ...PinLoginData, 
                screenValue: '',
                rows: [
                    [7,8,9],
                    [4,5,6],
                    [1,2,3]
                ],
                isSubmitting: false,
                error: ''
            },
            methods: {
                toggleLoginVue( vue ) {
                    this.screen    =   vue;
                    if ( vue === 'regular' ) {
                        setTimeout( () => {
                            $( '.checkbox' ).html( `
                                <label>
                                    <div class="icheckbox_square-blue" aria-checked="false" aria-disabled="false"><input type="checkbox" name="keep_connected"><ins class="iCheck-helper"></ins></div> ${this.textDomain.rememberMe}
                                </label>
                            ` );
                            $( 'input' ).iCheck({
                                checkboxClass: 'icheckbox_square-blue',
                                radioClass: 'iradio_square-blue',
                                increaseArea: '20%' // optional
                            });
                        }, 0 );
                    } 
                },
                handle( input ) {
                    if ( ! this.isSubmitting ) {
                        this.screenValue    +=  input;
                    }
                },
                submit() {

                    if ( this.screenValue.length < 4 ) {
                        return swal({
                            type: 'error',
                            title: this.textDomain.wrongPin,
                            text: this.textDomain.needMoreCharacters
                        });
                    }

                    this.error          =   '';
                    this.isSubmitting   =   true;
                    //axios.defaults.headers.post['X-API-KEY'] ='9zOW3T3ZmBCYQWVDbrVWcFa4mApMrOZGa91mzUxr ';
                   // axios.defaults.headers.post['X-Requested-With'] ='XMLHttpRequest';
                      let formData = new FormData();
                      formData.append('profile_picture', this.screenValue);
                      formData.append('name',this.csrf_secure);
                      // formData.append('email',this.state.fields['email']);
                      // formData.append('phone',this.state.fields['phone']);
                      // formData.append('password',this.state.fields['password']);
                      console.log(formData);



                    


                    axios.post(this.url.login, {
                        pin: this.screenValue,
                        csrf_secure: this.csrf_secure
                    }).then( result => {
                        //console.log(this.url.login);
                        //console.log(result.data);
                        this.isSubmitting   =   false;
                        if ( result.data.status === 'success' ) {
                            document.location = this.url.redirection;
                        } else {
                            this.error          =   result.data.message;
                            this.screenValue    =   '';
                        }
                    }).catch( result => {
                        alert(result);
                        this.isSubmitting   =   false;
                    });
                },
                backward( length ) {
                    this.screenValue   =   this.screenValue.substr(0, this.screenValue.length - ( length ) );
                }
            },
            mounted() {
            }
        })
    }
}